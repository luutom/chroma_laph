#include "inline_smear_quark_field.h"
#include "xml_help.h"
#include "chroma.h"

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
using namespace LaphEnv;

  namespace InlineSmearQuarkFieldEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "SmearQuarkFieldInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new SmearQuarkFieldInlineMeas(xml_in, path);
}


const std::string name = "SMEAR_QUARK_FIELD_TIMESLICES";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}

	
// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr

#if (QDP_ND == 3) 

void SmearQuarkFieldInlineMeas::operator()(unsigned long update_no,
                                           XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);
 GaugeConfigurationInfo gaugeinfo(xml_rdr);
 GluonSmearingInfo gsmear(xml_rdr);
 string smeared_gauge_filename;
 xmlread(xml_rdr,"SmearedGaugeFileName",smeared_gauge_filename,
         "SMEAR_QUARK_FIELD_TIMESLICES");
 QuarkSmearingInfo qsmear(xml_rdr);
 string smeared_quark_filestub;
 xmlread(xml_rdr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "SMEAR_QUARK_FIELD_TIMESLICES");

 bool laph_solve=true;
 LaphEigenSolverInfo *eigsolveinfo=0;
 if (xml_tag_count(xml_rdr,"LaphEigenSolverInfo")==1)
    eigsolveinfo = new LaphEigenSolverInfo(xml_rdr);
 else 
    laph_solve=false;

 int striping_factor=1,striping_unit=0;
 xmlreadif(xml_rdr,"StripingFactor",striping_factor,"SMEAR_QUARK_FIELD_TIMESLICES");
 xmlreadif(xml_rdr,"StripingUnit",striping_unit,"SMEAR_QUARK_FIELD_TIMESLICES");

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ************************************************************"<<endl;
 QDPIO::cout << " *                                                          *"<<endl;
 QDPIO::cout << " *   Laph Task 1A: Smear the quark field by computing the   *"<<endl;
 QDPIO::cout << " *                 eigenvectors of the smeared-covariant    *"<<endl;
 QDPIO::cout << " *                 Laplacian, then write to file            *"<<endl;
 QDPIO::cout << " *                                                          *"<<endl;
 QDPIO::cout << " ************************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<gsmear.output()<<endl;
 QDPIO::cout << "Smeared gauge field file name = "<<smeared_gauge_filename<<endl;
 QDPIO::cout <<endl<<qsmear.output()<<endl;
 QDPIO::cout << "Striping factor = "<<striping_factor<<endl;
 QDPIO::cout << "Striping unit = "<<striping_unit<<endl;
 if (laph_solve){
    QDPIO::cout << "Smeared quark field file stub = "<<smeared_quark_filestub<<endl;
    QDPIO::cout <<endl<<eigsolveinfo->output()<<endl;}
 else{
    QDPIO::cout << endl << "Estimate largest eigenvalue of -Laplacian only"<<endl;}
 QDPIO::cout <<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"SMEAR_QUARK_FIELD_TIMESLICES");
 gaugeinfo.output(xml_out);
 gsmear.output(xml_out);
 qsmear.output(xml_out);
 if (laph_solve) eigsolveinfo->output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

#ifdef TESTING
 printSmearedLinks(gaugeinfo,gsmear,smeared_gauge_filename,"smeargauge3d.log");
#endif

    // create the handler (in write mode)

 QuarkSmearingHandler Q(gsmear,gaugeinfo,qsmear,smeared_quark_filestub,false);
 START_CODE();
 StopWatch outer;
 outer.start();

 if (laph_solve) Q.computeLaphEigenvectors(*eigsolveinfo,smeared_gauge_filename,
                                           striping_factor,striping_unit);
 else{
    double lambda_max=Q.estimateLargestLaplacianEigenvalue(smeared_gauge_filename);
    QDPIO::cout << endl<<"Estimate of largest eigenvalue of -Laplacian is "
                << lambda_max <<endl<<endl;}

 delete eigsolveinfo;
 outer.stop();
 QDPIO::cout << name << ": total time = " << outer.getTimeInSeconds() 
             << " secs" << endl;
 QDPIO::cout << name << ": ran successfully" << endl;

 END_CODE();
} 

#elif (QDP_ND == 4)

void SmearQuarkFieldInlineMeas::operator()(unsigned long update_no,
                                           XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);
 GaugeConfigurationInfo gaugeinfo(xml_rdr);
 GluonSmearingInfo gsmear(xml_rdr);
 QuarkSmearingInfo qsmear(xml_rdr);
 string smeared_quark_filestub;
 xmlread(xml_rdr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "SMEAR_QUARK_FIELD_TIMESLICES");
 int striping_factor=1,striping_unit=0;
 xmlreadif(xml_rdr,"StripingFactor",striping_factor,"SMEAR_QUARK_FIELD_TIMESLICES");
 xmlreadif(xml_rdr,"StripingUnit",striping_unit,"SMEAR_QUARK_FIELD_TIMESLICES");

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 1B: Re-organize the file structure of the   *"<<endl;
 QDPIO::cout << " *                 previously-computed eigenvectors of the *"<<endl;
 QDPIO::cout << " *                 smeared-covariant Laplacian.  Combine   *"<<endl;
 QDPIO::cout << " *                 all time slices of each level.          *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<gsmear.output()<<endl;
 QDPIO::cout <<endl<<qsmear.output()<<endl;
 QDPIO::cout << "Smeared quark field file stub = "<<smeared_quark_filestub<<endl<<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"SMEAR_QUARK_FIELD_TIMESLICES");
 gaugeinfo.output(xml_out);
 gsmear.output(xml_out);
 qsmear.output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

    // create the handler (in write mode)

 QuarkSmearingHandler Q(gsmear,gaugeinfo,qsmear,smeared_quark_filestub,false);
 START_CODE();
 StopWatch outer;
 outer.start();

 Q.combineTimeSlices(striping_factor,striping_unit);

 outer.stop();
 QDPIO::cout << name << ": total time = " << outer.getTimeInSeconds() 
             << " secs" << endl;
 QDPIO::cout << name << ": ran successfully" << endl;

 END_CODE();
} 

#endif

// ******************************************************************
  }
} // namespace Chroma
