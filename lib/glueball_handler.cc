#include "glueball_handler.h"
#include "dir_path.h"

namespace Chroma {
  namespace LaphEnv {

#if (QDP_ND == 3)

// **************************************************************


void GlueballHandler::RecordKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"RecordKey");
 write(xmlw,"TimeValue",time_value);
 pop(xmlw);
}


// *************************************************************************

GlueballHandler::CombinedKey::CombinedKey() {}

GlueballHandler::CombinedKey::~CombinedKey() {}

GlueballHandler::CombinedKey::CombinedKey(const GlueballOperatorInfo& in_gop,
                                       const RecordKey& in_rkey)
  : rkey(in_rkey), gop(in_gop) {}

GlueballHandler::CombinedKey::CombinedKey(const GlueballHandler::CombinedKey& in)
  : rkey(in.rkey), gop(in.gop) {}

GlueballHandler::CombinedKey& GlueballHandler::CombinedKey::operator=(
                             const GlueballHandler::CombinedKey& in)
{
 gop=in.gop;
 rkey=in.rkey;
 return *this;
}


bool GlueballHandler::CombinedKey::operator<(
            const GlueballHandler::CombinedKey& rhs) const
{
 return multiLessThan(rkey,rhs.rkey, gop,rhs.gop);
}

bool GlueballHandler::CombinedKey::operator==(
            const GlueballHandler::CombinedKey& rhs) const
{
 return multiEqual(rkey,rhs.rkey, gop,rhs.gop);
}

bool GlueballHandler::CombinedKey::operator!=(
            const GlueballHandler::CombinedKey& rhs) const
{
 return multiNotEqual(rkey,rhs.rkey, gop,rhs.gop);
}

void GlueballHandler::CombinedKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"CombinedKey");
 gop.output(xmlw);
 rkey.output(xmlw);
 pop(xmlw);
}


// *************************************************************************


GlueballHandler::GlueballHandler()
        :  uPtr(0), gSmearPtr(0), fPtr(0), DHputPtr(0), DHgetPtr(0), 
           DHSFputPtr(0), m_read_mode(true) {}


GlueballHandler::GlueballHandler(const GaugeConfigurationInfo& gaugeinfo,
                                 const GluonSmearingInfo& gluonsmear,
                                 const FileListInfo& flist,
                                 bool read_mode)
        :  uPtr(0), gSmearPtr(0), fPtr(0), DHputPtr(0), DHgetPtr(0), DHSFputPtr(0)
{
 set_info(gaugeinfo,gluonsmear,flist,read_mode);
}


void GlueballHandler::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                              const GluonSmearingInfo& gluonsmear,
                              const FileListInfo& flist,
                              bool read_mode)
{
 clear();
 set_info(gaugeinfo,gluonsmear,flist,read_mode);
}


void GlueballHandler::set_info(const GaugeConfigurationInfo& gaugeinfo,
                               const GluonSmearingInfo& gluonsmear,
                               const FileListInfo& flist,
                               bool read_mode)
{
 m_read_mode=read_mode;
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    fPtr = new FileListInfo(flist);   
    if (m_read_mode)
       DHgetPtr = new DataGetHandlerMF<GlueballHandler,FileKey,RecordKey,
                          DataType>(*this,flist,"Laph--GlueballFile","GlueballHandlerDataFile");}
 catch(...){
    QDPIO::cerr << "allocation problem in GlueballHandler"<<endl;
    clear();
    QDP_abort(1);}
}



GlueballHandler::~GlueballHandler()
{
 clear();
}

void GlueballHandler::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete fPtr;
    delete DHputPtr;
    delete DHgetPtr;
    delete DHSFputPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 fPtr=0;
 DHputPtr=0;
 DHgetPtr=0;
 DHSFputPtr=0;
}




bool GlueballHandler::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(fPtr!=0));
}


const GaugeConfigurationInfo& GlueballHandler::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& GlueballHandler::getGluonSmearingInfo() const
{
 check_info_set("getFieldSmearingInfo");
 return *gSmearPtr;
}

const FileListInfo& GlueballHandler::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

void GlueballHandler::getFileMap(XmlWriter& xmlout) const
{
 if ((isInfoSet())&&(m_read_mode))
    DHgetPtr->getFileMap(xmlout);
}

void GlueballHandler::outputKeys(XmlWriter& xmlout)
{
 if ((isInfoSet())&&(m_read_mode)) 
    DHgetPtr->outputKeys(xmlout);
}

   // ***************************************************************
   
        //  Compute line ends for all glueball operators in "GopInfos".
        //  Handler must NOT be in read mode.


void GlueballHandler::compute(
                      const set<GlueballOperatorInfo>& GopInfos,
                      const string& smeared_gauge_filename,
                      const string& smeared_quark_filestub)
{

   // check info is set and handler is NOT in read mode
 check_info_set("compute",2);

 QDPIO::cout <<endl<<endl;
 QDPIO::cout << "********************************************************"<<endl;
 QDPIO::cout << endl<<"  GlueballHandler::compute commencing..."<<endl<<endl;

     // **********************************************************
     // *                                                        *
     // *  set up structures related to the glueball operators   *
     // *                                                        *
     // **********************************************************

 int nglueball_ops=GopInfos.size();
 if (nglueball_ops==0){
    QDPIO::cerr << "No glueball operators in GlueballHandler::compute"<<endl;
    return;}
 SetIndexer<GlueballOperatorInfo> Gops(GopInfos);

     //  Output glueball operators; set need_lapheigvals

 bool need_lapheigvals=false;
 for (int k=0;k<nglueball_ops;k++){
    QDPIO::cout << "Glueball operator "<<k<<":"<<endl;
    QDPIO::cout << Gops[k].output()<<endl;
    string sptype(Gops[k].getSpatialType());
    if (!((sptype==string("TrEig"))||(sptype==string("TrW1Eig")))){
       QDPIO::cerr << "Glueball operator type "<<sptype<<" currently NOT supported in GlueballHandler"<<endl;
       return;}
    if (Gops[k].needsLaphEigenvalues()) need_lapheigvals=true;}
    
 int Textent=uPtr->getTimeExtent();
 multi1d<double> LaphEigvals;
 string fileid("Laph--SmearedQuarkTimeFile");

 try{
    int suffix=fPtr->getMinFileNumber();
    FileListInfo finfo(fPtr->getFileStub(),suffix,suffix,
                       fPtr->isModeOverwrite());
    string fname=finfo.getFileName(suffix);
    DHSFputPtr = new DataPutHandlerSF<GlueballHandler,CombinedKey,DataType>(
            *this,fname,"Laph--GlueballFile",fPtr->isModeOverwrite(),true);}
  catch(...){
    std::cerr << "problem setting up output files in GlueballHandler"<<endl;
    QDP_abort(1);}

   // loop over all times on lattice

 for (int tval=0;tval<Textent;++tval){

    QDPIO::cout << "Starting evaluations for time = "<<tval<<endl;
    RecordKey rkey(tval);
    if (need_lapheigvals){
       try{
          string headerxml;
          string filename(smeared_quark_filestub+"_time."+int_to_string(tval));
          IOMap<UIntKey,LatticeColorVector> iom;
          if (!iom.peekHeader(headerxml,filename,fileid))
             throw(string("could not open file "+filename));
          XmlReader xmlr(headerxml,false);
          read(xmlr,"Eigenvalues",LaphEigvals);
          QDPIO::cout << "Number of Laph Eigenvalues read = "<<LaphEigvals.size()<<endl;}
//          for (int k=0;k<LaphEigvals.size();++k) QDPIO::cout << "eigval["<<k<<"] = "<<LaphEigvals[k]<<endl;}
       catch(const string& errmsg){
          LaphEigvals.resize(0);
          QDPIO::cerr << "Problem getting Laph Eigenvalues: "<<errmsg<<endl;}
       catch(...){
          LaphEigvals.resize(0);
          QDPIO::cerr << "Problem getting Laph Eigenvalues"<<endl;}}
    
    for (int k=0;k<nglueball_ops;k++){
       QDPIO::cout << "Evaluating glueball operator "<<k<<endl;
       double result=0.0;
       if (Gops[k].getSpatialType()==string("TrEig")){
          for (int kk=0;kk<LaphEigvals.size();++kk)
             result+=LaphEigvals[kk];}
       else if (Gops[k].getSpatialType()==string("TrW1Eig")){
          for (int kk=0;kk<LaphEigvals.size();++kk){
             double lambda=LaphEigvals[kk];
             result+=lambda*exp(-64.0*lambda*lambda);}}
             QDPIO::cout << "result = "<<result<<endl;
       try{
          Complex data(result);
          DHSFputPtr->putData(CombinedKey(Gops[k],rkey),data);}
//          DHputPtr->putData(Gops[k],rkey,data);}
       catch(...){
          QDPIO::cerr << "Problem doing putData in GlueballHandler"<<endl;}
       }
    }  

 delete DHSFputPtr;
 DHSFputPtr=0;
 QDPIO::cout << endl<<endl<<" ************************************"<<endl<<endl;
 QDPIO::cout << "GlueballHandler::compute finished"<<endl<<endl;
}


// ***************************************************************************************


bool GlueballHandler::checkHeader(XmlReader& xmlr, int suffix)
{
 if (xml_tag_count(xmlr,"GlueballHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::GlueballHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);}
 catch(...){ return false;}
 return true;
}


void GlueballHandler::writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                                int suffix)
{
 push(xmlout,"GlueballHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}


bool GlueballHandler::checkHeader(XmlReader& xmlr)
{
 if (xml_tag_count(xmlr,"GlueballHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::GlueballHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check); }
 catch(...){ return false;}
 return true;
}


void GlueballHandler::writeHeader(XmlWriter& xmlout)
{
 push(xmlout,"GlueballHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 pop(xmlout);
}

void GlueballHandler::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"GlueballHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    pop(xmlout);}
}

                                           
// ***************************************************************************************



const GlueballHandler::DataType& GlueballHandler::getData(
                                         const GlueballOperatorInfo& gop,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(gop,RecordKey(timevalue));
}



          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

bool GlueballHandler::queryData(const GlueballOperatorInfo& gop,
                                int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(gop,RecordKey(timevalue));
}


bool GlueballHandler::queryData(const GlueballOperatorInfo& gop)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryFile(gop);
}

        // remove from internal memory

void GlueballHandler::removeData(const GlueballOperatorInfo& gop,
                                 int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(gop,RecordKey(timevalue));
}


   // removes all data associated with one glueball operator

void GlueballHandler::removeData(const GlueballOperatorInfo& gop)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(gop);
}


void GlueballHandler::clearData()
{
 if (DHgetPtr!=0) DHgetPtr->clearData();
} 
 
set<GlueballOperatorInfo> GlueballHandler::getGlueballOperators() const
{
 check_info_set("getGlueballOperators",1);
 return DHgetPtr->getFileKeys();
}


void GlueballHandler::filefail(const string& message)
{
 QDPIO::cerr << message << endl;
 clear();
 QDP_abort(1);
}

void GlueballHandler::check_info_set(const string& name,
                                  int check_mode) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in GlueballHandler:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
 if (check_mode==1){
    if (!m_read_mode){
       QDPIO::cerr << "error in GlueballHandler:"<<endl;
       QDPIO::cerr << "  must be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
 else if (check_mode==2){
    if (m_read_mode){
       QDPIO::cerr << "error in GlueballHandler:"<<endl;
       QDPIO::cerr << "  must not be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
}


// ***************************************************************************************

#endif
  }
}
 
