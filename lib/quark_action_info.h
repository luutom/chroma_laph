#ifndef LAPH_QUARK_ACTION_INFO_H
#define LAPH_QUARK_ACTION_INFO_H

#include "chromabase.h"
#include "xml_help.h"
#include "gauge_configuration_info.h"

namespace Chroma {
  namespace LaphEnv {

// ********************************************************************
// *                                                                  *
// *  Class "QuarkActionInfo" holds information about the valence     *
// *  quark action.  It also can check that this action is the same   *
// *  as compared to another QuarkActionInfo. This will also return   *
// *  the quark mass (or kappa value).                                *
// *                                                                  *
// *  There are multiple ways of constructing objects of this class.  *
// *                                                                  *
// *    XmlReader xml_in(...);                                        *
// *    QuarkActionInfo SQ(xml_in);                                   *
// *                                                                  *
// *      --> Looks for and extracts the <QuarkActionInfo> tag in the *
// *          XML input, which must have the form                     *
// *                                                                  *
// *             <QuarkActionInfo>                                    *
// *                <Mass> 0.321 </Mass> (or <Kappa>..</Kappa>)       *
// *                <Name> CLOVER </Name>                             *
// *                <Description> ..... </Description>                *
// *             </QuarkActionInfo>                                   *
// *                                                                  *
// *          This constructor is generally useful, but is the only   *
// *          way of specifying a valence quark action that is        *
// *          different from the action used to generate the          *
// *          configurations. Consult Chroma documentation for XML    *
// *          format of a quark action needed by the inverter.  The   *
// *          quark mass (or kappa value) is extracted from the XML.  *
// *                                                                  *
// *    XmlReader xml_in(...);                                        *
// *    GaugeConfigurationInfo U(...);                                *
// *    QuarkActionInfo SQ(xml_in,U);                                 *
// *                                                                  *
// *      --> The XML input expected here either has the form as for  *
// *          the constructor above, or a simplified version can be   *
// *          used:                                                   *
// *                                                                  *
// *             <QuarkActionInfo>                                    *
// *               <Dynamical>                                        *
// *                 <Mass> 0.332 </Mass>  (or <Kappa>...</Kappa>)    *
// *               </Dynamical>                                       *
// *             </QuarkActionInfo>                                   *
// *                                                                  *
// *          The XML header info for the configuration is then       *
// *          searched to extract the <FermionAction> tag with the    *
// *          appropriate mass.  This constructor is only useful if   *
// *          the desired valence quark action is the same as that    *
// *          of one of the quarks in the action used by HMC.         *
// *                                                                  *
// *    string header(...);                                           *
// *    QuarkActionInfo SQ(header);                                   *
// *                                                                  *
// *      --> This constructor takes a string from the header info    *
// *          of some file (such as a quark source/sink) and then     *
// *          extracts the information as in the first constructor    *
// *          above. The quark mass (or kappa value) is extracted.    *
// *                                                                  *
// *  Other usage:                                                    *
// *                                                                  *
// *    QuarkActionInfo S2(...);                                      *
// *    S.checkEqual(S2);  // checks S=S2, throw exception if not     *
// *    S.matchXMLverbatim(S2);                                       *
// *    if (S==S2) ...                                                *
// *                                                                  *
// *    string sval = S.output();   // xml output                     *
// *    string sval = S.output(2);  // xml indented output            *
// *    double rval = S.getMass();  // returns the quark mass         *
// *                                                                  *
// ********************************************************************                             


class QuarkActionInfo
{
    std::string description;
    double mass;
    std::string mass_name;
    std::string action_id;

  public:

    QuarkActionInfo(XmlReader& xml_in);

    QuarkActionInfo(XmlReader& xml_in, const GaugeConfigurationInfo& U);

    QuarkActionInfo(const QuarkActionInfo& rhs);

    QuarkActionInfo& operator=(const QuarkActionInfo& rhs);

    ~QuarkActionInfo(){}

    void checkEqual(const QuarkActionInfo& rhs) const;

    void matchXMLverbatim(const QuarkActionInfo& rhs) const;

    bool operator==(const QuarkActionInfo& rhs) const;



    std::string output(int indent = 0) const;

    void output(XmlWriter& xmlout) const;

    double getMass() const {return mass;}

    std::string getMassName() const {return mass_name;}
    
    std::string getActionName() const {return action_id;}

    std::string getDescription() const {return description;}


  private:

    void setMass(XmlReader& xmlr, string& massName, double& massValue);
    void set_info1(XmlReader& xmlr);
    std::string indentDescription(int indent) const;

};


// ****************************************************************
  }
}
#endif
