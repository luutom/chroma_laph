#ifndef LAPH_MESON_OPERATOR_H
#define LAPH_MESON_OPERATOR_H

#include "qdp.h"
#include "chromabase.h"
#include "meson_operator_info.h"
#include "momenta.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   MesonOperator takes a MesonOperatorInfo object as input       *
// *   to its constructor.  It reads the expansion coefficients      *
// *   of the meson operator in terms of its elemental operators.    *
// *                                                                 *
// *   The superposition coefficients have been pre-computed         *
// *   (using a Maple program) and stored in a pre-defined           *
// *   format in a file located in a particular directory            *
// *   structure.  The elementals and coefficients for any           *
// *   given meson are stored in a single file -- one file           *
// *   for each meson operator, such as                              *
// *                                                                 *
// *     <top_directory>/isovector_du/mom_ray_000/A1um_1/SS_0        *
// *                                                                 *
// *   The class needs access to the files which describe the        *
// *   meson operators in terms of elementals.  Hence, the class     *
// *   needs to know the top directory above. You must call          *                                      *
// *                                                                 *
// *       MesonOperator::initialize(coefsTopDirectory);             *
// *                                                                 *
// *   first before using this class.  There is no default           *
// *   constructor for this class.  The constructor takes the        *
// *   above XML (in the form of an XMLReader object) as input.      *
// *   Example usage:                                                *
// *                                                                 *
// *       MesonOperator::initialize("coefsTopDirectory");           *
// *       XMLReader xml_in;   // assigned somehow                   *
// *       MesonOperator M(xml_in);                                  *
// *                                                                 *
// *   The constructor does some checks on the input, then attempts  *
// *   to read the coefficients in terms of elementals for that      *
// *   operator.  If found, the object is assigned and the           *
// *   expansion details and coefficients in terms of elementals     *
// *   is stored.  If not found, an error message is output and      *
// *   an abort is issued.                                           *
// *                                                                 *
// *   Allowed momentum rays for mesons:                             *
// *     000  +00  0+0  00+  -00  0-0  00-                           *
// *     ++0  +-0  +0+  +0-  0++  0+-  --0  -+0  -0-  -0+  0--  0-+  *
// *     +++  ++-  +-+  +--  ---  --+  -+-  -++  spc                 *
// *                                                                 *
// *******************************************************************

class MesonOperator
{

 public:  

  class AntiQuark
   {

    int spin;
    int displace_dir;

   public:

    AntiQuark(int in_spin, int in_disp_dir) 
         : spin(in_spin), displace_dir(in_disp_dir)
     {if ((spin<1)||(spin>4)||(displace_dir<-3)||(displace_dir>3)){
         QDPIO::cerr << "invalid MesonOperator::AntiQuark"<<endl;
         QDP_abort(1);}}

    AntiQuark(const AntiQuark& rhs) 
     : spin(rhs.spin), displace_dir(rhs.displace_dir) {}

    AntiQuark& operator=(const AntiQuark& rhs)
     {spin=rhs.spin; displace_dir=rhs.displace_dir; return *this;}

    int getSpin() const {return spin;}

    int getDisplacementDirection() const {return displace_dir;}

    bool operator<(const AntiQuark& rhs) const
     {return multiLessThan(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

    bool operator==(const AntiQuark& rhs) const
     {return multiEqual(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

    bool operator!=(const AntiQuark& rhs) const
     {return multiNotEqual(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

   };


  class Quark
   {

    int spin;
    int displace_dir1,displace_dir2;

   public:

    Quark(int in_spin, int in_disp_dir1, int in_disp_dir2) 
        : spin(in_spin), displace_dir1(in_disp_dir1), displace_dir2(in_disp_dir2)
     {if ((spin<1)||(spin>4)||(displace_dir1<-3)||(displace_dir1>3)
         ||(displace_dir2<-3)||(displace_dir2>3)){
         QDPIO::cerr << "invalid MesonOperator::Quark"<<endl;
         QDP_abort(1);}}

    Quark(const Quark& rhs) 
     : spin(rhs.spin), displace_dir1(rhs.displace_dir1), 
       displace_dir2(rhs.displace_dir2) {}

    Quark& operator=(const Quark& rhs)
     {spin=rhs.spin; displace_dir1=rhs.displace_dir1; 
      displace_dir2=rhs.displace_dir2; return *this;}

    int getSpin() const {return spin;}

    int getDisplacementDirection1() const {return displace_dir1;}

    int getDisplacementDirection2() const {return displace_dir2;}

    bool operator<(const Quark& rhs) const
     {return multiLessThan(spin,rhs.spin,  displace_dir1,rhs.displace_dir1,
                           displace_dir2,rhs.displace_dir2);}

    bool operator==(const Quark& rhs) const
     {return multiEqual(spin,rhs.spin,  displace_dir1,rhs.displace_dir1,
                        displace_dir2,rhs.displace_dir2);}

    bool operator!=(const Quark& rhs) const
     {return multiNotEqual(spin,rhs.spin,  displace_dir1,rhs.displace_dir1,
                           displace_dir2,rhs.displace_dir2);}

   };


  class Elemental
   {

    AntiQuark qbar;
    Quark q;

   public:

    Elemental(int qbarspin, int qbardisp, int qspin, int qdisp1, int qdisp2) 
          : qbar(qbarspin,qbardisp), q(qspin,qdisp1,qdisp2) {}

    Elemental(const AntiQuark& qbarin, const Quark& qin)
          : qbar(qbarin), q(qin) {}

    Elemental(const Elemental& rhs) 
     : qbar(rhs.qbar), q(rhs.q) {}

    Elemental& operator=(const Elemental& rhs)
     {qbar=rhs.qbar; q=rhs.q; return *this;}

    Quark getQuark() const {return q;}

    AntiQuark getAntiQuark() const {return qbar;}


    int getAntiQuarkSpin() const {return qbar.getSpin();}

    int getQuarkSpin() const {return q.getSpin();}

    int getAntiQuarkDisp() const {return qbar.getDisplacementDirection();}

    int getQuarkDisp1() const {return q.getDisplacementDirection1();}

    int getQuarkDisp2() const {return q.getDisplacementDirection2();}

    bool operator<(const Elemental& rhs) const
     {return  multiLessThan(qbar,rhs.qbar,  q,rhs.q);}

    bool operator==(const Elemental& rhs) const
     {return  multiEqual(qbar,rhs.qbar,  q,rhs.q);}

    bool operator!=(const Elemental& rhs) const
     {return  multiNotEqual(qbar,rhs.qbar,  q,rhs.q);}

    std::string output() const
     {std::ostringstream oss;
      oss << " qbar=("<<getAntiQuarkSpin()<<","<<getAntiQuarkDisp()<<")";
      oss << " q=("<<getQuarkSpin()<<","<<getQuarkDisp1()<<","<<getQuarkDisp2()<<")";
      return oss.str();}

   };

  struct ElementalTerm
   {
    Elemental el;
    DComplex coef;

    ElementalTerm(const Elemental& inel, const DComplex& incf)
      : el(inel), coef(incf) {}
    ElementalTerm(const ElementalTerm& in)
      : el(in.el), coef(in.coef) {}
    ElementalTerm& operator=(const ElementalTerm& in)
     {el=in.el; coef=in.coef; return *this;}
   };

  struct IndexCoef
   {
    int index;
    DComplex coef;

    IndexCoef(int ind, const DComplex& incf) : index(ind), coef(incf) {}
    IndexCoef(const IndexCoef& in) : index(in.index), coef(in.coef) {}
    IndexCoef& operator=(const IndexCoef& in)
     {index=in.index; coef=in.coef; return *this;}
   };

 private:

  static std::string coefsTopDirectory;
  static bool coefsFlag;

  MesonOperatorInfo m_info;

  list<ElementalTerm> terms;

         // no copying ... like a handler
  MesonOperator(const MesonOperator& M);
  MesonOperator& operator=(const MesonOperator& M);


 public:

  static void initialize(const std::string& meson_coefsTopDirectory);

  MesonOperator(const MesonOperatorInfo& bop_info);


    // output functions

  int getDisplacementLength() const { return m_info.getDisplacementLength(); }

  Momentum getMomentumValue() const { return m_info.getMomentum(); }

  const MesonOperatorInfo& getInfo() const { return m_info; }

  int getNumberOfElementals() const { return terms.size(); }

  typedef list<ElementalTerm>::const_iterator ElementalIterator;

  ElementalIterator begin() const { return terms.begin(); }

  ElementalIterator end() const { return terms.end(); }

  std::string output() const;  // XML output including elementals

};



// **************************************************

  }
}
#endif  
