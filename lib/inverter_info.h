#ifndef LAPH_INVERTER_INFO_H
#define LAPH_INVERTER_INFO_H

#include "chromabase.h"
#include "xml_help.h"

namespace Chroma {
  namespace LaphEnv {

// **********************************************************************
// *                                                                    *
// *  Class "InverterInfo" holds information about the inverter.        *
// *  The role of the inverters is to solve the linear system of        *
// *  equations   M*x=y  for given source y.  Conjugate gradient        *
// *  methods actually solve M^dagger M x = M^dagger y, but at the      *
// *  cost of a worse condition number.  The solution methods are       *
// *  iterative, so one must specify the maximum number of iterations   *
// *  before giving up and the relative residuum for adequate           *
// *  convergence to a solution.  Convergence is usually specified      *
// *  by   r = Mx-y  with  norm(r) <= eps^2 * norm(y).  This            *
// *  convergence criterion is applied to the **pre-conditioned**       *
// *  system (pre-conditioning is usually done, based on the            *
// *  fermion action specified).  The top-level solver reports the      *
// *  obtained residual (the relative residual is divided by norm(y)).  *
// *  Note that the relative residuum for the original                  *
// *  not-preconditioned system may be a little larger (usually         *
// *  a factor of 2-3) than for the preconditioned system.              *
// *                                                                    *
// *  Details about the inverters can be obtained from Chroma           *
// *  documentation, but sample XML inputs are given below.             *
// *                                                                    *
// *    Conjugate-Gradient:                                             *
// *                                                                    *
// *   <InvertParam>                                                    *
// *     <invType>CG_INVERTER</invType>                                 *
// *     <RsdCG>1.0e-5</RsdCG>    <!-- Normalized Relative Residuum     *
// *     <MaxCG>10000</MaxCG>  <!-- Max no of iterations                *
// *   </InvertParam>                                                   *
// *                                                                    *
// *    Biconjugate-gradient stabilized: (does not use M^dagger M)      *
// *                                                                    *
// *   <InvertParam>                                                    *
// *     <invType>BICGSTAB_INVERTER</invType>                           *
// *     <RsdBiCGStab>1.0e-5</RsdBiCGStab>                              *
// *     <MaxBiCGStab>100000</MaxBiCGStab>                              *
// *   </InvertParam>                                                   *
// *                                                                    *
// *  You can also use IBICGSTAB_INVERTER  with the rest of the param   *
// *  tags the same as BiCGStab. This is an 'improved BiCGStab' with    *
// *  only 1 global reduction per iteration which may scale better      *
// *  than normal BiCGStab on architectures where global reductions     *
// *  are very expensive.                                               *
// *                                                                    *
// *    Eigenvector deflated conjugate gradient                         *
// *                                                                    *
// *   <InvertParam>                                                    *
// *      <invType>EIG_CG_INVERTER</invType>                            *
// *      <RsdCG>1e-05</RsdCG>                                          *
// *      <RsdCGRestart>5e-07</RsdCGRestart>                            *
// *      <MaxCG>20000</MaxCG>                                          *
// *      <Nmax>60</Nmax>                                               *
// *      <Neig>8</Neig>                                                *
// *      <Neig_max>192</Neig_max>                                      *
// *      <updateRestartTol>0</updateRestartTol>                        *
// *      <PrintLevel>1</PrintLevel>                                    *
// *      <eigen_id>linop_evs.-0.0840</eigen_id>                        *
// *      <cleanUpEvecs>false</cleanUpEvecs>                            *
// *   </InvertParam>                                                   *
// *                                                                    *
// *  The "reliable" inverters below only work for the Wilson clover    *
// *  action.  They are mixed precision solvers.  "Delta" describes     *
// *  the initial decrease in the residuum in single precision, then    *
// *  double precision is used after that to reach the required         *
// *  tolerance.                                                        * 
// *                                                                    *
// *   <InvertParam>                                                    *
// *      <invType>RELIABLE_IBICGSTAB_MP_CLOVER_INVERTER</invType>      *
// *      <MaxIter>15000</MaxIter>                                      *
// *      <Delta>1.0e-3</Delta>                                         *
// *      <RsdTarget>1.0e-6</RsdTarget>                                 *
// *      <CloverParams>                                                *
// *        <Mass>-0.0860</Mass>                                        *
// *        <clovCoeffR>1.58932722549812</clovCoeffR>                   *
// *        <clovCoeffT>0.902783591772098</clovCoeffT>                  *
// *        <AnisoParam>                                                *
// *          <anisoP>true</anisoP>                                     *
// *          <t_dir>3</t_dir>                                          *
// *          <xi_0>4.3</xi_0>                                          *
// *          <nu>1.265</nu>                                            *
// *        </AnisoParam>                                               *
// *      </CloverParams>                                               *
// *   </InvertParam>                                                   *
// *                                                                    *
// *  This above XML also works for the inverters                       *
// *                                                                    *
// *      <invType>RELIABLE_IBICGSTAB_MP_CLOVER_INVERTER</invType>      *
// *      <invType>RELIABLE_CG_MP_CLOVER_INVERTER</invType>             *
// *                                                                    *
// *                                                                    *
// *  Usage:                                                            *
// *                                                                    *
// *    XmlReader xmlr(...);                                            *
// *    InverterInfo inv(xmlr);                                         *
// *              --> checks that this reader contains some valid       *
// *                  info and extracts the tolerance                   *
// *                                                                    *
// *    InverterInfo inv2(...);                                         *
// *    inv.checkEqual(inv2);                                           *
// *             -->  checks inv and inv2 have same XML content,        *
// *                  throwing string exception if not                  *
// *    inv.checkEqualTolerance(inv2);                                  *
// *             -->  checks inv and inv2 have same tolerance,          *
// *                  throwing string exception if not                  *
// *    inv.matchXMLverbatim(inv2);                                     *
// *             -->  checks inv and inv2 have exactly same XML,        *
// *                  throwing string exception if not                  *
// *                                                                    *
// *    string sval = inv.output();    <-- outputs the inverter xml     *
// *    sval = inv.output(2);          <-- indented xml output          *
// *    double val = inv.getTolerance(); <-- returns the tolerance      *
// *                                                                    *
// *  For guidance, inversions on a single 24^3 x 128 configuration     *
// *  at a pion mass around 230 MeV are given below for the stout       *
// *  smeared anisotropic clover action.  Full spin dilution was used,  *
// *  but no Laph eigenvector dilution (112 vectors).                   *
// *  "rel res" is the relative residuum |Mx-y|/|y| of the original     *
// *  unpreconditioned matrix Mx=y (the even-odd system is actually     *
// *  solved for the clover action).  There is significant variation    *
// *  from one inversion to the next, but the numbers below             * 
// *  represent typical values.  Calculations were done in single       *
// *  precision.  Delta = 1e-3 was used in all "reliable" inverters.    *
// *  The time is for 128 cores on Athena.                              *
// *                                                                    *
// *                                  unprec                            *
// *                inverter   tol    rel res  iters   time             *
// *                                                                    *
// *             CG_INVERTER   1e-3    1.9e-2    320    17 sec          *
// *             CG_INVERTER   1e-4    3.4e-3   2100   120 sec          *
// *             CG_INVERTER   1e-5    7.3e-4   6300   340 sec          *
// *             CG_INVERTER   1e-6    4.4e-5   9700   520 sec          *
// *             CG_INVERTER   1e-7    6.4e-5  11800   630 sec          *
// *                                                                    *
// *      RELIABLE_IBICGSTAB   1e-3    9.2e-4   1400    90 sec          *
// *      RELIABLE_IBICGSTAB   1e-4    1.0e-4   1800   120 sec          *
// *      RELIABLE_IBICGSTAB   1e-5    1.4e-5   2100   140 sec          *
// *      RELIABLE_IBICGSTAB   1e-6    3.6e-6   2500   165 sec          *
// *      RELIABLE_IBICGSTAB   1e-7    3.3e-6   2800   190 sec          *
// *      RELIABLE_IBICGSTAB   1e-8    3.3e-6   3200   220 sec          *
// *                                                                    *
// *                BICGSTAB   1e-3    1.0e-3   1300    80 sec          *
// *                BICGSTAB   1e-4    1.1e-4   1700   110 sec          *
// *                BICGSTAB   1e-5    3.9e-5   2000   125 sec          *
// *                BICGSTAB   1e-6    4.0e-5   2400   150 sec          *
// *                BICGSTAB   1e-7    4.3e-5   2650   165 sec          *
// *                                                                    *
// *             RELIABLE_CG   1e-4                                     *
// *             RELIABLE_CG   1e-5    2.2e-4   8000   436 sec          *
// *             RELIABLE_CG   1e-6  failed to converge by 50000        *
// *             RELIABLE_CG   1e-7  failed to converge by 50000        *
// *                                                                    *
// *                                                                    *
// *  In single precision, 1e-5 or slightly smaller is the limit on     *
// *  the precision.  The solvers will continue to reduce the           *
// *  residual for the preconditioned system, but the improvements      *
// *  are false since the changes are in the truncation error.  The     *
// *  residual of the original system does not improve.  So setting     *
// *  a tolerance below 1e-5 or so is a waste of computer time in       *
// *  single precision.  Since CG actually solves M^dag M x = M^dag y,  *
// *  which has a higher condition number, the residual achieved        *
// *  saturates earlier, so less accuracy is obtainable, compared to    *
// *  BiCGStab.                                                         *
// *                                                                    *
// **********************************************************************


class InverterInfo
{

   std::string inverter_xml;
   std::string id;
   double tol;
   int max_iterations;

  public:

   InverterInfo(XmlReader& xml_in);

   InverterInfo(const InverterInfo& rhs);
                                          
   InverterInfo& operator=(const InverterInfo& rhs);
     
   ~InverterInfo(){}

   void checkEqual(const InverterInfo& rhs) const;

   void checkEqualTolerance(const InverterInfo& rhs) const;

   void matchXMLverbatim(const InverterInfo& rhs) const;

   bool operator==(const InverterInfo& rhs) const;



   std::string output(int indent = 0) const;

   void output(XmlWriter& xmlout) const;

   double getTolerance() const {return tol;}

   int getMaxIterations() const {return max_iterations;}

   std::string getId() const { return id;}

  private:

   void extract_info_from_reader(XmlReader& xmlr);

};


// *****************************************************************
  }
}
#endif
