#ifndef LAPH_MESON_OPERATOR_INFO_H
#define LAPH_MESON_OPERATOR_INFO_H

#include "qdp.h"
#include "chromabase.h"
#include <set>
#include "momenta.h"
#include "xml_help.h"

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   Objects of class "MesonOperatorInfo" store identifying info   *
// *   about one particular meson operator.  Each meson is a         *
// *   linear superposition of so-called elemental operators.        *
// *   The Chroma tasks that compute the meson source/sink           *
// *   functions need to read these superposition coefficients.      *
// *   The XML input into the Chroma tasks must inform Chroma        *
// *   which meson operators to compute, so a pre-defined input      *
// *   format must be followed.  The needed input format and the     *
// *   required storage format of the elementals in each meson       *
// *   are described below.                                          *
// *                                                                 *
// *   Chroma input format for each meson operator:                  *
// *                                                                 *
// *       <MesonOp>                                                 *
// *          <IsospinName> isovector </IsospinName>                 *
// *          <Flavor> ud </Flavor>   (antiquark first)              *
// *          <Momentum>  0 1 -1  </Momentum>                        *
// *          <Irrep> A1um </Irrep>                                  *
// *          <IrrepRow> 1 </IrrepRow>                               *
// *          <SpatialType> SD </SpatialType>                        *
// *          <SpatialIdNum> 1 </SpatialIdNum>                       *
// *          <DispLength> 3 </DispLength>                           *
// *       </MesonOp>                                                *
// *                                                                 *
// *   READING multiple meson operators:                             *
// *                                                                 *
// *   Chroma input format:                                          *
// *                                                                 *
// *      <MesonOperators>                                           *
// *         <ElementalCoefficientDirectory>                         *
// *             coefsTopDirectory                                   *
// *         </ElementalCoefficientDirectory>                        *
// *           <MesonOp>                                             *
// *            ...                                                  *
// *           </MesonOp>                                            *
// *           <MesonOp>                                             *
// *            ...                                                  *
// *           </MesonOp>                                            *
// *         ...                                                     *
// *      </MesonOperators>                                          *
// *                                                                 *
// *   Given the above XML input, one just needs to issue the        *
// *   following:                                                    *
// *                                                                 *
// *      XmlReader xml_in;  // assigned somehow                     *
// *      set<MesonOperatorInfo> bset;                               *
// *      string coefdir;                                            *
// *      readMesonOperators(xml_in,bset,coefdir);                   *
// *                                                                 *
// *   The above code initializes the class, then reads all of       *
// *   meson operators inside the <mesonOperators> tags in           *
// *   the input XML file, storing the results in a set.             *
// *                                                                 *
// *******************************************************************

class MesonOperatorInfo
{

  unsigned icode1,icode2;

 public:
 
  MesonOperatorInfo(XmlReader& xml_in);

  MesonOperatorInfo(const MesonOperatorInfo& M) 
     :  icode1(M.icode1), icode2(M.icode2) {}

  MesonOperatorInfo& operator=(const MesonOperatorInfo& M)
   {icode1=M.icode1; icode2=M.icode2;
    return *this;}

  MesonOperatorInfo()  :  icode1(0), icode2(0) {}

    // output functions

  std::string getIsospinName() const;

  std::string getFlavor() const;

  Momentum getMomentum() const;

  int getXMomentum() const;

  int getYMomentum() const;

  int getZMomentum() const;

  std::string getIrrep() const;

  int getIrrepRow() const;

  std::string getSpatialType() const;

  int getSpatialIdNumber() const;

  int getDisplacementLength() const;

  std::string output() const;  // XML output of baryon tags

  void output(XmlWriter& xmlout) const;  // XML output of baryon tags

    // returns the flavor std::string, but replaces all 'u' and
    // 'd' characters by 'l'

  std::string getFlavorLS() const
   {std::string tmp=getFlavor();
    for (unsigned int i=0;i<tmp.length();i++)
       if ((tmp[i]=='u')||(tmp[i]=='d')) tmp[i]='l';
    return tmp;}

  bool operator==(const MesonOperatorInfo& rhs) const;
  bool operator!=(const MesonOperatorInfo& rhs) const;
  bool operator<(const MesonOperatorInfo& rhs) const;

  friend class MesonOperator;
  friend class MesonHandler;
  friend class MesonInternalLoopHandler;


};



// **************************************************

   //  useful routine for reading multiple MesonOperators

void readMesonOperators(XmlReader& xml_in, 
                        set<MesonOperatorInfo>& mops,
                        std::string& coef_dir);


// **************************************************
  }
}
#endif  
