#include "quark_handler.h"
#include "fermact.h"
#include "actions/ferm/fermacts/fermact_factory_w.h"
#include "actions/ferm/fermacts/fermacts_aggregate_w.h"
#include "util/ferm/diractodr.h"
#include "time_slices.h"
#include "meas/smear/displace.h"
#include "multi_compare.h"
#include "actions/ferm/fermacts/clover_fermact_params_w.h"
#include "actions/ferm/linop/unprec_clover_linop_w.h"

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
  namespace LaphEnv {
   
// *************************************************************************

void QuarkHandler::RecordKey::output(XmlWriter& xmlw) const 
{
 push(xmlw,"RecordKey");
 write(xmlw,"Spin",getSpin());
 write(xmlw,"Time",getTime());
 write(xmlw,"SpinLaphEigvecIndex",getSpinLaphEigvecIndex());
 pop(xmlw);
}

// *************************************************************************


QuarkHandler::FileKey::FileKey(const LaphNoiseInfo& in_noise, int tprojind)
    : noise(in_noise), time_proj_index(tprojind) {}

QuarkHandler::FileKey::FileKey(XmlReader& xmlr)
{
 try{
    XmlReader xmlf(xmlr,"./descendant-or-self::FileKey");
    noise=LaphNoiseInfo(xmlf);
    xmlread(xmlf,"TimeProjectorIndex",time_proj_index,"QuarkHandler::FileKey");}
 catch(...){
    QDPIO::cerr << "Could not read QuarkHandler::FileKey"<<endl;
    QDP_abort(1);}
}

QuarkHandler::FileKey::FileKey(const FileKey& rhs)
    : noise(rhs.noise), time_proj_index(rhs.time_proj_index) {}

QuarkHandler::FileKey& QuarkHandler::FileKey::operator=(const FileKey& rhs)
{
 noise=rhs.noise;
 time_proj_index=rhs.time_proj_index;
 return *this;
}

void QuarkHandler::FileKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"FileKey");
 noise.output(xmlw);
 write(xmlw,"TimeProjectorIndex",time_proj_index);
 pop(xmlw);
}

bool QuarkHandler::FileKey::operator<(const FileKey& rhs) const
{
// return ((time_proj_index<rhs.time_proj_index)
//     || ((time_proj_index==rhs.time_proj_index)&&(noise<rhs.noise)));

 return multiLessThan(time_proj_index,rhs.time_proj_index,  noise,rhs.noise);
}

bool QuarkHandler::FileKey::operator==(const FileKey& rhs) const
{
// return ((noise==rhs.noise)&&(time_proj_index==rhs.time_proj_index));
 return multiEqual(time_proj_index,rhs.time_proj_index,  noise,rhs.noise);
}

bool QuarkHandler::FileKey::operator!=(const FileKey& rhs) const
{
// return ((noise!=rhs.noise)||(time_proj_index!=rhs.time_proj_index));
 return multiNotEqual(time_proj_index,rhs.time_proj_index,  noise,rhs.noise);
}

// *************************************************************************

#if (QDP_ND == 3)


QuarkHandler::StorageKey::StorageKey(const FileKey& in_fkey, 
                                     const RecordKey& in_stkey,
                                     const DirPath& in_disp, int in_disp_length)
    : fkey(in_fkey), rkey(in_stkey), disp(in_disp), 
      disp_length(in_disp_length)
{
 if (disp.Length()==0) disp_length=0;
 if ((disp.Length()>0)&&(disp_length<=0)){
    QDPIO::cerr << "Invalid displacement length"<<endl;
    QDP_abort(1);}
}

QuarkHandler::StorageKey::StorageKey(const FileKey& in_fkey, 
                                     const RecordKey& in_stkey)
    : fkey(in_fkey), rkey(in_stkey), disp_length(0)
{}

QuarkHandler::StorageKey::StorageKey(
           const QuarkHandler::StorageKey& in)
    : fkey(in.fkey), rkey(in.rkey), disp(in.disp), 
      disp_length(in.disp_length) {}


QuarkHandler::StorageKey& QuarkHandler::StorageKey::operator=(
        const QuarkHandler::StorageKey& in)
{
 fkey=in.fkey;
 rkey=in.rkey;
 disp=in.disp;
 disp_length=in.disp_length;
 return *this;
}

bool QuarkHandler::StorageKey::operator<(const QuarkHandler::StorageKey& rhs) const
{
 return multiLessThan(rkey,rhs.rkey,  disp,rhs.disp,  
                      disp_length,rhs.disp_length,  fkey,rhs.fkey);

// return   ((rkey<rhs.rkey)               || ((rkey==rhs.rkey)
//       && ((disp<rhs.disp)               || ((disp==rhs.disp)
//       && ((disp_length<rhs.disp_length) || ((disp_length==rhs.disp_length)
//       &&  (fkey<rhs.fkey)))))));
}

bool QuarkHandler::StorageKey::operator==(const QuarkHandler::StorageKey& rhs) const
{
 return multiEqual(rkey,rhs.rkey,  disp,rhs.disp,  
                   disp_length,rhs.disp_length,  fkey,rhs.fkey);

// return   ((rkey==rhs.rkey)
//         &&(disp==rhs.disp)
//         &&(disp_length==rhs.disp_length)
//         &&(fkey==rhs.fkey));
}

bool QuarkHandler::StorageKey::operator!=(const QuarkHandler::StorageKey& rhs) const
{
 return multiNotEqual(rkey,rhs.rkey,  disp,rhs.disp,  
                      disp_length,rhs.disp_length,  fkey,rhs.fkey);

// return   ((rkey!=rhs.rkey)
//         ||(disp!=rhs.disp)
//         ||(disp_length!=rhs.disp_length)
//         ||(fkey!=rhs.fkey));
} 

#endif

// *************************************************************************




QuarkHandler::QuarkHandler()
          : uPtr(0), gSmearPtr(0), qSmearPtr(0), dilPtr(0), qactionPtr(0),
            fPtr(0), invertPtr(0), dilHandler(0), DHputPtr(0), 
            DHgetPtr(0), normal_mode(true)  {}


QuarkHandler::QuarkHandler(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkSmearingInfo& quarksmear,
                           const DilutionSchemeInfo& dil,
                           const QuarkActionInfo& quark,
                           const FileListInfo& flist,
                           const string& smeared_quark_filestub,
                           const string& gauge_str)
          : invertPtr(0), dilHandler(0), DHputPtr(0), DHgetPtr(0), 
            normal_mode(true)
{
 set_info(gaugeinfo,gluonsmear,quarksmear,dil,quark,flist,
          smeared_quark_filestub,gauge_str);
}

void QuarkHandler::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkSmearingInfo& quarksmear,
                           const DilutionSchemeInfo& dil,
                           const QuarkActionInfo& quark,
                           const FileListInfo& flist,
                           const string& smeared_quark_filestub,
                           const string& gauge_str)
{
 clear();
 set_info(gaugeinfo,gluonsmear,quarksmear,dil,quark,flist,
          smeared_quark_filestub,gauge_str);
}


void QuarkHandler::set_info(const GaugeConfigurationInfo& gaugeinfo,
                            const GluonSmearingInfo& gluonsmear,
                            const QuarkSmearingInfo& quarksmear,
                            const DilutionSchemeInfo& dil,
                            const QuarkActionInfo& quark,
                            const FileListInfo& flist,
                            const string& smeared_quark_filestub,
                            const string& gauge_str)
{
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    qSmearPtr = new QuarkSmearingInfo(quarksmear);
    dilPtr = new DilutionSchemeInfo(dil);
    qactionPtr = new QuarkActionInfo(quark);
    fPtr = new FileListInfo(flist);

#if (QDP_ND == 4)
    DHputPtr=new DataPutHandlerMF<QuarkHandler,FileKey,RecordKey,DataType>(
                    *this,*fPtr,"Laph--QuarkSink","QuarkHandlerDataFile");
#elif (QDP_ND == 3)
    DHgetPtr=new DataGetHandlerMF<QuarkHandler,FileKey,RecordKey,DataType>(
                    *this,*fPtr,"Laph--QuarkSink","QuarkHandlerDataFile");
#endif
    }
 catch(...){
    QDPIO::cerr << "allocation problem in QuarkHandler"<<endl;
    QDP_abort(1);}

#if (QDP_ND == 4)
 connectGaugeConfigurationHandler(gauge_str);
#elif (QDP_ND == 3)
 connectGluonSmearingHandler(gauge_str);
#endif
 connectQuarkSmearingHandler(smeared_quark_filestub);
 connectDilutionHandler();

}


QuarkHandler::~QuarkHandler()
{
 clear();
}


void QuarkHandler::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete qSmearPtr;
    delete dilPtr;
    delete qactionPtr;
    delete fPtr;
    delete invertPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 qSmearPtr=0;
 dilPtr=0;
 qactionPtr=0;
 fPtr=0;
 invertPtr=0;
 normal_mode=true;

#if (QDP_ND == 4)
 disconnectGaugeConfigurationHandler();
#elif (QDP_ND == 3)
 disconnectGluonSmearingHandler();
#endif
 disconnectQuarkSmearingHandler();
 disconnectDilutionHandler();

#if (QDP_ND == 3)
 clearData();
 delete DHgetPtr; DHgetPtr=0;
#elif (QDP_ND == 4)
 delete DHputPtr; DHputPtr=0;
#endif
}


  // ********************************
  // *
  // *    sub-handler connections  (private)
  // *
  // ********************************

void QuarkHandler::connectGaugeConfigurationHandler(const string& gauge_id)
{
 if ((gaugeCounter==0)&&(gaugeHandler.get()==0)){
    try{
       gaugeHandler.reset(new GaugeConfigurationHandler(*uPtr,gauge_id));
       gaugeCounter=1;}
    catch(...){
       QDPIO::cerr << "allocation problem in QuarkHandler::connectGaugeConfigurationHandler"<<endl;
       QDP_abort(1);}}
 else{
    try{
       if (gaugeHandler.get()==0) throw(string("error"));
       uPtr->checkEqual(gaugeHandler->getGaugeConfigurationInfo());
       gaugeCounter++;}
    catch(...){
       QDPIO::cerr << "inconsistent QuarkHandler::connectGaugeConfigurationHandler"<<endl;
       QDP_abort(1);}}
}

void QuarkHandler::disconnectGaugeConfigurationHandler()
{
 gaugeCounter--;
// if ((gaugeCounter==0)&&(!keepInMemory)){
 if (gaugeCounter==0){
    try{ gaugeHandler.reset();}
    catch(...){
       QDPIO::cerr << "delete problem in QuarkHandler::disconnectGaugeConfigurationHandler"<<endl;
       QDP_abort(1);}}
}




void QuarkHandler::connectGluonSmearingHandler(const string& smeared_gauge_filename)
{
 if ((gSmearCounter==0)&&(gSmearHandler.get()==0)){
    try{
       gSmearHandler.reset(new GluonSmearingHandler(*gSmearPtr,*uPtr,smeared_gauge_filename));
       gSmearCounter=1;}
    catch(...){
       QDPIO::cerr << "allocation problem in QuarkHandler::connectGluonSmearingHandler"<<endl;
       QDP_abort(1);}}
 else{
    try{
       if (gSmearHandler.get()==0) throw(string("error"));
       uPtr->checkEqual(gSmearHandler->getGaugeConfigurationInfo());
       gSmearPtr->checkEqual(gSmearHandler->getGluonSmearingInfo());
       gSmearCounter++;}
    catch(...){
       QDPIO::cerr << "inconsistent QuarkHandler::connectGluonSmearingHandler"<<endl;
       QDP_abort(1);}}
}

void QuarkHandler::disconnectGluonSmearingHandler()
{
 gSmearCounter--;
// if ((gSmearCounter==0)&&(!keepInMemory)){
 if (gSmearCounter==0){
    try{ gSmearHandler.reset();}
    catch(...){
       QDPIO::cerr << "delete problem in QuarkHandler::disconnectGluonSmearingHandler"<<endl;
       QDP_abort(1);}}
}



void QuarkHandler::connectQuarkSmearingHandler(const string& smeared_quark_filestub)
{
 if ((qSmearCounter==0)&&(qSmearHandler.get()==0)){
    try{
       qSmearHandler.reset(new QuarkSmearingHandler(*gSmearPtr,*uPtr,*qSmearPtr,
                                                    smeared_quark_filestub));
       qSmearCounter=1;}
    catch(...){
       QDPIO::cerr << "allocation problem in QuarkHandler::connectQuarkSmearingHandler"<<endl;
       QDP_abort(1);}}
 else{
    try{
       if (qSmearHandler.get()==0) throw(string("error"));
       uPtr->checkEqual(qSmearHandler->getGaugeConfigurationInfo());
       qSmearHandler->updateSmearing(*qSmearPtr);  // increase eigvecs if needed
       qSmearCounter++;}
    catch(...){
       QDPIO::cerr << "inconsistent QuarkHandler::connectQuarkSmearingHandler"<<endl;
       QDP_abort(1);}}
}

void QuarkHandler::disconnectQuarkSmearingHandler()
{
 qSmearCounter--;
// if ((qSmearCounter==0)&&(!keepInMemory)){
 if (qSmearCounter==0){
    try{ qSmearHandler.reset();}
    catch(...){
       QDPIO::cerr << "delete problem in QuarkHandler::disconnectQuarkSmearingHandler"<<endl;
       QDP_abort(1);}}
}


void QuarkHandler::connectDilutionHandler() const
{
 if (dilHandler!=0){
    QDPIO::cerr << "QuarkHandler::connectDilutionHandler already connected"<<endl;
    QDP_abort(1);}
 try{
    dilHandler = new DilutionHandler(*dilPtr,*uPtr,*qSmearPtr);}
 catch(...){
    QDPIO::cerr << "allocation problem in QuarkHandler::connectDilutionHandler"<<endl;
    QDP_abort(1);}
}

void QuarkHandler::disconnectDilutionHandler() const
{
 try{ delete dilHandler;}
 catch(...){
    QDPIO::cerr << "delete problem in QuarkHandler::disconnectDilutionHandler"<<endl;
    QDP_abort(1);}
 dilHandler=0;
}


#if (QDP_ND == 3)


void QuarkHandler::setNormalMode()
{
 if (normal_mode) return;
 clearData();
 normal_mode=true;
}

void QuarkHandler::setGamma5HermiticityMode()
{
 if (!normal_mode) return;
 clearData();
 normal_mode=false;
}

void QuarkHandler::getFileMap(XmlWriter& xmlout) const
{
 if (isInfoSet()) DHgetPtr->getFileMap(xmlout);
}

void QuarkHandler::outputKeys(XmlWriter& xmlout)
{
 if (isInfoSet()) DHgetPtr->outputKeys(xmlout);
}

set<QuarkHandler::FileKey> QuarkHandler::getNoisesAndTimeProjectors() const
{
 check_info_set("getNoisesAndTimeProjectors");
 return DHgetPtr->getFileKeys();
}

void QuarkHandler::getNoisesAndTimeProjectors(set<LaphNoiseInfo>& noises,
                                              set<int>& time_proj_indices) const
{
 check_info_set("getNoisesAndTimeProjectors");
 set<NoiseAndTimeProjector> nt=DHgetPtr->getFileKeys();
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    time_proj_indices.insert(it->time_proj_index);}
}

void QuarkHandler::getNoisesAndSourceTimes(set<LaphNoiseInfo>& noises,
                                           set<int>& source_times) const
{
 check_info_set("getNoisesAndSourceTimes");
 set<NoiseAndTimeProjector> nt=DHgetPtr->getFileKeys();
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    const list<int>& onts=getOnTimes(it->time_proj_index);
    source_times.insert(onts.begin(),onts.end());}
}


map<int,QuarkHandler::FileKey> QuarkHandler::getSuffixMap() const
{
 check_info_set("getSuffixMap");
 return DHgetPtr->getSuffixMap();
}


void QuarkHandler::outputSuffixMap()
{
 check_info_set("getSuffixMap");
 map<int,QuarkHandler::FileKey> suffixmap=DHgetPtr->getSuffixMap();
 QDPIO::cout <<endl<<"Suffix map:"<<endl;
 for (map<int,QuarkHandler::FileKey>::const_iterator it=suffixmap.begin();
      it!=suffixmap.end();++it){
    QDPIO::cout << "suffix "<<it->first<<":  LaphNoiseInfo seed = "
      << it->second.noise.getSeed()<<"  time proj index = "
      << it->second.time_proj_index << endl;}
 QDPIO::cout << endl;
}


void QuarkHandler::outputSuffixMap(TextFileWriter& fout)
{
 check_info_set("getSuffixMap");
 map<int,QuarkHandler::FileKey> suffixmap=DHgetPtr->getSuffixMap();
 fout <<"\nSuffix map:\n";
 for (map<int,QuarkHandler::FileKey>::const_iterator it=suffixmap.begin();
      it!=suffixmap.end();++it){
    fout << "suffix "<<it->first<<":  LaphNoiseInfo seed = "
      << it->second.noise.getSeed()<<"  time proj index = "
      << it->second.time_proj_index << "\n";}
 fout << "\n";
}

#elif (QDP_ND == 4)

void QuarkHandler::setInverter(const InverterInfo& invinfo)
{
 try{
    delete invertPtr;
    invertPtr = new InverterInfo(invinfo);}
 catch(...){
    QDPIO::cerr << "allocation error in QuarkHandler::setInverter"<<endl;
    QDP_abort(1);}
}

const InverterInfo& QuarkHandler::getInverterInfo() const 
{
 if (invertPtr!=0){
    QDPIO::cerr << "error in QuarkHandler:"<<endl;
    QDPIO::cerr << "  must setInverter before calling getInverterInfo"<<endl;
    QDP_abort(1);}
 return *invertPtr;
}

void QuarkHandler::getFileMap(XmlWriter& xmlout) const
{
 if (isInfoSet()) DHputPtr->getFileMap(xmlout);
}

void QuarkHandler::outputSuffixMap()
{
 check_info_set("getSuffixMap");
 map<int,QuarkHandler::FileKey> suffixmap=DHputPtr->getSuffixMap();
 QDPIO::cout <<endl<<"Suffix map:"<<endl;
 for (map<int,QuarkHandler::FileKey>::const_iterator it=suffixmap.begin();
      it!=suffixmap.end();++it){
    QDPIO::cout << "suffix "<<it->first<<":  LaphNoiseInfo seed = "
      << it->second.noise.getSeed()<<"  time proj index = "
      << it->second.time_proj_index << endl;}
 QDPIO::cout << endl;
}

#endif




bool QuarkHandler::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(qSmearPtr!=0)&&(fPtr!=0)
        &&(dilPtr!=0)&&(qactionPtr!=0));
}


void QuarkHandler::check_info_set(const string& name) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in QuarkHandler:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
}


const GaugeConfigurationInfo& QuarkHandler::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& QuarkHandler::getGluonSmearingInfo() const
{
 check_info_set("getGluonSmearingInfo");
 return *gSmearPtr;
}

const QuarkSmearingInfo& QuarkHandler::getQuarkSmearingInfo() const
{
 check_info_set("getQuarkSmearingInfo");
 return *qSmearPtr;
}

const DilutionSchemeInfo& QuarkHandler::getDilutionSchemeInfo() const 
{
 check_info_set("getDilutionSchemeInfo");
 return *dilPtr;
}

const QuarkActionInfo& QuarkHandler::getQuarkActionInfo() const 
{
 check_info_set("getQuarkActionInfo");
 return *qactionPtr;
}

const FileListInfo& QuarkHandler::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

int QuarkHandler::getNumberOfSpinEigvecDilutionProjectors() const 
{
 check_info_set("getNumberOfSpinEigvecDilutionProjectors");
 return dilHandler->getNumberOfSpinEigvecProjectors();
}

int QuarkHandler::getNumberOfTimeDilutionProjectors() const 
{
 check_info_set("getNumberOfTimeDilutionProjectors");
 return dilHandler->getNumberOfTimeProjectors();
}

int QuarkHandler::getTimeDilutionProjectorIndex(int time_val) const 
{
 check_info_set("getTimeDilutionProjectorIndex");
 return dilHandler->getTimeProjectorIndex(time_val);
}

const list<int>& QuarkHandler::getOnTimes(int time_proj_index) const 
{
 check_info_set("getOnTimes");
 return dilHandler->getOnTimes(time_proj_index);
}

bool QuarkHandler::isFullTimeDilution() const 
{
 check_info_set("isFullTimeDilution");
 return dilHandler->isFullTimeDilution();
}

int QuarkHandler::getTimeExtent() const 
{
 check_info_set("getTimeExtent");
 return uPtr->getTimeExtent();
}


void QuarkHandler::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"QuarkHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    qSmearPtr->output(xmlout);
    dilPtr->output(xmlout);
    qactionPtr->output(xmlout);
    pop(xmlout);}
}

bool QuarkHandler::checkHeader(XmlReader& xml_in, int suffix)
{
 if (xml_tag_count(xml_in,"QuarkHandlerDataFile")!=1) return false;
 XmlReader xmlr(xml_in,"./descendant-or-self::QuarkHandlerDataFile");
 GaugeConfigurationInfo gauge_check(xmlr);
 GluonSmearingInfo gsmear_check(xmlr);
 QuarkSmearingInfo qsmear_check(xmlr);
 DilutionSchemeInfo dil_check(xmlr);
 QuarkActionInfo qaction_check(xmlr);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    qSmearPtr->checkEqual(qsmear_check);
    dilPtr->checkEqual(dil_check);
    qactionPtr->checkEqual(qaction_check); }
 catch(...){ return false;}
 return true;
}

void QuarkHandler::writeHeader(XmlWriter& xmlout, 
                               const QuarkHandler::FileKey& fkey,
                               int suffix)
{
 push(xmlout, "QuarkHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 qSmearPtr->output(xmlout);
 dilPtr->output(xmlout);
 qactionPtr->output(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}


#if (QDP_ND == 4)



   // ***************************************************************


    //  do inversions for all spin-laph-eigenvector dilution indices
    //  but for one noise, one time projector index


void QuarkHandler::computeSink(const LaphNoiseInfo& noise, 
                               int time_proj_index, bool verbose)
{
 if ((!isInfoSet())||(invertPtr==0)){
    QDPIO::cerr << "cannot computeSink in QuarkHandler until"
                << " info and inverter set"<<endl;
    QDP_abort(1);}

 if (!dilHandler->isValidTimeProjectorIndex(time_proj_index)){
    QDPIO::cerr << "invalid time projector index "<<time_proj_index
                <<" for compute in QuarkHandler"<<endl;
    QDPIO::cerr << " skipping this computation"<<endl;
    return;}

 int Textent = uPtr->getTimeExtent();
 const list<int>& onTimes=dilHandler->getOnTimes(time_proj_index);

 
 QDPIO::cout <<endl<< "Quark sink computation for all dilutions, one noise,"
             << " one time dilution projector beginning"<<endl;
 QDPIO::cout << " Time dilution projector index = "<<time_proj_index<<endl;
 START_CODE();
 StopWatch totaltime;
 totaltime.start();
 double iotime=0.0, srctime=0.0, snktime=0.0, invtime=0.0;
 double evreadtime=0.0, sumtime=0.0, evread2time=0.0;
 double noisetime=0.0, inittime=0.0, rotatetime=0.0, latfermAtime=0.0;
 double invchecktime=0.0, snkrottime=0.0, peekspintime=0.0, contracttime=0.0;
 
     // set the noise vector

 StopWatch rolex,bulava; rolex.start();bulava.start();
 LaphZnNoise rho(noise.getZNGroup(),noise.getSeed(*uPtr));
 int Tdir = uPtr->getTimeDir();
 int Nspin = QDP::Ns;
 int nEigs = qSmearPtr->getNumberOfLaplacianEigenvectors();
  
 multi3d<DComplex> laph_noise(Textent,Nspin,nEigs);
 for (int t=0;t<Textent;t++)
    for (int s=0;s<Nspin;s++)
       for (int v=0;v<nEigs;v++)
          laph_noise(t,s,v) = rho.generate();   // same on all compute nodes
 QDPIO::cout << " Laph noise generated"<<endl;
 rolex.stop(); noisetime+=rolex.getTimeInSeconds();
 
 rolex.start();
 Set timeslices;                         // needed for time slice masks
 timeslices.make(TimeSlice(Tdir)); 
                                                      // rotate to DeGrand-Rossi, then 
 SpinMatrix SrcRotate = Gamma(8) * DiracToDRMat();   //  multiply by gamma_4
 SpinMatrix SnkRotate = adj(DiracToDRMat());    // rotate back to Dirac-Pauli

 string fermact_xml = qactionPtr->getDescription();
 string fermact_id = qactionPtr->getActionName();

   // Typedefs to save typing
 typedef LatticeFermion               T;
 typedef multi1d<LatticeColorMatrix>  P;
 typedef multi1d<LatticeColorMatrix>  Q;

 GroupXML_t solverInfo;
 solverInfo.xml =  invertPtr->output();
 solverInfo.id = invertPtr->getId();
 solverInfo.path = "//InvertParam";
 
   // Initialize fermion action
   
 istringstream xml_s(fermact_xml);
 XMLReader fermacttop0(xml_s);
 XMLReader fermacttop(fermacttop0,"./descendant-or-self::Description");   // due to XmlReader bug

 Handle< FermionAction<T,P,Q> > S_f; 
 Handle< FermState<T,P,Q> > state;
 Handle< SystemSolver<LatticeFermion> > PP;

 try{
    QDPIO::cout << "creating Fermion Action Factory"<<endl;
    QDPIO::cout << "  fermion action id = "<<qactionPtr->getActionName()<<endl;
    QDPIO::cout << "          solver id = "<<solverInfo.id<<endl;
    QDPIO::cout << "  fermact_xml:"<<endl<<fermact_xml<<endl;
    S_f=TheFermionActionFactory::Instance().createObject(
            fermact_id,fermacttop,"./descendant-or-self::FermionAction");
    state=S_f->createState(gaugeHandler->getData());
    PP = S_f->qprop(state,solverInfo); }
 catch(const string& err){
    QDPIO::cerr << " Fermion action and inverter could not be initialized"
                << " in QuarkHandler"<<endl;
    QDPIO::cerr << "solverInfo.xml:"<<endl<<solverInfo.xml<<endl;
    QDPIO::cerr << "fermact_xml:"<<endl<<fermact_xml<<endl;
    QDP_abort(1);}
 QDPIO::cout << "Suitable FermionActionFactory found: Dirac matrix inversions may begin" << endl;

      // get the file key and open file for writing

 FileKey fkey(noise,time_proj_index);
 DHputPtr->open(fkey);
 int ndil=dilHandler->getNumberOfSpinEigvecProjectors();
 bulava.stop(); srctime+=bulava.getTimeInSeconds();
 rolex.stop(); inittime+=rolex.getTimeInSeconds();

     // loop over dilutions
 
 for (int dil=0;dil<ndil;dil++){

    bulava.start();
    QDPIO::cout <<endl<< "Starting dilution "<<dil<<" (with "<<ndil-1<<" as last)"<<endl;
    bool doneflag=true;
    rolex.start();
    for (int t=0;t<Textent;t++)
    for (int s=1;s<=Nspin;s++){
       if (!DHputPtr->queryData(RecordKey(s,t,dil))){
          doneflag=false; s=Nspin+1; t=Textent;}}
    rolex.stop(); evreadtime+=rolex.getTimeInSeconds();

    if ((!fPtr->isModeOverwrite())&&(doneflag)){
       QDPIO::cout << "warning: these quark sinks already computed..."
                   << "skip re-computing since fileMode not overwrite"<<endl;
       continue;}

        // get the lists of which spins and which eigenvectors are
        // "on" for this dilution projector
        
    const list<int>& on_spins=dilHandler->getOnSpinIndices(dil);
    const list<int>& on_eigs=dilHandler->getOnEigvecIndices(dil);

        //  initialize output field for sources
    StopWatch seiko; seiko.start();
    LatticeFermion latfermA = zero;
    seiko.stop(); latfermAtime+=seiko.getTimeInSeconds();
    for (list<int>::const_iterator vmask= on_eigs.begin(); vmask!=on_eigs.end(); vmask++){
       rolex.start();
       const LatticeColorVector& Vs = qSmearHandler->getLaphEigenvector(*vmask);
       rolex.stop(); evreadtime+=rolex.getTimeInSeconds();
       seiko.start();
       LatticeSpinVector sv = zero;
       for (list<int>::const_iterator smask= on_spins.begin(); smask!=on_spins.end(); smask++){
          LatticeComplex temp = zero;
               // apply time dilution projector
          for (list<int>::const_iterator it0=onTimes.begin();
             it0!=onTimes.end();it0++){
             temp[timeslices[*it0]] = laph_noise(*it0,*smask,*vmask);}
          pokeSpin(sv,temp,*smask);}
       for (list<int>::const_iterator it0=onTimes.begin();
          it0!=onTimes.end();it0++){
          latfermA[timeslices[*it0]] += sv * Vs;}
       seiko.stop(); latfermAtime+=seiko.getTimeInSeconds();
       }

    seiko.start();       
    LatticeFermion latfermB = SrcRotate * latfermA;  // rotate to DeGrand-Rossi, mult by gamma_4

    QDPIO::cout << "Norm of source vector = "<<sqrt(norm2(latfermB))<<endl;
    latfermA = zero;
    QDPIO::cout << "source created...starting inversion"<<endl;
    bulava.stop(); srctime+=bulava.getTimeInSeconds();
    seiko.stop(); rotatetime+=seiko.getTimeInSeconds();
    
             // now do the inversion
    bulava.start();
    SystemSolverResults_t res = (*PP)(latfermA, latfermB);  // solution in latfermA
    bulava.stop(); invtime+=bulava.getTimeInSeconds();

    QDPIO::cout << "Inversion done:  number of iterations = "<<res.n_count<<endl;
    QDPIO::cout << " Residual = "<<res.resid<<endl;

       // output the relative residual of the unpreconditioned system, if possible
       // The fermion action named "CLOVER" is even-odd preconditioned.
       // So one solves for either the even or the odd sites only with a modified
       // Dirac matrix, then the other sites are obtained algebraically.

    bulava.start(); rolex.start();
    if (fermact_id=="CLOVER"){
       try{
        CloverFermActParams cparams(fermacttop,
                      "./descendant-or-self::FermionAction");
        UnprecCloverLinOp Dclover(state,cparams);
        T r=latfermB;
        T Mx;
        Dclover( Mx, latfermA, PLUS);
        r -= Mx;

        double ynorm2=toDouble(norm2(latfermB));
        LatticeReal lnorm=localNorm2(r);
        double localmag=toDouble(sqrt(globalMax(lnorm)/ynorm2));
        double relresidunprec=toDouble(sqrt(sum(lnorm)/ynorm2));

        QDPIO::cout << " Relative residual is  r = M*x-y / |y|"
                    << " for unpreconditioned system"<<endl;
        QDPIO::cout << " Relative residual of unpreconditioned system = |r| = "
                    << relresidunprec << endl;
        QDPIO::cout << "                       Maximum local magnitude of r = "
                    <<localmag<< endl<<endl;}
       catch(...){
          QDPIO::cout << "could not evaluate relative residual of "
                      << "unpreconditioned clover system"<<endl;}}
    bulava.stop(); snktime+=bulava.getTimeInSeconds();
    rolex.stop(); invchecktime+=rolex.getTimeInSeconds();

    if ((res.n_count>0)&&(res.n_count<invertPtr->getMaxIterations())){

                   // sink = Vs^dagger * phi

       rolex.start();
       latfermB = SnkRotate * latfermA;         // rotate back to Dirac-Pauli
       rolex.stop(); snkrottime+=rolex.getTimeInSeconds();
       multi2d<Complex> quark_sink(Textent,nEigs);
       multi1d<DComplex> tres(Textent);

       for (int s=0;s<Nspin;s++){
          bulava.start(); rolex.start();
          LatticeColorVector phi_s = peekSpin(latfermB, s);
          rolex.stop(); peekspintime+=rolex.getTimeInSeconds();
          for (int n=0;n<nEigs;n++){
             rolex.start();
             const LatticeColorVector& Ws= qSmearHandler->getLaphEigenvector(n);
             rolex.stop(); evread2time+=rolex.getTimeInSeconds();
             rolex.start();
             LatticeComplex tmp = localInnerProduct( Ws, phi_s );  // color contraction
             rolex.stop(); contracttime+=rolex.getTimeInSeconds();
             rolex.start();
             tres = sumMulti(tmp,timeslices);  // spatial sums on each timeslice
             rolex.stop(); sumtime+=rolex.getTimeInSeconds();
             for (int t=0;t<Textent;t++){
                quark_sink(t,n)=tres[t];}}
          bulava.stop(); snktime+=bulava.getTimeInSeconds();

           // output to file
          StopWatch iotimer; iotimer.start();
          for (int t=0;t<Textent;t++)
             DHputPtr->putData(RecordKey(s+1,t,dil),quark_sink[t]);

          if (verbose){
          QDPIO::cout << "spin = "<<s+1<<endl;
          for (int t=0;t<Textent;t++){
             QDPIO::cout << "time = "<<t<<endl;
             for (int n=0;n<nEigs;n++){
                QDPIO::cout << "coef for eigenlevel "<<n<<" = "
                            << quark_sink(t,n)<<endl;}}}

          iotimer.stop();
          iotime+=iotimer.getTimeInSeconds();}}

    else{
    
       QDPIO::cout << endl<<endl;
       QDPIO::cout << "Inversion FAILED to converge before max iteration reached"<<endl;
       QDPIO::cout << "Solution NOT WRITTEN to file"<<endl;
       QDPIO::cout << endl<<endl;}

    DHputPtr->flush();
    }

 totaltime.stop();
 QDPIO::cout << endl<<endl;
 QDPIO::cout << "computeQuarkSink: one noise, all dilutions, one source time: ran successfully" << endl;
 QDPIO::cout << "                 Total time = "<<totaltime.getTimeInSeconds() << " seconds" << endl;
 QDPIO::cout << "   Total source set up time = "<<srctime<<" seconds"<<endl;
 QDPIO::cout << "           ZN noise creation time = "<<noisetime<<" seconds"<<endl;
 QDPIO::cout << "              Initialization time = "<<inittime<<" seconds"<<endl;
 QDPIO::cout << "            LapH eigvec read time = "<<evreadtime<<" seconds"<<endl;
 QDPIO::cout << "            latfermA compute time = "<<latfermAtime<<" seconds"<<endl;
 QDPIO::cout << "                src rotation time = "<<rotatetime<<" seconds"<<endl;
 QDPIO::cout << "     Total time in inverter = "<<invtime<<" seconds"<<endl;
 QDPIO::cout << " Total sink completion time = "<<snktime<<" seconds"<<endl;
 QDPIO::cout << "             Inversion check time = "<<invchecktime<<" seconds"<<endl;
 QDPIO::cout << "               Sink rotation time = "<<snkrottime<<" seconds"<<endl;
 QDPIO::cout << "                   Peek spin time = "<<peekspintime<<" seconds"<<endl;
 QDPIO::cout << "          Color contractions time = "<<contracttime<<" seconds"<<endl;
 QDPIO::cout << "             Time in lattice sums = "<<sumtime<<" seconds"<<endl;
 QDPIO::cout << "            LapH eigvec read time = "<<evread2time<<" seconds"<<endl;
 QDPIO::cout << "    Total file writing time = "<<iotime <<" seconds"<<endl;
 QDPIO::cout << endl<<endl;
}


// *******************************************************************

#elif (QDP_ND == 3)


const LatticeColorVector* QuarkHandler::getData(
                                     bool source, 
                                     const LaphNoiseInfo& noise,
                                     int time_proj_index,
                                     int spinlev_dilution_index, int spin,
                                     const DirPath& displace, int disp_length,
                                     int time)
{
 map<StorageKey,LatticeColorVector*>& store_map 
   = (source) ? store_sources : store_sinks;

 StorageKey skey(FileKey(noise,time_proj_index),
                 RecordKey(spin,time,spinlev_dilution_index),
                 displace,disp_length);

    // if already in map, return pointer

 map<StorageKey,LatticeColorVector*>::const_iterator it
              =store_map.find(skey);
 if (it!=store_map.end()){
    return (it->second);}

    // else, must go to file and compute

 check_info_set("getData");
 if (source){
      // check time projector index, spin dilution
    if ((!dilHandler->isOnTime(time_proj_index,time)) 
      ||(!dilHandler->isOnSpin(spinlev_dilution_index,mode_spin(spin)-1))){
			store_map.insert(pair<StorageKey,LatticeColorVector*>(skey,0));
       return 0;}}

      // read undisplaced data from file (include in map)
 const LatticeColorVector* start
   =(source) ? getSourceNoDisplace(skey.fkey,skey.rkey)
             : getSinkNoDisplace(skey.fkey,skey.rkey);

 if (skey.disp_length==0) return start;

   // calculate displacement
 LatticeColorVector *finish;
 try{
    finish=new LatticeColorVector;}
 catch(...){
    QDPIO::cerr << "could not allocation memory for set in QuarkHandler"<<endl;
    QDP_abort(1);}
 store_map.insert(make_pair(skey,finish));
 
 int numdisplace=skey.disp.Length();
 int seglength=skey.disp_length;
 DirPath::const_reverse_iterator seg=skey.disp.rbegin();

 if ((numdisplace%2)==1){  // odd number of displacements
    covdisplace(*start,*finish,*seg,seglength,time); ++seg;}
 if (numdisplace==1) return finish;

 LatticeColorVector temp;
 if ((numdisplace%2)==0){
    covdisplace(*start,temp,*seg,seglength,time); ++seg;
    covdisplace(temp,*finish,*seg,seglength,time); ++seg;}
 for (int k=0;k<((numdisplace-1)/2);k++){
    covdisplace(*finish,temp,*seg,seglength,time); ++seg;
    covdisplace(temp,*finish,*seg,seglength,time); ++seg;}

 return finish;
}


const LatticeColorVector* QuarkHandler::getSinkNoDisplace(
                              const FileKey& fkey, const RecordKey& rkey)
{
 StorageKey skey(fkey,rkey,DirPath(),0);
 map<StorageKey,LatticeColorVector*>::const_iterator fitr
         =store_sinks.find(skey);
 if (fitr!=store_sinks.end()) return (fitr->second);

 int gsign=1;
 DataType coefs;
 int nEigs = qSmearPtr->getNumberOfLaplacianEigenvectors();
 int t=rkey.getTime();
 RecordKey rgkey(rkey);
 if (!normal_mode) rgkey.applyGamma5Herm(gsign);

 coefs=DHgetPtr->getData(fkey,rgkey);
 if (coefs.size()!=nEigs){
    QDPIO::cerr << "number of eigenvectors is wrong!!"<<endl;
    QDP_abort(1);}

 LatticeColorVector *buf=0;
 try {
    buf=new LatticeColorVector;
    *buf=zero;
    for (int n=0;n<nEigs;n++) 
       (*buf)+=coefs[n]*qSmearHandler->getLaphEigenvector(t,n);
    if (gsign==-1) *buf=-(*buf);}
 catch(...){
    delete buf;
    QDPIO::cerr << "Error during QuarkHandler lookup"<<endl;
    QDP_abort(1);}

 DHgetPtr->removeData(fkey,rgkey);
 store_sinks.insert(make_pair(skey,buf));
 return buf;
}


const LatticeColorVector* QuarkHandler::getSourceNoDisplace(
                              const FileKey& fkey, const RecordKey& rkey)
{
 StorageKey skey(fkey,rkey,DirPath(),0);
 map<StorageKey,LatticeColorVector*>::const_iterator fitr
         =store_sources.find(skey);
 if (fitr!=store_sources.end()) return (fitr->second);

 int gsign=1;
 int time=rkey.getTime();
 RecordKey rgkey(rkey);
 if (!normal_mode){
    rgkey.applyGamma5Herm(gsign);
    gsign=-gsign;}
 int spin=rgkey.getSpin()-1;  // need zero-base here

     // set the noise vector 

 LaphZnNoise rho(fkey.noise.getZNGroup(),fkey.noise.getSeed(*uPtr));
 int Textent = uPtr->getTimeExtent();
 int Nspin = QDP::Ns;
 int nEigs = qSmearPtr->getNumberOfLaplacianEigenvectors();
  
    // must generate noise in **exactly** the same way as for the sinks
    // store results in array to ensure optimizations do not omit terms in
    // sequence

 multi3d<DComplex> laph_noise(Textent,Nspin,nEigs);
 for (int t=0;t<Textent;t++){
    for (int s=0;s<Nspin;s++)
       for (int v=0;v<nEigs;v++)
          laph_noise(t,s,v) = rho.generate();   // same on all compute nodes
    if (t==time) break;}
 if (gsign==-1){
    for (int v=0;v<nEigs;v++) laph_noise(time,spin,v)*=-1.0;}

 int sevdil=rkey.getSpinLaphEigvecIndex();
 const list<int>& on_eigs=dilHandler->getOnEigvecIndices(sevdil);
 LatticeColorVector *buf=0;
 try {
    buf=new LatticeColorVector;
    *buf=zero;
    for (list<int>::const_iterator vmask=on_eigs.begin();vmask!=on_eigs.end();vmask++)
       (*buf)+=laph_noise(time,spin,*vmask)
               *qSmearHandler->getLaphEigenvector(time,*vmask);}
 catch(...){
    delete buf;
    QDPIO::cerr << "Error during QuarkHandler lookup"<<endl;
    QDP_abort(1);}

 store_sources.insert(make_pair(skey,buf));
 return buf;
}


void QuarkHandler::covdisplace(const LatticeColorVector& start,
                               LatticeColorVector& finish,
                               int dir, int length, int timeval)
{
 const multi1d<LatticeColorMatrix>& usmear
        =gSmearHandler->getSmearedGaugeFieldTimeSlice(timeval);

 int qdp_length=(dir>0)?length:-length;
 int qdp_dir=abs(dir)-1;       // in coefficient file, 1=x, 2=y, 3=z
                               // in displace subroutine x=0, y=1, z=2
// StopWatch timer;
// timer.start();
 finish=displace(usmear,start,qdp_length,qdp_dir);
// timer.stop();
// cout << "time to displace = "<<timer.getTimeInMicroseconds()<<" microseconds"<<endl;
}


QuarkHandler::DataType QuarkHandler::getLaphEigenvectorSinkCoefficients(
                                     const LaphNoiseInfo& noise,
                                     int time_proj_index,
                                     int spinlev_dilution_index, 
                                     int spin, int time)
{
 FileKey fkey(noise,time_proj_index);
 RecordKey rkey(spin,time,spinlev_dilution_index);
 DataType coefs;
 if (!DHgetPtr->queryData(fkey,rkey)){
    QDPIO::cerr << "requested coefficients could not be found"<<endl;}
 else
    coefs=DHgetPtr->getData(fkey,rkey);
 return coefs;
}

 // *****************************************************


bool QuarkHandler::queryData(bool source, const LaphNoiseInfo& noise, 
                             int time_proj_index, int spinlev_dilution_index, 
                             int spin, int time)
{
 if (source) return true;
 StorageKey skey(FileKey(noise,time_proj_index),
                 RecordKey(spin,time,spinlev_dilution_index),
                 DirPath(),0);
    // if already in map, return true
 map<StorageKey,LatticeColorVector*>::const_iterator it
              =store_sinks.find(skey);
 if (it!=store_sinks.end()) return true;
 check_info_set("getData");
 RecordKey rgkey(skey.rkey);
 int gsign;
 if (!normal_mode) rgkey.applyGamma5Herm(gsign);
 return DHgetPtr->queryData(skey.fkey,rgkey);
}


        // remove from internal memory

void QuarkHandler::removeData(bool source, const LaphNoiseInfo& noise,
                              int time_proj_index, int spinlev_dilution_index, 
                              int spin, const DirPath& displace, 
                              int disp_length, int time)
{
 map<StorageKey,LatticeColorVector*>& store_map 
   = (source) ? store_sources : store_sinks;

 StorageKey skey(FileKey(noise,time_proj_index),
                 RecordKey(spin,time,spinlev_dilution_index),
                 displace,disp_length);

    // if already in map, remove

 map<StorageKey,LatticeColorVector*>::iterator it
              =store_map.find(skey);
 if (it!=store_map.end()){
    if (it->second != 0) delete it->second;
    store_map.erase(it);}
}



void QuarkHandler::clearOnlyDisplacedData()
{
 for (map<StorageKey,LatticeColorVector*>::iterator it=store_sources.begin();
    it!=store_sources.end();){
    if ((it->first).disp_length==0) it++;
    else{
       map<StorageKey,LatticeColorVector*>::iterator bt=it; it++;
       if (bt->second != 0) delete bt->second;
       store_sources.erase(bt);}}
 for (map<StorageKey,LatticeColorVector*>::iterator it=store_sinks.begin();
    it!=store_sinks.end();){
    if ((it->first).disp_length==0) it++;
    else{
       map<StorageKey,LatticeColorVector*>::iterator bt=it; it++;
       delete bt->second;
       store_sinks.erase(bt);}}
}


void QuarkHandler::clearData()
{
 for (map<StorageKey,LatticeColorVector*>::iterator it=store_sources.begin();
    it!=store_sources.end();++it){
    if (it->second != 0) delete it->second;}
 store_sources.clear();
 for (map<StorageKey,LatticeColorVector*>::iterator it=store_sinks.begin();
    it!=store_sinks.end();++it){
    delete it->second;}
 store_sinks.clear();
}


void QuarkHandler::clearGaugeData()
{
 if (gSmearHandler.get()) gSmearHandler->clearData();
 if (qSmearHandler.get()) qSmearHandler->clearLaphEigenvectors();
}


#endif

 
// ***************************************************************

  //  static pointers (set to null in default constructor)

auto_ptr<GluonSmearingHandler> QuarkHandler::gSmearHandler;
auto_ptr<QuarkSmearingHandler> QuarkHandler::qSmearHandler;
auto_ptr<GaugeConfigurationHandler> QuarkHandler::gaugeHandler;

int QuarkHandler::gSmearCounter=0;
int QuarkHandler::qSmearCounter=0;
int QuarkHandler::gaugeCounter=0;
//bool QuarkHandler::keepInMemory=false;





  // **************************************************************
  //
  //   The class below is used for checking the computations
  //   of the needed quark sinks has been done.
  // 
  // **************************************************************


QuarkChecker::QuarkChecker()
          : uPtr(0), gSmearPtr(0), qSmearPtr(0), dilPtr(0), qactionPtr(0),
            fPtr(0), dilHandler(0), DHgetPtr(0) {}


QuarkChecker::QuarkChecker(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkSmearingInfo& quarksmear,
                           const DilutionSchemeInfo& dil,
                           const QuarkActionInfo& quark,
                           const FileListInfo& flist)
          : dilHandler(0), DHgetPtr(0)
{
 set_info(gaugeinfo,gluonsmear,quarksmear,dil,quark,flist);
}

void QuarkChecker::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkSmearingInfo& quarksmear,
                           const DilutionSchemeInfo& dil,
                           const QuarkActionInfo& quark,
                           const FileListInfo& flist)
{
 clear();
 set_info(gaugeinfo,gluonsmear,quarksmear,dil,quark,flist);
}


void QuarkChecker::set_info(const GaugeConfigurationInfo& gaugeinfo,
                            const GluonSmearingInfo& gluonsmear,
                            const QuarkSmearingInfo& quarksmear,
                            const DilutionSchemeInfo& dil,
                            const QuarkActionInfo& quark,
                            const FileListInfo& flist)
{
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    qSmearPtr = new QuarkSmearingInfo(quarksmear);
    dilPtr = new DilutionSchemeInfo(dil);
    qactionPtr = new QuarkActionInfo(quark);
    fPtr = new FileListInfo(flist);

    DHgetPtr=new DataGetHandlerMF<QuarkChecker,QuarkHandler::FileKey,
                     QuarkHandler::RecordKey,QuarkHandler::DataType>(
                    *this,*fPtr,"Laph--QuarkSink","QuarkHandlerDataFile");
    }
 catch(...){
    QDPIO::cerr << "allocation problem in QuarkChecker"<<endl;
    QDP_abort(1);}

 connectDilutionHandler();

}


QuarkChecker::~QuarkChecker()
{
 clear();
}


void QuarkChecker::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete qSmearPtr;
    delete dilPtr;
    delete qactionPtr;
    delete fPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 qSmearPtr=0;
 dilPtr=0;
 qactionPtr=0;
 fPtr=0;
 disconnectDilutionHandler();
 delete DHgetPtr; DHgetPtr=0;
}


  // ********************************
  // *
  // *    sub-handler connections  (private)
  // *
  // ********************************

void QuarkChecker::connectDilutionHandler() const
{
 if (dilHandler!=0){
    QDPIO::cerr << "QuarkChecker::connectDilutionHandler already connected"<<endl;
    QDP_abort(1);}
 try{
    dilHandler = new DilutionHandler(*dilPtr,*uPtr,*qSmearPtr);}
 catch(...){
    QDPIO::cerr << "allocation problem in QuarkChecker::connectDilutionHandler"<<endl;
    QDP_abort(1);}
}

void QuarkChecker::disconnectDilutionHandler() const
{
 try{ delete dilHandler;}
 catch(...){
    QDPIO::cerr << "delete problem in QuarkChecker::disconnectDilutionHandler"<<endl;
    QDP_abort(1);}
 dilHandler=0;
}



void QuarkChecker::getFileMap(XmlWriter& xmlout) const
{
 if (isInfoSet()) DHgetPtr->getFileMap(xmlout);
}

void QuarkChecker::outputKeys(XmlWriter& xmlout)
{
 if (isInfoSet()) DHgetPtr->outputKeys(xmlout);
}

set<QuarkHandler::FileKey> QuarkChecker::getNoisesAndTimeProjectors() const
{
 check_info_set("getNoisesAndTimeProjectors");
 return DHgetPtr->getFileKeys();
}

void QuarkChecker::getNoisesAndTimeProjectors(set<LaphNoiseInfo>& noises,
                                              set<int>& time_proj_indices) const
{
 check_info_set("getNoisesAndTimeProjectors");
 set<NoiseAndTimeProjector> nt=DHgetPtr->getFileKeys();
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    time_proj_indices.insert(it->time_proj_index);}
}

void QuarkChecker::getNoisesAndSourceTimes(set<LaphNoiseInfo>& noises,
                                           set<int>& source_times) const
{
 check_info_set("getNoisesAndSourceTimes");
 set<NoiseAndTimeProjector> nt=DHgetPtr->getFileKeys();
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    const list<int>& onts=getOnTimes(it->time_proj_index);
    source_times.insert(onts.begin(),onts.end());}
}


map<int,QuarkHandler::FileKey> QuarkChecker::getSuffixMap() const
{
 check_info_set("getSuffixMap");
 return DHgetPtr->getSuffixMap();
}


void QuarkChecker::outputSuffixMap()
{
 check_info_set("getSuffixMap");
 map<int,QuarkHandler::FileKey> suffixmap=DHgetPtr->getSuffixMap();
 QDPIO::cout <<endl<<"Suffix map:"<<endl;
 for (map<int,QuarkHandler::FileKey>::const_iterator it=suffixmap.begin();
      it!=suffixmap.end();++it){
    QDPIO::cout << "suffix "<<it->first<<":  LaphNoiseInfo seed = "
      << it->second.noise.getSeed()<<"  time proj index = "
      << it->second.time_proj_index << endl;}
 QDPIO::cout << endl;
}


void QuarkChecker::outputSuffixMap(TextFileWriter& fout)
{
 check_info_set("getSuffixMap");
 map<int,QuarkHandler::FileKey> suffixmap=DHgetPtr->getSuffixMap();
 fout <<"\nSuffix map:\n";
 for (map<int,QuarkHandler::FileKey>::const_iterator it=suffixmap.begin();
      it!=suffixmap.end();++it){
    fout << "suffix "<<it->first<<":  LaphNoiseInfo seed = "
      << it->second.noise.getSeed()<<"  time proj index = "
      << it->second.time_proj_index << "\n";}
 fout << "\n";
}



bool QuarkChecker::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(qSmearPtr!=0)&&(fPtr!=0)
        &&(dilPtr!=0)&&(qactionPtr!=0));
}


void QuarkChecker::check_info_set(const string& name) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in QuarkChecker:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
}


const GaugeConfigurationInfo& QuarkChecker::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& QuarkChecker::getGluonSmearingInfo() const
{
 check_info_set("getGluonSmearingInfo");
 return *gSmearPtr;
}

const QuarkSmearingInfo& QuarkChecker::getQuarkSmearingInfo() const
{
 check_info_set("getQuarkSmearingInfo");
 return *qSmearPtr;
}

const DilutionSchemeInfo& QuarkChecker::getDilutionSchemeInfo() const 
{
 check_info_set("getDilutionSchemeInfo");
 return *dilPtr;
}

const QuarkActionInfo& QuarkChecker::getQuarkActionInfo() const 
{
 check_info_set("getQuarkActionInfo");
 return *qactionPtr;
}

const FileListInfo& QuarkChecker::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

int QuarkChecker::getNumberOfSpinEigvecDilutionProjectors() const 
{
 check_info_set("getNumberOfSpinEigvecDilutionProjectors");
 return dilHandler->getNumberOfSpinEigvecProjectors();
}

int QuarkChecker::getNumberOfTimeDilutionProjectors() const 
{
 check_info_set("getNumberOfTimeDilutionProjectors");
 return dilHandler->getNumberOfTimeProjectors();
}

int QuarkChecker::getTimeDilutionProjectorIndex(int time_val) const 
{
 check_info_set("getTimeDilutionProjectorIndex");
 return dilHandler->getTimeProjectorIndex(time_val);
}

const list<int>& QuarkChecker::getOnTimes(int time_proj_index) const 
{
 check_info_set("getOnTimes");
 return dilHandler->getOnTimes(time_proj_index);
}

bool QuarkChecker::isFullTimeDilution() const 
{
 check_info_set("isFullTimeDilution");
 return dilHandler->isFullTimeDilution();
}

int QuarkChecker::getTimeExtent() const 
{
 check_info_set("getTimeExtent");
 return uPtr->getTimeExtent();
}


void QuarkChecker::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"QuarkHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    qSmearPtr->output(xmlout);
    dilPtr->output(xmlout);
    qactionPtr->output(xmlout);
    pop(xmlout);}
}

bool QuarkChecker::checkHeader(XmlReader& xml_in, int suffix)
{
 if (xml_tag_count(xml_in,"QuarkHandlerDataFile")!=1) return false;
 XmlReader xmlr(xml_in,"./descendant-or-self::QuarkHandlerDataFile");
 GaugeConfigurationInfo gauge_check(xmlr);
 GluonSmearingInfo gsmear_check(xmlr);
 QuarkSmearingInfo qsmear_check(xmlr);
 DilutionSchemeInfo dil_check(xmlr);
 QuarkActionInfo qaction_check(xmlr);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    qSmearPtr->checkEqual(qsmear_check);
    dilPtr->checkEqual(dil_check);
    qactionPtr->checkEqual(qaction_check); }
 catch(...){ return false;}
 return true;
}

 // *****************************************************

QuarkHandler::DataType QuarkChecker::getLaphEigenvectorSinkCoefficients(
                                     const LaphNoiseInfo& noise,
                                     int time_proj_index,
                                     int spinlev_dilution_index, 
                                     int spin, int time)
{
 check_info_set("getLaphEigenvectorSinkCoefficients");
 QuarkHandler::FileKey fkey(noise,time_proj_index);
 QuarkHandler::RecordKey rkey(spin,time,spinlev_dilution_index);
 QuarkHandler::DataType coefs;
 if (!DHgetPtr->queryData(fkey,rkey)){
    QDPIO::cerr << "requested coefficients could not be found"<<endl;}
 else
    coefs=DHgetPtr->getData(fkey,rkey);
 return coefs;
}


bool QuarkChecker::queryData(bool source, const LaphNoiseInfo& noise, 
                             int time_proj_index, int spinlev_dilution_index, 
                             int spin, int time)
{
 if (source) return true;
 check_info_set("queryData");
 QuarkHandler::FileKey fkey(noise,time_proj_index);
 QuarkHandler::RecordKey rkey(spin,time,spinlev_dilution_index);
 return DHgetPtr->queryData(fkey,rkey);
}


void QuarkChecker::query(const set<LaphNoiseInfo>& noise_list,
                         int num_time_proj, TextFileWriter& outlog)
{
 outlog << "QUERY:\n";
 outlog << "  Number of time dilution projectors required = "<<num_time_proj<<"\n";
 outlog << "  Noise seeds: ";
 for (set<LaphNoiseInfo>::const_iterator nt=noise_list.begin();
      nt!=noise_list.end();nt++) outlog << " "<<nt->getSeed();
 outlog << "\n\n"; 

 set<QuarkHandler::FileKey> fkeylist=getNoisesAndTimeProjectors();
 map<LaphNoiseInfo,set<int> > comps;
 for (set<QuarkHandler::FileKey>::const_iterator it=fkeylist.begin();
      it!=fkeylist.end();it++){
     comps[it->noise].insert(it->time_proj_index);}

 map<LaphNoiseInfo,set<int> >::const_iterator mt;
 for (set<LaphNoiseInfo>::const_iterator nt=noise_list.begin();
      nt!=noise_list.end();nt++){
   mt=comps.find(*nt);
   if (mt==comps.end()){
      outlog << "Noise "<<nt->getSeed()<<" is MISSING\n";}
   else{
      outlog << "For noise "<<nt->getSeed()<<" found time projector indices: ";
      for (set<int>::const_iterator st=mt->second.begin();st!=mt->second.end();++st)
         outlog <<" "<<*st;
      outlog << "\n";
      int nmiss=num_time_proj-mt->second.size();
      if (nmiss==1)
         outlog << "  1 time projector index is MISSING\n";
      else if (nmiss>1)
         outlog << "  "<<nmiss<<" time projector indices are MISSING\n";
      for (set<int>::const_iterator st=mt->second.begin();st!=mt->second.end();++st)
         checkCompletion(*nt,*st,outlog);}}
}


void QuarkChecker::checkCompletion(const LaphNoiseInfo& noise, 
                                   int time_proj_index, TextFileWriter& outlog)
{
 if (!isInfoSet()){
    QDPIO::cerr << "cannot checkCompletion in QuarkChecker until info set"<<endl;
    QDP_abort(1);}
 if (!dilHandler->isValidTimeProjectorIndex(time_proj_index)) return;

 int Nspin = QDP::Ns;
 int Textent = uPtr->getTimeExtent();
 int ndil=dilHandler->getNumberOfSpinEigvecProjectors();
 QuarkHandler::FileKey fkey(noise,time_proj_index);

   DataGetHandlerMF<QuarkChecker,QuarkHandler::FileKey,
       QuarkHandler::RecordKey,QuarkHandler::DataType>* FDH=DHgetPtr;

 if (!FDH->queryFile(fkey)){
    outlog << "\n MISSING:  LaphNoiseInfo seed= "<<noise.getSeed()
           << " time proj index = "<<time_proj_index<<"\n";
    outlog <<"    All dilution indices\n\n";
    return;}

 bool firsttime=true;
 for (int dil=0;dil<ndil;dil++){
    bool fail=false;
    for (int t=0;t<Textent;t++)
    for (int s=1;s<=Nspin;s++){
       if (!FDH->queryData(fkey,QuarkHandler::RecordKey(s,t,dil))) fail=true;}
    if (fail){
       if (firsttime){
          outlog << "\n MISSING:  LaphNoiseInfo seed= "<<noise.getSeed()
                 << " time proj index = "<<time_proj_index<<"\n";
          firsttime=false;}
       outlog << "    Dilution index: "<<dil<<" \n";}}
 if (!firsttime) outlog << "\n\n";
}



  // **************************************************************
  //
  //   The class below is used for changing the LapH eigenvector
  //   dilution scheme.  We use LI8 for mesons, but LI4 for baryons.
  //   This converts from LI8 -> LI4.
  // 
  // **************************************************************


QuarkUndiluter::QuarkUndiluter(const GaugeConfigurationInfo& gaugeinfo,
                               const GluonSmearingInfo& gluonsmear,
                               const QuarkSmearingInfo& quarksmear,
                               const DilutionSchemeInfo& dil,
                               const QuarkActionInfo& quark,
                               const FileListInfo& inflist,
                               const FileListInfo& outflist)
{
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    qSmearPtr = new QuarkSmearingInfo(quarksmear);
    dilPtrIn = new DilutionSchemeInfo(dil);
    dilPtrOut = new DilutionSchemeInfo(dil);
    dilPtrOut->undilute();
    qactionPtr = new QuarkActionInfo(quark);
    fPtrIn = new FileListInfo(inflist);
    fPtrOut = new FileListInfo(outflist);
    DHgetPtr=new DataGetHandlerMF<QuarkUndiluter,QuarkHandler::FileKey,
                     QuarkHandler::RecordKey,QuarkHandler::DataType>(
                    *this,*fPtrIn,"Laph--QuarkSink","QuarkHandlerDataFile");
    DHputPtr=new DataPutHandlerMF<QuarkUndiluter,QuarkHandler::FileKey,
                     QuarkHandler::RecordKey,QuarkHandler::DataType>(
                    *this,*fPtrOut,"Laph--QuarkSink","QuarkHandlerDataFile");
    indilHandler=new DilutionHandler(*dilPtrIn,*uPtr,*qSmearPtr);
    outdilHandler=new DilutionHandler(*dilPtrOut,*uPtr,*qSmearPtr);
    }
 catch(...){
    QDPIO::cerr << "allocation problem in QuarkUndiluter"<<endl;
    QDP_abort(1);}
}


QuarkUndiluter::~QuarkUndiluter()
{
 clear();
}


void QuarkUndiluter::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete qSmearPtr;
    delete dilPtrIn;
    delete dilPtrOut;
    delete qactionPtr;
    delete fPtrIn;
    delete fPtrOut;
    delete DHgetPtr;
    delete DHputPtr;
    delete indilHandler;
    delete outdilHandler;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 qSmearPtr=0;
 dilPtrIn=0;
 dilPtrOut=0;
 qactionPtr=0;
 fPtrIn=0;
 fPtrOut=0;
 DHgetPtr=0;
 DHputPtr=0;
 indilHandler=0;
 outdilHandler=0;
}



bool QuarkUndiluter::checkHeader(XmlReader& xml_in, int suffix)
{
 if (xml_tag_count(xml_in,"QuarkHandlerDataFile")!=1) return false;
 XmlReader xmlr(xml_in,"./descendant-or-self::QuarkHandlerDataFile");
 GaugeConfigurationInfo gauge_check(xmlr);
 GluonSmearingInfo gsmear_check(xmlr);
 QuarkSmearingInfo qsmear_check(xmlr);
 DilutionSchemeInfo dil_check(xmlr);
 QuarkActionInfo qaction_check(xmlr);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    qSmearPtr->checkEqual(qsmear_check);
    dilPtrIn->checkEqual(dil_check);
    qactionPtr->checkEqual(qaction_check); }
 catch(...){ return false;}
 return true;
}

void QuarkUndiluter::writeHeader(XmlWriter& xmlout, 
                                 const QuarkHandler::FileKey& fkey,
                                 int suffix)
{
 push(xmlout, "QuarkHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 qSmearPtr->output(xmlout);
 dilPtrOut->output(xmlout);
 qactionPtr->output(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}

int QuarkUndiluter::getInSuffix(const QuarkHandler::FileKey& fkey) const
{
 std::map<int,QuarkHandler::FileKey> suffices=DHgetPtr->getSuffixMap();
 for (std::map<int,QuarkHandler::FileKey>::const_iterator it=suffices.begin();
      it!=suffices.end();++it)
    if (it->second == fkey) return it->first;
 QDPIO::cerr << " Could not get in suffix"<<endl;
 throw(string("error"));
 return -1;
}

int QuarkUndiluter::getOutSuffix(const QuarkHandler::FileKey& fkey) const
{
 std::map<int,QuarkHandler::FileKey> suffices=DHputPtr->getSuffixMap();
 for (std::map<int,QuarkHandler::FileKey>::const_iterator it=suffices.begin();
      it!=suffices.end();++it)
    if (it->second == fkey) return it->first;
 QDPIO::cerr << " Could not get in suffix"<<endl;
 throw(string("error"));
 return -1;
}


void QuarkUndiluter::undoDilution()
{

    //  figure out which two older indices are equivalent
    //  to each new spin-eigvec dilution index

 int ndil=outdilHandler->getNumberOfSpinEigvecProjectors();
 int old_ndil=indilHandler->getNumberOfSpinEigvecProjectors();
 
 QDPIO::cout <<endl<<"Performing undilution of quark sinks"<<endl<<endl;
 QDPIO::cout << "Original number of spin-lapheigv dilution indices = "<<old_ndil<<endl;
 QDPIO::cout << "     New number of spin-lapheigv dilution indices = "<<ndil<<endl<<endl;

 vector<set<int> > oldind_to_sum(ndil);
 for (int odil=0;odil<old_ndil;++odil){
    int spin=(indilHandler->getOnSpinIndices(odil)).front();
    int eigv=(indilHandler->getOnEigvecIndices(odil)).front();
    for (int dil=0;dil<ndil;++dil){
       if ((outdilHandler->isOnSpin(dil,spin))&&(outdilHandler->isOnEigvec(dil,eigv))){
          oldind_to_sum[dil].insert(odil);
          break;}}}

  //  do sum checks
 {bool flag=true;
 set<int> checker;  
 for (int dil=0;dil<ndil;++dil){
    if (oldind_to_sum[dil].size()!=2)
       flag=false;
    else{
       int od1=*(oldind_to_sum[dil].begin());
       int od2=*(++(oldind_to_sum[dil].begin()));
       QDPIO::cout << "new dilution index = "<<dil
                   <<" contains old dilution indices "<<od1<<" "<<od2<<endl;
       checker.insert(od1);
       checker.insert(od2);}}
 if (int(checker.size())!=old_ndil) flag=false;
 if (!flag){
    QDPIO::cerr << "Determination of dilution index mapping failed"<<endl;
    throw(string("Bad mapping"));}}
      
 int Textent = uPtr->getTimeExtent();
 int Nspin = QDP::Ns;
 QuarkHandler::DataType coefs1,coefs2;
 int neigs=qSmearPtr->getNumberOfLaplacianEigenvectors();
 QDPIO::cout << "Number of Laplacian eigenvectors = "<<neigs<<endl<<endl;

 set<QuarkHandler::FileKey> infkeys=DHgetPtr->getFileKeys();
 QDPIO::cout << endl<<"Number of files = "<<infkeys.size()<<endl;
 for (set<QuarkHandler::FileKey>::const_iterator it=infkeys.begin();it!=infkeys.end();++it){
    DHputPtr->open(*it);
    QDPIO::cout << "Undiluting file with suffix "<<getInSuffix(*it);
    QDPIO::cout << "  Output suffix is "<<getOutSuffix(*it);
    int count=0;
    for (int dil=0;dil<ndil;dil++){
       set<int>::const_iterator dt=oldind_to_sum[dil].begin();
       int dil1=*dt; ++dt;
       int dil2=*dt;
       for (int t=0;t<Textent;t++)
       for (int s=1;s<=Nspin;s++){
//          QDPIO::cout << "dil = "<<dil<<" spin = "<<s<<" time = "<<t;
          QuarkHandler::RecordKey rkeyout(s,t,dil);
          QuarkHandler::RecordKey rkey1(s,t,dil1);
          QuarkHandler::RecordKey rkey2(s,t,dil2);
          try{
             if ((!DHgetPtr->queryData(*it,rkey1))||(!DHgetPtr->queryData(*it,rkey2))){
               QDPIO::cerr << "requested coefficients could not be found: dil = "<<dil<<endl;
               throw(string("error"));}
             coefs1=DHgetPtr->getData(*it,rkey1);
//             QDPIO::cout << " number of coefs = "<<coefs1.size()<<endl;
             coefs2=DHgetPtr->getData(*it,rkey2);
             if ((coefs1.size()!=neigs)||(coefs2.size()!=neigs)) throw(string("error"));
             coefs1+=coefs2;
             DHputPtr->putData(rkeyout,coefs1);
             ++count;
             DHgetPtr->removeData(*it,rkey1);
             DHgetPtr->removeData(*it,rkey2);}
          catch(...){
             QDPIO::cerr << "undilution for dil = "<<dil<<" failed"<<endl;}}}
    QDPIO::cout << "  Number of records =  "<<count<<endl;
    DHgetPtr->clearData();
    DHputPtr->close();}
 QDPIO::cout <<endl<<endl<<"Undilution of quark sinks done."<<endl<<endl;
}


// ***************************************************************
  }
}
 
