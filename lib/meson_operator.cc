#include "meson_operator.h"
#include "xml_help.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {



void MesonOperator::initialize(const string& mesonCoefsTopDirectory)
{
 coefsTopDirectory=tidyString(mesonCoefsTopDirectory);
 if (*coefsTopDirectory.rbegin()!='/') coefsTopDirectory+="/";
 string checkfile=coefsTopDirectory+"isovector_du/mom_ray_000/A1um_1/SS_0";

 if (fileExists(checkfile)){
    coefsFlag=true;}
 else{
    QDPIO::cerr << "Meson coefficient top directory not found"<<endl;
    QDP_abort(1);}
}
  


MesonOperator::MesonOperator(const MesonOperatorInfo& bop_info)
               : m_info(bop_info)
{
 if (!coefsFlag){
    QDPIO::cerr << "Must call MesonOperator::Initialize(...) before creating meson objects"<<endl;
    QDP_abort(1);}

    // first, determine the momentum "ray"

 string momRay;
 if (!getMomentumRay(m_info.getXMomentum(),
                     m_info.getYMomentum(),
                     m_info.getZMomentum(), momRay)){
//    QDPIO::cerr << "Meson momentum is not part of allowed ray"<<endl;
//    QDP_abort(1);}
    momRay="mom_ray_spc";}

    // now look for the meson operator in the XML coefficient file

 ostringstream oss;
 oss << coefsTopDirectory << m_info.getIsospinName() 
     << "_" << m_info.getFlavor() << "/" << momRay 
     << "/" << m_info.getIrrep() << "_" << m_info.getIrrepRow()
     << "/" << m_info.getSpatialType() << "_" 
     << m_info.getSpatialIdNumber();
 string coefs_file=oss.str();

 if (!fileExists(coefs_file)){
    QDPIO::cerr << "Invalid meson operator requested"<<endl;
    QDPIO::cerr << "Coefficient file requested was "<<coefs_file<<endl;
    QDP_abort(1);}

 TextFileReader in(coefs_file);

 int nterms;
 Double re,im;
 int s,sbar,d1,d2,dbar;
 in >> nterms;
 for (int k=0;k<nterms;k++){
    in >> sbar >> s >> dbar >> d1 >> d2 >> re >> im;
    terms.push_back(ElementalTerm(Elemental(sbar,dbar,s,d1,d2),
                    cmplx(re,im)));}

 if (in.fail()){
    in.close();
    QDPIO::cerr << "error occurred during meson operator read"<<endl;
    QDP_abort(1);}

 in.close();
}


string MesonOperator::output() const
{
 ostringstream oss;
 oss << "<MesonOperatorDetails>"<<endl;
 oss << m_info.output()<<endl;
 oss << "  <Projection>"<<endl;
 for (MesonOperator::ElementalIterator it=terms.begin();it!=terms.end();it++){
    oss << "     <Term> "<<it->el.output()
           << " coef=cmplx"<<it->coef<<" </Term>"<<endl;
    }
 oss << "  </Projection>"<<endl;
 oss << "</MesonOperatorDetails>"<<endl;
 return oss.str();
}

    //  static data members initialization

string MesonOperator::coefsTopDirectory;
bool MesonOperator::coefsFlag=false;


// **************************************************************
  }
}
