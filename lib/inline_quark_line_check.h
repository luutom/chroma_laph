#ifndef __INLINE_QUARK_LINE_CHECK_H__
#define __INLINE_QUARK_LINE_CHECK_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "quark_handler.h"


// ******************************************************************
// *                                                                *
// * Driver inline measurement that checks that the requested quark *
// * line ends have been computed.  Can be run in either 3D or 4D   *
// * chroma_laph.  XML input must have the form:                    *
// *                                                                *
// *  <chroma>                                                      *
// *   <RNG><Seed> ... </Seed></RNG>                                *
// *   <Param>                                                      *
// *    <nrow>12 12 12 96</nrow>   # lattice size Nx Ny Nz Nt       *
// *    <InlineMeasurements>                                        *
// *    <elem>                                                      *
// *                                                                *
// *     <Name> LAPH_QUARK_LINE_CHECK </Name>                       *
// *     <QuarkLineEndInfo>                                         *
// *       <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>   *
// *       <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>   *
// *       <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>     *
// *       <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>       *
// *       <LaphDilutionScheme> ... </LaphDilutionScheme>           *
// *       <QuarkActionInfo> ... </QuarkActionInfo>                 *
// *       <FileListInfo> ... </FileListInfo>                       *
// *     </QuarkLineEndInfo>                                        *
// *                                                                *
// *     <LogFile> ... </LogFile>                                   *
// *                                                                *
// *     <LaphNoiseList>       (optional)                           *
// *        <LaphNoiseInfo> ... </LaphNoiseInfo>                    *
// *       ... (other LaphNoiseInfo tags)                           *
// *     </LaphNoiseList>                                           *
// *                                                                *
// *     <TimeProjIndexList>        (optional)                      *
// *        <Values>3 5 9</Values>                                  *
// *     </TimeProjIndexList>                                       *
// *                                                                *
// *     <TimeProjNumber>4</TimeProjNumber> (optional)              *
// *                                                                *
// *    </elem>                                                     *
// *    </InlineMeasurements>                                       *
// *   </Param>                                                     *
// *  </chroma>                                                     *
// *                                                                *
// *                                                                *
// *  Header information must match.  Search results are dumped     *
// *  to the file specified in the <LogFile> tag.  The suffix       *
// *  map is always output to the log file.                         *
// *                                                                *
// *  If noises are specified in <LaphNoiseList> and time projector *
// *  indices are specified in  <TimeProjIndexList>, then the       *
// *  routine checks that these noises and time projector indices   *
// *  are present, otherwise a "MISSING" string is output to the    *
// *  log file.  If <TimeProjNumber> is specified instead, then the *
// *  routine checks that at least this number of time projectors   *
// *  are available for each noise listed.                          *
// *                                                                *
// ******************************************************************


namespace Chroma {
                  
  namespace InlineStochCheckQuarkEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochCheckQuarkInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below

   set<LaphEnv::LaphNoiseInfo> noises;
   set<int> timeProjIndices;
   int NTimeProjIndices; 

 public:

   StochCheckQuarkInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochCheckQuarkInlineMeas() {}
      
   void setNoisesTimeProjectors();

      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}
   
};
	

// ***********************************************************
  }
}

#endif
