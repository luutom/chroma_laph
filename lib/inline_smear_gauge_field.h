#ifndef __INLINE_SMEAR_GAUGE_FIELD_H__
#define __INLINE_SMEAR_GAUGE_FIELD_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "field_smearing_handler.h"
#include "xml_help.h"

// ******************************************************************
// *                                                                *
// * Driver inline measurement that stout smears the gauge field    *
// * in 4D chroma_laph, and outputs the results to file in terms    *
// * of time slices readable by 3D chroma_laph.  XML input must     *
// * have the form:                                                 *
// *                                                                *
// *  <chroma>                                                      *
// *   <RNG><Seed> ... </Seed></RNG>                                *
// *   <Cfg> ... </Cfg>                                             *
// *   <Param>                                                      *
// *    <nrow>12 12 12 96</nrow>   # lattice size Nx Ny Nz Nt       *
// *    <InlineMeasurements>                                        *
// *    <elem>                                                      *
// *                                                                *
// *     <Name>SMEAR_GAUGE_FIELD_TIMESLICES</Name>                  *
// *       <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>   *
// *       <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>   *
// *       <SmearedGaugeFileName> ... </SmearedGaugeFileName>       *
// *                                                                *
// *    </elem>                                                     *
// *    </InlineMeasurements>                                       *
// *   </Param>                                                     *
// *  </chroma>                                                     *
// *                                                                *
// ******************************************************************


namespace Chroma { 

#if (QDP_ND == 4)

  namespace InlineSmearGaugeFieldEnv {

 // **************************************************************

extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class SmearGaugeFieldInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                       // measurement, for use by the operator()
                       // member below

 public:

   SmearGaugeFieldInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~SmearGaugeFieldInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}
   
};
	

// ***********************************************************
  }

#endif
}

#endif
