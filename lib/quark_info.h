#ifndef QUARK_INFO_H
#define QUARK_INFO_H

#include "chromabase.h"
#include "xml_help.h"
#include "gauge_configuration_info.h"
#include "dilution_scheme_info.h"
#include "quark_action_info.h"
#include "field_smearing_info.h"

namespace Chroma {
  namespace LaphEnv {

// **********************************************************************
// *                                                                    *
// *  Class "QuarkInfo" holds information about a quark inside of a     *
// *  hadron.  In particular, it stores                                 *
// *                                                                    *
// *    (a) information about the action of the quark                   *
// *    (b) the quark smearing scheme used                              * 
// *    (c) the dilution scheme info for fixed-time sources             *
// *    (d) the dilution scheme info for relative-time sources          *
// *                                                                    *
// *  This class can also check that two objects of this class are      *
// *  the same or not.  The XML input must have the form                *
// *                                                                    *
// *    <QuarkInfo>                                                     *
// *       <QuarkActionInfo> ... </QuarkActionInfo>                     *
// *       <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>         *
// *       <FixedSource>                                                *
// *          <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *       </FixedSource>                                               *
// *       <RelativeSource>                                             *
// *          <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *       </RelativeSource>                                            *
// *    </QuarkInfo>                                                    *
// *                                                                    *
// *   The "RelativeSource" tag is optional.  If absent, the relative   *
// *   dilution scheme is set to "None".                                *
// *                                                                    *
// *                                                                    *
// *  Typical usage:                                                    *
// *                                                                    *
// *    XmlReader xmlin(....);                                          *
// *    QuarkInfo Q1(xmlin);                                            *
// *    QuarkActionInfo qinfo = Q1.getAction();                         *
// *    QuarkSmearingInfo qsmear = Q1.getSmearing();                    *
// *    DilutionSchemeInfo dil1 = Q1.getFixedSourceDilutionScheme();    *
// *    DilutionSchemeInfo dil2 = Q1.getRelativeSourceDilutionScheme(); *
// *                                                                    *
// *    QuarkInfo Q2(xmlin2);                                           *
// *    Q2.checkEqual(Q1);  // checks Q1==Q2, throw exception if not    *                                       *
// *    if (Q1==Q2) ...                                                 *
// *                                                                    *
// *    string sval = Q1.output();   // xml output                      *
// *    string sval = Q1.output(2);  // xml indented output             *
// *                                                                    *
// **********************************************************************                            


class QuarkInfo
{

   QuarkActionInfo *m_action;
   QuarkSmearingInfo *m_smearing;
   DilutionSchemeInfo* m_fixed_dilscheme;
   DilutionSchemeInfo* m_rel_dilscheme;

 public:

   QuarkInfo(XmlReader& xml_in);

   QuarkInfo(XmlReader& xml_in, const GaugeConfigurationInfo& gaugeinfo);

   QuarkInfo(const QuarkInfo& rhs);

   QuarkInfo& operator=(const QuarkInfo& rhs);

   ~QuarkInfo();


   void checkEqual(const QuarkInfo& rhs) const;

   bool operator==(const QuarkInfo& rhs) const;



   std::string output(int indent = 0) const;

   void output(XmlWriter& xmlout) const;


   std::string getHeader() const { return output(0); }

   void getHeader(XmlWriter& xmlout) const { output(xmlout); }



   const DilutionSchemeInfo& getFixedSourceDilutionScheme() const { return *m_fixed_dilscheme; }

   const DilutionSchemeInfo& getRelativeSourceDilutionScheme() const { return *m_rel_dilscheme; }

   const QuarkActionInfo& getAction() const {return *m_action;}

   const QuarkSmearingInfo& getSmearing() const {return *m_smearing;}


};


// ****************************************************************
  }
}
#endif
