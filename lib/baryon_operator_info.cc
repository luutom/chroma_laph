#include "baryon_operator_info.h"
#include "xml_help.h"
#include "multi_compare.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {



  // "current" element of xml_in should be <BaryonOp>

  //  encode operator information into two unsigned 32-bit integers
  //   icode1:
  //      put all integers (except spatialIdNum) into a single unsigned integer 
  //         5 bits irrep row, 7 bits each momx, momy, momz
  //         (first bit is sign, 6 bits remaining), 6 bits displacement length
  //   icode2:
  //      spatial-index,isospin,flavor,irrep,spatial-type

BaryonOperatorInfo::BaryonOperatorInfo(XmlReader& xml_in)
{
 xml_tag_assert(xml_in,"BaryonOp","BaryonOperatorInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::BaryonOp");

 string isospinName,flavor,irrep,spatialType;
 int momx,momy,momz,irrepRow,spatialIdNum,dispLength;

 xmlread(xmlr, "IsospinName", isospinName, "BaryonOperatorInfo");
 isospinName=tidyString(isospinName);

 xmlread(xmlr, "Flavor", flavor, "BaryonOperatorInfo");
 flavor=tidyString(flavor);

 multi1d<int> p;
 xmlread(xmlr, "Momentum", p, "BaryonOperatorInfo");
 if (p.size()!=3){
    xml_cerr(xml_in,"Bad baryon momentum");
    xmlreadfail(xml_in,"BaryonOperatorInfo");}
 momx=p[0];
 momy=p[1];
 momz=p[2];

 xmlread(xmlr, "Irrep", irrep, "BaryonOperatorInfo");
 irrep=tidyString(irrep);
 xmlread(xmlr, "SpatialType", spatialType, "BaryonOperatorInfo");
 spatialType=tidyString(spatialType);
 xmlread(xmlr, "IrrepRow", irrepRow, "BaryonOperatorInfo");
 xmlread(xmlr, "SpatialIdNum", spatialIdNum, "BaryonOperatorInfo");
 xmlread(xmlr, "DispLength", dispLength, "BaryonOperatorInfo");

 if ((dispLength<0)||(spatialIdNum<0)||(irrepRow<1)){
    xml_cerr(xml_in,"Bad baryon operator input xml data");
    xmlreadfail(xml_in,"BaryonOperatorInfo");}
 if (irrepRow>31){
    xml_cerr(xml_in,"Irrep row > 31 not currently supported");
    QDP_abort(1);}
 if (dispLength>63){
    xml_cerr(xml_in,"Displacement length > 63 not currently supported");
    QDP_abort(1);}
 if ((abs(momx)>63)||(abs(momy)>63)||(abs(momz)>63)){
    xml_cerr(xml_in,"momentum component magnitude > 63 not currently supported");
    QDP_abort(1);}

 if ((spatialType!="SS")&&(dispLength==0)){
    xml_cerr(xml_in,"Bad baryon operator input xml data");
    xml_cerr(xml_in,"Operator with displacement must have displacement length > 0");
    xmlreadfail(xml_in,"BaryonOperatorInfo");}

    // check if a single-site operator, then make the displacement length zero
 if (spatialType=="SS") dispLength=0;

    //  now do the encoding
 icode1=irrepRow; 
 icode1<<=1; icode1|=(momx<0)?1:0; 
 icode1<<=6; icode1|=abs(momx);
 icode1<<=1; icode1|=(momy<0)?1:0;
 icode1<<=6; icode1|=abs(momy);
 icode1<<=1; icode1|=(momz<0)?1:0;
 icode1<<=6; icode1|=abs(momz);
 icode1<<=6; icode1|=dispLength;

 unsigned int iso,fl,ir1,ir2,ir3,st;
 
 if      (isospinName=="nucleon") iso=0;
 else if (isospinName=="delta")   iso=1;
 else if (isospinName=="lambda")  iso=2;
 else if (isospinName=="sigma")   iso=3;
 else if (isospinName=="xi")      iso=4;
 else if (isospinName=="omega")   iso=5;
 else{
    xml_cerr(xml_in,"isospin "+isospinName+" not currently supported in BaryonOperatorInfo");
    QDP_abort(1);}

 if      (flavor=="uud") fl=0;
 else if (flavor=="uds") fl=1;
 else if (flavor=="ssu") fl=2;
 else if (flavor=="sss") fl=3;
 else{
    xml_cerr(xml_in,"flavor "+flavor+" not currently supported in BaryonOperatorInfo");
    QDP_abort(1);}
 
 if      (spatialType=="SS" ) st=0;
 else if (spatialType=="SD" ) st=1;
 else if (spatialType=="DDI") st=2;
 else if (spatialType=="DDL") st=3;
 else if (spatialType=="LSD") st=4;
 else if (spatialType=="TDT") st=5;
 else if (spatialType=="TDO") st=6;
 else{
    xml_cerr(xml_in,"spatialType "+spatialType+" not currently supported in BaryonOperatorInfo");
    QDP_abort(1);}

 string irp=irrep+"   ";  // so length at least 3 char
 if      (irp[0]=='F') ir1=0;
 else if (irp[0]=='G') ir1=1;
 else if (irp[0]=='H') ir1=2;
 else{
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in BaryonOperatorInfo");
    QDP_abort(1);}

 int k=1;
 if (irp[k]=='1'){ ir2=0; k++;}
 else if (irp[k]=='2'){ ir2=1; k++;}
 else ir2=2;
 
 if (irp[k]=='g'){ ir3=0; k++;}
 else if (irp[k]=='u'){ ir3=1; k++;}
 else ir3=2;
 
 if (k!=irrep.length()){
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in BaryonOperatorInfo");
    QDP_abort(1);}
 
 icode2=spatialIdNum;
 icode2<<=3; icode2|=iso; 
 icode2<<=2; icode2|=fl;
 icode2<<=2; icode2|=ir1; 
 icode2<<=2; icode2|=ir2;
 icode2<<=2; icode2|=ir3;
 icode2<<=3; icode2|=st;

}


std::string BaryonOperatorInfo::getIsospinName() const
{
 unsigned int iso=(icode2>>11)& 0x7u;
 if      (iso==0) return string("nucleon");
 else if (iso==1) return string("delta");
 else if (iso==2) return string("lambda");
 else if (iso==3) return string("sigma");
 else if (iso==4) return string("xi"); 
 else if (iso==5) return string("omega");
 return string("");
}
 
std::string BaryonOperatorInfo::getFlavor() const
{
 unsigned int fl=(icode2>>9)& 0x3u;
 if      (fl==0) return string("uud");
 else if (fl==1) return string("uds");
 else if (fl==2) return string("ssu");
 else if (fl==3) return string("sss");
 return string("");
}

Momentum BaryonOperatorInfo::getMomentum() const
{
 unsigned int tmp=(icode1>>6);
 int pz=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) pz=-pz;
 tmp>>=7;
 int py=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) py=-py;
 tmp>>=7;
 int px=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) px=-px;
 return Momentum(px,py,pz);
}

int BaryonOperatorInfo::getXMomentum() const
{
 unsigned int tmp=(icode1>>20);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}
  
int BaryonOperatorInfo::getYMomentum() const
{
 unsigned int tmp=(icode1>>13);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}

int BaryonOperatorInfo::getZMomentum() const
{
 unsigned int tmp=(icode1>>6);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}

std::string BaryonOperatorInfo::getIrrep() const
{
 unsigned int irp=icode2>>3;
 unsigned int ir3=irp & 0x3u; irp>>=2;
 unsigned int ir2=irp & 0x3u; irp>>=2;
 unsigned int ir1=irp & 0x3u; irp>>=2;
 string irrep;
 if      (ir1==0) irrep+='F';
 else if (ir1==1) irrep+='G';
 else if (ir1==2) irrep+='H';
 if      (ir2==0) irrep+='1';
 else if (ir2==1) irrep+='2';
 if      (ir3==0) irrep+='g';
 else if (ir3==1) irrep+='u'; 
 return irrep;
}

int BaryonOperatorInfo::getIrrepRow() const
{
 return (icode1>>27);
}

std::string BaryonOperatorInfo::getSpatialType() const
{
 unsigned int st= icode2 & 0x7u;
 if      (st==0) return string("SS" );
 else if (st==1) return string("SD" );
 else if (st==2) return string("DDI");
 else if (st==3) return string("DDL");
 else if (st==4) return string("LSD");
 else if (st==5) return string("TDT");
 else if (st==6) return string("TDO");
 return string("");
}

int BaryonOperatorInfo::getSpatialIdNumber() const
{
 return (icode2>>14);
}

int BaryonOperatorInfo::getDisplacementLength() const
{
 return icode1 & 0x3F;
}


bool BaryonOperatorInfo::operator==(const BaryonOperatorInfo& rhs) const
{
 return multiEqual(icode1,rhs.icode1, icode2,rhs.icode2);   
}

bool BaryonOperatorInfo::operator!=(const BaryonOperatorInfo& rhs) const
{
 return multiNotEqual(icode1,rhs.icode1, icode2,rhs.icode2);   
}

bool BaryonOperatorInfo::operator<(const BaryonOperatorInfo& rhs) const
{
 return multiLessThan(icode1,rhs.icode1, icode2,rhs.icode2);   
}


string BaryonOperatorInfo::output() const
{
 ostringstream oss;
 oss << "<BaryonOp>"<<endl;
 oss << "  <IsospinName> " << getIsospinName() << " </IsospinName>"<<endl;
 oss << "  <Flavor> "<< getFlavor()<<" </Flavor>"<<endl;
 oss << "  <Momentum> " <<getXMomentum()<<" "<<getYMomentum()
      <<" "<<getZMomentum()<<" </Momentum>"<<endl;
 oss << "  <Irrep> "<<getIrrep()<<" </Irrep>"<<endl;
 oss << "  <IrrepRow> "<<getIrrepRow()<<" </IrrepRow>"<<endl;
 oss << "  <SpatialType> "<<getSpatialType()<<" </SpatialType>"<<endl;
 oss << "  <SpatialIdNum> "<<getSpatialIdNumber()
      <<" </SpatialIdNum>"<<endl;
 oss << "  <DispLength> "<<getDisplacementLength()
      <<" </DispLength>"<<endl;
 oss << "</BaryonOp>"<<endl;

 return oss.str();
}

void BaryonOperatorInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"BaryonOp");
 write(xmlout,"IsospinName",getIsospinName());
 write(xmlout,"Flavor",getFlavor());
 multi1d<int> p(3);  p[0]=getXMomentum(); 
 p[1]=getYMomentum(); p[2]=getZMomentum();
 write(xmlout,"Momentum",p);
 write(xmlout,"Irrep",getIrrep());
 write(xmlout,"IrrepRow",getIrrepRow());
 write(xmlout,"SpatialType",getSpatialType());
 write(xmlout,"SpatialIdNum",getSpatialIdNumber());
 write(xmlout,"DispLength",getDisplacementLength());
 pop(xmlout);
}


 // ********************************************************

   //  useful routine for reading multiple BaryonOperatorInfo;
   //  also reads name of coefficient file

void readBaryonOperators(XmlReader& xml_in, 
                         set<BaryonOperatorInfo>& bops,
                         string& coef_dir)
{
 if (xml_tag_count(xml_in,"BaryonOperators")!=1){
    xml_cerr(xml_in,"XML input to CreateBaryonOperators must have");
    xml_cerr(xml_in," a single <BaryonOperators> ... </BaryonOperators>");
    throw(string("error"));}
 bops.clear();

 xmlread(xml_in,"BaryonOperators/ElementalCoefficientDirectory",coef_dir,
         "readBaryonOperators");
 coef_dir=tidyString(coef_dir);

 int num_bop=xml_tag_count(xml_in,"BaryonOperators/BaryonOp");
 for (int k=1;k<=num_bop;k++){
    ostringstream path;
    path << "./descendant-or-self::BaryonOperators/BaryonOp["<<k<<"]";
    XmlReader xml_op(xml_in,path.str());
    bops.insert(BaryonOperatorInfo(xml_op));}
}

// **************************************************************
  }
}
