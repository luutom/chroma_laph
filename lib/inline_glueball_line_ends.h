#ifndef __INLINE_GLUEBALL_LINE_ENDS_H__
#define __INLINE_GLUEBALL_LINE_ENDS_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "glueball_handler.h"

// ***********************************************************************
// *                                                                     *
// *  Driver inline measurement that constructs the glueball line ends   *
// *  (source/sinks).  Assumes that the smeared gauge field              *
// *  time slices and the Laph eigenvectors are available.  Must be run  *
// *  in 3D chroma_laph.  XML input must have the form:                  *
// *                                                                     *
// *  <chroma>                                                           *
// *   <RNG><Seed> ... </Seed></RNG>                                     *
// *   <Param>                                                           *
// *    <nrow>12 12 12</nrow>   # lattice size Nx Ny Nz                  *
// *    <InlineMeasurements>                                             *
// *    <elem>                                                           *
// *                                                                     *
// *     <Name> LAPH_GLUEBALL_LINE_ENDS </Name>                          *
// *                                                                     *
// *     <GlueballCommonInfo>        ...   </GlueballCommonInfo>         *
// *     <GlueballOperators>         ...   </GlueballOperators>          *
// *     <JobName> ... </JobName>  (optional)                            *
// *                                                                     *
// *    </elem>                                                          *
// *    </InlineMeasurements>                                            *
// *   </Param>                                                          *
// *  </chroma>                                                          *
// *                                                                     *
// *  The details in the major XML tags above are described below.       *
// *                                                                     *
// *  (1) Major tag <GlueballCommonInfo>:                                *
// *                                                                     *
// *     <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>          *
// *     <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>          *
// *     <GlueballFileList>   ...  </GlueballFileList>                   *
// *     <SmearedGaugeFileName> ... </SmearedGaugeFileName>              *
// *     <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>              *
// *                                                                     *
// *                                                                     *
// *  (2) Major tag <GlueballOperators>:                                 *
// *                                                                     *
// *     <GlueballOperators>                                             *
// *       <GlueballOp>                                                  *
// *         ...                                                         *
// *       </GlueballOp>                                                 *
// *       <GlueballOp>                                                  *
// *         ...                                                         *
// *       </GlueballOp>                                                 *
// *         ...                                                         *
// *     </GlueballOperators>                                            *
// *                                                                     *
// *                                                                     *
// ***********************************************************************


namespace Chroma { 

#if (QDP_ND == 3)

  namespace InlineStochLaphGlueballEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochLaphGlueballInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below


 public:

   StochLaphGlueballInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochLaphGlueballInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}

   
};
	

// ***********************************************************
  }
#endif
}

#endif
