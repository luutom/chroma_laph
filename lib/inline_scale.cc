#include "inline_scale.h"
#include "xml_help.h"
#include "chroma.h"


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 3)

  namespace InlineScaleLaphEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "LaphScaleInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new LaphScaleInlineMeas(xml_in, path);
}

    //  The name of this inline measurement.   This is the name 
    //  with which the createMeasurement function is associated in the 
    //  Object Factory. You must include this name in the XML input 
    //  to Chroma through
    //     <InlineMeasurements>
    //        <elem>
    //            <Name> LAPH_SCALE </Name>
    //             ...
    //        </elem>
    //    </InlineMeasurements>

const std::string name = "LAPH_SCALE";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}

   
// *********************************************************************
   
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void LaphScaleInlineMeas::operator()(unsigned long update_no,
                                     XMLWriter& xmlout) 
{
 XmlReader xml_rdr(xml_rd);
 calcPionCorrelator(xml_rdr);
 calcOmegaBaryonCorrelator(xml_rdr);
 calcNucleonCorrelator(xml_rdr);
}

// ******************************************************************


void LaphScaleInlineMeas::pickMesonNoises(const set<LaphNoiseInfo>& noises,
                    vector<MesonHandler::MesonNoiseInfo>& choices)
{
 const int num_noises=8;
 srand ( time(NULL) );
 choices.clear();
 for (set<LaphNoiseInfo>::const_iterator n1=noises.begin();n1!=noises.end();++n1)
 for (set<LaphNoiseInfo>::const_iterator n2=noises.begin();n2!=noises.end();++n2)
    if (*n1 != *n2){
        choices.push_back(MesonHandler::MesonNoiseInfo(*n1,*n2));}
 random_shuffle(choices.begin(),choices.end());
 while (choices.size()>num_noises) choices.pop_back();
}
 

void LaphScaleInlineMeas::pickBaryonNoises(const set<LaphNoiseInfo>& noises,
                    vector<BaryonHandler::BaryonNoiseInfo>& choices)
{
 const int num_noises=8;
 srand ( time(NULL) );
 choices.clear();
 for (set<LaphNoiseInfo>::const_iterator n1=noises.begin();n1!=noises.end();++n1)
 for (set<LaphNoiseInfo>::const_iterator n2=noises.begin();n2!=noises.end();++n2)
 for (set<LaphNoiseInfo>::const_iterator n3=noises.begin();n3!=noises.end();++n3)
    if ((*n1 != *n2)&&(*n2 != *n3)&&(*n1 != *n3)){
        choices.push_back(BaryonHandler::BaryonNoiseInfo(*n1,*n2,*n3));}
 random_shuffle(choices.begin(),choices.end());
 while (choices.size()>num_noises) choices.pop_back();
}


// ******************************************************************


         //  gamma_5 pion   (1 3 + 2 4 - 3 1 - 4 2)   chi - psi format
         //  q1 should be set to gamma_5 Hermitian mode, q2 normal mode

void LaphScaleInlineMeas::makePions(
               QuarkHandler& q1, const LaphNoiseInfo& noise1, 
               QuarkHandler& q2, const LaphNoiseInfo& noise2, 
               bool source, int source_time, int time_value, 
               multi2d<Complex>& pion, int ndil)
{
 const LatticeColorVector *left1,*left2,*left3,*left4,
                          *right1,*right2,*right3,*right4;
 int tproj1=q1.getTimeDilutionProjectorIndex(source_time);
 int tproj2=q2.getTimeDilutionProjectorIndex(source_time);

 for (int dil1=0;dil1<ndil;dil1++)
 for (int dil2=0;dil2<ndil;dil2++){

    left1=q1.getData(source,noise1,tproj1,dil1,1,DirPath(),0,time_value);
    left2=q1.getData(source,noise1,tproj1,dil1,2,DirPath(),0,time_value);
    left3=q1.getData(source,noise1,tproj1,dil1,3,DirPath(),0,time_value);
    left4=q1.getData(source,noise1,tproj1,dil1,4,DirPath(),0,time_value);
    right1=q2.getData(source,noise2,tproj2,dil2,1,DirPath(),0,time_value);
    right2=q2.getData(source,noise2,tproj2,dil2,2,DirPath(),0,time_value);
    right3=q2.getData(source,noise2,tproj2,dil2,3,DirPath(),0,time_value);
    right4=q2.getData(source,noise2,tproj2,dil2,4,DirPath(),0,time_value);

    auto_ptr<LatticeComplex> temp;
    evaluateLocalInnerProduct(left1,right3,temp);
    pion(dil1,dil2)=evaluateSum(temp);
    evaluateLocalInnerProduct(left2,right4,temp);
    pion(dil1,dil2)+=evaluateSum(temp);
    evaluateLocalInnerProduct(left3,right1,temp);
    pion(dil1,dil2)-=evaluateSum(temp);
    evaluateLocalInnerProduct(left4,right2,temp);
    pion(dil1,dil2)-=evaluateSum(temp);
    }

 q1.clearData();
 q2.clearData();
}


void LaphScaleInlineMeas::pionCorrelate(const multi2d<Complex>& pion_sink,
                                        const multi2d<Complex>& pion_source,
                                        multi1d<Complex>& pion_corr_avg,
                                        int t, int ndil)
{
 Complex pcor=zero;
         // minus below due to anticommuting of Grassmann fields
 for (int dil1=0;dil1<ndil;dil1++)
    for (int dil2=0;dil2<ndil;dil2++)
       pcor-=pion_sink(dil1,dil2)*conj(pion_source(dil1,dil2));
 pion_corr_avg[t]+=pcor;
}


// ********************************************************************

   //  d-bar u  gamma_5 pion   (1 3 + 2 4 - 3 1 - 4 2)   chi - psi format

void LaphScaleInlineMeas::calcPionCorrelator(XmlReader& xml_in)
{
 if (xml_tag_count(xml_in,"PionCorrelator")==0)
 return;

 QDPIO::cout << endl<<"Now starting PionCorrelator"<<endl<<endl;
 XmlReader xmlr(xml_in,"./PionCorrelator");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 QuarkActionInfo quark(xmlr,gaugeinfo);
 QuarkSmearingInfo qSmear(xmlr);

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "PionCorrelator");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "PionCorrelator");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

 DilutionSchemeInfo dilScheme(xmlr);
 if (!dilScheme.isFullTimeDilution()){
    QDPIO::cerr << "Dilution scheme must include full time dilution"<<endl;
    QDP_abort(1);}

 FileListInfo qfiles(xmlr);

 string outdata;
 xmlread(xmlr,"OutputData",outdata,"PionCorrelator");
 outdata=tidyString(outdata);
 if (outdata.length()==0){
    QDPIO::cerr << "Empty string for output data file; aborting"<<endl;
    QDP_abort(1);}

 int Textent=gaugeinfo.getTimeExtent();
 int tmax,tmin;
 xmlread(xmlr,"MinTimeSeparation",tmin,"PionCorrelator");
 xmlread(xmlr,"MaxTimeSeparation",tmax,"PionCorrelator");
 if ((tmin<0)||(tmin>=Textent/2)){
    QDPIO::cerr << "Bad minimum time separation"<<endl;
    QDP_abort(1);}
 if ((tmax<tmin)||(tmax>Textent/2)){
    QDPIO::cerr << "Bad maximum time separation"<<endl;
    QDP_abort(1);}
 int ntimes=tmax-tmin+1;


 QuarkHandler q(gaugeinfo,gSmear,qSmear,dilScheme,quark,qfiles,
                smeared_quark_filestub,smeared_gauge_file);

 QuarkHandler qbar(gaugeinfo,gSmear,qSmear,dilScheme,quark,qfiles,
                   smeared_quark_filestub,smeared_gauge_file);
 qbar.setGamma5HermiticityMode();

 set<NoiseAndTimeProjector> nt=q.getNoisesAndTimeProjectors();
 set<LaphNoiseInfo> noises;
 set<int> time_projs;
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    time_projs.insert(it->time_proj_index);}
 if (noises.size()<2){
    QDPIO::cerr << "2 or more noises are required"<<endl;
    QDP_abort(1);}

    // randomly pick some noise combinations

 vector<MesonHandler::MesonNoiseInfo> mnoises;
 pickMesonNoises(noises,mnoises);

 int ndil=q.getNumberOfSpinEigvecDilutionProjectors();

 multi1d<Complex> pion_corr_avg(ntimes);
 for (int t=0;t<ntimes;++t){
    pion_corr_avg[t]=zero;}
 int count=0;

      // loop over all available time sources

 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp){

  int tproj=*tp;
  const list<int>& ontimes=q.getOnTimes(tproj);
  int t0=ontimes.front();   // must be full time dilution..checked above

    // loop over noise permutations

  for (vector<MesonHandler::MesonNoiseInfo>::const_iterator mit=mnoises.begin();
       mit!=mnoises.end();++mit){

   multi2d<Complex> pion_source(ndil,ndil);
   multi2d<Complex> pion_sink(ndil,ndil);

         // make the source

   makePions(qbar,mit->noise1,q,mit->noise2,true,t0,t0,pion_source,ndil);

         // make sinks and correlator;  use dt and Textent-dt for increased stats
   for (int t=0;t<ntimes;t++){
       int dt=t+tmin;
       int tval=t0+dt;
       while (tval>=Textent) tval-=Textent;
       makePions(qbar,mit->noise1,q,mit->noise2,false,t0,tval,pion_sink,ndil);
       pionCorrelate(pion_sink,pion_source,pion_corr_avg,t,ndil);
       dt=Textent-t-tmin;
       int tvalr=t0+dt;
       while (tvalr>=Textent) tvalr-=Textent;
       if (tvalr!=tval)
          makePions(qbar,mit->noise1,q,mit->noise2,false,t0,tvalr,pion_sink,ndil);
       pionCorrelate(pion_sink,pion_source,pion_corr_avg,t,ndil);
       }
    count+=2;

   }}

 for (int t=0;t<ntimes;++t){
    pion_corr_avg[t]/=double(count);}
 
 QDPIO::cout << "Pion correlator:\nEach set below is one source time, one choice of noises\n";
 QDPIO::cout << "Imaginary part shown, but interchanging noise of quark and antiquark\n";
 QDPIO::cout << "  yield complex conjugate, so imaginary part will average to zero exactly\n";
 QDPIO::cout << "Time projector indices:";
 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp)
    QDPIO::cout << "  "<<*tp;
 QDPIO::cout << "\nMeson noises:\n";
 for (vector<MesonHandler::MesonNoiseInfo>::const_iterator mit=mnoises.begin();
      mit!=mnoises.end();++mit)
    QDPIO::cout << "  "<<mit->noise1.getSeed()<<"  "<<mit->noise2.getSeed()<<"\n";
 QDPIO::cout <<"\n";
 QDPIO::cout <<"pion correlator:\n";
 for (int t=0;t<ntimes;t++){
    QDPIO::cout << "pion propagator at time separation "<<t+tmin<<" = ("
         <<pion_corr_avg[t].elem().elem().elem().real()<<", "
         <<pion_corr_avg[t].elem().elem().elem().imag()<<")\n";}

 TextFileWriter dataout(outdata);
 for (int t=0;t<ntimes;t++){
    dataout << t+tmin<<" "<<pion_corr_avg[t].elem().elem().elem().real()<<"  "
                          <<pion_corr_avg[t].elem().elem().elem().imag()<<"\n";}
 dataout.close();
}

  // ************************************************************


    //  Compute the Omega sss baryon operators.  Compute the Hg SS_0
    //  operator for each of the 4 rows.  The operators are   
    //        [111],  sqrt(3)*[112],  sqrt(3)[122],  [222]
    //  If source=true, then all six permutations of the noises are 
    //  combined.   omega1,omega2,omega3,omega4 refer to the 
    //  four irrep rows.


void LaphScaleInlineMeas::makeOmegaBaryons(
               QuarkHandler& s, const LaphNoiseInfo& noise1,
               const LaphNoiseInfo& noise2, const LaphNoiseInfo& noise3, 
               bool source, int source_time, int time_value, int ndil, int bsign, 
               multi3d<Complex>& omega1, multi3d<Complex>& omega2,
               multi3d<Complex>& omega3, multi3d<Complex>& omega4)
{
 int tproj=s.getTimeDilutionProjectorIndex(source_time);
 omega1=zero; omega2=zero; omega3=zero; omega4=zero;

 addOmegaBaryons(s,source,tproj,time_value,ndil,bsign,noise1,noise2,noise3,0,1,2, 
                 omega1,omega2,omega3,omega4);
 if (source){
   addOmegaBaryons(s,source,tproj,time_value,ndil,-bsign,noise2,noise1,noise3,1,0,2, 
                   omega1,omega2,omega3,omega4);
   addOmegaBaryons(s,source,tproj,time_value,ndil,bsign,noise2,noise3,noise1,1,2,0,
                   omega1,omega2,omega3,omega4);
   addOmegaBaryons(s,source,tproj,time_value,ndil,-bsign,noise1,noise3,noise2,0,2,1,
                   omega1,omega2,omega3,omega4);
   addOmegaBaryons(s,source,tproj,time_value,ndil,bsign,noise3,noise1,noise2,2,0,1,
                   omega1,omega2,omega3,omega4);
   addOmegaBaryons(s,source,tproj,time_value,ndil,-bsign,noise3,noise2,noise1,2,1,0,
                   omega1,omega2,omega3,omega4);}

 s.clearData();
}




void LaphScaleInlineMeas::addOmegaBaryons(QuarkHandler& s, bool source, 
               int tproj, int t, int ndil, int sign,
               const LaphNoiseInfo& noise1, const LaphNoiseInfo& noise2, 
               const LaphNoiseInfo& noise3, int d1, int d2, int d3,
               multi3d<Complex>& omega1, multi3d<Complex>& omega2,
               multi3d<Complex>& omega3, multi3d<Complex>& omega4)
{
 int s1=1, s2=2;
 const double sqrtthree=sqrt(3.0);
 vector<int> dil(3);

 auto_ptr<LatticeColorVector> diquark11,diquark12,diquark22;
 auto_ptr<LatticeComplex> singlet;
 for (dil[d1]=0;dil[d1]<ndil;dil[d1]++){
    const LatticeColorVector* Q11=s.getData(source,noise1,tproj,dil[d1],s1,DirPath(),0,t);
    const LatticeColorVector* Q12=s.getData(source,noise1,tproj,dil[d1],s2,DirPath(),0,t);
    for (dil[d2]=0;dil[d2]<ndil;dil[d2]++){
       const LatticeColorVector* Q21=s.getData(source,noise2,tproj,dil[d2],s1,DirPath(),0,t);
       const LatticeColorVector* Q22=s.getData(source,noise2,tproj,dil[d2],s2,DirPath(),0,t);
       evaluateColorCrossProduct(Q11,Q21,diquark11);
       evaluateColorCrossProduct(Q11,Q22,diquark12);
       evaluateColorCrossProduct(Q12,Q22,diquark22);
       for (dil[d3]=0;dil[d3]<ndil;dil[d3]++){
          const LatticeColorVector* Q31=s.getData(source,noise3,tproj,dil[d3],s1,DirPath(),0,t);
          const LatticeColorVector* Q32=s.getData(source,noise3,tproj,dil[d3],s2,DirPath(),0,t);
          evaluateColorVectorContract(diquark11,Q31,singlet);
          omega1(dil[0],dil[1],dil[2])+= sign*evaluateSum(singlet);
          evaluateColorVectorContract(diquark11,Q32,singlet);
          omega2(dil[0],dil[1],dil[2])+= sign*sqrtthree*evaluateSum(singlet);
          evaluateColorVectorContract(diquark12,Q32,singlet);
          omega3(dil[0],dil[1],dil[2])+= sign*sqrtthree*evaluateSum(singlet);
          evaluateColorVectorContract(diquark22,Q32,singlet);
          omega4(dil[0],dil[1],dil[2])+= sign*evaluateSum(singlet);}}}
}



void LaphScaleInlineMeas::baryonCorrelate(const multi3d<Complex>& baryon_sink,
                                          const multi3d<Complex>& baryon_source,
                                          multi1d<Complex>& baryon_corr_avg,
                                          int t, int ndil)
{
 Complex bcor=zero;
 for (int dil1=0;dil1<ndil;dil1++)
    for (int dil2=0;dil2<ndil;dil2++)
       for (int dil3=0;dil3<ndil;dil3++)
          bcor+=baryon_sink(dil1,dil2,dil3)
               *conj(baryon_source(dil1,dil2,dil3));
 baryon_corr_avg[t]+=bcor;
}



void LaphScaleInlineMeas::calcOmegaBaryonCorrelator(XmlReader& xml_in)
{
 if (xml_tag_count(xml_in,"OmegaBaryonCorrelator")==0)
 return;

 QDPIO::cout << endl<<"Now starting OmegaBaryonCorrelator"<<endl<<endl;
 XmlReader xmlr(xml_in,"./OmegaBaryonCorrelator");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 QuarkActionInfo quark(xmlr,gaugeinfo);
 QuarkSmearingInfo qSmear(xmlr);

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "OmegaBaryonCorrelator");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "OmegaBaryonCorrelator");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

 DilutionSchemeInfo dilScheme(xmlr);
 if (!dilScheme.isFullTimeDilution()){
    QDPIO::cerr << "Dilution scheme must include full time dilution"<<endl;
    QDP_abort(1);}

 FileListInfo qfiles(xmlr);

 string outdata;
 xmlread(xmlr,"OutputData",outdata,"OmegaBaryonCorrelator");
 outdata=tidyString(outdata);
 if (outdata.length()==0){
    QDPIO::cerr << "Empty string for output data file; aborting"<<endl;
    QDP_abort(1);}

 int Textent=gaugeinfo.getTimeExtent();
 int tmax,tmin;
 xmlread(xmlr,"MinTimeSeparation",tmin,"OmegaBaryonCorrelator");
 xmlread(xmlr,"MaxTimeSeparation",tmax,"OmegaBaryonCorrelator");
 if ((tmin<0)||(tmin>=Textent/2)){
    QDPIO::cerr << "Bad minimum time separation"<<endl;
    QDP_abort(1);}
 if ((tmax<tmin)||(tmax>Textent/2)){
    QDPIO::cerr << "Bad maximum time separation"<<endl;
    QDP_abort(1);}
 int ntimes=tmax-tmin+1;

 QuarkHandler q(gaugeinfo,gSmear,qSmear,dilScheme,quark,qfiles,
                smeared_quark_filestub,smeared_gauge_file);

 set<NoiseAndTimeProjector> nt=q.getNoisesAndTimeProjectors();
 set<LaphNoiseInfo> noises;
 set<int> time_projs;
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    time_projs.insert(it->time_proj_index);}
 if (noises.size()<3){
    QDPIO::cerr << "3 or more noises are required"<<endl;
    QDP_abort(1);}

    // randomly pick some noise combinations

 vector<BaryonHandler::BaryonNoiseInfo> bnoises;
 pickBaryonNoises(noises,bnoises);

 int ndil=q.getNumberOfSpinEigvecDilutionProjectors();

 multi1d<Complex> omega_corr_avg(ntimes);
 for (int t=0;t<ntimes;++t){
    omega_corr_avg[t]=zero;}
 int count=0;

      // loop over all available time sources

 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp){

    int tproj=*tp;
    const list<int>& ontimes=q.getOnTimes(tproj);
    int t0=ontimes.front();   // must be full time dilution..checked above

    // loop over noise permutations

  for (vector<BaryonHandler::BaryonNoiseInfo>::const_iterator bit=bnoises.begin();
       bit!=bnoises.end();++bit){

    multi3d<Complex> omega1_source(ndil,ndil,ndil);
    multi3d<Complex> omega1_sink(ndil,ndil,ndil);
    multi3d<Complex> omega2_source(ndil,ndil,ndil);
    multi3d<Complex> omega2_sink(ndil,ndil,ndil);
    multi3d<Complex> omega3_source(ndil,ndil,ndil);
    multi3d<Complex> omega3_sink(ndil,ndil,ndil);
    multi3d<Complex> omega4_source(ndil,ndil,ndil);
    multi3d<Complex> omega4_sink(ndil,ndil,ndil);

         // make the sources

    makeOmegaBaryons(q,bit->noise1,bit->noise2,bit->noise3,true,t0,t0,ndil,1,
                     omega1_source,omega2_source,omega3_source,omega4_source);

         // make sinks and correlator

   for (int t=0;t<ntimes;t++){
      int bcsign=1;
      int dt=t+tmin;
      int tval=t0+dt;
      if (tval>=Textent){
         bcsign=-1; tval-=Textent;}
      makeOmegaBaryons(q,bit->noise1,bit->noise2,bit->noise3,false,t0,tval,ndil, 
                       bcsign,omega1_sink,omega2_sink,omega3_sink,omega4_sink);
      baryonCorrelate(omega1_sink,omega1_source,omega_corr_avg,t,ndil);
      baryonCorrelate(omega2_sink,omega2_source,omega_corr_avg,t,ndil);
      baryonCorrelate(omega3_sink,omega3_source,omega_corr_avg,t,ndil);
      baryonCorrelate(omega4_sink,omega4_source,omega_corr_avg,t,ndil);}

   count+=4;
   }}

 for (int t=0;t<ntimes;++t){
    omega_corr_avg[t]/=double(count);}

 QDPIO::cout << "Omega correlator:\nEach set below is one source time, one choice of noises\n";
 QDPIO::cout << "Time projector indices:";
 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp)
    QDPIO::cout << "  "<<*tp;
 QDPIO::cout << "\nBaryon noises:\n";
 for (vector<BaryonHandler::BaryonNoiseInfo>::const_iterator bit=bnoises.begin();
      bit!=bnoises.end();++bit)
    QDPIO::cout << "  "<<bit->noise1.getSeed()<<"  "<<bit->noise2.getSeed()
                << "  "<<bit->noise3.getSeed()<<"\n";
 QDPIO::cout <<"\n";
 QDPIO::cout <<"omega baryon correlator:\n";
 for (int t=0;t<ntimes;t++){
    QDPIO::cout << "omega baryon propagator at time separation "<<t+tmin<<" = ("
         <<omega_corr_avg[t].elem().elem().elem().real()<<", "
         <<omega_corr_avg[t].elem().elem().elem().imag()<<")\n";}
 QDPIO::cout << endl;

 TextFileWriter dataout(outdata);
 for (int t=0;t<ntimes;t++){
    dataout << t+tmin<<" "<<omega_corr_avg[t].elem().elem().elem().real()<<"  "
                          <<omega_corr_avg[t].elem().elem().elem().imag()<<"\n";} 
 dataout.close();
}


  // ************************************************************


    //  Compute the nucleon operators.  Compute the G1g SS_0
    //  operator for each of the 2 rows.  The operators are   
    //         uud  121-112  nucleon (G1g_1)
    //         uud  221-122  nucleon (G1g_2)

    //  If source=true, then all permutations of the noises are 
    //  combined.   nucleon1,nucleon2 refer to the 
    //  two irrep rows.


void LaphScaleInlineMeas::makeNucleons(
               QuarkHandler& ud, const LaphNoiseInfo& noise1,
               const LaphNoiseInfo& noise2, const LaphNoiseInfo& noise3, 
               bool source, int source_time, int time_value, int ndil, int bsign, 
               multi3d<Complex>& nucleon1, multi3d<Complex>& nucleon2)
{
 int tproj=ud.getTimeDilutionProjectorIndex(source_time);
 nucleon1=zero; nucleon2=zero;

 addNucleons(ud,source,tproj,time_value,ndil,bsign,noise1,noise2,noise3,0,1,2, 
             nucleon1,nucleon2);
 if (source){
   addNucleons(ud,source,tproj,time_value,ndil,-bsign,noise2,noise1,noise3,1,0,2, 
               nucleon1,nucleon2);}

 ud.clearData();
}


void LaphScaleInlineMeas::addNucleons(QuarkHandler& ud, bool source, 
               int tproj, int t, int ndil, int sign,
               const LaphNoiseInfo& noise1, const LaphNoiseInfo& noise2, 
               const LaphNoiseInfo& noise3, int d1, int d2, int d3,
               multi3d<Complex>& nucleon1, multi3d<Complex>& nucleon2)
{
 int s1=1, s2=2;
 vector<int> dil(3);

 auto_ptr<LatticeColorVector> diquark11,diquark12,diquark22;
 auto_ptr<LatticeComplex> singlet;
 for (dil[d1]=0;dil[d1]<ndil;dil[d1]++){
    const LatticeColorVector* Q11=ud.getData(source,noise1,tproj,dil[d1],s1,DirPath(),0,t);
    const LatticeColorVector* Q12=ud.getData(source,noise1,tproj,dil[d1],s2,DirPath(),0,t);
    for (dil[d2]=0;dil[d2]<ndil;dil[d2]++){
       const LatticeColorVector* Q21=ud.getData(source,noise2,tproj,dil[d2],s1,DirPath(),0,t);
       const LatticeColorVector* Q22=ud.getData(source,noise2,tproj,dil[d2],s2,DirPath(),0,t);
       evaluateColorCrossProduct(Q11,Q21,diquark11);
       evaluateColorCrossProduct(Q11,Q22,diquark12);
       evaluateColorCrossProduct(Q12,Q22,diquark22);
       for (dil[d3]=0;dil[d3]<ndil;dil[d3]++){
          const LatticeColorVector* Q31=ud.getData(source,noise3,tproj,dil[d3],s1,DirPath(),0,t);
          const LatticeColorVector* Q32=ud.getData(source,noise3,tproj,dil[d3],s2,DirPath(),0,t);
          evaluateColorVectorContract(diquark12,Q31,singlet);
          nucleon1(dil[0],dil[1],dil[2])+= sign*evaluateSum(singlet);
          evaluateColorVectorContract(diquark11,Q32,singlet);
          nucleon1(dil[0],dil[1],dil[2])-= sign*evaluateSum(singlet);
          evaluateColorVectorContract(diquark22,Q31,singlet);
          nucleon2(dil[0],dil[1],dil[2])+= sign*evaluateSum(singlet);
          evaluateColorVectorContract(diquark12,Q32,singlet);
          nucleon2(dil[0],dil[1],dil[2])-= sign*evaluateSum(singlet);}}}
}




void LaphScaleInlineMeas::calcNucleonCorrelator(XmlReader& xml_in)
{
 if (xml_tag_count(xml_in,"NucleonCorrelator")==0)
 return;

 QDPIO::cout << endl<<"Now starting NucleonCorrelator"<<endl<<endl;
 XmlReader xmlr(xml_in,"./NucleonCorrelator");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 QuarkActionInfo quark(xmlr,gaugeinfo);
 QuarkSmearingInfo qSmear(xmlr);

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "NucleonCorrelator");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "NucleonCorrelator");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

 DilutionSchemeInfo dilScheme(xmlr);
 if (!dilScheme.isFullTimeDilution()){
    QDPIO::cerr << "Dilution scheme must include full time dilution"<<endl;
    QDP_abort(1);}

 FileListInfo qfiles(xmlr);

 string outdata;
 xmlread(xmlr,"OutputData",outdata,"NucleonCorrelator");
 outdata=tidyString(outdata);
 if (outdata.length()==0){
    QDPIO::cerr << "Empty string for output data file; aborting"<<endl;
    QDP_abort(1);}

 int Textent=gaugeinfo.getTimeExtent();
 int tmax,tmin;
 xmlread(xmlr,"MinTimeSeparation",tmin,"NucleonCorrelator");
 xmlread(xmlr,"MaxTimeSeparation",tmax,"NucleonCorrelator");
 if ((tmin<0)||(tmin>=Textent/2)){
    QDPIO::cerr << "Bad minimum time separation"<<endl;
    QDP_abort(1);}
 if ((tmax<tmin)||(tmax>Textent/2)){
    QDPIO::cerr << "Bad maximum time separation"<<endl;
    QDP_abort(1);}
 int ntimes=tmax-tmin+1;

 QuarkHandler q(gaugeinfo,gSmear,qSmear,dilScheme,quark,qfiles,
                smeared_quark_filestub,smeared_gauge_file);

 set<NoiseAndTimeProjector> nt=q.getNoisesAndTimeProjectors();
 set<LaphNoiseInfo> noises;
 set<int> time_projs;
 for (set<NoiseAndTimeProjector>::const_iterator it=nt.begin();
      it!=nt.end();++it){
    noises.insert(it->noise);
    time_projs.insert(it->time_proj_index);}
 if (noises.size()<3){
    QDPIO::cerr << "3 or more noises are required"<<endl;
    QDP_abort(1);}

    // randomly pick some noise combinations

 vector<BaryonHandler::BaryonNoiseInfo> bnoises;
 pickBaryonNoises(noises,bnoises);

 int ndil=q.getNumberOfSpinEigvecDilutionProjectors();

 multi1d<Complex> nucleon_corr_avg(ntimes);
 for (int t=0;t<ntimes;++t){
    nucleon_corr_avg[t]=zero;}
 int count=0; 

      // loop over all available time sources

 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp){

    int tproj=*tp;
    const list<int>& ontimes=q.getOnTimes(tproj);
    int t0=ontimes.front();   // must be full time dilution..checked above

    // loop over noise permutations

  for (vector<BaryonHandler::BaryonNoiseInfo>::const_iterator bit=bnoises.begin();
       bit!=bnoises.end();++bit){

    multi3d<Complex> nucleon1_source(ndil,ndil,ndil);
    multi3d<Complex> nucleon1_sink(ndil,ndil,ndil);
    multi3d<Complex> nucleon2_source(ndil,ndil,ndil);
    multi3d<Complex> nucleon2_sink(ndil,ndil,ndil);

         // make the sources

    makeNucleons(q,bit->noise1,bit->noise2,bit->noise3,true,t0,t0,ndil,1,
                 nucleon1_source,nucleon2_source);

         // make sinks and correlator

   for (int t=0;t<ntimes;t++){
      int bcsign=1;
      int dt=t+tmin;
      int tval=t0+dt;
      if (tval>=Textent){
         bcsign=-1; tval-=Textent;}
      makeNucleons(q,bit->noise1,bit->noise2,bit->noise3,false,t0,tval,ndil, 
                   bcsign,nucleon1_sink,nucleon2_sink);
      baryonCorrelate(nucleon1_sink,nucleon1_source,nucleon_corr_avg,t,ndil);
      baryonCorrelate(nucleon2_sink,nucleon2_source,nucleon_corr_avg,t,ndil);}

   count+=2;
   }} 

 for (int t=0;t<ntimes;++t){
    nucleon_corr_avg[t]/=double(count);}

 QDPIO::cout << "Nucleon correlator:\nEach set below is one source time, one choice of noises\n";
 QDPIO::cout << "Time projector indices:";
 for (set<int>::const_iterator tp=time_projs.begin();tp!=time_projs.end();++tp)
    QDPIO::cout << "  "<<*tp;
 QDPIO::cout << "\nBaryon noises:\n";
 for (vector<BaryonHandler::BaryonNoiseInfo>::const_iterator bit=bnoises.begin();
      bit!=bnoises.end();++bit)
    QDPIO::cout << "  "<<bit->noise1.getSeed()<<"  "<<bit->noise2.getSeed()
                << "  "<<bit->noise3.getSeed()<<"\n";
 QDPIO::cout <<"\n";
 QDPIO::cout <<"Nucleon baryon correlator:\n";
 for (int t=0;t<ntimes;t++){
    QDPIO::cout << "nucleon baryon propagator at time separation "<<t+tmin<<" = ("
         <<nucleon_corr_avg[t].elem().elem().elem().real()<<", "
         <<nucleon_corr_avg[t].elem().elem().elem().imag()<<")\n";}
 QDPIO::cout << endl;

 TextFileWriter dataout(outdata);
 for (int t=0;t<ntimes;t++){
    dataout << t+tmin<<" "<<nucleon_corr_avg[t].elem().elem().elem().real()<<"  "
                          <<nucleon_corr_avg[t].elem().elem().elem().imag()<<"\n";} 
 dataout.close();
}


  // ************************************************************


void LaphScaleInlineMeas::evaluateLocalInnerProduct(
                               const LatticeColorVector *v1,
                               const LatticeColorVector *v2,
                               auto_ptr<LatticeComplex>& result)
{
 if ((v1==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeComplex);
 *result=localInnerProduct(*v1,*v2);
}

Complex LaphScaleInlineMeas::evaluateSum(auto_ptr<LatticeComplex>& in)
{
 if (in.get()==0) return zero;
 return sum(*in);
}

void LaphScaleInlineMeas::evaluateColorCrossProduct(
                               const LatticeColorVector *v1,
                               const LatticeColorVector *v2,
                               auto_ptr<LatticeColorVector>& result)
{
 if ((v1==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeColorVector);
 *result=colorCrossProduct(*v1,*v2);
}

void LaphScaleInlineMeas::evaluateColorVectorContract(
                                 const auto_ptr<LatticeColorVector>& v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeComplex>& result)
{
 if ((v1.get()==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeComplex);
 *result=colorVectorContract(*v1,*v2);
}


// ******************************************************************
  }
#endif
} // namespace Chroma
