#ifndef __INLINE_MESON_INT_LOOPS_H__
#define __INLINE_MESON_INT_LOOPS_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "quark_info.h"
#include "meson_handler.h"
#include "laph_noise_info.h"

// ***********************************************************************
// *                                                                     *
// *  Driver inline measurement that constructs the meson internal loops.*
// *  Assumes that the smeared gauge field time slices and the Laph      *
// *  eigenvectors are available, and that the needed quark line ends    *
// *  are also available.  Must be run in 3D chroma_laph.  XML input     *
// *  must have the form:                                                *
// *                                                                     *
// *  <chroma>                                                           *
// *   <RNG><Seed> ... </Seed></RNG>                                     *
// *   <Param>                                                           *
// *    <nrow>12 12 12</nrow>   # lattice size Nx Ny Nz                  *
// *    <InlineMeasurements>                                             *
// *    <elem>                                                           *
// *                                                                     *
// *     <Name> LAPH_MESON_INT_LOOPS </Name>                             *
// *                                                                     *
// *     <MesonCommonInfo>        ...   </MesonCommonInfo>               *
// *     <MesonOperators>         ...   </MesonOperators>                *
// *     <MesonInternalLoopCalculations>  ...                            *
// *                            </MesonInternalLoopCalculations>         *
// *     <JobName> ... </JobName>  (optional)                            *
// *                                                                     *
// *    </elem>                                                          *
// *    </InlineMeasurements>                                            *
// *   </Param>                                                          *
// *  </chroma>                                                          *
// *                                                                     *
// *  The details in the three major XML tags above are described below. *
// *                                                                     *
// *  (1) Major tag <MesonCommonInfo>:                                   *
// *                                                                     *
// *     <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>          *
// *     <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>          *
// *     <MesonFileList>   ...  </MesonFileList>                         *
// *     <SmearedGaugeFileName> ... </SmearedGaugeFileName>              *
// *     <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>              *
// *     <Quark>  ...  </Quark>                                          *
// *                                                                     *
// *  There is a <FileListInfo> under the <MesonFileList> tag.           *
// *  The antiquark information is inferred from the quark information.  *
// *  The <Quark> tag must contain under it a <QuarkInfo> tag,           *
// *  BUT additionally, file list info must be given under the           *
// *  <FixedSource> and <RelativeSource> tags in the QuarkInfo's.        *
// *  For example:                                                       *
// *                                                                     *
// *     <QuarkInfo>                                                     *
// *        <QuarkActionInfo> ... </QuarkActionInfo>                     *
// *        <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>         *
// *        <FixedSource>                                                *
// *           <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *           <FileListInfo>  ... </FileListInfo>   (additional tag)    *
// *        </FixedSource>                                               *
// *        <RelativeSource>                                             *
// *           <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *           <FileListInfo>  ... </FileListInfo>   (additional tag)    *
// *        </RelativeSource>                                            *
// *     </QuarkInfo>                                                    *
// *                                                                     *
// *  (2) Major tag <MesonOperators>:                                    *
// *                                                                     *
// *        <!-- set of meson operators must have same light/strange     *
// *             flavor content -->                                      *
// *                                                                     *
// *     <MesonOperators>                                                *
// *       <ElementalCoefficientDirectory>                               *
// *           coefsTopDirectory                                         *
// *       </ElementalCoefficientDirectory>                              *
// *       <MesonOp>                                                     *
// *         ...                                                         *
// *       </MesonOp>                                                    *
// *       <MesonOp>                                                     *
// *         ...                                                         *
// *       </MesonOp>                                                    *
// *         ...                                                         *
// *     </MesonOperators>                                               *
// *                                                                     *
// *  (3) Major tag <MesonInternalLoopCalculations>                      *
// *                                                                     *
// *     <MesonInternalLoopCalculations>                                 *
// *       <MesonIntLoopCalc>                                            *
// *         <QuarkType>SinkWithFixedSource</QuarkType>                  *
// *         <QuarkMode>Gamma5HermConj</QuarkMode>                       *
// *         <MesonNoises> ... </MesonNoises>                            *
// *       </MesonIntLoopCalc>                                           *
// *        ...                                                          *
// *     </MesonInternalLoopCalculations>                                *
// *                                                                     *
// *   The <MesonNoises> tag is specified by                             *
// *                                                                     *
// *    <MesonNoises>                                                    *
// *       <MesonNoise>                                                  *
// *         <QuarkLine><LaphNoiseInfo>...</LaphNoiseInfo></QuarkLine>   *
// *       </MesonNoise>                                                 *
// *         ...                                                         *
// *    </MesonNoises>                                                   *
// *                                                                     *
// *   Or you can just specify <All/> and the program will look for      *
// *   all noises in the quark sinks and do all permutations.            *
// *                                                                     *
// *                                                                     *
// *                                                                     *
// *   Alternative:  After several separate calculations output to       *
// *   different sets of files, one may wish to merge the output files   *
// *   into a single set.  This inline task also does this.  The input   *
// *   XML to accomplish this is as below:                               *
// *                                                                     *
// *     <Name> LAPH_MESON_INT_LOOPS </Name>                             *
// *                                                                     *
// *     <MesonCommonInfo>        ...   </MesonCommonInfo>               *
// *     <MergeOutput>   filelist to merge   </MergeOutput>              *
// *                                                                     *
// *   This must be run in serial mode (non-parallel).                   *
// *                                                                     *
// *                                                                     *
// ***********************************************************************


namespace Chroma { 

#if (QDP_ND == 3)

  namespace InlineStochLaphMesonIntLoopEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochLaphMesonIntLoopInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below


 public:

   StochLaphMesonIntLoopInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochLaphMesonIntLoopInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}

   
};
	

// ***********************************************************
  }
#endif
}

#endif
