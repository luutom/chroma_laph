#include "inline_baryon_line_ends.h"
#include "chroma.h"
#include "time_slices.h"

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 3)

  namespace InlineStochLaphBaryonEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "StochLaphBaryonInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochLaphBaryonInlineMeas(xml_in, path);
}


const std::string name = "LAPH_BARYON_LINE_ENDS";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}


// *********************************************************************

     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochLaphBaryonInlineMeas::operator()(unsigned long update_no,
                                           XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

    //  read the common info first

 if (xml_tag_count(xml_rdr,"BaryonCommonInfo")!=1){
    QDPIO::cerr << "Must have one <BaryonCommonInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./BaryonCommonInfo");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 FileListInfo baryonFiles(xmlr,"BaryonFileList");

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "LAPH_BARYON_LINE_ENDS");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "LAPH_BARYON_LINE_ENDS");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

 if ((xml_tag_count(xmlr,"Quark1")!=1)
   ||(xml_tag_count(xmlr,"Quark2")!=1)
   ||(xml_tag_count(xmlr,"Quark3")!=1)){
    QDPIO::cerr << "could not read the three quark infos"<<endl;
    QDP_abort(1);}

 XmlReader xml1(xmlr,"./Quark1");
 QuarkInfo quark1(xml1,gaugeinfo);
 XmlReader xml2(xmlr,"./Quark2");
 QuarkInfo quark2(xml2,gaugeinfo);
 XmlReader xml3(xmlr,"./Quark3");
 QuarkInfo quark3(xml3,gaugeinfo);

    // echo common input before starting the computations

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 3A: Compute the baryon line ends            *"<<endl;
 QDPIO::cout << " *                 and write to file as time slices        *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<"Baryon File Stub:"<<endl<<baryonFiles.getFileStub()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark 1:"<<endl<<quark1.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark 2:"<<endl<<quark2.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark 3:"<<endl<<quark3.output()<<endl<<endl;

    //  check to see if just a simple merge is required

 int nmerge=xml_tag_count(xml_rdr,"Merge");
 if (nmerge>=1){
    if (QDP::Layout::numNodes()>1){
       QDPIO::cerr << "ERROR: Merge of baryon handler output files must be run in serial"<<endl;
       QDP_abort(1);}
    vector<FileListInfo> vecmergefiles;
    
    for (int k=1;k<=nmerge;k++){
       XmlReader xmlg(xml_rdr,"./Merge["+int_to_string(k)+"]");
       FileListInfo mergefiles(xmlg);
       QDPIO::cout << endl<<" Merging baryon handler output files"<<endl<<endl;
       QDPIO::cout << "     Files to merge:"<<endl;
       QDPIO::cout << "             Stub: "<<mergefiles.getFileStub()<<endl;
       QDPIO::cout << "  Min file number: "<<mergefiles.getMinFileNumber()<<endl;
       QDPIO::cout << "  Max file number: "<<mergefiles.getMaxFileNumber()<<endl<<endl;
       vecmergefiles.push_back(mergefiles);}
    BaryonHandler BH(gaugeinfo,gSmear,quark1,quark2,quark3,baryonFiles,false);
    QDPIO::cout << "Starting the merge"<<endl;
    BH.reorganize(vecmergefiles);
    QDPIO::cout << endl<<"Merge complete."<<endl<<endl;
    return;}

 BaryonHandler::QuarkFileLists Qfiles(xmlr);
 QDPIO::cout <<endl<<endl<<"Quark sink details:"<<endl<<Qfiles.output()<<endl<<endl;

    //  read the baryon operators

 set<BaryonOperatorInfo> BOps;
 string CoefsTopDirectory;
 readBaryonOperators(xml_rdr,BOps,CoefsTopDirectory);

    //  read the baryon line calculations to be done

 if (xml_tag_count(xml_rdr,"BaryonLineCalculations")!=1){
    QDPIO::cerr << "Must have one <BaryonLineCalculations> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlc(xml_rdr,"./BaryonLineCalculations");
 set<BaryonHandler::BaryonLineCalc> bcalcs;
 int ncalc=xml_tag_count(xmlc,"BaryonLineCalc");
 if (ncalc==0){
    QDPIO::cerr << "No baryon line calculations given; nothing to do"<<endl;
    QDP_abort(1);}
 for (int k=1;k<=ncalc;++k) 
    bcalcs.insert(BaryonHandler::BaryonLineCalc(xmlc,k));


    // echo the input before starting the computations

// QDPIO::cout <<endl<<endl<<"Baryon operators:"<<endl<<endl;
// for (set<BaryonOperatorInfo>::const_iterator bit=BOps.begin();bit!=BOps.end();++bit)
//    QDPIO::cout << bit->output()<<endl;
 QDPIO::cout <<endl<<endl<<"Baryon line calculations: ("
             <<bcalcs.size()<<" in total)" <<endl<<endl;
 for (set<BaryonHandler::BaryonLineCalc>::const_iterator bcit=bcalcs.begin();
      bcit!=bcalcs.end();++bcit)
    QDPIO::cout << bcit->output()<<endl;
 QDPIO::cout << endl<<endl;

     // read optional job name

 string jobname;
 xmlreadif(xml_rdr,"JobName",jobname,"LAPH_BARYON_LINE_ENDS");

     // echo to the output xml file

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_BARYON_LINE_ENDS");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 push(xml_out,"Quark1");
 quark1.output(xml_out);
 pop(xml_out);
 push(xml_out,"Quark2");
 quark2.output(xml_out);
 pop(xml_out);
 push(xml_out,"Quark3");
 quark3.output(xml_out);
 pop(xml_out);
 Qfiles.output(xml_out);
 push(xml_out,"BaryonOperators");
 for (set<BaryonOperatorInfo>::const_iterator bit=BOps.begin();bit!=BOps.end();++bit)
    bit->output(xml_out);
 pop(xml_out);
 push(xml_out,"BaryonLineCalculations");
 for (set<BaryonHandler::BaryonLineCalc>::const_iterator bcit=bcalcs.begin();
      bcit!=bcalcs.end();++bcit)
    bcit->output(xml_out);
 pop(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

   // construct the baryon handler (NOT in read mode)

 BaryonHandler BH(gaugeinfo,gSmear,quark1,quark2,quark3,baryonFiles,false);

 BH.compute(BOps,CoefsTopDirectory,Qfiles,bcalcs,
            smeared_gauge_file,smeared_quark_filestub);

 if (jobname.length()>0)
    QDPIO::cout <<endl<< "SUCCESSFUL BARYON RUN: "<<jobname<<endl;

}

// ******************************************************************
  }
#endif
} // namespace Chroma

