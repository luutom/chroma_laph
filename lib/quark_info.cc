#include "quark_info.h"
#include "chromabase.h"
#include <sstream>

namespace Chroma {
  namespace LaphEnv {


// ************************************************************

QuarkInfo::QuarkInfo(XmlReader& xml_in)
{ 
 xml_tag_assert(xml_in,"QuarkInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::QuarkInfo");

 m_action=new QuarkActionInfo(xmlr);      // throws exception if fails
 m_smearing=new QuarkSmearingInfo(xmlr);  // aborts if exception not caught

 XmlReader xmld(xmlr,"FixedSource");
 m_fixed_dilscheme=new DilutionSchemeInfo(xmld);

 if (xml_tag_count(xmlr,"RelativeSource")==0)
    m_rel_dilscheme=new DilutionSchemeInfo;   // none
 else{
    XmlReader xmld2(xmlr,"RelativeSource");
    m_rel_dilscheme=new DilutionSchemeInfo(xmld2);}
}

  // ************************************************************

QuarkInfo::QuarkInfo(XmlReader& xml_in, const GaugeConfigurationInfo& gaugein)
{ 
 xml_tag_assert(xml_in,"QuarkInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::QuarkInfo");

 m_action=new QuarkActionInfo(xmlr,gaugein);      // throws exception if fails
 m_smearing=new QuarkSmearingInfo(xmlr);  // aborts if exception not caught

 XmlReader xmld(xmlr,"FixedSource");
 m_fixed_dilscheme=new DilutionSchemeInfo(xmld);

 if (xml_tag_count(xmlr,"RelativeSource")==0)
    m_rel_dilscheme=new DilutionSchemeInfo;   // none
 else{
    XmlReader xmld2(xmlr,"RelativeSource");
    m_rel_dilscheme=new DilutionSchemeInfo(xmld2);}
}

  // ************************************************************

QuarkInfo::~QuarkInfo()
{
 delete m_action;
 delete m_smearing;
 delete m_fixed_dilscheme;
 delete m_rel_dilscheme;
}


     // copy constructor

QuarkInfo::QuarkInfo(const QuarkInfo& rhs)
{
 m_action=new QuarkActionInfo(*rhs.m_action);
 m_smearing=new QuarkSmearingInfo(*rhs.m_smearing);
 m_fixed_dilscheme=new DilutionSchemeInfo(*rhs.m_fixed_dilscheme);
 m_rel_dilscheme=new DilutionSchemeInfo(*rhs.m_rel_dilscheme);   
}

QuarkInfo& QuarkInfo::operator=(const QuarkInfo& rhs)
{
 *m_action=*rhs.m_action;
 *m_smearing=*rhs.m_smearing;
 *m_fixed_dilscheme=*rhs.m_fixed_dilscheme;
 *m_rel_dilscheme=*rhs.m_rel_dilscheme;   
/* if (this!=&rhs){
    delete m_action;
    delete m_smearing;
    delete m_fixed_dilscheme;
    delete m_rel_dilscheme;
    m_action=new QuarkActionInfo(*rhs.m_action);
    m_smearing=new QuarkSmearingInfo(*rhs.m_smearing);
    m_fixed_dilscheme=new DilutionSchemeInfo(*rhs.m_fixed_dilscheme);
    m_rel_dilscheme=new DilutionSchemeInfo(*rhs.m_rel_dilscheme);} */
 return *this;
}


void QuarkInfo::checkEqual(const QuarkInfo& rhs) const
{
 if (!(*this == rhs)){
    std::cerr << "QuarkInfo checkEqual failed"<<endl;
    std::cerr << "LHS:"<<endl<<output()<<endl<<"RHS:"<<endl<<rhs.output()<<endl;
    throw string("QuarkInfo does not checkEqual...abort");}
}

bool QuarkInfo::operator==(const QuarkInfo& rhs) const
{
 return ((*m_action == *rhs.m_action)
       &&(*m_smearing == *rhs.m_smearing)
       &&(*m_fixed_dilscheme == *rhs.m_fixed_dilscheme)
       &&(*m_rel_dilscheme == *rhs.m_rel_dilscheme));
}


string QuarkInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<QuarkInfo>"<<endl;
 oss << m_action->output(indent+1);
 oss << m_smearing->output(indent+1);
 oss << pad << "   <FixedSource> "<<endl;
 oss << m_fixed_dilscheme->output(indent+2);
 oss << pad << "   </FixedSource>"<<endl;
 oss << pad << "   <RelativeSource> "<<endl;
 oss << m_rel_dilscheme->output(indent+2);
 oss << pad << "   </RelativeSource>"<<endl;
 oss << pad << "</QuarkInfo>"<<endl;
 return oss.str();
}

void QuarkInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkInfo");
 m_action->output(xmlout);
 m_smearing->output(xmlout);
 push(xmlout,"FixedSource");
 m_fixed_dilscheme->output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 m_rel_dilscheme->output(xmlout);
 pop(xmlout);
 pop(xmlout);
}


// ***********************************************************
  }
}
