#ifndef MULTI_BOOLEAN_H
#define MULTI_BOOLEAN_H


   //  Objects used as keys in C++ maps require a less than
   //  operator.  The templated code here is useful for performing
   //  the less than operation on objects constructed from 
   //  several other objects.  The equality and inequality operations
   //  are also given.


namespace Chroma {
  namespace LaphEnv {

// ******************************************************************


template <typename T1, typename T2>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2)
{
 return   ((u1<v1) || ((u1==v1)
        && (u2<v2) ));
}

template <typename T1, typename T2>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2)
{
 return (u1==v1)&&(u2==v2);
}
 
template <typename T1, typename T2>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2)
{
 return (u1!=v1)||(u2!=v2);
}


// ******************************************************************


template <typename T1, typename T2, typename T3>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3)
{
 return   ((u1<v1) || ((u1==v1) 
       && ((u2<v2) || ((u2==v2)
       &&  (u3<v3) ))));
}

template <typename T1, typename T2, typename T3>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2,
                const T3& u3, const T3& v3)
{
 return (u1==v1)&&(u2==v2)&&(u3==v3);
}
 
template <typename T1, typename T2, typename T3>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3)
{
 return (u1!=v1)||(u2!=v2)||(u3!=v3);
}

// ******************************************************************


template <typename T1, typename T2, typename T3, typename T4>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4)
{
 return   ((u1<v1) || ((u1==v1) 
       && ((u2<v2) || ((u2==v2)
       && ((u3<v3) || ((u3==v3)
       &&  (u4<v4) ))))));
}


template <typename T1, typename T2, typename T3, typename T4>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2,
                const T3& u3, const T3& v3,
                const T4& u4, const T4& v4)
{
 return (u1==v1)&&(u2==v2)&&(u3==v3)&&(u4==v4);
}

 
template <typename T1, typename T2, typename T3, typename T4>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4)
{
 return (u1!=v1)||(u2!=v2)||(u3!=v3)||(u4!=v4);
}


// ******************************************************************


template <typename T1, typename T2, typename T3, typename T4, typename T5>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5)
{
 return   ((u1<v1) || ((u1==v1) 
       && ((u2<v2) || ((u2==v2)
       && ((u3<v3) || ((u3==v3)
       && ((u4<v4) || ((u4==v4)
       &&  (u5<v5) ))))))));
}


template <typename T1, typename T2, typename T3, typename T4, typename T5>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2,
                const T3& u3, const T3& v3,
                const T4& u4, const T4& v4,
                const T5& u5, const T5& v5)
{
 return (u1==v1)&&(u2==v2)&&(u3==v3)&&(u4==v4)&&(u5==v5);
}

 
template <typename T1, typename T2, typename T3, typename T4, typename T5>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5)
{
 return (u1!=v1)||(u2!=v2)||(u3!=v3)||(u4!=v4)||(u5!=v5);
}


// ******************************************************************


template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5,
                   const T6& u6, const T6& v6)
{
 return   ((u1<v1) || ((u1==v1) 
       && ((u2<v2) || ((u2==v2)
       && ((u3<v3) || ((u3==v3)
       && ((u4<v4) || ((u4==v4)
       && ((u5<v5) || ((u5==v5)
       &&  (u6<v6) ))))))))));
}


template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2,
                const T3& u3, const T3& v3,
                const T4& u4, const T4& v4,
                const T5& u5, const T5& v5,
                const T6& u6, const T6& v6)
{
 return (u1==v1)&&(u2==v2)&&(u3==v3)&&(u4==v4)&&(u5==v5)&&(u6==v6);
}

 
template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5,
                   const T6& u6, const T6& v6)
{
 return (u1!=v1)||(u2!=v2)||(u3!=v3)||(u4!=v4)||(u5!=v5)||(u6!=v6);
}


// ******************************************************************


template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6, typename T7>
bool multiLessThan(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5,
                   const T6& u6, const T6& v6,
                   const T7& u7, const T7& v7)
{
 return   ((u1<v1) || ((u1==v1) 
       && ((u2<v2) || ((u2==v2)
       && ((u3<v3) || ((u3==v3)
       && ((u4<v4) || ((u4==v4)
       && ((u5<v5) || ((u5==v5)
       && ((u6<v6) || ((u6==v6)
       &&  (u7<v7) ))))))))))));
}


template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6, typename T7>
bool multiEqual(const T1& u1, const T1& v1, 
                const T2& u2, const T2& v2,
                const T3& u3, const T3& v3,
                const T4& u4, const T4& v4,
                const T5& u5, const T5& v5,
                const T6& u6, const T6& v6,
                const T7& u7, const T7& v7)
{
 return (u1==v1)&&(u2==v2)&&(u3==v3)&&(u4==v4)&&(u5==v5)&&(u6==v6)&&(u7==v7);
}

 
template <typename T1, typename T2, typename T3, typename T4, 
          typename T5, typename T6, typename T7>
bool multiNotEqual(const T1& u1, const T1& v1, 
                   const T2& u2, const T2& v2,
                   const T3& u3, const T3& v3,
                   const T4& u4, const T4& v4,
                   const T5& u5, const T5& v5,
                   const T6& u6, const T6& v6,
                   const T7& u7, const T7& v7)
{
 return (u1!=v1)||(u2!=v2)||(u3!=v3)||(u4!=v4)||(u5!=v5)||(u6!=v6)||(u7!=v7);
}


// ******************************************************************
 }
}

#endif
