#ifndef LAPH_QUARK_LINE_INFO_H
#define LAPH_QUARK_LINE_INFO_H

#include "chromabase.h"
#include "xml_help.h"
#include "gauge_configuration_info.h"
#include "dilution_scheme_info.h"

namespace Chroma {
  namespace LaphEnv {

// *********************************************************************
// *                                                                   *
// *  Class "QuarkLineEndInfo" holds information about an end of a     *
// *  valence quark line.  In particular, it stores                    *
// *                                                                   *
// *    (a) whether the end is a source (noise) or a sink (solution),  *
// *          and specifies "fixed" or "relative" source time          *
// *    (b) source time value or offset                                *
// *    (c) whether or not gamma-5 hermiticity needs to be applied     *
// *                                                                   *
// *  This class can also check that two objects of this class are     *
// *  the same or not, and can check that two objects of this class    *
// *  are a matching source/sink combination.                          *
// *                                                                   *
// *    XmlReader xml_in(...);                                         *
// *    QuarkLineEndInfo Qend(xml_in);                                 *
// *                                                                   *
// *   The XML input must have the form                                *
// *                                                                   *
// *    <QuarkLineEndInfo>                                             *
// *       <Type> ... </Type>                                          *
// *       <SourceTimeIndex> ... </SourceTimeIndex>                    *
// *       <Mode> ... </Mode>  (optional: default is normal)           *
// *    </QuarkLineEndInfo>                                            *
// *                                                                   *
// *   (1) The "Type" tag must be one of                               *
// *             "FixedSource"                                         *
// *             "RelativeSource"                                      *
// *             "SinkWithFixedSource"                                 *
// *             "SinkWithRelativeSource"                              *
// *       Both a fixed source and a relative source are needed since  *
// *       the spin-eigvec dilution scheme could be different.         *
// *                                                                   *
// *   (2) The "SourceTimeIndex" must be an integer from 0 to Nt-1,    *
// *       where Nt is the temporal extent of the lattice.  For a      *
// *       sink with fixed source time, the value of "SourceTimeIndex" *
// *       is the actual time slice of the quark source.  For a sink   *
// *       with relative source time, the value of "SourceTimeIndex"   *
// *       is the offset (going into the past) from the sink time,     *
// *       so a value of zero indicates the sink and source are        *
// *       always on the same time slice.  A value of 1 means that     *
// *       the source is on time t-1, where t is the sink time.        *
// *       The "SourceTimeIndex" is not used when type is a source.    *
// *                                                                   *
// *   (3) The "Mode" tag must have one of the following values:       *
// *             "Normal"         --> quark propagators used           *
// *                                    without modification           *
// *             "Gamma5HermConj" --> quark propagators used           *
// *                                    gamma-5 hermitian conjugation  *
// *                                                                   *
// *                                                                   *
// *********************************************************************                             


  // internal representation:  
  //    (a) right-most 2 bits are type
  //         0 = fixed source, 1 = relative source,
  //         2 = sink with fixed source,
  //         3 = sink with relative source
  //    (b) next bit from right is mode
  //         0 = normal, 1 = use gamma-5 hermiticity
  //    (c) remaining bits are source time index

class QuarkLineEndInfo
{

   unsigned int store;

 public:

   QuarkLineEndInfo() : store(0) {}

   QuarkLineEndInfo(XmlReader& xml_in);

   QuarkLineEndInfo(XmlReader& xml_in, const std::string& path);

      //    type = 0,1,2,3    mode = 0,1    
   QuarkLineEndInfo(int type, int mode, int source_index, int lat_time_extent)
    {if ((type<0)||(type>3)||(source_index<0)||(source_index>=lat_time_extent)){
        QDPIO::cerr << "invalid QuarkLineEndInfo assignment"<<endl;
        QDP_abort(1);}
     int imode=(mode==0)?0:1;
     int isource=(type<2)?0:source_index;
     encode(type,isource,imode);}

   QuarkLineEndInfo(const QuarkLineEndInfo& rhs) : store(rhs.store) {}

   QuarkLineEndInfo& operator=(const QuarkLineEndInfo& rhs)
    {store=rhs.store; return *this;}

   ~QuarkLineEndInfo() {}



   void setFixedSource() {store&=modemask;}

   void setRelativeSource() {store&=modemask; store|=0x1u;}

   void setSinkWithFixedSource(int source_index, int lat_time_extent)
    {if ((source_index<0)||(source_index>=lat_time_extent)){
        QDPIO::cerr << "invalid QuarkLineEndInfo assignment"<<endl;
        QDP_abort(1);}
     encode(2,source_index,(store&modemask)?1:0);}

   void setSinkWithRelativeSource(int source_index, int lat_time_extent)
    {if ((source_index<0)||(source_index>=lat_time_extent)){
        QDPIO::cerr << "invalid QuarkLineEndInfo assignment"<<endl;
        QDP_abort(1);}
     encode(3,source_index,(store&modemask)?1:0);}

   void setNormalMode() {store&= ~modemask;}

   void setGamma5HermConj() {store|= modemask;}



   void checkEqual(const QuarkLineEndInfo& rhs) const;

   bool operator==(const QuarkLineEndInfo& rhs) const
    {return (store==rhs.store);}

   bool operator!=(const QuarkLineEndInfo& rhs) const
    {return (store!=rhs.store);}

   bool operator<(const QuarkLineEndInfo& rhs) const
    {return (store<rhs.store);}

   bool match(const QuarkLineEndInfo& rhs) const;  // source-sink match up



   std::string output(int indent = 0) const;

   void output(XmlWriter& xmlout) const;


   std::string getHeader() const { return output(0); }

   void getHeader(XmlWriter& xmlout) const { output(xmlout); }



   int getSourceTime(int time, const GaugeConfigurationInfo& U) const;

   bool isNormalMode() const {return ((store&modemask)==0);}

   bool isGamma5HermConj() const {return ((store&modemask)!=0);}

   bool isSource() const {return ((store&typemask)<2);}

   bool isSink() const {return ((store&typemask)>1);}

   bool isFixedSource() const {return ((store&typemask)==0);}

   bool isRelativeSource() const {return ((store&typemask)==1);}

   bool isSinkWithFixedSource() const {return ((store&typemask)==2);}

   bool isSinkWithRelativeSource() const {return ((store&typemask)==3);}

   bool usesFixed() const {return ((store&0x1u)==0);}
  
   bool usesRelative() const {return ((store&0x1u)==1);}


 private:

   void encode(int type, int source, int mode)
    {
     store=source;
     store<<=1; store|=mode;
     store<<=2; store|=type;
    }

   void extract_info_from_reader(XmlReader& xmlr);

   static const unsigned int typemask=0x3u;
   static const unsigned int modemask=0x4u;
   
   friend class MesonHandler;
   friend class BaryonHandler;
   friend class MesonInternalLoopHandler;

};


// ****************************************************************
  }
}
#endif
