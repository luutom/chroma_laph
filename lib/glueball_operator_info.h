#ifndef LAPH_GLUEBALL_OPERATOR_INFO_H
#define LAPH_GLUEBALL_OPERATOR_INFO_H

#include "qdp.h"
#include "chromabase.h"
#include <set>
#include "momenta.h"
#include "xml_help.h"

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   Objects of class "GlueballOperatorInfo" store identifying     *
// *   info about one particular glueball operator.  The needed      *
// *   input format and the required storage format of the           *
// *   elementals in each glueball are described below.              *
// *                                                                 *
// *   Chroma input format for each glueball operator:               *
// *                                                                 *
// *       <GlueballOp>                                              *
// *          <Momentum>  0 1 -1  </Momentum>                        *
// *          <Irrep> A1um </Irrep>                                  *
// *          <IrrepRow> 1 </IrrepRow>                               *
// *          <SpatialType> TrEig </SpatialType>                     *
// *       </GlueballOp>                                             *
// *                                                                 *
// *   READING multiple glueball operators:                          *
// *                                                                 *
// *   Chroma input format:                                          *
// *                                                                 *
// *      <GlueballOperators>                                        *
// *           <GlueballOp>                                          *
// *            ...                                                  *
// *           </GlueballOp>                                         *
// *           <GlueballOp>                                          *
// *            ...                                                  *
// *           </GlueballOp>                                         *
// *         ...                                                     *
// *      </GlueballOperators>                                       *
// *                                                                 *
// *   Given the above XML input, one just needs to issue the        *
// *   following:                                                    *
// *                                                                 *
// *      XmlReader xml_in;  // assigned somehow                     *
// *      set<GlueballOperatorInfo> gset;                            *
// *      readGlueballOperators(xml_in,gset);                        *
// *                                                                 *
// *   The above code initializes the class, then reads all of       *
// *   glueball operators inside the <glueballOperators> tags in     *
// *   the input XML file, storing the results in a set.             *
// *                                                                 *
// *******************************************************************

class GlueballOperatorInfo
{

  unsigned icode;

 public:
 
  GlueballOperatorInfo(XmlReader& xml_in);

  GlueballOperatorInfo(const GlueballOperatorInfo& G) 
     :  icode(G.icode) {}

  GlueballOperatorInfo& operator=(const GlueballOperatorInfo& G)
   {icode=G.icode; 
    return *this;}

  GlueballOperatorInfo()  :  icode(0) {}

    // output functions

  Momentum getMomentum() const;

  int getXMomentum() const;

  int getYMomentum() const;

  int getZMomentum() const;

  std::string getIrrep() const;

  int getIrrepRow() const;

  std::string getSpatialType() const;
  
  bool needsLaphEigenvalues() const;

  std::string output() const;  // XML output of baryon tags

  void output(XmlWriter& xmlout) const;  // XML output of baryon tags

  bool operator==(const GlueballOperatorInfo& rhs) const;
  bool operator!=(const GlueballOperatorInfo& rhs) const;
  bool operator<(const GlueballOperatorInfo& rhs) const;

  friend class GlueballHandler;


};



// **************************************************

   //  useful routine for reading multiple GlueballOperators

void readGlueballOperators(XmlReader& xml_in, 
                           set<GlueballOperatorInfo>& gops);


// **************************************************
  }
}
#endif  
