#include "inline_meson_line_ends.h"
#include "chroma.h"
#include "time_slices.h"
//#include <sys/resource.h>

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 3)

  namespace InlineStochLaphMesonEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "StochLaphMesonInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochLaphMesonInlineMeas(xml_in, path);
}


const std::string name = "LAPH_MESON_LINE_ENDS";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}


// *********************************************************************

     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochLaphMesonInlineMeas::operator()(unsigned long update_no,
                                          XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

    //  read the common info first

 if (xml_tag_count(xml_rdr,"MesonCommonInfo")!=1){
    QDPIO::cerr << "Must have one <MesonCommonInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./MesonCommonInfo");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 FileListInfo mesonFiles(xmlr,"MesonFileList");

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "LAPH_MESON_LINE_ENDS");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "LAPH_MESON_LINE_ENDS");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

 if ((xml_tag_count(xmlr,"AntiQuark")!=1)
   ||(xml_tag_count(xmlr,"Quark")!=1)){
    QDPIO::cerr << "could not read the antiquark/quark infos"<<endl;
    QDP_abort(1);}

 XmlReader xml1(xmlr,"./AntiQuark");
 QuarkInfo quark1(xml1,gaugeinfo);
 XmlReader xml2(xmlr,"./Quark");
 QuarkInfo quark2(xml2,gaugeinfo);

    // echo common input before starting the computations

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 3B: Compute the meson line ends             *"<<endl;
 QDPIO::cout << " *                 and write to file as time slices        *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<"Meson File Stub:"<<endl<<mesonFiles.getFileStub()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"AntiQuark:"<<endl<<quark1.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"    Quark:"<<endl<<quark2.output()<<endl<<endl;

    //  check to see if just a simple merge is required

 int nmerge=xml_tag_count(xml_rdr,"Merge");
 if (nmerge>=1){
    if (QDP::Layout::numNodes()>1){
       QDPIO::cerr << "ERROR: Merge of meson handler output files must be run in serial"<<endl;
       QDP_abort(1);}
    vector<FileListInfo> vecmergefiles;
    
    for (int k=1;k<=nmerge;k++){
       XmlReader xmlg(xml_rdr,"./Merge["+int_to_string(k)+"]");
       FileListInfo mergefiles(xmlg);
       QDPIO::cout << endl<<" Merging meson handler output files"<<endl<<endl;
       QDPIO::cout << "     Files to merge:"<<endl;
       QDPIO::cout << "             Stub: "<<mergefiles.getFileStub()<<endl;
       QDPIO::cout << "  Min file number: "<<mergefiles.getMinFileNumber()<<endl;
       QDPIO::cout << "  Max file number: "<<mergefiles.getMaxFileNumber()<<endl<<endl;
       vecmergefiles.push_back(mergefiles);}
    MesonHandler MH(gaugeinfo,gSmear,quark1,quark2,mesonFiles,false);
    QDPIO::cout << "Starting the merge"<<endl;
    MH.reorganize(vecmergefiles);
    QDPIO::cout << endl<<"Merge complete."<<endl<<endl;
    return;}

 MesonHandler::QuarkFileLists Qfiles(xmlr);
 QDPIO::cout <<endl<<endl<<"Quark sink details:"<<endl<<Qfiles.output()<<endl<<endl;

    //  read the meson operators

 set<MesonOperatorInfo> MOps;
 string CoefsTopDirectory;
 readMesonOperators(xml_rdr,MOps,CoefsTopDirectory);

    //  read the meson line calculations to be done

 if (xml_tag_count(xml_rdr,"MesonLineCalculations")!=1){
    QDPIO::cerr << "Must have one <MesonLineCalculations> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlc(xml_rdr,"./MesonLineCalculations");
 set<MesonHandler::MesonLineCalc> mcalcs;
 int ncalc=xml_tag_count(xmlc,"MesonLineCalc");
 if (ncalc==0){
    QDPIO::cerr << "No meson line calculations given; nothing to do"<<endl;
    QDP_abort(1);}
 for (int k=1;k<=ncalc;++k) 
    mcalcs.insert(MesonHandler::MesonLineCalc(xmlc,k));

 int relsrctmin=0, relsrctmax=-1;
 xmlreadif(xmlc,"RelativeSourceMinimumTime",relsrctmin,"LAPH_MESON_LINE_ENDS");
 xmlreadif(xmlc,"RelativeSourceMaximumTime",relsrctmax,"LAPH_MESON_LINE_ENDS");

    // echo the input before starting the computations

// QDPIO::cout <<endl<<endl<<"Meson operators:"<<endl<<endl;
// for (set<MesonOperatorInfo>::const_iterator mit=MOps.begin();mit!=MOps.end();++mit)
//    QDPIO::cout << mit->output()<<endl;
 QDPIO::cout <<endl<<endl<<"Meson line calculations: ("
             <<mcalcs.size()<<" in total)" <<endl<<endl;
 for (set<MesonHandler::MesonLineCalc>::const_iterator mcit=mcalcs.begin();
      mcit!=mcalcs.end();++mcit)
    QDPIO::cout << mcit->output()<<endl;
 QDPIO::cout << endl<<endl;

     // read optional job name

 string jobname;
 xmlreadif(xml_rdr,"JobName",jobname,"LAPH_MESON_LINE_ENDS");

     // echo to the output xml file

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_MESON_LINE_ENDS");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 push(xml_out,"AntiQuark");
 quark1.output(xml_out);
 pop(xml_out);
 push(xml_out,"Quark");
 quark2.output(xml_out);
 pop(xml_out);
 Qfiles.output(xml_out);
 push(xml_out,"MesonOperators");
 for (set<MesonOperatorInfo>::const_iterator mit=MOps.begin();mit!=MOps.end();++mit)
    mit->output(xml_out);
 pop(xml_out);
 push(xml_out,"MesonLineCalculations");
 for (set<MesonHandler::MesonLineCalc>::const_iterator mcit=mcalcs.begin();
      mcit!=mcalcs.end();++mcit)
    mcit->output(xml_out);
 write(xml_out,"RelativeSourceMinimumTime",relsrctmin);
 write(xml_out,"RelativeSourceMaximumTime",relsrctmax);
 pop(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

   // construct the meson handler (NOT in read mode)

 MesonHandler MH(gaugeinfo,gSmear,quark1,quark2,mesonFiles,false);

 MH.compute(MOps,CoefsTopDirectory,Qfiles,mcalcs,
            smeared_gauge_file,smeared_quark_filestub,
            relsrctmin,relsrctmax);

 if (jobname.length()>0)
    QDPIO::cout <<endl<< "SUCCESSFUL MESON RUN: "<<jobname<<endl;

}

// ******************************************************************
  }
#endif
} // namespace Chroma

