#include "meson_operator_info.h"
#include "xml_help.h"
#include "multi_compare.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {



  // "current" element of xml_in should be <MesonOp>

  //  encode operator information into two unsigned 32-bit integers
  //   icode1:
  //      put all integers (except spatialIdNum) into a single unsigned integer 
  //         5 bits irrep row, 7 bits each momx, momy, momz
  //         (first bit is sign, 6 bits remaining), 6 bits displacement length
  //   icode2:
  //      spatial-index(17 bits),isospin+flavor(4),irrep(8),spatial-type(3)

MesonOperatorInfo::MesonOperatorInfo(XmlReader& xml_in)
{
 xml_tag_assert(xml_in,"MesonOp","MesonOperatorInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::MesonOp");

 string isospinName,flavor,irrep,spatialType;
 int momx,momy,momz,irrepRow,spatialIdNum,dispLength;

 xmlread(xmlr, "IsospinName", isospinName, "MesonOperatorInfo");
 isospinName=tidyString(isospinName);

 xmlread(xmlr, "Flavor", flavor, "MesonOperatorInfo");
 flavor=tidyString(flavor);

 multi1d<int> p;
 xmlread(xmlr, "Momentum", p, "MesonOperatorInfo");
 if (p.size()!=3){
    xml_cerr(xml_in,"Bad meson momentum");
    xmlreadfail(xml_in,"MesonOperatorInfo");}
 momx=p[0];
 momy=p[1];
 momz=p[2];

 xmlread(xmlr, "Irrep", irrep, "MesonOperatorInfo");
 irrep=tidyString(irrep);
 xmlread(xmlr, "SpatialType", spatialType, "MesonOperatorInfo");
 spatialType=tidyString(spatialType);
 xmlread(xmlr, "IrrepRow", irrepRow, "MesonOperatorInfo");
 xmlread(xmlr, "SpatialIdNum", spatialIdNum, "MesonOperatorInfo");
 xmlread(xmlr, "DispLength", dispLength, "MesonOperatorInfo");

 if ((dispLength<0)||(spatialIdNum<0)||(spatialIdNum>8192)||(irrepRow<1)){
    xml_cerr(xml_in,"Bad meson operator input xml data");
    xmlreadfail(xml_in,"MesonOperatorInfo");}
 if (irrepRow>31){
    xml_cerr(xml_in,"Irrep row > 31 not currently supported");
    QDP_abort(1);}
 if (dispLength>63){
    xml_cerr(xml_in,"Displacement length > 63 not currently supported");
    QDP_abort(1);}
 if ((abs(momx)>63)||(abs(momy)>63)||(abs(momz)>63)){
    xml_cerr(xml_in,"momentum component magnitude > 63 not currently supported");
    QDP_abort(1);}

 if ((spatialType!="SS")&&(dispLength==0)){
    xml_cerr(xml_in,"Bad meson operator input xml data");
    xml_cerr(xml_in,"Operator with displacement must have displacement length > 0");
    xmlreadfail(xml_in,"MesonOperatorInfo");}

    // check if a single-site operator, then make the displacement length zero
 if (spatialType=="SS") dispLength=0;

    //  now do the encoding
 icode1=irrepRow; 
 icode1<<=1; icode1|=(momx<0)?1:0; 
 icode1<<=6; icode1|=abs(momx);
 icode1<<=1; icode1|=(momy<0)?1:0;
 icode1<<=6; icode1|=abs(momy);
 icode1<<=1; icode1|=(momz<0)?1:0;
 icode1<<=6; icode1|=abs(momz);
 icode1<<=6; icode1|=dispLength;

 unsigned int isofl=0,ir1=0,ir2=0,ir3=0,ir4=0,st=0;
 /*
 if      (isospinName=="isovector") iso=0;
 else if (isospinName=="isoscalar") iso=1;
 else if (isospinName=="ssbar"    ) iso=2;
 else if (isospinName=="kaon"     ) iso=3;
 else{
    xml_cerr(xml_in,"isospin "+isospinName+" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}

 if      (flavor=="du") fl=0;
 else if (flavor=="uu") fl=1;
 else if (flavor=="ss") fl=2;
 else if (flavor=="su") fl=3;
 else{
    xml_cerr(xml_in,"flavor "+flavor+" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}
 */

 if      ((isospinName=="isovector")&&(flavor=="du")) isofl=0;
 else if ((isospinName=="isoscalar")&&(flavor=="uu")) isofl=5;
 else if ((isospinName=="isoscalar")&&(flavor=="ss")) isofl=6;
 else if ((isospinName=="kaon"     )&&(flavor=="su")) isofl=15;
 else if ((isospinName=="antikaon" )&&(flavor=="ds")) isofl=8;
 else{
    xml_cerr(xml_in,"isospin "+isospinName+" flavor "+flavor
              +" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}

 if      (spatialType=="SS" ) st=0;
 else if (spatialType=="SD" ) st=1;
 else if (spatialType=="LSD") st=2;
 else if (spatialType=="TSD") st=3;
 else if (spatialType=="DDL") st=4;
 else if (spatialType=="TDU") st=5;
 else if (spatialType=="TDO") st=6;
 else{
    xml_cerr(xml_in,"spatialType "+spatialType+" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}

 string irp=irrep+"    ";  // so length at least 4 char
 if      (irp[0]=='A') ir1=0;
 else if (irp[0]=='B') ir1=1;
 else if (irp[0]=='E') ir1=2;
 else if (irp[0]=='T') ir1=3;
 else{
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}

 int k=1;
 if (irp[k]=='1'){ ir2=0; k++;}
 else if (irp[k]=='2'){ ir2=1; k++;}
 else ir2=2;
 
 if (irp[k]=='g'){ ir3=0; k++;}
 else if (irp[k]=='u'){ ir3=1; k++;}
 else ir3=2;
 
 if (irp[k]=='p'){ ir4=0; k++;}
 else if (irp[k]=='m'){ ir4=1; k++;}
 else ir4=2;
 
 if (k!=int(irrep.length())){
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in MesonOperatorInfo");
    QDP_abort(1);}
 
 icode2=spatialIdNum;
// icode2<<=2; icode2|=iso; 
// icode2<<=2; icode2|=fl;
 icode2<<=4; icode2|=isofl; 
 icode2<<=2; icode2|=ir1; 
 icode2<<=2; icode2|=ir2;
 icode2<<=2; icode2|=ir3;
 icode2<<=2; icode2|=ir4;
 icode2<<=3; icode2|=st;

} 


std::string MesonOperatorInfo::getIsospinName() const
{
/* unsigned int iso=(icode2>>13)& 0x3u;
 if      (iso==0) return string("isovector");
 else if (iso==1) return string("isoscalar");
 else if (iso==2) return string("ssbar");
 else if (iso==3) return string("kaon");
 return string(""); */
 unsigned int isofl=(icode2>>11)& 0xFu;
 if      (isofl==0)  return string("isovector");
 else if (isofl==5)  return string("isoscalar");
 else if (isofl==6)  return string("isoscalar");
 else if (isofl==15) return string("kaon");
 else if (isofl==8)  return string("antikaon");
 return string("");
}
 
std::string MesonOperatorInfo::getFlavor() const
{
/* unsigned int fl=(icode2>>11)& 0x3u;
 if      (fl==0) return string("du");
 else if (fl==1) return string("uu");
 else if (fl==2) return string("ss");
 else if (fl==3) return string("su");
 return string(""); */
 unsigned int isofl=(icode2>>11)& 0xFu;
 if      (isofl==0)  return string("du");
 else if (isofl==5)  return string("uu");
 else if (isofl==6)  return string("ss");
 else if (isofl==15) return string("su");
 else if (isofl==8)  return string("ds");
 return string("");
}

Momentum MesonOperatorInfo::getMomentum() const
{
 unsigned int tmp=(icode1>>6);
 int pz=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) pz=-pz;
 tmp>>=7;
 int py=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) py=-py;
 tmp>>=7;
 int px=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) px=-px;
 return Momentum(px,py,pz);
}

int MesonOperatorInfo::getXMomentum() const
{
 unsigned int tmp=(icode1>>20);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}
  
int MesonOperatorInfo::getYMomentum() const
{
 unsigned int tmp=(icode1>>13);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}

int MesonOperatorInfo::getZMomentum() const
{
 unsigned int tmp=(icode1>>6);
 int res=tmp & 0x3Fu;
 if (((tmp>>6)&0x1u)==1) return -res;
 return res;
}

std::string MesonOperatorInfo::getIrrep() const
{
 unsigned int irp=icode2>>3;
 unsigned int ir4=irp & 0x3u; irp>>=2;
 unsigned int ir3=irp & 0x3u; irp>>=2;
 unsigned int ir2=irp & 0x3u; irp>>=2;
 unsigned int ir1=irp & 0x3u; irp>>=2;
 string irrep;
 if      (ir1==0) irrep+='A';
 else if (ir1==1) irrep+='B';
 else if (ir1==2) irrep+='E';
 else if (ir1==3) irrep+='T';
 if      (ir2==0) irrep+='1';
 else if (ir2==1) irrep+='2';
 if      (ir3==0) irrep+='g';
 else if (ir3==1) irrep+='u'; 
 if      (ir4==0) irrep+='p';
 else if (ir4==1) irrep+='m';
 return irrep;
}

int MesonOperatorInfo::getIrrepRow() const
{
 return (icode1>>27);
}

std::string MesonOperatorInfo::getSpatialType() const
{
 unsigned int st= icode2 & 0x7u;
 if      (st==0) return string("SS");
 else if (st==1) return string("SD");
 else if (st==2) return string("LSD");
 else if (st==3) return string("TSD");
 else if (st==4) return string("DDL");
 else if (st==5) return string("TDU");
 else if (st==6) return string("TDO");
 return string("");
}

int MesonOperatorInfo::getSpatialIdNumber() const
{
 return (icode2>>15);
}

int MesonOperatorInfo::getDisplacementLength() const
{
 return icode1 & 0x3F;
}


bool MesonOperatorInfo::operator==(const MesonOperatorInfo& rhs) const
{
 return multiEqual(icode1,rhs.icode1, icode2,rhs.icode2);   
}

bool MesonOperatorInfo::operator!=(const MesonOperatorInfo& rhs) const
{
 return multiNotEqual(icode1,rhs.icode1, icode2,rhs.icode2);   
}

bool MesonOperatorInfo::operator<(const MesonOperatorInfo& rhs) const
{
 return multiLessThan(icode1,rhs.icode1, icode2,rhs.icode2);   
}


string MesonOperatorInfo::output() const
{
 ostringstream oss;
 oss << "<MesonOp>"<<endl;
 oss << "  <IsospinName> " << getIsospinName() << " </IsospinName>"<<endl;
 oss << "  <Flavor> "<< getFlavor()<<" </Flavor>"<<endl;
 oss << "  <Momentum> " <<getXMomentum()<<" "<<getYMomentum()
      <<" "<<getZMomentum()<<" </Momentum>"<<endl;
 oss << "  <Irrep> "<<getIrrep()<<" </Irrep>"<<endl;
 oss << "  <IrrepRow> "<<getIrrepRow()<<" </IrrepRow>"<<endl;
 oss << "  <SpatialType> "<<getSpatialType()<<" </SpatialType>"<<endl;
 oss << "  <SpatialIdNum> "<<getSpatialIdNumber()
      <<" </SpatialIdNum>"<<endl;
 oss << "  <DispLength> "<<getDisplacementLength()
      <<" </DispLength>"<<endl;
 oss << "</MesonOp>"<<endl;

 return oss.str();
}

void MesonOperatorInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"MesonOp");
 write(xmlout,"IsospinName",getIsospinName());
 write(xmlout,"Flavor",getFlavor());
 multi1d<int> p(3);  p[0]=getXMomentum(); 
 p[1]=getYMomentum(); p[2]=getZMomentum();
 write(xmlout,"Momentum",p);
 write(xmlout,"Irrep",getIrrep());
 write(xmlout,"IrrepRow",getIrrepRow());
 write(xmlout,"SpatialType",getSpatialType());
 write(xmlout,"SpatialIdNum",getSpatialIdNumber());
 write(xmlout,"DispLength",getDisplacementLength());
 pop(xmlout);
}


 // ********************************************************

   //  useful routine for reading multiple MesonOperatorInfo;
   //  also reads name of coefficient file

void readMesonOperators(XmlReader& xml_in, 
                        set<MesonOperatorInfo>& mops,
                        string& coef_dir)
{
 if (xml_tag_count(xml_in,"MesonOperators")!=1){
    xml_cerr(xml_in,"XML input to CreateMesonOperators must have");
    xml_cerr(xml_in," a single <MesonOperators> ... </MesonOperators>");
    throw(string("error"));}
 mops.clear();

 xmlread(xml_in,"MesonOperators/ElementalCoefficientDirectory",coef_dir,
         "readMesonOperators");
 coef_dir=tidyString(coef_dir);

 int num_mop=xml_tag_count(xml_in,"MesonOperators/MesonOp");
 for (int k=1;k<=num_mop;k++){
    ostringstream path;
    path << "./descendant-or-self::MesonOperators/MesonOp["<<k<<"]";
    XmlReader xml_op(xml_in,path.str());
    mops.insert(MesonOperatorInfo(xml_op));}
}

// **************************************************************
  }
}
