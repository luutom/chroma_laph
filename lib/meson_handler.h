#ifndef MESON_HANDLER_H
#define MESON_HANDLER_H

#include "qdp.h"
#include "chromabase.h"
#include "gauge_configuration_info.h"
#include "gauge_configuration_handler.h"
#include "xml_help.h"
#include "field_smearing_info.h"
#include "field_smearing_handler.h"
#include "dilution_scheme_info.h"
#include "laph_noise_info.h"
#include "quark_info.h"
#include "quark_line_end_info.h"
#include "quark_handler.h"
#include "meson_operator.h"
#include "filelist_info.h"
#include "time_slices.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {

#if (QDP_ND == 3)

// *****************************************************************
// *                                                               *
// *  "MesonHandler" handles computation of and subsequent access  *
// *  to the meson line ends.  When opened NOT in read mode, it    *
// *  uses the "compute" function to calculate the meson line      *
// *  ends and output them to binary file.  When opened in read    *
// *  mode, one uses the "queryData" and "getData" members to      *
// *  read the previously-calculated meson line ends from file.    *
// *                                                               *
// *  One of these handlers deals with meson line ends for         *
// *  **one** set of common info parameters, given by              *
// *                                                               *
// *         GaugeConfigurationInfo                                *
// *         GluonSmearingInfo                                     *
// *         QuarkInfo (for each of the antiquark/quark)           *
// *         FileListInfo                                          *
// *                                                               *
// *  but one handler deals with different                         *
// *                                                               *
// *         meson operators                                       *
// *         time dilution projector indices                       *
// *         noises                                                *
// *         all spin-eigvec dilution indices                      *
// *                                                               *
// *  File structure and contents:                                 *
// *                                                               *
// *   - Results manipulated by one handler are contained in       *
// *     several files.  Each file has the same stub, but          *
// *     different positive integer suffices.                      *
// *             stub.0                                            *
// *             stub.1                                            *
// *             ...                                               *
// *             stub.N                                            *
// *     The files included are specified in a FileListInfo.       *
// *                                                               *
// *   - The header info in each file has a common part and a      *
// *     part that is specific to that one file:                   *
// *        <MesonHandlerDataFile>                                 *
// *           - common part                                       *
// *           - specific part                                     *
// *        </MesonHandlerDataFile>                                *
// *                                                               *
// *   - Info common to ALL files in FileListInfo:                 *
// *                                                               *
// *       GaugeConfigurationInfo                                  *
// *       GluonSmearingInfo                                       *
// *       QuarkInfo (for each of the antiquark/quark)             *
// *                                                               *
// *   - Info specific to EACH file in FileListInfo:               *
// *                                                               *
// *       MesonOperatorInfo  (see comments below)                 *
// *                                                               *
// *   - DataBase Key for records in EACH file are specified by    *
// *     a MesonHandler::RecordKey                                 *
// *                                                               *
// *       QuarkLineEndInfo (for each of the antiquark/quark)      *
// *       LaphNoiseInfo (for each of the antiquark/quark)         *
// *       time value                                              *
// *                                                               *
// *     Each record has various dilution indices. Each DB data    *
// *     element has the form                                      *
// *                                                               *
// *       multi2d<Complex> data;                                  *
// *       data(dilind1,dilind2);                                  *
// *                                                               *
// *  This handler is NOT meant for helping to compute the meson   *
// *  internal loops. Use the MesonInternalLoopHander for that.    *
// *                                                               *
// *                                                               *
// *     Many different meson operators are constructed            *
// *     simultaneously (since they have common elemental          *
// *     operators).                                               *
// *                                                               *
// *  All Laph Handlers follow the member naming convention:       *
// *                                                               *
// *    compute....()  to do original computation                  *
// *    getData()      provides access to results                  *
// *    queryData()    to check if data is available               *
// *                                                               *
// *  "MesonLineCalc" is defined in this handler.  It contains     *
// *  the line types of each quark (sink or source, fixed or       *
// *  relative time), the mode of each quark line (normal or       *
// *  with gamma5 hermitian conjugate), and maximum time           *
// *  separation.  This is used only in the "compute" member.      *
// *                                                               *
// *  In each calculation, the following assumptions are made:     *
// *   -- all relative-scheme lines use sink=source times          *
// *   -- all fixed-scheme lines use the same source time          *
// *   -- if all lines use relative scheme, max_sep is set to zero *
// *        and all start times are computed                       *
// *   -- any fixed source (not sink) line sets max_sep to zero    *
// *                                                               *
// *                                                               *
// *  Important REMARKS:   The file structure discussed above      *
// *  is the **final** form used by the get handlers.  However,    *
// *  the compute subroutine produces a small number of files      *
// *  that are much larger, containing multiple hadron operators.  *
// *  This is necessary since Lustre does not deal well with       *
// *  many small files.  The idea here is to compute the larger    *
// *  but fewer files on XSEDE machines that use the Lustre file   *
// *  system, tar these fewer files up, then copy to CMU, and use  *
// *  the "reorganize" subroutine to extract the different hadron  *
// *  operators into different files, and to combine the different *
// *  hadron lines into the same files.  After the reorganization, *
// *  all hadron line ends for one hadron operator should be       *
// *  gathered together into a single file.                        *
// *                                                               *
// *****************************************************************


class MesonHandler
{

 public:

   struct MesonNoiseInfo 
    {
       LaphNoiseInfo noise1,noise2; 

       MesonNoiseInfo(XmlReader& xmlin) 
         : noise1(xmlin,"MesonNoise/AntiQuarkLine"),
           noise2(xmlin,"MesonNoise/QuarkLine") {}

       MesonNoiseInfo(XmlReader& xmlin, int k) 
         : noise1(xmlin,"MesonNoise["+int_to_string(k)+"]/AntiQuarkLine"),
           noise2(xmlin,"MesonNoise["+int_to_string(k)+"]/QuarkLine") {}

       MesonNoiseInfo(const LaphNoiseInfo& in_noise1,
                      const LaphNoiseInfo& in_noise2) 
        : noise1(in_noise1), noise2(in_noise2) {}

       MesonNoiseInfo(const MesonNoiseInfo& in) 
        : noise1(in.noise1), noise2(in.noise2) {}

       MesonNoiseInfo& operator=(const MesonNoiseInfo& in)
        {noise1=in.noise1; noise2=in.noise2; return *this;}

       ~MesonNoiseInfo(){}

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool operator<(const MesonNoiseInfo& rhs) const
        {return multiLessThan(noise1,rhs.noise1, noise2,rhs.noise2);}

       bool operator==(const MesonNoiseInfo& rhs) const
        {return multiEqual(noise1,rhs.noise1, noise2,rhs.noise2);}

       bool operator!=(const MesonNoiseInfo& rhs) const
        {return multiNotEqual(noise1,rhs.noise1, noise2,rhs.noise2);}

    };


  struct MesonLineEndInfo
    {
       QuarkLineEndInfo qline1,qline2; 

       MesonLineEndInfo(XmlReader& xmlin) 
         : qline1(xmlin,"MesonLineEndInfo/AntiQuarkLine"),
           qline2(xmlin,"MesonLineEndInfo/QuarkLine") {}

       MesonLineEndInfo(XmlReader& xmlin, int k) 
         : qline1(xmlin,"MesonLineEndInfo["+int_to_string(k)+"]/AntiQuarkLine"),
           qline2(xmlin,"MesonLineEndInfo["+int_to_string(k)+"]/QuarkLine") {}

       MesonLineEndInfo(const QuarkLineEndInfo& in_qline1, 
                        const QuarkLineEndInfo& in_qline2)
         : qline1(in_qline1), qline2(in_qline2) {}

       MesonLineEndInfo(const MesonLineEndInfo& in)
         : qline1(in.qline1), qline2(in.qline2) {}

       MesonLineEndInfo& operator=(const MesonLineEndInfo& in)
         {qline1=in.qline1; qline2=in.qline2; return *this;}

       ~MesonLineEndInfo() {}

       bool operator<(const MesonLineEndInfo& rhs) const
        {return multiLessThan(qline1,rhs.qline1, qline2,rhs.qline2);}

       bool operator==(const MesonLineEndInfo& rhs) const
        {return multiEqual(qline1,rhs.qline1, qline2,rhs.qline2);}

       bool operator!=(const MesonLineEndInfo& rhs) const
        {return multiNotEqual(qline1,rhs.qline1, qline2,rhs.qline2);}
    };


       // MesonLineEndInfo stores two quark lines which hold info
       // about the mode and type, but ALSO about the time projector
       // indices.  MesonLineCalc only holds info about the mode/type.
       // The set of meson noises to use is also stored.

   class MesonLineCalc
    {                        // q2, q1 order stored in code
       unsigned int code;    // for each quark, 3 bits (mode, src/snk, fxd/rel)
       int min_time_sep;     // min time separation (forced to be zero if all
                             // lines rel or one line is source+fixed)
       int max_time_sep;     // max time separation (forced to be zero if all
                             // lines rel or one line is source+fixed)
       set<MesonNoiseInfo> mnoises;

     public:

       MesonLineCalc(XmlReader& xmlin, int k);

       MesonLineCalc(const MesonLineCalc& in)
         : code(in.code), min_time_sep(in.min_time_sep),
           max_time_sep(in.max_time_sep), mnoises(in.mnoises)  {}

       MesonLineCalc& operator=(const MesonLineCalc& in)
         {code=in.code; mnoises=in.mnoises;
          min_time_sep=in.min_time_sep;
          max_time_sep=in.max_time_sep;
          return *this;}

       ~MesonLineCalc() {}

          // If noises were input as "<All/>", this routine takes the
          // available noises in "antiQuarkNoises" and "QuarkNoises"
          // and forms all possible MesonNoiseInfo objects in "mnoises".
          // Otherwise, it checks the current "mnoises" and removes those
          // noises that are not available as specified by "antiQuarkNoises" 
          // and "QuarkNoises". Also enforces that the quark/antiquark noises 
          // in the meson are different.

       set<MesonNoiseInfo> getAvailableNoises(
                      const set<LaphNoiseInfo>& antiQuarkNoises,
                      const set<LaphNoiseInfo>& QuarkNoises) const;

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool isSource(int quark) const;

       bool isNormalMode(int quark) const;

       bool isGamma5HermConjMode(int quark) const;

       bool isFixedSourceScheme(int quark) const;

       bool isRelativeSource(int quark) const;

       int getMinTimeSeparation() const {return min_time_sep;}

       int getMaxTimeSeparation() const {return max_time_sep;}

       bool canShareQuarkHandler(int quarkA, int quarkB) const;

       QuarkLineEndInfo getLineInfo(int quark, int t0, int lat_textent) const;


       bool operator<(const MesonLineCalc& rhs) const
        {return multiLessThan(code,rhs.code, min_time_sep,rhs.min_time_sep,
                              max_time_sep,rhs.max_time_sep);}

       bool operator==(const MesonLineCalc& rhs) const
        {return multiEqual(code,rhs.code, min_time_sep,rhs.min_time_sep,
                           max_time_sep,rhs.max_time_sep);}

       bool operator!=(const MesonLineCalc& rhs) const
        {return multiNotEqual(code,rhs.code, min_time_sep,rhs.min_time_sep,
                              max_time_sep,rhs.max_time_sep);}

     private:

       unsigned int read_type(XmlReader& xmlin, const std::string& tag);
       unsigned int read_mode(XmlReader& xmlin, const std::string& tag);
       std::string write_type(unsigned int end_type) const;
       std::string write_mode(unsigned int mode) const;

    };


   class QuarkFileLists
    {
       FileListInfo q1fxdfiles,q2fxdfiles;
       FileListInfo q1relfiles,q2relfiles;

     public:

       QuarkFileLists(XmlReader& xmlin)
         : q1fxdfiles(xmlin,"AntiQuark/QuarkInfo/FixedSource"),
           q2fxdfiles(xmlin,"Quark/QuarkInfo/FixedSource"),
           q1relfiles(xmlin,"AntiQuark/QuarkInfo/RelativeSource"),
           q2relfiles(xmlin,"Quark/QuarkInfo/RelativeSource") {}

       QuarkFileLists(const FileListInfo& q1fixed, const FileListInfo& q1rel,
                      const FileListInfo& q2fixed, const FileListInfo& q2rel)
         :  q1fxdfiles(q1fixed),q2fxdfiles(q2fixed),
            q1relfiles(q1rel),q2relfiles(q2rel) {}

       ~QuarkFileLists() {}

       QuarkFileLists(const QuarkFileLists& in)
         :  q1fxdfiles(in.q1fxdfiles),q2fxdfiles(in.q2fxdfiles),
            q1relfiles(in.q1relfiles),q2relfiles(in.q2relfiles){}

       QuarkFileLists& operator=(const QuarkFileLists& in)
         {q1fxdfiles=in.q1fxdfiles; q2fxdfiles=in.q2fxdfiles;
          q1relfiles=in.q1relfiles; q2relfiles=in.q2relfiles; return *this;}

       const FileListInfo& getFixedSchemeFiles(int quark) const;

       const FileListInfo& getRelativeSchemeFiles(int quark) const;

       void output(XmlWriter& xmlout) const;

       std::string output() const;

    };


   struct RecordKey
    {
       LaphNoiseInfo noise1,noise2;
       QuarkLineEndInfo qline1,qline2;
       int time_value;

       RecordKey();
       RecordKey(const LaphNoiseInfo& in_noise1, 
                 const LaphNoiseInfo& in_noise2,
                 const QuarkLineEndInfo& in_qline1,
                 const QuarkLineEndInfo& in_qline2,
                 int in_time);
       RecordKey(const MesonNoiseInfo& in_mnoise, 
                 const MesonLineEndInfo& in_mline,
                 int in_time);
       RecordKey(const RecordKey& in);
       RecordKey& operator=(const RecordKey& in);
       ~RecordKey();
       
       bool operator<(const RecordKey& rhs) const;
       bool operator==(const RecordKey& rhs) const;
       bool operator!=(const RecordKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       RecordKey(const unsigned int* buf)
       {noise1.store=buf[0]; qline1.store=buf[1];
        noise2.store=buf[2]; qline2.store=buf[3];
        time_value=buf[4];}

       int numints() const {return 5;}
       size_t numbytes() const {return 5*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {buf[0]=noise1.store; buf[1]=qline1.store;
        buf[2]=noise2.store; buf[3]=qline2.store;
        buf[4]=time_value;}
       
    }; 

   struct CombinedKey
    {
       RecordKey rkey;
       MesonOperatorInfo mop;

       CombinedKey();
       CombinedKey(const MesonOperatorInfo& in_mop,
                   const RecordKey& in_rkey);
       CombinedKey(const CombinedKey& in);
       CombinedKey& operator=(const CombinedKey& in);
       ~CombinedKey();
       
       bool operator<(const CombinedKey& rhs) const;
       bool operator==(const CombinedKey& rhs) const;
       bool operator!=(const CombinedKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       CombinedKey(const unsigned int* buf) : rkey(buf)
       {mop.icode1=buf[5]; mop.icode2=buf[6];}

       int numints() const {return 7;}
       size_t numbytes() const {return 7*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {rkey.copyTo(buf); buf[5]=mop.icode1; buf[6]=mop.icode2;}

    }; 


   typedef MesonOperatorInfo  FileKey;
   typedef multi2d<Complex>   DataType;


 protected:

       // pointers to internal infos (managed by this handler
       // with new and delete)

   const GaugeConfigurationInfo *uPtr;
   const GluonSmearingInfo *gSmearPtr;
   const QuarkInfo *quark1Ptr, *quark2Ptr;
   const FileListInfo *fPtr;


       // pointers to needed sub-handlers (managed by this handler)
 
   QuarkHandler *qHandler1, *qHandler2;


       // data I/O handler pointers

   DataPutHandlerMF<MesonHandler,FileKey,RecordKey,DataType> *DHputPtr;
   DataGetHandlerMF<MesonHandler,FileKey,RecordKey,DataType> *DHgetPtr;
   DataPutHandlerSF<MesonHandler,CombinedKey,DataType> *DHSFputPtr;
   DataGetHandlerSF<MesonHandler,CombinedKey,DataType> *DHSFgetPtr;
   bool m_read_mode;


       // Prevent copying ... handler might contain large
       // amounts of data

   MesonHandler(const MesonHandler&);
   MesonHandler& operator=(const MesonHandler&);



 public:


   MesonHandler();

   MesonHandler(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const QuarkInfo& antiquark, 
                const QuarkInfo& quark,
                const FileListInfo& mesonfilelist,
                bool read_mode=true);

   void setInfo(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const QuarkInfo& antiquark, 
                const QuarkInfo& quark,
                const FileListInfo& mesonfilelist,
                bool read_mode=true);

   ~MesonHandler();

   void clear();



   bool isInfoSet() const;

   const GaugeConfigurationInfo& getGaugeConfigurationInfo() const;

   const GluonSmearingInfo& getGluonSmearingInfo() const;

   const QuarkInfo& getQuarkInfo(int quark_number) const;

   const FileListInfo& getFileListInfo() const;

   void getHeader(XmlWriter& xmlout) const;

   void getFileMap(XmlWriter& xmlout) const;

   void outputKeys(XmlWriter& xmlout);


        //  Compute line ends for all meson operators in "MopInfos" and
        //  all dilution indices.  Repeat for all requested meson calculations
        //  given in "mcalcs" (which contains the meson noises to compute).
        //  If the list of meson noises is empty, all available noises 
        //  in the quark sink files are used. Handler must NOT be in read mode.
        //  For relative sources, only times from "relativesourcetmin" to 
        //  "relativesourcetmax" will be done (if "relativesourcetmax" is -1,
        //  then ALL times on the lattice are done.
 
   void compute(const set<MesonOperatorInfo>& MopInfos,
                const string& CoefsTopDirectory,
                const QuarkFileLists& Qfiles,
                const set<MesonLineCalc>& mcalcs,
                const string& smeared_gauge_filename,
                const string& smeared_quark_filestub,
                int relativesourcetmin=0, int relativesourcetmax=-1);


        //  Merge the meson line ends in "infiles" into the files in
        //  "mesonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "mesonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
        //  WARNING:  This routine is meant to be used by SERIAL code only.
        //    It will still work in parallel, but a lot of unnecessary broadcasts
        //    are done.
 
   void merge(const FileListInfo& infiles);
   void merge(const vector<FileListInfo>& infiles);
   void reorganize(const vector<FileListInfo>& infiles);

        // read from file (loop through file list to find)
        // when in read mode

   const DataType& getData(const MesonOperatorInfo& mop,
                           const MesonLineEndInfo& mline,
                           const MesonNoiseInfo& mnoise,
                           int timevalue);

   const DataType& getData(const MesonOperatorInfo& mop,
                           const QuarkLineEndInfo& qbarline,
                           const QuarkLineEndInfo& qline,
                           const LaphNoiseInfo& qbarnoise,
                           const LaphNoiseInfo& qnoise,
                           int timevalue);


          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

   bool queryData(const MesonOperatorInfo& mop,
                  const MesonLineEndInfo& mline,
                  const MesonNoiseInfo& mnoise,
                  int timevalue);

   bool queryData(const MesonOperatorInfo& mop,
                  const QuarkLineEndInfo& qbarline,
                  const QuarkLineEndInfo& qline,
                  const LaphNoiseInfo& qbarnoise,
                  const LaphNoiseInfo& qnoise,
                  int timevalue); 

   bool queryData(const MesonOperatorInfo& mop);


        // remove from internal memory

   void removeData(const MesonOperatorInfo& mop,
                   const MesonLineEndInfo& mline,
                   const MesonNoiseInfo& mnoise,
                   int timevalue);

   void removeData(const MesonOperatorInfo& mop,
                   const QuarkLineEndInfo& qbarline,
                   const QuarkLineEndInfo& qline,
                   const LaphNoiseInfo& qbarnoise,
                   const LaphNoiseInfo& qnoise,
                   int timevalue); 

   void removeData(const MesonOperatorInfo& mop);


   void clearData();   

          // more query subroutines

   set<MesonOperatorInfo> getMesonOperators() const;

   set<MesonNoiseInfo> getMesonNoises(const MesonOperatorInfo& minfo) const; 

   set<MesonLineEndInfo> getMesonLineEndInfos(const MesonOperatorInfo& minfo) const;


 private:


   void set_info(const GaugeConfigurationInfo& gaugeinfo,
                 const GluonSmearingInfo& smear,
                 const QuarkInfo& antiquark, 
                 const QuarkInfo& quark,
                 const FileListInfo& flist,
                 bool read_mode);


   bool checkHeader(XmlReader& xmlr, int suffix);
   void writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                    int suffix);

   bool checkHeader(XmlReader& xmlr);
   void writeHeader(XmlWriter& xmlout);

   void connectQuarkHandlers(const MesonLineCalc& Bcalc,
                             const QuarkFileLists& Qfiles,
                             const std::string& smeared_gauge_filename,
                             const std::string& smeared_quark_filestub);
   void disconnectQuarkHandlers();
   void filefail(const std::string& message);
   void check_info_set(const std::string& name,
                       int check_mode=0) const;

   void evaluateLocalInnerProduct(const LatticeColorVector *v1,
                                  const LatticeColorVector *v2,
                                  auto_ptr<LatticeComplex>& result);

   friend class DataPutHandlerMF<MesonHandler,FileKey,RecordKey,DataType>;
   friend class DataGetHandlerMF<MesonHandler,FileKey,RecordKey,DataType>;
   friend class DataPutHandlerSF<MesonHandler,CombinedKey,DataType>;
   friend class DataGetHandlerSF<MesonHandler,CombinedKey,DataType>;

};


// ***************************************************************



// *****************************************************************
// *                                                               *
// *  "MesonInternalLoopHandler" handles computation of and        *
// *  subsequent access to the meson line ends of isoscalars in    *
// *  which there is an internal loop.  Access is through the      *
// *  **quark** info, NOT the antiquark.  Info about the antiquark *
// *  is inferred from the info of the quark.                      *
// *                                                               *
// *****************************************************************


class MesonInternalLoopHandler
{

 public:

   struct MesonIntLoopNoiseInfo 
    {
       LaphNoiseInfo noise;

       MesonIntLoopNoiseInfo(XmlReader& xmlin) 
         : noise(xmlin,"MesonNoise") {}

       MesonIntLoopNoiseInfo(XmlReader& xmlin, int k) 
         : noise(xmlin,"MesonNoise["+int_to_string(k)+"]") {}

       MesonIntLoopNoiseInfo(const LaphNoiseInfo& in_noise) 
        : noise(in_noise) {}

       MesonIntLoopNoiseInfo(const MesonIntLoopNoiseInfo& in) 
        : noise(in.noise) {}

       MesonIntLoopNoiseInfo& operator=(const MesonIntLoopNoiseInfo& in)
        {noise=in.noise; return *this;}

       ~MesonIntLoopNoiseInfo(){}

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool operator<(const MesonIntLoopNoiseInfo& rhs) const
        {return (noise < rhs.noise);}

       bool operator==(const MesonIntLoopNoiseInfo& rhs) const
        {return (noise==rhs.noise);}

       bool operator!=(const MesonIntLoopNoiseInfo& rhs) const
        {return (noise!=rhs.noise);}

    };


  struct MesonIntLoopLineEndInfo
    {
       QuarkLineEndInfo qline; 

       MesonIntLoopLineEndInfo(XmlReader& xmlin) 
         : qline(xmlin,"MesonIntLoopInfo/QuarkLine") {}

       MesonIntLoopLineEndInfo(XmlReader& xmlin, int k) 
         : qline(xmlin,"MesonIntLoopInfo/QuarkLine["+int_to_string(k)+"]") {}

       MesonIntLoopLineEndInfo(const QuarkLineEndInfo& in_qline)
         : qline(in_qline) {}

       MesonIntLoopLineEndInfo(const MesonIntLoopLineEndInfo& in)
         : qline(in.qline) {}

       MesonIntLoopLineEndInfo& operator=(const MesonIntLoopLineEndInfo& in)
         {qline=in.qline; return *this;}

       ~MesonIntLoopLineEndInfo() {}

       bool operator<(const MesonIntLoopLineEndInfo& rhs) const
        {return (qline<rhs.qline);}

       bool operator==(const MesonIntLoopLineEndInfo& rhs) const
        {return (qline==rhs.qline);}

       bool operator!=(const MesonIntLoopLineEndInfo& rhs) const
        {return (qline!=rhs.qline);}
    };


       // MesonIntLoopLineEndInfo stores the quark line which holds info
       // about the mode and type. The set of meson noises to use is also stored.

   class MesonIntLoopCalc
    {                      
       unsigned int code;    // for the quark, 3 bits (mode, src/snk, fxd/rel)
       set<MesonIntLoopNoiseInfo> mnoises;

     public:

       MesonIntLoopCalc(XmlReader& xmlin, int k);

       MesonIntLoopCalc(const MesonIntLoopCalc& in)
         : code(in.code), mnoises(in.mnoises) {}

       MesonIntLoopCalc& operator=(const MesonIntLoopCalc& in)
         {code=in.code; mnoises=in.mnoises;
          return *this;}

       ~MesonIntLoopCalc() {}

          // If noises were input as "<All/>", this routine takes the
          // available noises as those in "QuarkNoises".
          // Otherwise, it checks the current "mnoises" and removes those
          // noises that are not available as specified by "QuarkNoises". 

       set<MesonIntLoopNoiseInfo> getAvailableNoises(
                      const set<LaphNoiseInfo>& QuarkNoises) const;

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool isSource(int quark) const;

       bool isNormalMode() const;

       bool isGamma5HermConjMode() const;

       bool isFixedSourceScheme() const;

       bool isRelativeSource() const;

       QuarkLineEndInfo getLineInfo(int quark, int t0, int lat_textent) const;


       bool operator<(const MesonIntLoopCalc& rhs) const
        {return (code<rhs.code);}

       bool operator==(const MesonIntLoopCalc& rhs) const
        {return (code==rhs.code);}

       bool operator!=(const MesonIntLoopCalc& rhs) const
        {return (code!=rhs.code);}

     private:

       unsigned int read_type(XmlReader& xmlin, const std::string& tag);
       unsigned int read_mode(XmlReader& xmlin, const std::string& tag);
       std::string write_type(unsigned int end_type) const;
       std::string write_mode(unsigned int mode) const;

    };


   class QuarkFileLists
    {
       FileListInfo qfxdfiles;
       FileListInfo qrelfiles;

     public:

       QuarkFileLists(XmlReader& xmlin)
         : qfxdfiles(xmlin,"FixedSource"),
           qrelfiles(xmlin,"RelativeSource") {}

       QuarkFileLists(const FileListInfo& qfixed, const FileListInfo& qrel)
         :  qfxdfiles(qfixed),qrelfiles(qrel) {}

       ~QuarkFileLists() {}

       QuarkFileLists(const QuarkFileLists& in)
         :  qfxdfiles(in.qfxdfiles),qrelfiles(in.qrelfiles) {}

       QuarkFileLists& operator=(const QuarkFileLists& in)
         {qfxdfiles=in.qfxdfiles; qrelfiles=in.qrelfiles; return *this;}

       const FileListInfo& getFixedSchemeFiles() const;

       const FileListInfo& getRelativeSchemeFiles() const;

       void output(XmlWriter& xmlout) const;

       std::string output() const;

    };


   struct RecordKey
    {
       LaphNoiseInfo noise;
       QuarkLineEndInfo qline;
       int time_value;

       RecordKey();
       RecordKey(const LaphNoiseInfo& in_noise,
                 const QuarkLineEndInfo& in_qline,
                 int in_time);
       RecordKey(const MesonIntLoopNoiseInfo& in_mnoise, 
                 const MesonIntLoopLineEndInfo& in_mline,
                 int in_time);
       RecordKey(const RecordKey& in);
       RecordKey& operator=(const RecordKey& in);
       ~RecordKey();
       
       bool operator<(const RecordKey& rhs) const;
       bool operator==(const RecordKey& rhs) const;
       bool operator!=(const RecordKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       RecordKey(const unsigned int* buf)
       {noise.store=buf[0]; qline.store=buf[1];
        time_value=buf[2];}

       int numints() const {return 3;}
       size_t numbytes() const {return 3*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {buf[0]=noise.store; buf[1]=qline.store;
        buf[2]=time_value;}
       
    }; 


   struct CombinedKey
    {
       RecordKey rkey;
       MesonOperatorInfo mop;

       CombinedKey();
       CombinedKey(const MesonOperatorInfo& in_mop,
                   const RecordKey& in_rkey);
       CombinedKey(const CombinedKey& in);
       CombinedKey& operator=(const CombinedKey& in);
       ~CombinedKey();
       
       bool operator<(const CombinedKey& rhs) const;
       bool operator==(const CombinedKey& rhs) const;
       bool operator!=(const CombinedKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       CombinedKey(const unsigned int* buf) : rkey(buf)
       {mop.icode1=buf[3]; mop.icode2=buf[4];}

       int numints() const {return 5;}
       size_t numbytes() const {return 5*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {rkey.copyTo(buf); buf[3]=mop.icode1; buf[4]=mop.icode2;}

    }; 


   typedef MesonOperatorInfo  FileKey;
   typedef Complex            DataType;


 protected:

       // pointers to internal infos (managed by this handler
       // with new and delete)

   const GaugeConfigurationInfo *uPtr;
   const GluonSmearingInfo *gSmearPtr;
   const QuarkInfo *quarkPtr;
   const FileListInfo *fPtr;


       // pointer to needed sub-handler (managed by this handler)
 
   QuarkHandler *qHandler;


       // data I/O handler pointers

   DataPutHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,DataType> *DHputPtr;
   DataGetHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,DataType> *DHgetPtr;
   DataPutHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType> *DHSFputPtr;
   DataGetHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType> *DHSFgetPtr;
   bool m_read_mode;


       // Prevent copying ... handler might contain large
       // amounts of data

   MesonInternalLoopHandler(const MesonInternalLoopHandler&);
   MesonInternalLoopHandler& operator=(const MesonInternalLoopHandler&);



 public:


   MesonInternalLoopHandler();

   MesonInternalLoopHandler(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const QuarkInfo& quark,
                const FileListInfo& mesonfilelist,
                bool read_mode=true);

   void setInfo(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const QuarkInfo& quark,
                const FileListInfo& mesonfilelist,
                bool read_mode=true);

   ~MesonInternalLoopHandler();

   void clear();



   bool isInfoSet() const;

   const GaugeConfigurationInfo& getGaugeConfigurationInfo() const;

   const GluonSmearingInfo& getGluonSmearingInfo() const;

   const QuarkInfo& getQuarkInfo() const;

   const FileListInfo& getFileListInfo() const;

   void getHeader(XmlWriter& xmlout) const;

   void getFileMap(XmlWriter& xmlout) const;

   void outputKeys(XmlWriter& xmlout);


        //  Compute line ends for all meson operators in "MopInfos" and
        //  all dilution indices.  Repeat for all requested meson calculations
        //  given in "mcalcs" (which contains the meson noises to compute).
        //  If the list of meson noises is empty, all available noises 
        //  in the quark sink files are used. Handler must NOT be in read mode.
        //  For relative sources, only times from "relativesourcetmin" to 
        //  "relativesourcetmax" will be done (if "relativesourcetmax" is -1,
        //  then ALL times on the lattice are done.

   void compute(const set<MesonOperatorInfo>& MopInfos,
                const string& CoefsTopDirectory,
                const QuarkFileLists& Qfiles,
                const set<MesonIntLoopCalc>& mcalcs,
                const string& smeared_gauge_filename,
                const string& smeared_quark_filestub,
                int relativesourcetmin=0, int relativesourcetmax=-1);


        //  Merge the meson line ends in "infiles" into the files in
        //  "mesonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "mesonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
        //  WARNING:  This routine is meant to be used by SERIAL code only.
        //    It will still work in parallel, but a lot of unnecessary broadcasts
        //    are done.
 
   void merge(const FileListInfo& infiles);
   void merge(const vector<FileListInfo>& infiles);
   void reorganize(const vector<FileListInfo>& infiles);


        // read from file (loop through file list to find)
        // when in read mode

   const DataType& getData(const MesonOperatorInfo& mop,
                           const MesonIntLoopLineEndInfo& mline,
                           const MesonIntLoopNoiseInfo& mnoise,
                           int timevalue);

   const DataType& getData(const MesonOperatorInfo& mop,
                           const QuarkLineEndInfo& qline,
                           const LaphNoiseInfo& qnoise,
                           int timevalue);


          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

   bool queryData(const MesonOperatorInfo& mop,
                  const MesonIntLoopLineEndInfo& mline,
                  const MesonIntLoopNoiseInfo& mnoise,
                  int timevalue);

   bool queryData(const MesonOperatorInfo& mop,
                  const QuarkLineEndInfo& qline,
                  const LaphNoiseInfo& qnoise,
                  int timevalue); 

   bool queryData(const MesonOperatorInfo& mop);


        // remove from internal memory

   void removeData(const MesonOperatorInfo& mop,
                   const MesonIntLoopLineEndInfo& mline,
                   const MesonIntLoopNoiseInfo& mnoise,
                   int timevalue);

   void removeData(const MesonOperatorInfo& mop,
                   const QuarkLineEndInfo& qline,
                   const LaphNoiseInfo& qnoise,
                   int timevalue); 

   void removeData(const MesonOperatorInfo& mop);


   void clearData();   

          // more query subroutines

   set<MesonOperatorInfo> getMesonOperators() const;

   set<MesonIntLoopNoiseInfo> getMesonNoises(const MesonOperatorInfo& minfo) const; 

   set<MesonIntLoopLineEndInfo> getMesonLineEndInfos(const MesonOperatorInfo& minfo) const;


 private:


   void set_info(const GaugeConfigurationInfo& gaugeinfo,
                 const GluonSmearingInfo& smear,
                 const QuarkInfo& quark,
                 const FileListInfo& flist,
                 bool read_mode);


   bool checkHeader(XmlReader& xmlr, int suffix);
   void writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                    int suffix);

   bool checkHeader(XmlReader& xmlr);
   void writeHeader(XmlWriter& xmlout);

   void connectQuarkHandlers(const MesonIntLoopCalc& Bcalc,
                             const QuarkFileLists& Qfiles,
                             const std::string& smeared_gauge_filename,
                             const std::string& smeared_quark_filestub);
   void disconnectQuarkHandlers();
   void filefail(const std::string& message);
   void check_info_set(const std::string& name,
                       int check_mode=0) const;

   void evaluateLocalInnerProduct(const LatticeColorVector *v1,
                                  const LatticeColorVector *v2,
                                  auto_ptr<LatticeComplex>& result);

   friend class DataPutHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,DataType>;
   friend class DataGetHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,DataType>;
   friend class DataPutHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType>;
   friend class DataGetHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType>;

};


// ***************************************************************

#endif
  }
}
#endif  
