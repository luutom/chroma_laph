#include "meson_handler.h"
#include "time_slices.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {

#if (QDP_ND == 3)

// **************************************************************


MesonHandler::RecordKey::RecordKey() : time_value(0) {}

MesonHandler::RecordKey::~RecordKey() {}

MesonHandler::RecordKey::RecordKey(const LaphNoiseInfo& in_noise1, 
                                   const LaphNoiseInfo& in_noise2,
                                   const QuarkLineEndInfo& in_qline1,
                                   const QuarkLineEndInfo& in_qline2,
                                   int in_time)
  : noise1(in_noise1), noise2(in_noise2),
    qline1(in_qline1), qline2(in_qline2),
    time_value(in_time) {}

MesonHandler::RecordKey::RecordKey(const MesonNoiseInfo& in_mnoise, 
                                   const MesonLineEndInfo& in_mline,
                                   int in_time)
  : noise1(in_mnoise.noise1), noise2(in_mnoise.noise2),
    qline1(in_mline.qline1), qline2(in_mline.qline2),
    time_value(in_time) {}

MesonHandler::RecordKey::RecordKey(const MesonHandler::RecordKey& in)
  : noise1(in.noise1), noise2(in.noise2),
    qline1(in.qline1), qline2(in.qline2),
    time_value(in.time_value) {}

MesonHandler::RecordKey& MesonHandler::RecordKey::operator=(
                             const MesonHandler::RecordKey& in)
{
 noise1=in.noise1; noise2=in.noise2;
 qline1=in.qline1; qline2=in.qline2;
 time_value=in.time_value;
 return *this;
}


bool MesonHandler::RecordKey::operator<(
            const MesonHandler::RecordKey& rhs) const
{
 return multiLessThan(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2, 
               qline1,rhs.qline1, qline2,rhs.qline2);
}

bool MesonHandler::RecordKey::operator==(
            const MesonHandler::RecordKey& rhs) const
{
 return multiEqual(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2,
               qline1,rhs.qline1, qline2,rhs.qline2);
}

bool MesonHandler::RecordKey::operator!=(
            const MesonHandler::RecordKey& rhs) const
{
 return multiNotEqual(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2,
               qline1,rhs.qline1, qline2,rhs.qline2);
}

void MesonHandler::RecordKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"RecordKey");
 push(xmlw,"AntiQuarkLine");
 noise1.output(xmlw);
 qline1.output(xmlw);
 pop(xmlw);
 push(xmlw,"QuarkLine");
 noise2.output(xmlw);
 qline2.output(xmlw);
 pop(xmlw);
 write(xmlw,"TimeValue",time_value);
 pop(xmlw);
}


// *************************************************************************

MesonHandler::CombinedKey::CombinedKey() {}

MesonHandler::CombinedKey::~CombinedKey() {}

MesonHandler::CombinedKey::CombinedKey(const MesonOperatorInfo& in_mop,
                                       const RecordKey& in_rkey)
  : rkey(in_rkey),mop(in_mop)  {}

MesonHandler::CombinedKey::CombinedKey(const MesonHandler::CombinedKey& in)
  : rkey(in.rkey), mop(in.mop) {}

MesonHandler::CombinedKey& MesonHandler::CombinedKey::operator=(
                             const MesonHandler::CombinedKey& in)
{
 mop=in.mop;
 rkey=in.rkey;
 return *this;
}


bool MesonHandler::CombinedKey::operator<(
            const MesonHandler::CombinedKey& rhs) const
{
 return multiLessThan(rkey,rhs.rkey, mop,rhs.mop);
}

bool MesonHandler::CombinedKey::operator==(
            const MesonHandler::CombinedKey& rhs) const
{
 return multiEqual(rkey,rhs.rkey, mop,rhs.mop);
}

bool MesonHandler::CombinedKey::operator!=(
            const MesonHandler::CombinedKey& rhs) const
{
 return multiNotEqual(rkey,rhs.rkey, mop,rhs.mop);
}

void MesonHandler::CombinedKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"CombinedKey");
 mop.output(xmlw);
 rkey.output(xmlw);
 pop(xmlw);
}


// *************************************************************************


const FileListInfo& MesonHandler::QuarkFileLists::getFixedSchemeFiles(int quark) const
{
 if (quark==1) return q1fxdfiles;
 else if (quark==2) return q2fxdfiles;
 else{
    QDPIO::cerr << "invalid quark number in MesonHandler::QuarkFileLists"<<endl;
    QDP_abort(1);}
 return q1fxdfiles;  // to avoid compiler warning
}

const FileListInfo& MesonHandler::QuarkFileLists::getRelativeSchemeFiles(int quark) const
{
 if (quark==1) return q1relfiles;
 else if (quark==2) return q2relfiles;
 else{
    QDPIO::cerr << "invalid quark number in MesonHandler::QuarkFileLists"<<endl;
    QDP_abort(1);}
 return q1relfiles;   // to avoid compiler warning
}


// *************************************************************************

MesonHandler::MesonLineCalc::MesonLineCalc(XmlReader& xmlin, int k)
{
 string tagname="MesonLineCalc["+int_to_string(k)+"]";
 if (xml_tag_count(xmlin,tagname)!=1){
    QDPIO::cerr << "Bad Xml input to MesonLineCalc"<<endl;
    xmlreadfail(xmlin,"MesonLineCalc");}
 XmlReader xmlr(xmlin, "./descendant-or-self::"+tagname);
 max_time_sep=-1;
 unsigned int rcode;
 code=read_mode(xmlr,"QuarkMode");
 rcode=read_type(xmlr,"QuarkType"); code<<=2; code|=rcode;
 if (rcode==0) max_time_sep=0;  // fixed source
 rcode=read_mode(xmlr,"AntiQuarkMode"); code<<=1; code|=rcode;
 rcode=read_type(xmlr,"AntiQuarkType"); code<<=2; code|=rcode;
 min_time_sep=0; // default
 if (rcode==0) max_time_sep=0;  // fixed source
 if ((0x9u & code)==0x9u) max_time_sep=0; // all relative

   // if all lines are relative or one line is fixed+source,
   // max_time_sep is set to zero; otherwise, must be read
 if (max_time_sep<0){
   xmlreadif(xmlr,"MaxTimeSeparation",max_time_sep,
             "MesonHandler::MesonLineCalc");
   xmlreadif(xmlr,"MinTimeSeparation",min_time_sep,
             "MesonHandler::MesonLineCalc");}
 if (max_time_sep<0){
    QDPIO::cerr << "Invalid MaxTimeSeparation input to MesonLineCalc"<<endl;
    xmlreadfail(xmlin,"MesonLineCalc");}
 if ((min_time_sep<0)||(min_time_sep>max_time_sep)){
    QDPIO::cerr << "Invalid MinTimeSeparation input to MesonLineCalc"<<endl;
    xmlreadfail(xmlin,"MesonLineCalc");}

    //  read the meson noises

 if (xml_tag_count(xmlr,"MesonNoises")!=1){
    QDPIO::cerr << "Error in reading MesonLineCalc: must have one <MesonNoises> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmln(xmlr,"./MesonNoises");
 if (xml_tag_count(xmln,"All")!=1){
    int ncalc=xml_tag_count(xmln,"MesonNoise");
    if (ncalc==0){
       QDPIO::cerr << "No meson noises given and <All/> not specified; quitting"<<endl;
       QDP_abort(1);}
    for (int k=1;k<=ncalc;++k) 
       mnoises.insert(MesonHandler::MesonNoiseInfo(xmln,k));}
}

void MesonHandler::MesonLineCalc::output(XmlWriter& xmlout) const
{
 push(xmlout,"MesonLineCalc");
 unsigned int wcode=code;
 write(xmlout,"AntiQuarkType",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"AntiQuarkMode",write_mode(0x1u & wcode)); wcode>>=1;
 write(xmlout,"QuarkType",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"QuarkMode",write_mode(0x1u & wcode));
 write(xmlout,"MinTimeSeparation",min_time_sep);
 write(xmlout,"MaxTimeSeparation",max_time_sep);
 push(xmlout,"MesonNoises");
 if (mnoises.size()==0){ push(xmlout,"All"); pop(xmlout);}
 else{
    for (set<MesonHandler::MesonNoiseInfo>::const_iterator mnit=mnoises.begin();
       mnit!=mnoises.end();++mnit)
       mnit->output(xmlout);}
 pop(xmlout);
 pop(xmlout);
}

string MesonHandler::MesonLineCalc::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 unsigned int wcode=code;
 oss << pad << "<MesonLineCalc>"<<endl;
 oss << pad << "  <AntiQuarkType>"<< write_type(0x3u & wcode)
     << "</AntiQuarkType>"<<endl; wcode>>=2;
 oss << pad << "  <AntiQuarkMode>"<< write_mode(0x1u & wcode)
     << "</AntiQuarkMode>"<<endl; wcode>>=1;
 oss << pad << "  <QuarkType>"<< write_type(0x3u & wcode)
     << "</QuarkType>"<<endl; wcode>>=2;
 oss << pad << "  <QuarkMode>"<< write_mode(0x1u & wcode)
     << "</QuarkMode>"<<endl;
 oss << pad << "  <MaxTimeSeparation>"<<max_time_sep
            <<"</MaxTimeSeparation>"<<endl;
 oss << pad << "  <MesonNoises>"<<endl;
 if (mnoises.size()==0) oss << pad <<"    <All/>"<<endl;
 else{
    for (set<MesonHandler::MesonNoiseInfo>::const_iterator mnit=mnoises.begin();
       mnit!=mnoises.end();++mnit)
       oss << mnit->output(indent+1)<<endl;}
 oss << pad << "  </MesonNoises>"<<endl;
 oss << pad << "</MesonLineCalc>"<<endl;
 return oss.str();
}

unsigned int MesonHandler::MesonLineCalc::read_type(
                   XmlReader& xmlr, const string& tag)
{
 string end_type;
 xmlread(xmlr,tag,end_type,"MesonHandler::MesonLineCalc");
 end_type=tidyString(end_type);
 int type=0;
 if (end_type=="FixedSource") type=0;
 else if (end_type=="RelativeSource") type=1;
 else if (end_type=="SinkWithFixedSource") type=2;
 else if (end_type=="SinkWithRelativeSource") type=3;
 else{
    QDPIO::cerr << "Bad Xml input to MesonHandler::MesonLineCalc"<<endl;
    QDPIO::cerr << "Invalid <Type> tag"<<endl;
    xmlreadfail(xmlr,"MesonHandler::MesonLineCalc");}
 return type;
}

string MesonHandler::MesonLineCalc::write_type(unsigned int end_type) const
{
 if (end_type==0) return "FixedSource";
 else if (end_type==1) return "RelativeSource";
 else if (end_type==2) return "SinkWithFixedSource";
 else return "SinkWithRelativeSource";
}

unsigned int MesonHandler::MesonLineCalc::read_mode(
                   XmlReader& xmlr, const string& tag)
{
 int mode=0;
 string modestr;
 if (xmlreadif(xmlr,tag,modestr,"MesonHandler::MesonLineCalc")){
    modestr=tidyString(modestr);
    if (modestr=="Normal") mode=0;
    else if (modestr=="Gamma5HermConj") mode=1;
    else{
       QDPIO::cerr << "Bad Xml input to MesonHandler::MesonLineCalc"<<endl;
       QDPIO::cerr << "Invalid <Mode> tag"<<endl;
       xmlreadfail(xmlr,"MesonHandler::MesonLineCalc");}}
 return mode;
}

string MesonHandler::MesonLineCalc::write_mode(unsigned int mode) const
{
 if (mode==0) return "Normal";
 else return "Gamma5HermConj";
}

bool MesonHandler::MesonLineCalc::isSource(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+1;
 return ((vcode&0x1u)==0);
}

bool MesonHandler::MesonLineCalc::isNormalMode(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+2;
 return ((vcode&0x1u)==0);
}

bool MesonHandler::MesonLineCalc::isGamma5HermConjMode(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+2;
 return ((vcode&0x1u)!=0);
}

bool MesonHandler::MesonLineCalc::isFixedSourceScheme(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1));
 return ((vcode&0x1u)==0);
}

bool MesonHandler::MesonLineCalc::isRelativeSource(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1));
 return ((vcode&0x3u)==1);
}

   //  must be same mode, same dilution scheme to share quark handler

bool MesonHandler::MesonLineCalc::canShareQuarkHandler(
                             int quarkA, int quarkB) const
{
 if ((quarkA<1)||(quarkA>2)||(quarkB<1)||(quarkB>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 if (quarkA==quarkB) return true;
 unsigned int modeA=code;
 modeA>>=(3*(quarkA-1))+2;
 modeA&=0x1u;
 unsigned int modeB=code;
 modeB>>=(3*(quarkB-1))+2;
 modeB&=0x1u;
 if (modeA!=modeB) return false;
 unsigned int schemeA=code;
 schemeA>>=(3*(quarkA-1));
 schemeA&=0x1u;
 unsigned int schemeB=code;
 schemeB>>=(3*(quarkB-1));
 schemeB&=0x1u;
 return (schemeA==schemeB);
}

QuarkLineEndInfo MesonHandler::MesonLineCalc::getLineInfo(
                      int quark, int t0, int lat_textent) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int qmode=code;
 qmode>>=(3*(quark-1))+2;
 qmode&=0x1u; 
 unsigned int qtype=code;
 qtype>>=(3*(quark-1));
 qtype&=0x3u;
 int source_index= (qtype==2)? t0 : 0; 
 return QuarkLineEndInfo(qtype,qmode,source_index,lat_textent);   
}


set<MesonHandler::MesonNoiseInfo> MesonHandler::MesonLineCalc::getAvailableNoises(
                               const set<LaphNoiseInfo>& antiQuarkNoises,
                               const set<LaphNoiseInfo>& QuarkNoises) const
{
 set<MesonNoiseInfo> result;
 if (mnoises.size()==0){
    for (set<LaphNoiseInfo>::const_iterator it1=antiQuarkNoises.begin();
         it1!=antiQuarkNoises.end();it1++)
    for (set<LaphNoiseInfo>::const_iterator it2=QuarkNoises.begin();
         it2!=QuarkNoises.end();it2++)
       if ((*it1)!=(*it2))
          result.insert(MesonHandler::MesonNoiseInfo(*it1,*it2));}
 else{
    for (set<MesonNoiseInfo>::const_iterator it=mnoises.begin();
         it!=mnoises.end();it++)
       if (((antiQuarkNoises.find(it->noise1)!=antiQuarkNoises.end())||(isSource(1)))  
          && ((QuarkNoises.find(it->noise2)!=QuarkNoises.end())||(isSource(2)))
          && (it->noise1 != it->noise2))
          result.insert(*it);}
 return result;
}

// *************************************************************************


void MesonHandler::MesonNoiseInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"MesonNoiseInfo");
 push(xmlout,"AntiQuarkLine");
 noise1.output(xmlout);
 pop(xmlout);
 push(xmlout,"QuarkLine");
 noise2.output(xmlout);
 pop(xmlout);
 pop(xmlout);
}

string MesonHandler::MesonNoiseInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<MesonNoiseInfo>"<<endl;
 oss << pad << "  <AntiQuarkLine>"<<endl;
 oss << noise1.output(indent+1);
 oss << pad << "  </AntiQuarkLine>"<<endl;
 oss << pad << "  <QuarkLine>"<<endl;
 oss << noise2.output(indent+1);
 oss << pad << "  </QuarkLine>"<<endl;
 oss << pad << "</MesonNoiseInfo>"<<endl;
 return oss.str();
}

// *************************************************************************

void MesonHandler::QuarkFileLists::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkFileLists");
 push(xmlout,"AntiQuark");
 push(xmlout,"FixedSource");
 q1fxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 q1relfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 push(xmlout,"Quark");
 push(xmlout,"FixedSource");
 q2fxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 q2relfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 pop(xmlout);
}

string MesonHandler::QuarkFileLists::output() const
{
 XmlBufferWriter xmlout;
 output(xmlout);
 return xmlout.str();
}


// *************************************************************************


MesonHandler::MesonHandler()
        :  uPtr(0), gSmearPtr(0), quark1Ptr(0), quark2Ptr(0),
           fPtr(0), qHandler1(0), qHandler2(0), DHputPtr(0), DHgetPtr(0), 
           DHSFputPtr(0), DHSFgetPtr(0), m_read_mode(true) {}


MesonHandler::MesonHandler(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkInfo& antiquark, 
                           const QuarkInfo& quark,
                           const FileListInfo& flist,
                           bool read_mode)
        :  uPtr(0), gSmearPtr(0), quark1Ptr(0), quark2Ptr(0),
           fPtr(0), qHandler1(0), qHandler2(0), 
           DHputPtr(0), DHgetPtr(0), DHSFputPtr(0), DHSFgetPtr(0)
{
 set_info(gaugeinfo,gluonsmear,antiquark,quark,flist,read_mode);
}


void MesonHandler::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkInfo& antiquark, 
                           const QuarkInfo& quark,
                           const FileListInfo& flist,
                           bool read_mode)
{
 clear();
 set_info(gaugeinfo,gluonsmear,antiquark,quark,flist,read_mode);
}


void MesonHandler::set_info(const GaugeConfigurationInfo& gaugeinfo,
                            const GluonSmearingInfo& gluonsmear,
                            const QuarkInfo& antiquark, 
                            const QuarkInfo& quark,
                            const FileListInfo& flist,
                            bool read_mode)
{
 m_read_mode=read_mode;
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    quark1Ptr = new QuarkInfo(antiquark);
    quark2Ptr = new QuarkInfo(quark);
    fPtr = new FileListInfo(flist);   
    if (m_read_mode)
       DHgetPtr = new DataGetHandlerMF<MesonHandler,FileKey,RecordKey,
                          DataType>(*this,flist,"Laph--MesonFile","MesonHandlerDataFile");}
 catch(...){
    QDPIO::cerr << "allocation problem in MesonHandler"<<endl;
    clear();
    QDP_abort(1);}
}



void MesonHandler::connectQuarkHandlers(const MesonLineCalc& Mcalc,
                                        const QuarkFileLists& Qfiles,
                                        const string& smeared_gauge_filename,
                                        const string& smeared_quark_filestub)
{
 check_info_set("connectQuarkHandlers",2);
 disconnectQuarkHandlers();
 try{

         // connect to quark handler for first quark

    const DilutionSchemeInfo& dil1=(Mcalc.isFixedSourceScheme(1))?
             quark1Ptr->getFixedSourceDilutionScheme() :
             quark1Ptr->getRelativeSourceDilutionScheme();
    const FileListInfo& qfiles1=(Mcalc.isFixedSourceScheme(1))?
             Qfiles.getFixedSchemeFiles(1) :
             Qfiles.getRelativeSchemeFiles(1);
    qHandler1 = new QuarkHandler(*uPtr,*gSmearPtr,quark1Ptr->getSmearing(),
                                 dil1,quark1Ptr->getAction(),qfiles1,
                                 smeared_quark_filestub,
                                 smeared_gauge_filename);
    if (Mcalc.isGamma5HermConjMode(1)) 
       qHandler1->setGamma5HermiticityMode();

         // connect to quark handler for second quark;
         // if line 2 has the same dilution scheme (rel or fxd), same mode,
         // and same quark flavor/action as line 1, then lines 1 and 2
         // can share the same handler (the quark file infos can be used
         // to determine if the same flavor/action)

    const FileListInfo& qfiles2=(Mcalc.isFixedSourceScheme(2))?
             Qfiles.getFixedSchemeFiles(2) :
             Qfiles.getRelativeSchemeFiles(2);
    if ((Mcalc.canShareQuarkHandler(1,2))&&(qfiles2==qfiles1)
        &&((*quark2Ptr)==(*quark1Ptr)))
       qHandler2=qHandler1;
    else{
       const DilutionSchemeInfo& dil2=(Mcalc.isFixedSourceScheme(2))?
                quark2Ptr->getFixedSourceDilutionScheme() :
                quark2Ptr->getRelativeSourceDilutionScheme();
       qHandler2 = new QuarkHandler(*uPtr,*gSmearPtr,quark2Ptr->getSmearing(),
                                    dil2,quark2Ptr->getAction(),qfiles2,
                                    smeared_quark_filestub,
                                    smeared_gauge_filename);
       if (Mcalc.isGamma5HermConjMode(2)) 
          qHandler2->setGamma5HermiticityMode(); } 

    }
 catch(...){
    QDPIO::cerr << "allocation problem in MesonHandler::connectQuarkHandlers"<<endl;
    QDP_abort(1);}
}



void MesonHandler::disconnectQuarkHandlers()
{
 if (qHandler1==qHandler2) delete qHandler1;
 else{
    delete qHandler1;
    delete qHandler2;}
 qHandler1=0; qHandler2=0;
}



MesonHandler::~MesonHandler()
{
 clear();
}

void MesonHandler::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete quark1Ptr;
    delete quark2Ptr;
    delete fPtr;
    delete qHandler1;
    delete qHandler2;
    delete DHputPtr;
    delete DHgetPtr;
    delete DHSFputPtr;
    delete DHSFgetPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 quark1Ptr=0;
 quark2Ptr=0;
 fPtr=0;
 qHandler1=0;
 qHandler2=0;
 DHputPtr=0;
 DHgetPtr=0;
 DHSFputPtr=0;
 DHSFgetPtr=0;
}




bool MesonHandler::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(fPtr!=0)
        &&(quark1Ptr!=0)&&(quark2Ptr!=0));
}


const GaugeConfigurationInfo& MesonHandler::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& MesonHandler::getGluonSmearingInfo() const
{
 check_info_set("getFieldSmearingInfo");
 return *gSmearPtr;
}

const QuarkInfo& MesonHandler::getQuarkInfo(int quark_number) const 
{
 check_info_set("getQuarkInfo");
 if ((quark_number<1)||(quark_number>2)){
    QDPIO::cerr << "invalid quark number in MesonHandler::getQuarkInfo"<<endl
                << "must have value 1,2"<<endl;
    QDP_abort(1);}
 if (quark_number==1) return *quark1Ptr;
 else return *quark2Ptr;
}

const FileListInfo& MesonHandler::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

void MesonHandler::getFileMap(XmlWriter& xmlout) const
{
 if ((isInfoSet())&&(m_read_mode))
    DHgetPtr->getFileMap(xmlout);
}

void MesonHandler::outputKeys(XmlWriter& xmlout)
{
 if ((isInfoSet())&&(m_read_mode)) 
    DHgetPtr->outputKeys(xmlout);
}

   // ***************************************************************
   
        //  Compute line ends for all meson operators in "Mops" and
        //  all dilution indices.  Repeat for all requested meson calculations
        //  given in "mcalcs" (which contains all mesons noises to compute).
        //  If the list of meson noises is empty, all available noises 
        //  in the quark sink files are used.
        //  For relative sources, only times from "relativesourcetmin" to 
        //  "relativesourcetmax" will be done (if "relativesourcetmax" is -1,
        //  then ALL times on the lattice are done.


void MesonHandler::compute(
                      const set<MesonOperatorInfo>& MopInfos,
                      const string& CoefsTopDirectory,
                      const QuarkFileLists& Qfiles,
                      const set<MesonLineCalc>& mcalcs,
                      const string& smeared_gauge_filename,
                      const string& smeared_quark_filestub,
                      int relativesourcetmin, int relativesourcetmax)
{

   // check info is set and handler is NOT in read mode
 check_info_set("compute",2);

 QDPIO::cout <<endl<<endl;
 QDPIO::cout << "********************************************************"<<endl;
 QDPIO::cout << endl<<"  MesonHandler::compute commencing..."<<endl<<endl;

 START_CODE();
 double mtimer=0.0,qtimer=0.0,otimer=0.0;
 double sstimer=0.0, cftimer=0.0, qqtimer=0.0;
 StopWatch rolex,outer;
 outer.start();
 rolex.start();

     // *******************************************************
     // *                                                     *
     // *  set up structures related to the meson operators   *
     // *                                                     *
     // *******************************************************

 int nmeson_ops=MopInfos.size();
 if (nmeson_ops==0){
    QDPIO::cerr << "No meson operators in MesonHandler::computeSourcesSinks"<<endl;
    return;}
 SetIndexer<MesonOperatorInfo> Mops(MopInfos);

   // Check that all meson operators have the same light-strange flavor

 string flavor=Mops[0].getFlavorLS();
 for (int i=1;i<nmeson_ops;i++){
    if (Mops[i].getFlavorLS()!=flavor){
       QDPIO::cerr << "All meson operators must have same flavor content"<<endl;
       return;}}

   // Check that all meson operators have the same displacement length if nonzero

 int displacement_length=0;
 for (int i=0;i<nmeson_ops;i++){
    int dl=Mops[i].getDisplacementLength();
    if (displacement_length==0) displacement_length=dl;
    if ((dl!=0)&&(dl!=displacement_length)){
       QDPIO::cerr << "All meson operators must have the same displacement"
                   << " length if nonzero"<<endl;
       return;}}

    // Create the operators by reading the elemental coefficients

 QDPIO::cout << "Reading the meson operator elemental terms"<<endl;
 MesonOperator::initialize(tidyString(CoefsTopDirectory));
 vector<MesonOperator*> mop(nmeson_ops);
 for (int k=0;k<nmeson_ops;k++) mop[k]=0;  // start with null pointers
 try{
    for (int k=0;k<nmeson_ops;k++)
       mop[k]=new MesonOperator(Mops[k]);}
 catch(const string& err){
    QDPIO::cerr << "Error in MesonHandler::compute"<<endl;
    QDPIO::cerr << err <<endl;
    for (int k=0;k<nmeson_ops;k++) delete mop[k];
    return;}

     //  Output meson operators and build momenta set

 map<Momentum,int> mominds;
 int momcount=0;
 for (int k=0;k<nmeson_ops;k++){
    QDPIO::cout << "Meson operator "<<k<<":"<<endl;
    QDPIO::cout << Mops[k].output()<<endl;
    if (mominds.find(mop[k]->getMomentumValue())==mominds.end())
       mominds.insert(make_pair(mop[k]->getMomentumValue(),momcount++));}

     //  Make and store the momenta phases: our annihilation operators
     //  need exp(-I p.x) 

 multi1d<LatticeComplex> phases(mominds.size());
 multi1d<Momentum> moms(mominds.size());
 for (map<Momentum,int>::const_iterator mit=mominds.begin();
      mit!=mominds.end();mit++){
    phases[mit->second]=makeMomPhaseField(mit->first);
    moms[mit->second]=mit->first;}

    // The next step is to construct the "computations" map.
    // A given "computation" is a site color contraction, combined
    // with a summation over all spatial sites, for various momenta.
    // We will then loop over all such "computations", adding the
    // results to the pertinent meson operators as specified in
    // the computations map.

 typedef vector<MesonOperator::IndexCoef>  ICList;
 typedef map<int, ICList> MomMap;
 typedef map<MesonOperator::Elemental, MomMap> CSMap;

 CSMap elementals;    // the crucial map structure
 bool comflag=true;   // for synchronizing nodes

   //  Divide "nmeson_ops" meson operators onto "nodes" nodes.
   //      x processors get m mesons,  nodes-x get  m-1 mesons.
   //         m = nmeson_ops/nodes + 1    x = nmeson_ops % nodes

 int nmeson_ops_thisnode;
 int opindex_start;

 {int nodes=Layout::numNodes();
 int thisnode=Layout::nodeNumber();
 int m = nmeson_ops/nodes + 1;
 int x = nmeson_ops % nodes;
 if (thisnode<x){
    nmeson_ops_thisnode=m;
    opindex_start=m*thisnode;}
 else{
    nmeson_ops_thisnode=m-1;
    opindex_start=m*x+(m-1)*(thisnode-x);}}
 int opindex_last=opindex_start+nmeson_ops_thisnode-1;


 for (int i=0;i<nmeson_ops;i++){

    int momind=mominds[mop[i]->getMomentumValue()];

    for (MesonOperator::ElementalIterator it=mop[i]->begin();
                                          it!=mop[i]->end();it++){

       MomMap& mommap=elementals[it->el];
       ICList& indcflist=mommap[momind];
       if ((i>=opindex_start)&&(i<=opindex_last))
          indcflist.push_back(MesonOperator::IndexCoef(i-opindex_start,it->coef));}}

 for (int k=0;k<nmeson_ops;k++) delete mop[k];  // don't need these anymore

    //  set up the files for output (local to each node)

 if (nmeson_ops_thisnode>0){
  try{
    int suffix=fPtr->getMinFileNumber()+Layout::nodeNumber();
    FileListInfo finfo_thisnode(fPtr->getFileStub(),suffix,suffix,
                                fPtr->isModeOverwrite());
    string fname=finfo_thisnode.getFileName(suffix);
    DHSFputPtr = new DataPutHandlerSF<MesonHandler,CombinedKey,DataType>(
            *this,fname,"Laph--MesonFile",fPtr->isModeOverwrite(),false);
    }
  catch(...){
    std::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}}

 CSMap::iterator elit;
 MomMap::iterator mmit;
 ICList::const_iterator icit;

 {vector<int> num_elementals(mominds.size(),0);
 for (elit=elementals.begin();elit!=elementals.end();elit++)
    for (mmit=(elit->second).begin();mmit!=(elit->second).end();mmit++)
       ++num_elementals[mmit->first];
 QDPIO::cout << endl;
 QDPIO::cout << "               Number of meson operators = "<<nmeson_ops<<endl;
 QDPIO::cout << "    Displacement length of all operators = "<<displacement_length<<endl;
 QDPIO::cout << "             Number of different momenta = "<<mominds.size()<<endl<<endl;
 for (unsigned int k=0;k<mominds.size();k++)
    QDPIO::cout << " Number of elementals for momentum["<<k<<"] = "
                <<num_elementals[k]<<endl;} 

 rolex.stop();
 otimer+=rolex.getTimeInSeconds();
 double twopi=6.2831853071795864770;
 double momquantum_x=twopi / toDouble(Layout::lattSize()[0]);
 double momquantum_y=twopi / toDouble(Layout::lattSize()[1]);
 double momquantum_z=twopi / toDouble(Layout::lattSize()[2]);

     // **********************************************************
     // *                                                        *
     // *    loop over the requested meson line calculations     *
     // *                                                        *
     // **********************************************************

 for (set<MesonLineCalc>::const_iterator 
      mcit=mcalcs.begin();mcit!=mcalcs.end();++mcit){

  rolex.start();
  QDPIO::cout << endl<<endl;
  QDPIO::cout <<" ************************************************"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" *    Starting new meson line calculations:     *"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" ************************************************"<<endl<<endl;
  QDPIO::cout << mcit->output() << endl<<endl;

   // check for valid meson operator: one psi and one psi-bar field

  bool sourceflag1=mcit->isSource(1);
  bool sourceflag2=mcit->isSource(2);

  int charge1=(sourceflag1)?1:-1;
  if (mcit->isGamma5HermConjMode(1)) charge1=-charge1;
  int charge2=(sourceflag2)?1:-1;
  if (mcit->isGamma5HermConjMode(2)) charge2=-charge2;
  if (charge1==charge2){
     QDPIO::cout << "Invalid meson field; skipping this computation"<<endl;
     continue;}

   // fire up the needed sub-handlers

  connectQuarkHandlers(*mcit,Qfiles,smeared_gauge_filename,
                       smeared_quark_filestub);
  QDPIO::cout << "connection to QuarkHandlers established"<<endl;

  int ndilproj1=qHandler1->getNumberOfSpinEigvecDilutionProjectors();
  int ndilproj2=qHandler2->getNumberOfSpinEigvecDilutionProjectors();
  QDPIO::cout << "Number of spin-eigenvector dilution indices:"<<endl;
  QDPIO::cout << "   AntiQuark: "<<ndilproj1<<endl;
  QDPIO::cout << "       Quark: "<<ndilproj2<<endl<<endl;

  rolex.stop();
  otimer+=rolex.getTimeInSeconds();

     // get the noises and source times available for each
     // quark line (use "relativesourcetmin" to "relativesourcetmax" 
     // for RelativeSource, all times if "relativesourcetmax" is -1); 
     // then take intersection of the source times; for noises, take 
     // intersections with requested noises (if any)

  int Textent=uPtr->getTimeExtent();
  set<int> start_times;
  set<MesonNoiseInfo> mnoise_set;

  {set<LaphNoiseInfo> noise1set,noise2set;
   set<int> tsrc1set,tsrc2set;
   qHandler1->getNoisesAndSourceTimes(noise1set,tsrc1set);
   qHandler2->getNoisesAndSourceTimes(noise2set,tsrc2set);
     
  // hadron noises to compute
   mnoise_set=mcit->getAvailableNoises(noise1set,noise2set);

   if (mcit->isRelativeSource(1)){
      if (relativesourcetmax<0){
         for (int t=0;t<Textent;t++) tsrc1set.insert(t);}
      else{ 
         int ttmin=relativesourcetmin;
         if (ttmin<0) ttmin=0;
         int ttmax=relativesourcetmax;
         if (ttmax>=Textent) ttmax=Textent-1;
         tsrc1set.clear();
         for (int t=ttmin;t<=ttmax;t++) tsrc1set.insert(t);}}
   if (mcit->isRelativeSource(2)){
      if (relativesourcetmax<0){
         for (int t=0;t<Textent;t++) tsrc2set.insert(t);}
      else{
         int ttmin=relativesourcetmin;
         if (ttmin<0) ttmin=0;
         int ttmax=relativesourcetmax;
         tsrc2set.clear();
         for (int t=ttmin;t<=ttmax;t++) tsrc2set.insert(t);}}

   set_intersection(tsrc1set.begin(),tsrc1set.end(),
                    tsrc2set.begin(),tsrc2set.end(),
                    inserter(start_times,start_times.begin()));}

   // loop over source times and time separations

  for (set<int>::const_iterator t0=start_times.begin();
       t0!=start_times.end();++t0)
  for (int dt=mcit->getMinTimeSeparation();
           dt<=mcit->getMaxTimeSeparation();++dt){

   int timeval=(*t0)+dt;
   if (timeval>=Textent) timeval-=Textent;

   QuarkLineEndInfo qline1(mcit->getLineInfo(1,*t0,Textent));
   QuarkLineEndInfo qline2(mcit->getLineInfo(2,*t0,Textent));

   int tsource1=qline1.getSourceTime(timeval,*uPtr);
   int tsource2=qline2.getSourceTime(timeval,*uPtr);
 
   int tproj1=qHandler1->getTimeDilutionProjectorIndex(tsource1);
   int tproj2=qHandler2->getTimeDilutionProjectorIndex(tsource2);

   // loop over meson noises

   for (set<MesonNoiseInfo>::const_iterator
        mnit=mnoise_set.begin();mnit!=mnoise_set.end();mnit++){

       // check that all the needed quark sources/sinks are available
       // before beginning the elementals

     double Amtimer=0.0,Aqtimer=0.0,Asstimer=0.0,Acftimer=0.0,Aqqtimer=0.0;

     bool flag=true;
     for (int dil1=0;dil1<ndilproj1;dil1++)
        flag = flag && qHandler1->queryData(sourceflag1,mnit->noise1,tproj1,dil1);
     for (int dil2=0;dil2<ndilproj2;dil2++)
        flag = flag && qHandler2->queryData(sourceflag2,mnit->noise2,tproj2,dil2);

     if (!flag){   // not all needed quark sinks available so skip
//       QDPIO::cerr << "Not all needed quark sources/sinks are available"
//                   << " for requested MesonSourceSink construction"<<endl;
//       QDPIO::cerr << "Skipping this computation"<<endl;
       continue;}

     QDPIO::cout << endl<<endl;
     QDPIO::cout << "Starting computation for time = "<<timeval<<endl;
     QDPIO::cout << "                      Quark line source times: "<<tsource1
                 <<" "<<tsource2<<endl;
     QDPIO::cout << "   Quark line time dilution projector indices: "<<tproj1
                 <<" "<<tproj2<<endl;
     QDPIO::cout << " Quark line noises: "<<mnit->noise1.getSeed()
                 <<" "<<mnit->noise2.getSeed() <<endl<<endl;
     
      // Now carry out the computations.  Each computation puts its results
      // in "results_add", but these are then added into "mresults" which
      // contains the meson operator source and sink functions by the
      // end of the computations.

     rolex.start();
     auto_ptr<LatticeComplex> singlet;
     multi1d<Complex> results_add;
     vector< multi2d<Complex> > mresults(nmeson_ops_thisnode);
     for (int k=0;k<nmeson_ops_thisnode;k++){
        mresults[k].resize(ndilproj1,ndilproj2);
        mresults[k]=zero;}

     rolex.stop();
     otimer+=rolex.getTimeInSeconds();
     StopWatch bulova,bulova2;
     bulova2.start();

     for (int dil1=0;dil1<ndilproj1;dil1++){
     for (int dil2=0;dil2<ndilproj2;dil2++){

       for (elit=elementals.begin();elit!=elementals.end();elit++){

        rolex.start();
        int dqbar=(elit->first).getAntiQuarkDisp();
        const LatticeColorVector* q1=qHandler1->getData(
                 sourceflag1,mnit->noise1,tproj1,dil1,
                 (elit->first).getAntiQuarkSpin(),DirPath(dqbar),
                 displacement_length,timeval);

        rolex.stop();
        Aqtimer+=rolex.getTimeInSeconds();
        if (q1==0) continue;
        rolex.start();
        int dq1=(elit->first).getQuarkDisp1();
        int dq2=(elit->first).getQuarkDisp2();
        const LatticeColorVector* q2=qHandler2->getData(
                 sourceflag2,mnit->noise2,tproj2,dil2,
                 (elit->first).getQuarkSpin(),DirPath(dq1,dq2),
                 displacement_length,timeval);

        rolex.stop();
        Aqtimer+=rolex.getTimeInSeconds();
        if (q2==0) continue;

        rolex.start();
        evaluateLocalInnerProduct(q1,q2,singlet);    // color contractions
        rolex.stop();
        Amtimer+=rolex.getTimeInSeconds();
        Aqqtimer+=rolex.getTimeInSeconds();

        rolex.start();
        multi1d<int> midpoint(3); midpoint=0;
        if (dqbar>0) midpoint[dqbar-1]+=displacement_length;
        else if (dqbar<0) midpoint[-dqbar-1]-=displacement_length;
        if (dq1>0) midpoint[dq1-1]+=displacement_length;
        else if (dq1<0) midpoint[-dq1-1]-=displacement_length;
        if (dq2>0) midpoint[dq2-1]+=displacement_length;
        else if (dq2<0) midpoint[-dq2-1]-=displacement_length;

        bulova.start();
        vector<LatticeComplex*> phptrs(elit->second.size());
        vector<DComplex> midphase(elit->second.size());
        int phcount=0;
        for (mmit=(elit->second).begin();mmit!=(elit->second).end();mmit++){
          int momind=mmit->first;
          Momentum p(moms[momind]);
          double midarg=0.5*(p.x*midpoint[0]*momquantum_x
                            +p.y*midpoint[1]*momquantum_y
                            +p.z*midpoint[2]*momquantum_z); 
          midphase[phcount]=cmplx(Double(cos(midarg)),Double(-sin(midarg)));
          phptrs[phcount++]=&(phases[momind]);}
        evaluateMomentumSums(phptrs,singlet.get(),results_add);
        bulova.stop();
        Asstimer+=bulova.getTimeInSeconds();
        for (phcount=0;phcount<results_add.size();phcount++){
            results_add[phcount]*=midphase[phcount];}

        bulova.start();
        phcount=0;
        for (mmit=(elit->second).begin();mmit!=(elit->second).end();phcount++,mmit++){
           const ICList& icit=mmit->second;
           for (unsigned int ic=0;ic<icit.size();ic++){
             int op_index=icit[ic].index;
             DComplex coef=icit[ic].coef;
             mresults[op_index](dil1,dil2)+=results_add[phcount]*coef;}}

         QDPInternal::broadcast(comflag);  // just to synchronize the nodes
         bulova.stop();
         rolex.stop();
         Amtimer+=rolex.getTimeInSeconds();
         Acftimer+=bulova.getTimeInSeconds();

        } // end of elit loop
       } // end of dil2 loop

      if (qHandler1!=qHandler2)
         qHandler1->clearData();
      } // end of dil1 loop

     QDPIO::cout << "  ... computation done"<<endl;

     rolex.start();
     qHandler1->clearData();
     qHandler2->clearData();
     rolex.stop();
     qtimer+=rolex.getTimeInSeconds();

         //  output to file

     rolex.start();
     RecordKey rkey(mnit->noise1,mnit->noise2,qline1,qline2,timeval);
     for (int k=0;k<nmeson_ops_thisnode;k++){
        DHSFputPtr->putData(CombinedKey(Mops[k+opindex_start],rkey),mresults[k]);}
     DHSFputPtr->flush();
     QDPInternal::broadcast(comflag);  // just to synchronize the nodes 
     rolex.stop();
     otimer+=rolex.getTimeInSeconds();
     QDPIO::cout << " Data base insertions done...this computation completed"<<endl;
     bulova2.stop();
     QDPIO::cout << " Time for this computation: "<<bulova2.getTimeInSeconds()<<" seconds"<<endl;

     QDPIO::cout << "    Quark handling time = "<<Aqtimer<<" seconds"<<endl;
     QDPIO::cout << "   Meson formation time = "<<Amtimer<<" seconds"<<endl;
     QDPIO::cout << "      time for q-qbar construction = "<<Aqqtimer<<"  seconds"<<endl;
     QDPIO::cout << "             time for spatial sums = "<<Asstimer<<"  seconds"<<endl;
     QDPIO::cout << "   time for operator accumulations = "<<Acftimer<<"  seconds"<<endl;

     mtimer+=Amtimer;
     qtimer+=Aqtimer;
     sstimer+=Asstimer;
     cftimer+=Acftimer;
     qqtimer+=Aqqtimer;

    }  // end of loop over noises

   qHandler1->clearGaugeData();
   qHandler2->clearGaugeData();
   
   }  // end loop over times 

  disconnectQuarkHandlers(); 
  }  // end loop over meson line calculations

 delete DHSFputPtr;
 DHSFputPtr=0;
 END_CODE();
 outer.stop();

 QDPIO::cout << endl<<endl<<" ************************************"<<endl<<endl;
 QDPIO::cout << "MesonHandler::compute finished: total time = "
             << outer.getTimeInSeconds()<<" seconds"<<endl;
 QDPIO::cout << "    Quark handling time = "<<qtimer<<" seconds"<<endl;
 QDPIO::cout << "   Meson formation time = "<<mtimer<<" seconds"<<endl;
 QDPIO::cout << "      time for q-qbar construction = "<<qqtimer<<"  seconds"<<endl;
 QDPIO::cout << "             time for spatial sums = "<<sstimer<<"  seconds"<<endl;
 QDPIO::cout << "   time for operator accumulations = "<<cftimer<<"  seconds"<<endl;
 QDPIO::cout << "  Other (incl. db) time = "<<otimer<<" seconds"<<endl<<endl;

}


// ***************************************************************************************


void MesonHandler::evaluateLocalInnerProduct(
                                 const LatticeColorVector *v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeComplex>& result)
{
 if ((v1==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeComplex);
 *result=localInnerProduct(*v1,*v2);
}


// ***************************************************************************************

bool MesonHandler::checkHeader(XmlReader& xmlr, int suffix)
{
 if (xml_tag_count(xmlr,"MesonHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::MesonHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml1(file_xml,"./AntiQuark");
 QuarkInfo quark1_check(xml1,gauge_check);
 XmlReader xml2(file_xml,"./Quark");
 QuarkInfo quark2_check(xml2,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quark1Ptr->checkEqual(quark1_check);
    quark2Ptr->checkEqual(quark2_check); }
 catch(...){ return false;}
 return true;
}


void MesonHandler::writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                                int suffix)
{
 push(xmlout,"MesonHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"AntiQuark");
 quark1Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark");
 quark2Ptr->output(xmlout);
 pop(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}


bool MesonHandler::checkHeader(XmlReader& xmlr)
{
 if (xml_tag_count(xmlr,"MesonHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::MesonHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml1(file_xml,"./AntiQuark");
 QuarkInfo quark1_check(xml1,gauge_check);
 XmlReader xml2(file_xml,"./Quark");
 QuarkInfo quark2_check(xml2,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quark1Ptr->checkEqual(quark1_check);
    quark2Ptr->checkEqual(quark2_check); }
 catch(...){ return false;}
 return true;
}


void MesonHandler::writeHeader(XmlWriter& xmlout)
{
 push(xmlout,"MesonHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"AntiQuark");
 quark1Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark");
 quark2Ptr->output(xmlout);
 pop(xmlout);
 pop(xmlout);
}

void MesonHandler::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"MesonHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    push(xmlout,"AntiQuark");
    quark1Ptr->output(xmlout);
    pop(xmlout);
    push(xmlout,"Quark");
    quark2Ptr->output(xmlout);
    pop(xmlout);
    pop(xmlout);}
}

                                           
// ***************************************************************************************



const MesonHandler::DataType& MesonHandler::getData(
                                         const MesonOperatorInfo& mop,
                                         const MesonLineEndInfo& mline,
                                         const MesonNoiseInfo& mnoise,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(mop,RecordKey(mnoise,mline,timevalue));
}


const MesonHandler::DataType& MesonHandler::getData(
                                         const MesonOperatorInfo& mop,
                                         const QuarkLineEndInfo& qbarline,
                                         const QuarkLineEndInfo& qline,
                                         const LaphNoiseInfo& qbarnoise,
                                         const LaphNoiseInfo& qnoise,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(mop,RecordKey(qbarnoise,qnoise,
                          qbarline,qline,timevalue));
}

          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

bool MesonHandler::queryData(const MesonOperatorInfo& mop,
                             const MesonLineEndInfo& mline,
                             const MesonNoiseInfo& mnoise,
                             int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(mop,RecordKey(mnoise,mline,timevalue));
}


bool MesonHandler::queryData(const MesonOperatorInfo& mop,
                             const QuarkLineEndInfo& qbarline,
                             const QuarkLineEndInfo& qline,
                             const LaphNoiseInfo& qbarnoise,
                             const LaphNoiseInfo& qnoise,
                             int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(mop,RecordKey(qbarnoise,qnoise,
                            qbarline,qline,timevalue));
}



bool MesonHandler::queryData(const MesonOperatorInfo& mop)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryFile(mop);
}

        // remove from internal memory

void MesonHandler::removeData(const MesonOperatorInfo& mop,
                              const MesonLineEndInfo& mline,
                              const MesonNoiseInfo& mnoise,
                              int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop,RecordKey(mnoise,mline,timevalue));
}


void MesonHandler::removeData(const MesonOperatorInfo& mop,
                              const QuarkLineEndInfo& qbarline,
                              const QuarkLineEndInfo& qline,
                              const LaphNoiseInfo& qbarnoise,
                              const LaphNoiseInfo& qnoise,
                              int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop,RecordKey(qbarnoise,qnoise,
                         qbarline,qline,timevalue));
}

   // removes all data associated with one meson operator

void MesonHandler::removeData(const MesonOperatorInfo& mop)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop);
}


void MesonHandler::clearData()
{
 if (DHgetPtr!=0) DHgetPtr->clearData();
} 
 
set<MesonOperatorInfo> MesonHandler::getMesonOperators() const
{
 check_info_set("getMesonOperators",1);
 return DHgetPtr->getFileKeys();
}

set<MesonHandler::MesonNoiseInfo> MesonHandler::getMesonNoises(const MesonOperatorInfo& minfo) const
{
 check_info_set("getMesonNoises",1);
 set<MesonHandler::RecordKey> buffer=DHgetPtr->getKeys(minfo);
 set<MesonHandler::MesonNoiseInfo> result;
 for (set<MesonHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(MesonNoiseInfo(it->noise1,it->noise2));
 return result;
}

set<MesonHandler::MesonLineEndInfo> MesonHandler::getMesonLineEndInfos(const MesonOperatorInfo& minfo) const
{
 check_info_set("getMesonLineEndInfos",1);
 set<MesonHandler::RecordKey> buffer=DHgetPtr->getKeys(minfo);
 set<MesonHandler::MesonLineEndInfo> result;
 for (set<MesonHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(MesonLineEndInfo(it->qline1,it->qline2));
 return result;
}


void MesonHandler::filefail(const string& message)
{
 QDPIO::cerr << message << endl;
 clear();
 QDP_abort(1);
}

void MesonHandler::check_info_set(const string& name,
                                  int check_mode) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in MesonHandler:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
 if (check_mode==1){
    if (!m_read_mode){
       QDPIO::cerr << "error in MesonHandler:"<<endl;
       QDPIO::cerr << "  must be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
 else if (check_mode==2){
    if (m_read_mode){
       QDPIO::cerr << "error in MesonHandler:"<<endl;
       QDPIO::cerr << "  must not be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
}


 // *********************************************************************

        //  Merge the meson line ends in "infiles" into the files in
        //  "mesonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "mesonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
 
void MesonHandler::merge(const FileListInfo& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonFile",
                "MesonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"MesonHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}


void MesonHandler::merge(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonFile",
                "MesonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"MesonHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}

void MesonHandler::reorganize(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonFile",
                "MesonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 
 for (unsigned int k=0;k<infiles.size();k++){
    QDPIO::cout << "merging file list k = "<<k<<endl;
    for (int suffix=infiles[k].getMinFileNumber();suffix<=infiles[k].getMaxFileNumber();suffix++){
       int nrecords=0;
       string fname=infiles[k].getFileName(suffix);
       if (!fileExists(fname)) continue;
       DataGetHandlerSF<MesonHandler,CombinedKey,DataType> inmerge(
                          *this,fname,"Laph--MesonFile");
       std::set<CombinedKey> rkeys=inmerge.getKeys();
       for (std::set<CombinedKey>::const_iterator rt=rkeys.begin();rt!=rkeys.end();rt++){
          const DataType& data=inmerge.getData(*rt);
          nrecords++;
          DHputPtr->putData(rt->mop,rt->rkey,data);
          inmerge.removeData(*rt);}
       QDPIO::cout << " file list "<<k<<":  from suffix "<<suffix
                   <<": number of records added = "<<nrecords<<endl;}
    }

 delete DHputPtr;
 DHputPtr=0;
}

// ***************************************************************************************







MesonInternalLoopHandler::RecordKey::RecordKey() : time_value(0) {}

MesonInternalLoopHandler::RecordKey::~RecordKey() {}

MesonInternalLoopHandler::RecordKey::RecordKey(const LaphNoiseInfo& in_noise, 
                                   const QuarkLineEndInfo& in_qline,
                                   int in_time)
  : noise(in_noise), qline(in_qline), time_value(in_time) {}

MesonInternalLoopHandler::RecordKey::RecordKey(const MesonIntLoopNoiseInfo& in_mnoise, 
                                   const MesonIntLoopLineEndInfo& in_mline,
                                   int in_time)
  : noise(in_mnoise.noise), qline(in_mline.qline), time_value(in_time) {}

MesonInternalLoopHandler::RecordKey::RecordKey(const MesonInternalLoopHandler::RecordKey& in)
  : noise(in.noise), qline(in.qline), time_value(in.time_value) {}

MesonInternalLoopHandler::RecordKey& MesonInternalLoopHandler::RecordKey::operator=(
                             const MesonInternalLoopHandler::RecordKey& in)
{
 noise=in.noise; 
 qline=in.qline; 
 time_value=in.time_value;
 return *this;
}


bool MesonInternalLoopHandler::RecordKey::operator<(
            const MesonInternalLoopHandler::RecordKey& rhs) const
{
 return multiLessThan(time_value,rhs.time_value,
               noise,rhs.noise, qline,rhs.qline);
}

bool MesonInternalLoopHandler::RecordKey::operator==(
            const MesonInternalLoopHandler::RecordKey& rhs) const
{
 return multiEqual(time_value,rhs.time_value,
               noise,rhs.noise, qline,rhs.qline);
}

bool MesonInternalLoopHandler::RecordKey::operator!=(
            const MesonInternalLoopHandler::RecordKey& rhs) const
{
 return multiNotEqual(time_value,rhs.time_value,
               noise,rhs.noise, qline,rhs.qline);
}

void MesonInternalLoopHandler::RecordKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"RecordKey");
 push(xmlw,"QuarkLine");
 noise.output(xmlw);
 qline.output(xmlw);
 pop(xmlw);
 write(xmlw,"TimeValue",time_value);
 pop(xmlw);
}


// *************************************************************************

MesonInternalLoopHandler::CombinedKey::CombinedKey() {}

MesonInternalLoopHandler::CombinedKey::~CombinedKey() {}

MesonInternalLoopHandler::CombinedKey::CombinedKey(const MesonOperatorInfo& in_mop,
                                                   const RecordKey& in_rkey)
  : rkey(in_rkey), mop(in_mop) {}

MesonInternalLoopHandler::CombinedKey::CombinedKey(const MesonInternalLoopHandler::CombinedKey& in)
  : rkey(in.rkey), mop(in.mop)  {}

MesonInternalLoopHandler::CombinedKey& MesonInternalLoopHandler::CombinedKey::operator=(
                             const MesonInternalLoopHandler::CombinedKey& in)
{
 mop=in.mop;
 rkey=in.rkey;
 return *this;
}


bool MesonInternalLoopHandler::CombinedKey::operator<(
            const MesonInternalLoopHandler::CombinedKey& rhs) const
{
 return multiLessThan(rkey,rhs.rkey, mop,rhs.mop);
}

bool MesonInternalLoopHandler::CombinedKey::operator==(
            const MesonInternalLoopHandler::CombinedKey& rhs) const
{
 return multiEqual(rkey,rhs.rkey, mop,rhs.mop);
}

bool MesonInternalLoopHandler::CombinedKey::operator!=(
            const MesonInternalLoopHandler::CombinedKey& rhs) const
{
 return multiNotEqual(rkey,rhs.rkey, mop,rhs.mop);
}

void MesonInternalLoopHandler::CombinedKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"CombinedKey");
 mop.output(xmlw);
 rkey.output(xmlw);
 pop(xmlw);
}


// *************************************************************************


const FileListInfo& MesonInternalLoopHandler::QuarkFileLists::getFixedSchemeFiles() const
{
 return qfxdfiles;
}

const FileListInfo& MesonInternalLoopHandler::QuarkFileLists::getRelativeSchemeFiles() const
{
 return qrelfiles;
}


// *************************************************************************

MesonInternalLoopHandler::MesonIntLoopCalc::MesonIntLoopCalc(XmlReader& xmlin, int k)
{
 string tagname="MesonIntLoopCalc["+int_to_string(k)+"]";
 if (xml_tag_count(xmlin,tagname)!=1){
    QDPIO::cerr << "Bad Xml input to MesonIntLoopCalc"<<endl;
    xmlreadfail(xmlin,"MesonIntLoopCalc");}
 XmlReader xmlr(xmlin, "./descendant-or-self::"+tagname);
 unsigned int rcode;
 code=read_mode(xmlr,"QuarkMode");
 rcode=read_type(xmlr,"QuarkType"); code<<=2; code|=rcode;

    //  read the meson noises

 if (xml_tag_count(xmlr,"MesonNoises")!=1){
    QDPIO::cerr << "Error in reading MesonIntLoopCalc: must have one <MesonNoises> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmln(xmlr,"./MesonNoises");
 if (xml_tag_count(xmln,"All")!=1){
    int ncalc=xml_tag_count(xmln,"MesonNoise");
    if (ncalc==0){
       QDPIO::cerr << "No meson noises given and <All/> not specified; quitting"<<endl;
       QDP_abort(1);}
    for (int k=1;k<=ncalc;++k) 
       mnoises.insert(MesonInternalLoopHandler::MesonIntLoopNoiseInfo(xmln,k));}
}

void MesonInternalLoopHandler::MesonIntLoopCalc::output(XmlWriter& xmlout) const
{
 push(xmlout,"MesonIntLoopCalc");
 unsigned int wcode=code;
 write(xmlout,"QuarkType",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"QuarkMode",write_mode(0x1u & wcode));
 push(xmlout,"MesonNoises");
 if (mnoises.size()==0){ push(xmlout,"All"); pop(xmlout);}
 else{
    for (set<MesonInternalLoopHandler::MesonIntLoopNoiseInfo>::const_iterator mnit=mnoises.begin();
       mnit!=mnoises.end();++mnit)
       mnit->output(xmlout);}
 pop(xmlout);
 pop(xmlout);
}

string MesonInternalLoopHandler::MesonIntLoopCalc::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 unsigned int wcode=code;
 oss << pad << "<MesonIntLoopCalc>"<<endl;
 oss << pad << "  <QuarkType>"<< write_type(0x3u & wcode)
     << "</QuarkType>"<<endl; wcode>>=2;
 oss << pad << "  <QuarkMode>"<< write_mode(0x1u & wcode)
     << "</QuarkMode>"<<endl;
 oss << pad << "  <MesonNoises>"<<endl;
 if (mnoises.size()==0) oss << pad <<"    <All/>"<<endl;
 else{
    for (set<MesonInternalLoopHandler::MesonIntLoopNoiseInfo>::const_iterator mnit=mnoises.begin();
       mnit!=mnoises.end();++mnit)
       oss << mnit->output(indent+1)<<endl;}
 oss << pad << "  </MesonNoises>"<<endl;
 oss << pad << "</MesonIntLoopCalc>"<<endl;
 return oss.str();
}

unsigned int MesonInternalLoopHandler::MesonIntLoopCalc::read_type(
                   XmlReader& xmlr, const string& tag)
{
 string end_type;
 xmlread(xmlr,tag,end_type,"MesonInternalLoopHandler::MesonIntLoopCalc");
 end_type=tidyString(end_type);
 int type=0;
 if (end_type=="FixedSource") type=0;
 else if (end_type=="RelativeSource") type=1;
 else if (end_type=="SinkWithFixedSource") type=2;
 else if (end_type=="SinkWithRelativeSource") type=3;
 else{
    QDPIO::cerr << "Bad Xml input to MesonInternalLoopHandler::MesonIntLoopCalc"<<endl;
    QDPIO::cerr << "Invalid <Type> tag"<<endl;
    xmlreadfail(xmlr,"MesonInternalLoopHandler::MesonIntLoopCalc");}
 return type;
}

string MesonInternalLoopHandler::MesonIntLoopCalc::write_type(unsigned int end_type) const
{
 if (end_type==0) return "FixedSource";
 else if (end_type==1) return "RelativeSource";
 else if (end_type==2) return "SinkWithFixedSource";
 else return "SinkWithRelativeSource";
}

unsigned int MesonInternalLoopHandler::MesonIntLoopCalc::read_mode(
                   XmlReader& xmlr, const string& tag)
{
 int mode=0;
 string modestr;
 if (xmlreadif(xmlr,tag,modestr,"MesonInternalLoopHandler::MesonIntLoopCalc")){
    modestr=tidyString(modestr);
    if (modestr=="Normal") mode=0;
    else if (modestr=="Gamma5HermConj") mode=1;
    else{
       QDPIO::cerr << "Bad Xml input to MesonInternalLoopHandler::MesonIntLoopCalc"<<endl;
       QDPIO::cerr << "Invalid <Mode> tag"<<endl;
       xmlreadfail(xmlr,"MesonInternalLoopHandler::MesonIntLoopCalc");}}
 return mode;
}

string MesonInternalLoopHandler::MesonIntLoopCalc::write_mode(unsigned int mode) const
{
 if (mode==0) return "Normal";
 else return "Gamma5HermConj";
}

bool MesonInternalLoopHandler::MesonIntLoopCalc::isSource(int quark) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=1;
 if (quark==2)
    return ((vcode&0x1u)==0);
 else
    return ((vcode&0x1u)!=0);
}

bool MesonInternalLoopHandler::MesonIntLoopCalc::isNormalMode() const
{
 unsigned int vcode=code;
 vcode>>=2;
 return ((vcode&0x1u)==0);
}

bool MesonInternalLoopHandler::MesonIntLoopCalc::isGamma5HermConjMode() const
{
 unsigned int vcode=code;
 vcode>>=2;
 return ((vcode&0x1u)!=0);
}

bool MesonInternalLoopHandler::MesonIntLoopCalc::isFixedSourceScheme() const
{
 return ((code&0x1u)==0);
}

bool MesonInternalLoopHandler::MesonIntLoopCalc::isRelativeSource() const
{
 return ((code&0x3u)==1);
}

QuarkLineEndInfo MesonInternalLoopHandler::MesonIntLoopCalc::getLineInfo(
                      int quark, int t0, int lat_textent) const
{
 if ((quark<1)||(quark>2)){
    QDPIO::cerr << "invalid quark number in meson"<<endl;
    QDP_abort(1);}
 unsigned int qmode=code;
 qmode>>=2;
 qmode&=0x1u;
 unsigned int qtype=code;
 qtype&=0x3u;
 if (quark==1){
    qmode^=0x1u;
    qtype^=0x3u;}
 int source_index= (qtype==2)? t0 : 0;
 return QuarkLineEndInfo(qtype,qmode,source_index,lat_textent);
}


set<MesonInternalLoopHandler::MesonIntLoopNoiseInfo> 
     MesonInternalLoopHandler::MesonIntLoopCalc::getAvailableNoises(
                              const set<LaphNoiseInfo>& Noises) const
{
 set<MesonIntLoopNoiseInfo> result;
 if (mnoises.size()==0){
    for (set<LaphNoiseInfo>::const_iterator it=Noises.begin();
         it!=Noises.end();it++)
       result.insert(MesonInternalLoopHandler::MesonIntLoopNoiseInfo(*it));}
 else{
    for (set<MesonIntLoopNoiseInfo>::const_iterator it=mnoises.begin();
         it!=mnoises.end();it++)
       if (Noises.find(it->noise)!=Noises.end())
          result.insert(*it);}
 return result;
}

// *************************************************************************


void MesonInternalLoopHandler::MesonIntLoopNoiseInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"MesonIntLoopNoiseInfo");
 noise.output(xmlout);
 pop(xmlout);
}

string MesonInternalLoopHandler::MesonIntLoopNoiseInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<MesonIntLoopNoiseInfo>"<<endl;
 oss << noise.output(indent+1);
 oss << pad << "</MesonIntLoopNoiseInfo>"<<endl;
 return oss.str();
}

// *************************************************************************

void MesonInternalLoopHandler::QuarkFileLists::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkFileLists");
 push(xmlout,"Quark");
 push(xmlout,"FixedSource");
 qfxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 qrelfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 pop(xmlout);
}

string MesonInternalLoopHandler::QuarkFileLists::output() const
{
 XmlBufferWriter xmlout;
 output(xmlout);
 return xmlout.str();
}


// *************************************************************************


MesonInternalLoopHandler::MesonInternalLoopHandler()
        :  uPtr(0), gSmearPtr(0), quarkPtr(0),
           fPtr(0), qHandler(0), DHputPtr(0), DHgetPtr(0), 
           DHSFputPtr(0), DHSFgetPtr(0), m_read_mode(true) {}


MesonInternalLoopHandler::MesonInternalLoopHandler(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkInfo& quark,
                           const FileListInfo& flist,
                           bool read_mode)
        :  uPtr(0), gSmearPtr(0), quarkPtr(0), fPtr(0), qHandler(0), 
           DHputPtr(0), DHgetPtr(0), DHSFputPtr(0), DHSFgetPtr(0)
{
 set_info(gaugeinfo,gluonsmear,quark,flist,read_mode);
}


void MesonInternalLoopHandler::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                           const GluonSmearingInfo& gluonsmear,
                           const QuarkInfo& quark,
                           const FileListInfo& flist,
                           bool read_mode)
{
 clear();
 set_info(gaugeinfo,gluonsmear,quark,flist,read_mode);
}


void MesonInternalLoopHandler::set_info(const GaugeConfigurationInfo& gaugeinfo,
                            const GluonSmearingInfo& gluonsmear,
                            const QuarkInfo& quark,
                            const FileListInfo& flist,
                            bool read_mode)
{
 m_read_mode=read_mode;
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    quarkPtr = new QuarkInfo(quark);
    fPtr = new FileListInfo(flist);   
    if (m_read_mode)
       DHgetPtr = new DataGetHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,
                          DataType>(*this,flist,"Laph--MesonIntLoopFile",
                          "MesonIntLoopHandlerDataFile");}
 catch(...){
    QDPIO::cerr << "allocation problem in MesonInternalLoopHandler"<<endl;
    clear();
    QDP_abort(1);}
}



void MesonInternalLoopHandler::connectQuarkHandlers(const MesonIntLoopCalc& Mcalc,
                                        const QuarkFileLists& Qfiles,
                                        const string& smeared_gauge_filename,
                                        const string& smeared_quark_filestub)
{
 check_info_set("connectQuarkHandlers",2);
 disconnectQuarkHandlers();
 try{

         // connect to quark handler for quark

    const DilutionSchemeInfo& dil=(Mcalc.isFixedSourceScheme())?
             quarkPtr->getFixedSourceDilutionScheme() :
             quarkPtr->getRelativeSourceDilutionScheme();
    const FileListInfo& qfiles=(Mcalc.isFixedSourceScheme())?
             Qfiles.getFixedSchemeFiles() :
             Qfiles.getRelativeSchemeFiles();
    qHandler = new QuarkHandler(*uPtr,*gSmearPtr,quarkPtr->getSmearing(),
                                dil,quarkPtr->getAction(),qfiles,
                                smeared_quark_filestub,
                                smeared_gauge_filename);
    if (Mcalc.isGamma5HermConjMode()) 
       qHandler->setGamma5HermiticityMode();

    }
 catch(...){
    QDPIO::cerr << "allocation problem in MesonInternalLoopHandler::connectQuarkHandlers"<<endl;
    QDP_abort(1);}
}



void MesonInternalLoopHandler::disconnectQuarkHandlers()
{
 delete qHandler;
 qHandler=0;
}



MesonInternalLoopHandler::~MesonInternalLoopHandler()
{
 clear();
}

void MesonInternalLoopHandler::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete quarkPtr;
    delete fPtr;
    delete qHandler;
    delete DHputPtr;
    delete DHgetPtr;
    delete DHSFputPtr;
    delete DHSFgetPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 quarkPtr=0;
 fPtr=0;
 qHandler=0;
 DHputPtr=0;
 DHgetPtr=0;
 DHSFputPtr=0;
 DHSFgetPtr=0;
}




bool MesonInternalLoopHandler::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(fPtr!=0)&&(quarkPtr!=0));
}


const GaugeConfigurationInfo& MesonInternalLoopHandler::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& MesonInternalLoopHandler::getGluonSmearingInfo() const
{
 check_info_set("getFieldSmearingInfo");
 return *gSmearPtr;
}

const QuarkInfo& MesonInternalLoopHandler::getQuarkInfo() const 
{
 check_info_set("getQuarkInfo");
 return *quarkPtr;
}

const FileListInfo& MesonInternalLoopHandler::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

void MesonInternalLoopHandler::getFileMap(XmlWriter& xmlout) const
{
 if ((isInfoSet())&&(m_read_mode))
    DHgetPtr->getFileMap(xmlout);
}

void MesonInternalLoopHandler::outputKeys(XmlWriter& xmlout)
{
 if ((isInfoSet())&&(m_read_mode)) 
    DHgetPtr->outputKeys(xmlout);
}

   // ***************************************************************
   
        //  Compute line ends for all meson operators in "Mops" and
        //  all dilution indices.  Repeat for all requested meson calculations
        //  given in "mcalcs" (which contains all mesons noises to compute).
        //  If the list of meson noises is empty, all available noises 
        //  in the quark sink files are used.
        //  For relative sources, only times from "relativesourcetmin" to 
        //  "relativesourcetmax" will be done (if "relativesourcetmax" is -1,
        //  then ALL times on the lattice are done.


void MesonInternalLoopHandler::compute(
                      const set<MesonOperatorInfo>& MopInfos,
                      const string& CoefsTopDirectory,
                      const QuarkFileLists& Qfiles,
                      const set<MesonIntLoopCalc>& mcalcs,
                      const string& smeared_gauge_filename,
                      const string& smeared_quark_filestub,
                      int relativesourcetmin, int relativesourcetmax)
{

   // check info is set and handler is NOT in read mode
 check_info_set("compute",2);

 QDPIO::cout <<endl<<endl;
 QDPIO::cout << "********************************************************"<<endl;
 QDPIO::cout << endl<<"  MesonInternalLoopHandler::compute commencing..."<<endl<<endl;

 START_CODE();
 double mtimer=0.0,qtimer=0.0,otimer=0.0;
 double sstimer=0.0, cftimer=0.0, qqtimer=0.0;
 StopWatch rolex,outer;
 outer.start();
 rolex.start();

     // *******************************************************
     // *                                                     *
     // *  set up structures related to the meson operators   *
     // *                                                     *
     // *******************************************************

 int nmeson_ops=MopInfos.size();
 if (nmeson_ops==0){
    QDPIO::cerr << "No meson operators in MesonInteralLoopHandler::computeSourcesSinks"<<endl;
    return;}
 SetIndexer<MesonOperatorInfo> Mops(MopInfos);

   // Check that all meson operators have the same flavor,
   // and that the quark and antiquark have the SAME flavor

 string flavor=Mops[0].getFlavor();
 if (flavor[0]!=flavor[1]){
    QDPIO::cerr << "Quark and antiquark must have same flavor"
                << " for a nonzero internal loop"<<endl;
    return;}
 for (int i=1;i<nmeson_ops;i++){
    if (Mops[i].getFlavor()!=flavor){
       QDPIO::cerr << "All meson operators must have same flavor content"<<endl;
       return;}}

   // Check that all meson operators have the same displacement length if nonzero

 int displacement_length=0;
 for (int i=0;i<nmeson_ops;i++){
    int dl=Mops[i].getDisplacementLength();
    if (displacement_length==0) displacement_length=dl;
    if ((dl!=0)&&(dl!=displacement_length)){
       QDPIO::cerr << "All meson operators must have the same displacement"
                   << " length if nonzero"<<endl;
       return;}}

    // Create the operators by reading the elemental coefficients

 QDPIO::cout << "Reading the meson operator elemental terms"<<endl;
 MesonOperator::initialize(tidyString(CoefsTopDirectory));
 vector<MesonOperator*> mop(nmeson_ops);
 for (int k=0;k<nmeson_ops;k++) mop[k]=0;  // start with null pointers
 try{
    for (int k=0;k<nmeson_ops;k++)
       mop[k]=new MesonOperator(Mops[k]);}
 catch(const string& err){
    QDPIO::cerr << "Error in MesonInternalLoopHandler::compute"<<endl;
    QDPIO::cerr << err <<endl;
    for (int k=0;k<nmeson_ops;k++) delete mop[k];
    return;}

     //  Output meson operators and build momenta set

 map<Momentum,int> mominds;
 int momcount=0;
 for (int k=0;k<nmeson_ops;k++){
    QDPIO::cout << "Meson operator "<<k<<":"<<endl;
    QDPIO::cout << Mops[k].output()<<endl;
    if (mominds.find(mop[k]->getMomentumValue())==mominds.end())
       mominds.insert(make_pair(mop[k]->getMomentumValue(),momcount++));}

     //  Make and store the momenta phases: our annihilation operators
     //  need exp(-I p.x) 

 multi1d<LatticeComplex> phases(mominds.size());
 multi1d<Momentum> moms(mominds.size());
 for (map<Momentum,int>::const_iterator mit=mominds.begin();
      mit!=mominds.end();mit++){
    phases[mit->second]=makeMomPhaseField(mit->first);
    moms[mit->second]=mit->first;}

    // The next step is to construct the "computations" map.
    // A given "computation" is a site color contraction, combined
    // with a summation over all spatial sites, for various momenta.
    // We will then loop over all such "computations", adding the
    // results to the pertinent meson operators as specified in
    // the computations map.

 typedef vector<MesonOperator::IndexCoef>  ICList;
 typedef map<int, ICList> MomMap;
 typedef map<MesonOperator::Elemental, MomMap> CSMap;

 CSMap elementals;    // the crucial map structure
 bool comflag=true;   // for synchronizing nodes

   //  Divide "nmeson_ops" meson operators onto "nodes" nodes.
   //      x processors get m mesons,  nodes-x get  m-1 mesons.
   //         m = nmeson_ops/nodes + 1    x = nmeson_ops % nodes

 int nmeson_ops_thisnode;
 int opindex_start;

 {int nodes=Layout::numNodes();
 int thisnode=Layout::nodeNumber();
 int m = nmeson_ops/nodes + 1;
 int x = nmeson_ops % nodes;
 if (thisnode<x){
    nmeson_ops_thisnode=m;
    opindex_start=m*thisnode;}
 else{
    nmeson_ops_thisnode=m-1;
    opindex_start=m*x+(m-1)*(thisnode-x);}}
 int opindex_last=opindex_start+nmeson_ops_thisnode-1;


 for (int i=0;i<nmeson_ops;i++){

    int momind=mominds[mop[i]->getMomentumValue()];

    for (MesonOperator::ElementalIterator it=mop[i]->begin();
                                          it!=mop[i]->end();it++){

       MomMap& mommap=elementals[it->el];
       ICList& indcflist=mommap[momind];
       if ((i>=opindex_start)&&(i<=opindex_last))
          indcflist.push_back(MesonOperator::IndexCoef(i-opindex_start,it->coef));}}

 for (int k=0;k<nmeson_ops;k++) delete mop[k];  // don't need these anymore

    //  set up the files for output (local to each node)

 if (nmeson_ops_thisnode>0){
  try{
    int suffix=fPtr->getMinFileNumber()+Layout::nodeNumber();
    FileListInfo finfo_thisnode(fPtr->getFileStub(),suffix,suffix,
                                fPtr->isModeOverwrite());
    string fname=finfo_thisnode.getFileName(suffix);
    DHSFputPtr = new DataPutHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType>(
            *this,fname,"Laph--MesonIntLoopFile",fPtr->isModeOverwrite(),false);
    }
  catch(...){
    std::cerr << "problem setting up output files in MesonInternalLoopHandler"<<endl;
    QDP_abort(1);}}

 CSMap::iterator elit;
 MomMap::iterator mmit;
 ICList::const_iterator icit;

 {vector<int> num_elementals(mominds.size(),0);
 for (elit=elementals.begin();elit!=elementals.end();elit++)
    for (mmit=(elit->second).begin();mmit!=(elit->second).end();mmit++)
       ++num_elementals[mmit->first];
 QDPIO::cout << endl;
 QDPIO::cout << "               Number of meson operators = "<<nmeson_ops<<endl;
 QDPIO::cout << "    Displacement length of all operators = "<<displacement_length<<endl;
 QDPIO::cout << "             Number of different momenta = "<<mominds.size()<<endl<<endl;
 for (unsigned int k=0;k<mominds.size();k++)
    QDPIO::cout << " Number of elementals for momentum["<<k<<"] = "
                <<num_elementals[k]<<endl;} 

 rolex.stop();
 otimer+=rolex.getTimeInSeconds();
 double twopi=6.2831853071795864770;
 double momquantum_x=twopi / toDouble(Layout::lattSize()[0]);
 double momquantum_y=twopi / toDouble(Layout::lattSize()[1]);
 double momquantum_z=twopi / toDouble(Layout::lattSize()[2]);

     // **********************************************************
     // *                                                        *
     // *    loop over the requested meson line calculations     *
     // *                                                        *
     // **********************************************************

 for (set<MesonIntLoopCalc>::const_iterator 
      mcit=mcalcs.begin();mcit!=mcalcs.end();++mcit){

  rolex.start();
  QDPIO::cout << endl<<endl;
  QDPIO::cout <<" ************************************************"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" *    Starting new meson line calculations:     *"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" ************************************************"<<endl<<endl;
  QDPIO::cout << mcit->output() << endl<<endl;

   // check for valid meson operator: one psi and one psi-bar field

  bool sourceflag1=mcit->isSource(1);
  bool sourceflag2=mcit->isSource(2);

   // fire up the needed sub-handlers

  connectQuarkHandlers(*mcit,Qfiles,smeared_gauge_filename,
                       smeared_quark_filestub);
  QDPIO::cout << "connection to QuarkHandlers established"<<endl;

  int ndilproj=qHandler->getNumberOfSpinEigvecDilutionProjectors();
  QDPIO::cout << "Number of spin-eigenvector dilution indices: "
              <<ndilproj<<endl<<endl;

  rolex.stop();
  otimer+=rolex.getTimeInSeconds();

     // get the noises and source times available for each
     // quark line (use all times for RelativeSource); then take
     // intersection of the source times; for noises, take 
     // intersections with requested noises (if any)

  int Textent=uPtr->getTimeExtent();
  set<int> start_times;
  set<MesonIntLoopNoiseInfo> mnoise_set;

  {set<LaphNoiseInfo> noiseset;
   qHandler->getNoisesAndSourceTimes(noiseset,start_times);
   mnoise_set=mcit->getAvailableNoises(noiseset);}

   if (mcit->isRelativeSource()){
      if (relativesourcetmax<0)
         for (int t=0;t<Textent;t++) start_times.insert(t);
      else{
         int ttmin=relativesourcetmin;
         if (ttmin<0) ttmin=0;
         int ttmax=relativesourcetmax;
         if (ttmax>=Textent) ttmax=Textent-1;
         start_times.clear();
         for (int t=ttmin;t<=ttmax;t++) start_times.insert(t);}}

   // loop over source times

  for (set<int>::const_iterator t0=start_times.begin();
       t0!=start_times.end();++t0){

   int timeval=(*t0);
   if (timeval>=Textent) timeval-=Textent;

   QuarkLineEndInfo qline1(mcit->getLineInfo(1,*t0,Textent));
   QuarkLineEndInfo qline2(mcit->getLineInfo(2,*t0,Textent));

   int tsource=qline2.getSourceTime(timeval,*uPtr);
   int tproj=qHandler->getTimeDilutionProjectorIndex(tsource);

   // loop over meson noises

   for (set<MesonIntLoopNoiseInfo>::const_iterator
        mnit=mnoise_set.begin();mnit!=mnoise_set.end();mnit++){

       // check that all the needed quark sources/sinks are available
       // before beginning the elementals

     bool flag=true;
     for (int dil=0;dil<ndilproj;dil++){
        flag = flag && qHandler->queryData(sourceflag1,mnit->noise,tproj,dil);
        flag = flag && qHandler->queryData(sourceflag2,mnit->noise,tproj,dil);}

     if (!flag){   // not all needed quark sinks available so skip
//       QDPIO::cerr << "Not all needed quark sources/sinks are available"
//                   << " for requested MesonSourceSink construction"<<endl;
//       QDPIO::cerr << "Skipping this computation"<<endl;
       continue;}

     QDPIO::cout << endl<<endl;
     QDPIO::cout << "Starting computation for time = "<<timeval<<endl;
     QDPIO::cout << "                      Source times: "<<tsource<<endl;
     QDPIO::cout << "   Time dilution projector indices: "<<tproj<<endl;
     QDPIO::cout << "     Noise: "<<mnit->noise.getSeed()<<endl<<endl;
     
      // Now carry out the computations.  Each computation puts its results
      // in "results_add", but these are then added into "mresults" which
      // contains the meson operator source and sink functions by the
      // end of the computations.

     rolex.start();
     auto_ptr<LatticeComplex> singlet;
     multi1d<Complex> results_add;
     vector< Complex > mresults(nmeson_ops_thisnode);
     for (int k=0;k<nmeson_ops_thisnode;k++){
        mresults[k]=zero;}

     rolex.stop();
     otimer+=rolex.getTimeInSeconds();
     StopWatch bulova,bulova2;
     bulova2.start();

     for (int dil=0;dil<ndilproj;dil++){

       for (elit=elementals.begin();elit!=elementals.end();elit++){

        rolex.start();
        int dqbar=(elit->first).getAntiQuarkDisp();
        const LatticeColorVector* q1=qHandler->getData(
                 sourceflag1,mnit->noise,tproj,dil,
                 (elit->first).getAntiQuarkSpin(),DirPath(dqbar),
                 displacement_length,timeval);

        rolex.stop();
        qtimer+=rolex.getTimeInSeconds();
        if (q1==0) continue;
        rolex.start();
        int dq1=(elit->first).getQuarkDisp1();
        int dq2=(elit->first).getQuarkDisp2();
        const LatticeColorVector* q2=qHandler->getData(
                 sourceflag2,mnit->noise,tproj,dil,
                 (elit->first).getQuarkSpin(),DirPath(dq1,dq2),
                 displacement_length,timeval);

        rolex.stop();
        qtimer+=rolex.getTimeInSeconds();
        if (q2==0) continue;

        rolex.start();
        evaluateLocalInnerProduct(q1,q2,singlet);    // color contractions
        rolex.stop();
        mtimer+=rolex.getTimeInSeconds();
        qqtimer+=rolex.getTimeInSeconds();

        rolex.start();
        multi1d<int> midpoint(3); midpoint=0;
        if (dqbar>0) midpoint[dqbar-1]+=displacement_length;
        else if (dqbar<0) midpoint[-dqbar-1]-=displacement_length;
        if (dq1>0) midpoint[dq1-1]+=displacement_length;
        else if (dq1<0) midpoint[-dq1-1]-=displacement_length;
        if (dq2>0) midpoint[dq2-1]+=displacement_length;
        else if (dq2<0) midpoint[-dq2-1]-=displacement_length;

        bulova.start();
        vector<LatticeComplex*> phptrs(elit->second.size());
        vector<DComplex> midphase(elit->second.size());
        int phcount=0;
        for (mmit=(elit->second).begin();mmit!=(elit->second).end();mmit++){
          int momind=mmit->first;
          Momentum p(moms[momind]);
          double midarg=0.5*(p.x*midpoint[0]*momquantum_x
                            +p.y*midpoint[1]*momquantum_y
                            +p.z*midpoint[2]*momquantum_z); 
          midphase[phcount]=cmplx(Double(cos(midarg)),Double(-sin(midarg)));
          phptrs[phcount++]=&(phases[momind]);}
        evaluateMomentumSums(phptrs,singlet.get(),results_add);
        bulova.stop();
        sstimer+=bulova.getTimeInSeconds();
        for (phcount=0;phcount<results_add.size();phcount++){
            results_add[phcount]*=midphase[phcount];}

        bulova.start();
        phcount=0;
        for (mmit=(elit->second).begin();mmit!=(elit->second).end();phcount++,mmit++){
           const ICList& icit=mmit->second;
           for (unsigned int ic=0;ic<icit.size();ic++){
             int op_index=icit[ic].index;
             DComplex coef=icit[ic].coef;
             mresults[op_index]+=results_add[phcount]*coef;}}

         QDPInternal::broadcast(comflag);  // just to synchronize the nodes
         bulova.stop();
         rolex.stop();
         mtimer+=rolex.getTimeInSeconds();
         cftimer+=bulova.getTimeInSeconds();

        } // end of elit loop
      } // end of dil loop

     QDPIO::cout << "  ... computation done"<<endl;

     rolex.start();
     qHandler->clearData();
     rolex.stop();
     qtimer+=rolex.getTimeInSeconds();

         //  output to file

     rolex.start();
     RecordKey rkey(mnit->noise,qline2,timeval);
     for (int k=0;k<nmeson_ops_thisnode;k++){
        DHSFputPtr->putData(CombinedKey(Mops[k+opindex_start],rkey),mresults[k]);}
     DHSFputPtr->flush();
     QDPInternal::broadcast(comflag);  // just to synchronize the nodes 
     rolex.stop();
     otimer+=rolex.getTimeInSeconds();
     QDPIO::cout << " Data base insertions done...this computation completed"<<endl;
     bulova2.stop();
     QDPIO::cout << " Time for this computation: "<<bulova2.getTimeInSeconds()<<" seconds"<<endl;

    }  // end of loop over noises

   qHandler->clearGaugeData();
   
   }  // end loop over times 

  disconnectQuarkHandlers(); 
  }  // end loop over meson line calculations

 delete DHSFputPtr;
 DHSFputPtr=0;
 END_CODE();
 outer.stop();

 QDPIO::cout << endl<<endl<<" ************************************"<<endl<<endl;
 QDPIO::cout << "MesonInternalLoopHandler::compute finished: total time = "
             << outer.getTimeInSeconds()<<" seconds"<<endl;
 QDPIO::cout << "    Quark handling time = "<<qtimer<<" seconds"<<endl;
 QDPIO::cout << "   Meson formation time = "<<mtimer<<" seconds"<<endl;
 QDPIO::cout << "      time for q-qbar construction = "<<qqtimer<<"  seconds"<<endl;
 QDPIO::cout << "             time for spatial sums = "<<sstimer<<"  seconds"<<endl;
 QDPIO::cout << "   time for operator accumulations = "<<cftimer<<"  seconds"<<endl;
 QDPIO::cout << "  Other (incl. db) time = "<<otimer<<" seconds"<<endl<<endl;

}


// ***************************************************************************************


void MesonInternalLoopHandler::evaluateLocalInnerProduct(
                                 const LatticeColorVector *v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeComplex>& result)
{
 if ((v1==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeComplex);
 *result=localInnerProduct(*v1,*v2);
}


// ***************************************************************************************

bool MesonInternalLoopHandler::checkHeader(XmlReader& xmlr, int suffix)
{
 if (xml_tag_count(xmlr,"MesonIntLoopHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::MesonIntLoopHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml2(file_xml,"./Quark");
 QuarkInfo quark2_check(xml2,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quarkPtr->checkEqual(quark2_check); }
 catch(...){ return false;}
 return true;
}


void MesonInternalLoopHandler::writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                                int suffix)
{
 push(xmlout,"MesonIntLoopHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"Quark");
 quarkPtr->output(xmlout);
 pop(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}

bool MesonInternalLoopHandler::checkHeader(XmlReader& xmlr)
{
 if (xml_tag_count(xmlr,"MesonIntLoopHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::MesonIntLoopHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml2(file_xml,"./Quark");
 QuarkInfo quark2_check(xml2,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quarkPtr->checkEqual(quark2_check); }
 catch(...){ return false;}
 return true;
}


void MesonInternalLoopHandler::writeHeader(XmlWriter& xmlout)
{
 push(xmlout,"MesonIntLoopHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"Quark");
 quarkPtr->output(xmlout);
 pop(xmlout);
 pop(xmlout);
}

void MesonInternalLoopHandler::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"MesonIntLoopHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    push(xmlout,"Quark");
    quarkPtr->output(xmlout);
    pop(xmlout);
    pop(xmlout);}
}

                                           
// ***************************************************************************************



const MesonInternalLoopHandler::DataType& MesonInternalLoopHandler::getData(
                                         const MesonOperatorInfo& mop,
                                         const MesonIntLoopLineEndInfo& mline,
                                         const MesonIntLoopNoiseInfo& mnoise,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(mop,RecordKey(mnoise,mline,timevalue));
}


const MesonInternalLoopHandler::DataType& MesonInternalLoopHandler::getData(
                                         const MesonOperatorInfo& mop,
                                         const QuarkLineEndInfo& qline,
                                         const LaphNoiseInfo& qnoise,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(mop,RecordKey(qnoise,qline,timevalue));
}

          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

bool MesonInternalLoopHandler::queryData(const MesonOperatorInfo& mop,
                             const MesonIntLoopLineEndInfo& mline,
                             const MesonIntLoopNoiseInfo& mnoise,
                             int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(mop,RecordKey(mnoise,mline,timevalue));
}


bool MesonInternalLoopHandler::queryData(const MesonOperatorInfo& mop,
                             const QuarkLineEndInfo& qline,
                             const LaphNoiseInfo& qnoise,
                             int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(mop,RecordKey(qnoise,qline,timevalue));
}



bool MesonInternalLoopHandler::queryData(const MesonOperatorInfo& mop)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryFile(mop);
}

        // remove from internal memory

void MesonInternalLoopHandler::removeData(const MesonOperatorInfo& mop,
                              const MesonIntLoopLineEndInfo& mline,
                              const MesonIntLoopNoiseInfo& mnoise,
                              int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop,RecordKey(mnoise,mline,timevalue));
}


void MesonInternalLoopHandler::removeData(const MesonOperatorInfo& mop,
                              const QuarkLineEndInfo& qline,
                              const LaphNoiseInfo& qnoise,
                              int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop,RecordKey(qnoise,qline,timevalue));
}

   // removes all data associated with one meson operator

void MesonInternalLoopHandler::removeData(const MesonOperatorInfo& mop)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(mop);
}


void MesonInternalLoopHandler::clearData()
{
 if (DHgetPtr!=0) DHgetPtr->clearData();
} 
 

set<MesonOperatorInfo> MesonInternalLoopHandler::getMesonOperators() const
{
 check_info_set("getMesonOperators",1);
 return DHgetPtr->getFileKeys();
}

set<MesonInternalLoopHandler::MesonIntLoopNoiseInfo> MesonInternalLoopHandler::getMesonNoises(
         const MesonOperatorInfo& minfo) const
{
 check_info_set("getMesonNoises",1);
 set<MesonInternalLoopHandler::RecordKey> buffer=DHgetPtr->getKeys(minfo);
 set<MesonInternalLoopHandler::MesonIntLoopNoiseInfo> result;
 for (set<MesonInternalLoopHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(MesonIntLoopNoiseInfo(it->noise));
 return result;
}

set<MesonInternalLoopHandler::MesonIntLoopLineEndInfo> MesonInternalLoopHandler::getMesonLineEndInfos(
            const MesonOperatorInfo& minfo) const
{
 check_info_set("getMesonLineEndInfos",1);
 set<MesonInternalLoopHandler::RecordKey> buffer=DHgetPtr->getKeys(minfo);
 set<MesonInternalLoopHandler::MesonIntLoopLineEndInfo> result;
 for (set<MesonInternalLoopHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(MesonIntLoopLineEndInfo(it->qline));
 return result;
}


void MesonInternalLoopHandler::filefail(const string& message)
{
 QDPIO::cerr << message << endl;
 clear();
 QDP_abort(1);
}

void MesonInternalLoopHandler::check_info_set(const string& name,
                                  int check_mode) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in MesonInternalLoopHandler:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
 if (check_mode==1){
    if (!m_read_mode){
       QDPIO::cerr << "error in MesonInternalLoopHandler:"<<endl;
       QDPIO::cerr << "  must be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
 else if (check_mode==2){
    if (m_read_mode){
       QDPIO::cerr << "error in MesonInternalLoopHandler:"<<endl;
       QDPIO::cerr << "  must not be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
}


 // *********************************************************************

        //  Merge the meson line ends in "infiles" into the files in
        //  "mesonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "mesonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
 
void MesonInternalLoopHandler::merge(const FileListInfo& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonIntLoopFile",
                "MesonIntLoopHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonInternalLoopHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"MesonIntLoopHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}


void MesonInternalLoopHandler::merge(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonIntLoopFile",
                "MesonIntLoopHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonInternalLoopHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"MesonIntLoopHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}

void MesonInternalLoopHandler::reorganize(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<MesonInternalLoopHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--MesonIntLoopFile",
                "MesonIntLoopHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonInternalLoopHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 
 for (unsigned int k=0;k<infiles.size();k++){
    QDPIO::cout << "merging file list k = "<<k<<endl;
    for (int suffix=infiles[k].getMinFileNumber();suffix<=infiles[k].getMaxFileNumber();suffix++){
       int nrecords=0;
       string fname=infiles[k].getFileName(suffix);
       if (!fileExists(fname)) continue;
       DataGetHandlerSF<MesonInternalLoopHandler,CombinedKey,DataType> inmerge(
                          *this,fname,"Laph--MesonIntLoopFile");
       std::set<CombinedKey> rkeys=inmerge.getKeys();
       for (std::set<CombinedKey>::const_iterator rt=rkeys.begin();rt!=rkeys.end();rt++){
          const DataType& data=inmerge.getData(*rt);
          nrecords++;
          DHputPtr->putData(rt->mop,rt->rkey,data);
          inmerge.removeData(*rt);}
       QDPIO::cout << " file list "<<k<<":  from suffix "<<suffix
                   <<": number of records added = "<<nrecords<<endl;}
    }

 delete DHputPtr;
 DHputPtr=0;
}

// ***************************************************************************************

#endif
  }
}
 
