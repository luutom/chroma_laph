#include "dilution_scheme_info.h"
#include "xml_help.h"
#include "multi_compare.h"
using namespace std;

namespace Chroma {
  namespace LaphEnv {


// *************************************************************

   // XmlReader constructor

DilutionSchemeInfo::DilutionSchemeInfo(XmlReader& xml_in)
{ 
 xml_tag_assert(xml_in,"LaphDilutionScheme","DilutionSchemeInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::LaphDilutionScheme");
 assign_from_reader(xmlr);
}

void DilutionSchemeInfo::assign_from_reader(XmlReader& xmlr)
{
 int time_dil_type, spin_dil_type, eigvec_dil_type;
 try{
    dil_in(xmlr,"./TimeDilution", time_dil_type );
    dil_in(xmlr,"./SpinDilution", spin_dil_type );
    dil_in(xmlr,"./EigvecDilution", eigvec_dil_type );
    }
 catch(const string& err){
    xml_cerr(xmlr,"could not initialize DilutionSchemeInfo from XML input");
    xmlreadfail(xmlr,"DilutionSchemeInfo");}
 try{
    assign(spin_dil_type, eigvec_dil_type, time_dil_type);}
 catch(...){ QDP_abort(1);}
}


 // *************************************************************

DilutionSchemeInfo::DilutionSchemeInfo()
{
 spinDilutionType=0;
 eigvecDilutionType=0;
 timeDilutionType=0;
}

  // ************************************************************

    // copy constructor

DilutionSchemeInfo::DilutionSchemeInfo(const DilutionSchemeInfo& in)
                   : spinDilutionType(in.spinDilutionType),
                     eigvecDilutionType(in.eigvecDilutionType),
                     timeDilutionType(in.timeDilutionType) {}


DilutionSchemeInfo& DilutionSchemeInfo::operator=(const DilutionSchemeInfo& in)
{
 spinDilutionType=in.spinDilutionType;
 eigvecDilutionType=in.eigvecDilutionType;
 timeDilutionType=in.timeDilutionType;
 return *this;
}


// ***************************************************************


void DilutionSchemeInfo::assign(int spin_dil_type, int eigvec_dil_type, 
                                int time_dil_type)
{
 try{
    if ((time_dil_type==-1)||(spin_dil_type==-1)||(eigvec_dil_type==-1)) 
       throw string("dilution types cannot have value -1");
    if ((spin_dil_type>2)||(spin_dil_type<-2))
       throw string("spin dilution type must have value -2, 0, 1, 2");
    }
 catch(const string& errmsg){
    std::cerr << "Invalid DilutionSchemeInfo assigment:"<<std::endl;
    std::cerr << "   ..."<<errmsg<<std::endl;
    throw(string("error"));}

 timeDilutionType=time_dil_type;
 spinDilutionType=spin_dil_type;
 eigvecDilutionType=eigvec_dil_type;
}


void DilutionSchemeInfo::checkEqual(const DilutionSchemeInfo& in) const
{
 if ((spinDilutionType!=in.spinDilutionType)
   ||(eigvecDilutionType!=in.eigvecDilutionType)
   ||(timeDilutionType!=in.timeDilutionType)){
    std::cerr << "DilutionSchemeInfo checkEqual failed"<<std::endl;
    std::cerr << "LHS:"<<std::endl<<output()<<std::endl<<"RHS:"<<std::endl<<in.output()<<std::endl;
    throw string("DilutionSchemeInfo does not checkEqual...abort");}
}

bool DilutionSchemeInfo::operator==(const DilutionSchemeInfo& in) const
{
 return multiEqual(spinDilutionType,in.spinDilutionType,
                   eigvecDilutionType,in.eigvecDilutionType,
                   timeDilutionType,in.timeDilutionType);
}

bool DilutionSchemeInfo::operator!=(const DilutionSchemeInfo& in) const
{
 return multiNotEqual(spinDilutionType,in.spinDilutionType,
                      eigvecDilutionType,in.eigvecDilutionType,
                      timeDilutionType,in.timeDilutionType);
}

bool DilutionSchemeInfo::operator<(const DilutionSchemeInfo& in) const
{
 return multiLessThan(spinDilutionType,in.spinDilutionType,
                      eigvecDilutionType,in.eigvecDilutionType,
                      timeDilutionType,in.timeDilutionType);
}


string DilutionSchemeInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<LaphDilutionScheme>"<<endl;
 oss << pad << "   <TimeDilution> " <<endl
     << dil_out(indent,timeDilutionType,true)
     << pad << "   </TimeDilution>"<<endl;
 oss << pad << "   <SpinDilution> " <<endl
     << dil_out(indent,spinDilutionType,false)
     << pad << "   </SpinDilution>"<<endl;
 oss << pad << "   <EigvecDilution> " <<endl
     << dil_out(indent,eigvecDilutionType,true) 
     << pad << "   </EigvecDilution>"<<endl;
 oss << pad << "</LaphDilutionScheme>"<<endl;
 return oss.str();
}

void DilutionSchemeInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"LaphDilutionScheme");
 push(xmlout,"TimeDilution");
 dil_out(xmlout,timeDilutionType,true);
 pop(xmlout);
 push(xmlout,"SpinDilution");
 dil_out(xmlout,spinDilutionType,false);
 pop(xmlout);
 push(xmlout,"EigvecDilution");
 dil_out(xmlout,eigvecDilutionType,true); 
 pop(xmlout);
 pop(xmlout);
}


void DilutionSchemeInfo::dil_in(XmlReader& xml_in, const std::string& path, 
                               int& DilType)
{
 string tmp;
 DilType=0;
 try{
    read(xml_in,path+"/DilutionType",tmp);
    tmp=tidyString(tmp);
    if (tmp=="none"){
       DilType=0;
       }
    else if (tmp=="full"){
       DilType=1;
       }
    else if (tmp=="block"){
       DilType=2;
       int nproj=0;
       if (xml_in.count(path+"/NumberProjectors")==1){
          read(xml_in,path+"/NumberProjectors",nproj);}
       if (nproj>1) DilType=nproj;
       else throw string("invalid number of block projectors");
       }
    else if (tmp=="interlace"){
       DilType=-2;
       int nproj=0;
       if (xml_in.count(path+"/NumberProjectors")==1){
          read(xml_in,path+"/NumberProjectors",nproj);}
       if (nproj>1) DilType=-nproj;
       else throw string("invalid number of interlace projectors");
       }
    else{
       throw string("invalid read");}
    }
 catch(const string& errstr){
       xml_cerr(xml_in,"invalid DilutionSchemeInfo read from XML");
       xml_cerr(xml_in,errstr);
       throw(string("error"));}
}


string DilutionSchemeInfo::dil_out(int indent, int DilType, 
                                   bool out_nproj) const
{
 string pad(3*indent,' ');
 string dtype;
 if (DilType==0) dtype="none";
 else if (DilType==1) dtype="full";
 else if (DilType>1) dtype="block";
 else if (DilType<-1) dtype="interlace";
 ostringstream oss;
 oss << pad <<"      <DilutionType> "<<dtype<<" </DilutionType>"<<endl;
 if (out_nproj){
    if (DilType>1)
       oss << pad << "      <NumberProjectors> "<<DilType<<" </NumberProjectors>"<<endl;
    else if (DilType<-1)
       oss << pad << "      <NumberProjectors> "<<-DilType<<" </NumberProjectors>"<<endl;
    }
 return oss.str();
}

void DilutionSchemeInfo::dil_out(XmlWriter& xmlout,
                                 int DilType, bool out_nproj) const
{
 string dtype;
 if (DilType==0) dtype="none";
 else if (DilType==1) dtype="full";
 else if (DilType>1) dtype="block";
 else if (DilType<-1) dtype="interlace";
 write(xmlout,"DilutionType",dtype);
 if (out_nproj){
    if (DilType>1)
       write(xmlout,"NumberProjectors",DilType);
    else if (DilType<-1)
       write(xmlout,"NumberProjectors",-DilType);
    }
}


// *************************************************************
  }
}
