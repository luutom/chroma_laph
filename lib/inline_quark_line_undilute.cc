#include "inline_quark_line_undilute.h"
#include "chroma.h"


namespace Chroma {
using namespace LaphEnv;

  namespace InlineStochUndiluteQuarkEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class .

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochUndiluteQuarkInlineMeas(xml_in, path);
}


const std::string name = "LAPH_QUARK_LINE_UNDILUTE";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}


// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochUndiluteQuarkInlineMeas::operator()(unsigned long update_no,
                                              XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

 if (xml_tag_count(xml_rdr,"QuarkLineEndInfo")!=1){
    QDPIO::cerr << "Must have one <QuarkLineEndInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./QuarkLineEndInfo");
 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 QuarkSmearingInfo qSmear(xmlr);
 DilutionSchemeInfo indil(xmlr);
 QuarkActionInfo quark(xmlr);
 XmlReader xmlr1(xml_rdr,"./InputFiles");
 FileListInfo infiles(xmlr1);
 XmlReader xmlr2(xml_rdr,"./OutputFiles");
 FileListInfo outfiles(xmlr2);

 QDPIO::cout << endl << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark Smearing:"<<endl<<qSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<"Dilution Scheme Info:"<<endl<< indil.output()<<endl<<endl;
 QDPIO::cout << endl<<"QuarkAction:"<<endl<< quark.output()<<endl<<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_QUARK_LINE_UNDILUTE");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 qSmear.output(xml_out);
 indil.output(xml_out);
 quark.output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

 QuarkUndiluter Q(gaugeinfo,gSmear,qSmear,indil,quark,infiles,outfiles);
 Q.undoDilution();

} 

// ******************************************************************
  }
} // namespace Chroma
