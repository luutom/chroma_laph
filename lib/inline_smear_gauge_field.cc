#include "inline_smear_gauge_field.h"
#include "chroma.h"


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 4)

  using namespace LaphEnv;
  namespace InlineSmearGaugeFieldEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "SmearGaugeFieldInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new SmearGaugeFieldInlineMeas(xml_in, path);
}


const std::string name = "SMEAR_GAUGE_FIELD_TIMESLICES";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}

	
// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void SmearGaugeFieldInlineMeas::operator()(unsigned long update_no,
                                           XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);
 string gauge_config_xml;
 GaugeConfigurationInfo gaugeinfo(xml_rdr,gauge_config_xml);
 GluonSmearingInfo smear(xml_rdr);
 string smeared_filename;
 xmlread(xml_rdr,"SmearedGaugeFileName",smeared_filename,"SMEAR_GAUGE_FIELD_TIMESLICES");

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 0: Stout smear the gauge field and write    *"<<endl;
 QDPIO::cout << " *                to file as time slices                   *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<smear.output()<<endl<<endl;
 QDPIO::cout << "XML header in the gauge configuration:"<<endl;
 QDPIO::cout << gauge_config_xml<<endl<<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"SMEAR_GAUGE_FIELD_TIMESLICES");
 gaugeinfo.output(xml_out);
 smear.output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

    // create the handler

 GluonSmearingHandler G(smear,gaugeinfo,smeared_filename);

 START_CODE();
 StopWatch outer;
 outer.start();

    //  compute the stout-smeared gauge field, write to file as time slices

 G.computeSmearedGaugeField();

 outer.stop();
 QDPIO::cout << name << ": total time = " << outer.getTimeInSeconds() 
             << " secs" << endl;
 QDPIO::cout << name << ": ran successfully" << endl;

 END_CODE();
} 

// ******************************************************************
  }

#endif
} // namespace Chroma
