#include "quark_action_info.h"
#include "gauge_configuration_handler.h"
#include "chromabase.h"
#include <sstream>

namespace Chroma {
  namespace LaphEnv {


// ************************************************************

QuarkActionInfo::QuarkActionInfo(XmlReader& xml_in)
{ 
 xml_tag_assert(xml_in,"QuarkActionInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::QuarkActionInfo");
 set_info1(xmlr);
}

void QuarkActionInfo::set_info1(XmlReader& xmlr)
{
 if (xml_tag_count(xmlr,"Dynamical")>0){
    xml_cerr(xmlr,"QuarkActionInfo constructor of this type cannot use <Dynamical> tag");
    xmlreadfail(xmlr,"QuarkActionInfo");}
 setMass(xmlr,mass_name,mass);
 try {
    read(xmlr, "Name", action_id);}
 catch(...){
    xml_cerr(xmlr,"Invalid read of <Name> in QuarkActionInfo");
    xmlreadfail(xmlr,"QuarkActionInfo");}
 action_id=tidyString(action_id); 
 if (xml_tag_count(xmlr,"Description")==1){
    XmlReader xmld(xmlr,"./Description");
    ostringstream strm;
    strm << "<Description>";
    xmld.printCurrentContext(strm);
    strm << "</Description>";
    description = strm.str();}
 else{
    description = "<Description/>";}
}

void QuarkActionInfo::setMass(XmlReader& xmlr, string& massName, double& massValue)
{
 if (xml_tag_count(xmlr,"Mass")>0){
    massName="Mass";
    try{
       read(xmlr, "Mass", massValue);}
    catch(...){
       xml_cerr(xmlr,"Invalid read of <Mass> in QuarkActionInfo");
       xmlreadfail(xmlr,"QuarkActionInfo");}}
 else if (xml_tag_count(xmlr,"Kappa")>0){
    massName="Kappa";
    try{
       read(xmlr, "Kappa", massValue);}
    catch(...){
       xml_cerr(xmlr,"Invalid read of <Kappa> in QuarkActionInfo");
       xmlreadfail(xmlr,"QuarkActionInfo");}}
 else{
    xml_cerr(xmlr,"could not set mass in QuarkActionInfo");
    xmlreadfail(xmlr,"QuarkActionInfo");}
}


QuarkActionInfo::QuarkActionInfo(XmlReader& xml_in, const GaugeConfigurationInfo& U)
{
 if (xml_tag_count(xml_in,"QuarkActionInfo")!=1){
    xml_cerr(xml_in,"Bad XML input to QuarkActionInfo");
    xml_cerr(xml_in,"Expected one <QuarkActionInfo> tag");
    xmlreadfail(xml_in,"QuarkActionInfo");}
 XmlReader xmlr(xml_in, "./descendant-or-self::QuarkActionInfo");
 int dyn=xml_tag_count(xmlr,"Dynamical");
 if (dyn>1){
    xml_cerr(xml_in,"Bad XML input to QuarkActionInfo");
    xml_cerr(xml_in,"Multiple <Dynamical> tags found");
    xmlreadfail(xml_in,"QuarkActionInfo");}
 if (dyn==0){
    set_info1(xmlr);}
 else{
    try{
    XmlReader xmlm(xmlr,"./Dynamical");
    setMass(xmlm,mass_name,mass);
    description.clear();
    stringstream strm;
    strm << U.getFileXML();
    XmlReader xmlu(strm);
    if (xml_tag_count(xmlu,"HMCTrj")!=1){
       xml_cerr(xml_in,"QuarkActionInfo could not be initialized");
       xmlreadfail(xml_in,"QuarkActionInfo");}
    XmlReader xmlt(xmlu,"//HMCTrj");
    string mname;
    double mval;
    int nquarks=xml_tag_count(xmlt,"FermionAction");
    for (int k=1;k<=nquarks;k++){
       ostringstream elem;
       elem << "./descendant::FermionAction[" << k <<"]";
       XmlReader xmltt(xmlt,elem.str());
       setMass(xmltt,mname,mval);
       if ((mname==mass_name)&&(abs(mval-mass)<1e-8)){
          ostringstream temp;
          temp << "<Description>"<<endl;
          xmltt.print(temp);
          temp << "</Description>";
          description=temp.str();
          xmlread(xmltt,"FermAct", action_id, "QuarkActionInfo");
          break;}}
    if (description.empty()){
       xml_cerr(xml_in,"Could not initialize QuarkActionInfo:");
       xml_cerr(xml_in," could not find requested dynamical quark mass");
       xml_cerr(xml_in," in GaugeConfigurationInfo");
       xmlreadfail(xml_in,"QuarkActionInfo");}
    }
    catch(...){
       xml_cerr(xml_in,"getting QuarkActionInfo from gauge file xml failed");
       QDP_abort(1);}}
// QDPIO::cout << endl << endl <<"QuarkActionInfo constructor:"<<endl<<endl;
// QDPIO::cout << "action id = "<<action_id<<endl;
// QDPIO::cout << "action_header: "<<endl<< description << endl << endl;
// QDPIO::cout << mass_name <<" = "<<mass<<endl;
// QDPIO::cout << "QuarkActionInfo Initialized" << endl<<endl;
}


  // ************************************************************

     // copy constructor

QuarkActionInfo::QuarkActionInfo(const QuarkActionInfo& rhs)
                : description(rhs.description), mass(rhs.mass), 
                  mass_name(rhs.mass_name), action_id(rhs.action_id) {}

						
QuarkActionInfo& QuarkActionInfo::operator=(const QuarkActionInfo& rhs)
{
 description = rhs.description;
 mass = rhs.mass;
 mass_name = rhs.mass_name;
 action_id = rhs.action_id;
 return *this;
}


void QuarkActionInfo::checkEqual(const QuarkActionInfo& rhs) const
{
 if ((action_id != rhs.action_id)||(mass_name!=rhs.mass_name)
    ||(abs(mass-rhs.mass)>1e-8)||(!xmlContentIsEqual(description, rhs.description))){
    std::cerr << "QuarkActionInfo checkEqual failed"<<endl;
    std::cerr << "LHS:"<<endl<<output()<<endl<<"RHS:"<<endl<<rhs.output()<<endl;
    throw string("QuarkActionInfo xml contents do not match");}
}


void QuarkActionInfo::matchXMLverbatim(const QuarkActionInfo& rhs) const
{
 if ((action_id != rhs.action_id)||(mass_name!=rhs.mass_name)){
    throw string("QuarkActionInfos do not match");}
 if (abs(mass-rhs.mass)>1e-8){
    throw string("QuarkActionInfos do not match");}
 if (rhs.description != description)
    throw string("QuarkActionInfo xml strings do not match");
}


bool QuarkActionInfo::operator==(const QuarkActionInfo& rhs) const
{
 if ((action_id != rhs.action_id)||(mass_name!=rhs.mass_name)) return false;
 if (abs(mass-rhs.mass)>1e-8) return false;
 return xmlContentIsEqual(description,rhs.description);
}

string QuarkActionInfo::indentDescription(int indent) const
{
 if (indent==0) return description;
 string pad(3*indent,' ');
 string temp(pad);
 int pos1=0;
 int pos2=description.find('\n',pos1);
 while (pos2!=string::npos){
    temp+=description.substr(pos1,pos2-pos1+1)+pad;
    pos1=pos2+1;
    pos2=description.find('\n',pos1);}
 temp+=description.substr(pos1,description.length()-pos1+1);
 return temp;
}

string QuarkActionInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<QuarkActionInfo>"<<endl;
 oss << pad << "   <Name>" << action_id << "</Name>"<<endl;
 oss << pad << "   <"<<mass_name<<">" << mass << "</"<<mass_name<<">"<<endl;
 oss << indentDescription(indent+1)<<endl;
 oss << pad << "</QuarkActionInfo>"<<endl;
 return oss.str();
}

void QuarkActionInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkActionInfo");
 write(xmlout,"Name",action_id);
 write(xmlout,mass_name,mass);
 xmlout.writeXML("\n"+indentDescription(1)+"\n");
 pop(xmlout);
}



// ***********************************************************
  }
}
