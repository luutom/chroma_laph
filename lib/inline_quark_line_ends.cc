#include "inline_quark_line_ends.h"
#include "chroma.h"

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 4)

  using namespace LaphEnv;
  namespace InlineStochLaphQuarkEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "StochLaphQuarkInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochLaphQuarkInlineMeas(xml_in, path);
}


const std::string name = "LAPH_QUARK_LINE_ENDS";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}

// *********************************************************************
	

void StochLaphQuarkInlineMeas::clearSinkComputations()
{
 sinkComputations.clear();
}

      
void StochLaphQuarkInlineMeas::setSinkComputations(int NumTimeDilutionProjs)
{
 XmlReader xml_rdr(xml_rd);
 if (!sinkComputations.empty()) sinkComputations.clear();

 if (xml_tag_count(xml_rdr,"SinkComputations")==1){
    XmlReader xmlrd(xml_rdr,"./descendant-or-self::SinkComputations");

    if (xml_tag_count(xmlrd,"NoiseList_TimeProjIndexList")==1){
       XmlReader xmlr(xmlrd,"./descendant-or-self::NoiseList_TimeProjIndexList");
       multi1d<int> time_proj_inds;
       if (xml_tag_count(xmlr,"TimeProjIndexList")==1){
          if (xml_tag_count(xmlr,"TimeProjIndexList/All")==1){
             time_proj_inds.resize(NumTimeDilutionProjs);
             for (int t=0;t<NumTimeDilutionProjs;t++) time_proj_inds[t]=t;}
          else
             xmlread(xmlr,"TimeProjIndexList/Values",time_proj_inds,
                     "LAPH_QUARK_LINE_ENDS");}
       int num_noises=xml_tag_count(xmlr,"LaphNoiseList/LaphNoiseInfo");
       for (int k=1;k<=num_noises;k++){
          ostringstream path;
          path << "./descendant::LaphNoiseList/LaphNoiseInfo["<<k<<"]";
          XmlReader xml_noise(xmlr,path.str());
          LaphNoiseInfo aNoise(xml_noise);
          for (int t=0;t<time_proj_inds.size();t++){
             sinkComputations.push_back(
                  SinkComputation(aNoise,time_proj_inds[t]));}}}

    if (xml_tag_count(xmlrd,"ComputationList")==1){
       XmlReader xmlr(xmlrd,"./descendant-or-self::ComputationList");
       int ncomputations=xml_tag_count(xmlr,"Computation");
       for (int k=1;k<=ncomputations;k++){
          ostringstream path;
          path << "./descendant::Computation["<<k<<"]";
          XmlReader xml_comp(xmlr,path.str());
          LaphNoiseInfo aNoise(xml_comp);
          int time_proj_index;
          xmlread(xml_comp,"TimeProjIndex",time_proj_index,"LAPH_QUARK_LINE_ENDS");
          sinkComputations.push_back(
                SinkComputation(aNoise,time_proj_index));}}

    }

 QDPIO::cout << endl << "LAPH_QUARK_LINE_ENDS sink computations:"<<endl;
 QDPIO::cout << " Number of sink computations = "<<sinkComputations.size()<<endl;
 int count=0;
 for (list<SinkComputation>::const_iterator it=sinkComputations.begin();
      it!=sinkComputations.end();count++,it++){
    QDPIO::cout <<endl<< "SinkComputation "<<count<<":"<<endl;
    QDPIO::cout << it->Noise.output();
    QDPIO::cout << "<TimeProjIndex>"<<it->TimeProjIndex<<"</TimeProjIndex>"<<endl;}

}



// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochLaphQuarkInlineMeas::operator()(unsigned long update_no,
                                          XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

    // create the handler and set up the info from the
    // XML <QuarkSourceSinkInfo> tag

 if (xml_tag_count(xml_rdr,"QuarkLineEndInfo")!=1){
    QDPIO::cerr << "Must have one <QuarkLineEndInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./QuarkLineEndInfo");
 string gauge_xml;
 GaugeConfigurationInfo gaugeinfo(xmlr,gauge_xml);
 GluonSmearingInfo gSmear(xmlr);
 QuarkSmearingInfo qSmear(xmlr);
 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "LAPH_QUARK_LINE_ENDS");
 DilutionSchemeInfo dil(xmlr);
 QuarkActionInfo quark(xmlr,gaugeinfo);
 FileListInfo files(xmlr);
 InverterInfo invinfo(xmlr);
 bool verbose=false;
 string verbosity;
 xmlreadif(xmlr,"Verbosity",verbosity,"LAPH_QUARK_LINE_ENDS");
 if (tidyString(verbosity)=="full") verbose=true;

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 2: Compute the quark line ends              *"<<endl;
 QDPIO::cout << " *                and write to file as time slices         *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout << "XML header in the gauge configuration:"<<endl;
 QDPIO::cout << gauge_xml<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark Smearing:"<<endl<<qSmear.output()<<endl<<endl;
 QDPIO::cout <<"SmearedQuarkFileStub: "<<smeared_quark_filestub<<endl;
 QDPIO::cout <<endl<<"Dilution Scheme Info:"<<endl<< dil.output()<<endl<<endl;
 QDPIO::cout << endl<<"QuarkAction:"<<endl<< quark.output()<<endl<<endl;
 QDPIO::cout << endl<<"Inverter Info:"<<endl<<invinfo.output()<<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_QUARK_LINE_ENDS");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 qSmear.output(xml_out);
 dil.output(xml_out);
 quark.output(xml_out);
 invinfo.output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

#ifdef TESTING
 printLaphEigenvectors(gSmear,gaugeinfo,qSmear,smeared_quark_filestub,
                       "lapheigvec4d.log");
#endif


     // create handler

 QuarkHandler Q(gaugeinfo,gSmear,qSmear,dil,quark,files,
                smeared_quark_filestub);

    // read the list of computations (noises, time sources, file indices)
    // from xml_rdr and store in the "Computations" data member

 setSinkComputations(Q.getNumberOfTimeDilutionProjectors());
 int ncomp=sinkComputations.size();

     // set the inverter info

 Q.setInverter(invinfo);
 QDPIO::cout << endl <<"Info initialized in QuarkHandler"<<endl;
 Q.outputSuffixMap();

 START_CODE();
 StopWatch outer;
 outer.start();
 int count=0;
 for (list<SinkComputation>::const_iterator it=sinkComputations.begin();
      it!=sinkComputations.end();count++,it++){
    QDPIO::cout <<endl<<endl;
    QDPIO::cout <<" *************************************************************"<<endl;
    QDPIO::cout <<" *"<<endl;
    QDPIO::cout <<" *  Now starting sink computation "<<count<<" (with "<<ncomp-1<<" as last):"<<endl;
    QDPIO::cout <<" *"<<endl;
    QDPIO::cout <<" *************************************************************"<<endl;

    Q.computeSink(it->Noise,it->TimeProjIndex,verbose);}

 outer.stop();
 QDPIO::cout << name << ": total time = " << outer.getTimeInSeconds() 
             << " secs" << endl;
 QDPIO::cout << name << ": ran successfully" << endl;
 END_CODE();

} 

// ******************************************************************
  }
#endif
} // namespace Chroma
