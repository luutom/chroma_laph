#ifndef LAPH_BARYON_OPERATOR_INFO_H
#define LAPH_BARYON_OPERATOR_INFO_H

#include "qdp.h"
#include "chromabase.h"
#include <set>
#include "momenta.h"
#include "xml_help.h"

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   Objects of class "BaryonOperatorInfo" store identifying info  *
// *   about one particular baryon operator.  Each baryon is a       *
// *   linear superposition of so-called elemental operators.        *
// *   The Chroma tasks that compute the baryon source/sink          *
// *   functions need to read these superposition coefficients.      *
// *   The XML input into the Chroma tasks must inform Chroma        *
// *   which baryon operators to compute, so a pre-defined input     *
// *   format must be followed.  The needed input format and the     *
// *   required storage format of the elementals in each baryon      *
// *   are described below.                                          *
// *                                                                 *
// *   Chroma input format for each baryon operator:                 *
// *                                                                 *
// *       <BaryonOp>                                                *
// *          <IsospinName> nucleon </IsospinName>                   *
// *          <Flavor> uud </Flavor>                                 *
// *          <Momentum>  0 0 0  </Momentum>                         *
// *          <Irrep> Hu </Irrep>                                    *
// *          <IrrepRow> 3 </IrrepRow>                               *
// *          <SpatialType> DDL </SpatialType>                       *
// *          <SpatialIdNum> 4 </SpatialIdNum>                       *
// *          <DispLength> 3 </DispLength>                           *
// *       </BaryonOp>                                               *
// *                                                                 *
// *   READING multiple baryon operators:                            *
// *                                                                 *
// *   Chroma input format:                                          *
// *                                                                 *
// *      <BaryonOperators>                                          *
// *         <ElementalCoefficientDirectory>                         *
// *             coefsTopDirectory                                   *
// *         </ElementalCoefficientDirectory>                        *
// *           <BaryonOp>                                            *
// *            ...                                                  *
// *           </BaryonOp>                                           *
// *           <BaryonOp>                                            *
// *            ...                                                  *
// *           </BaryonOp>                                           *
// *         ...                                                     *
// *      </BaryonOperators>                                         *
// *                                                                 *
// *   Given the above XML input, one just needs to issue the        *
// *   following:                                                    *
// *                                                                 *
// *      XmlReader xml_in;  // assigned somehow                     *
// *      set<BaryonOperatorInfo> bset;                              *
// *      string coefdir;                                            *
// *      readBaryonOperators(xml_in,bset,coefdir);                  *
// *                                                                 *
// *   The above code initializes the class, then reads all of       *
// *   baryon operators inside the <BaryonOperators> tags in         *
// *   the input XML file, storing the results in a set.             *
// *                                                                 *
// *******************************************************************

class BaryonOperatorInfo
{

  unsigned icode1,icode2;

 public:

  BaryonOperatorInfo(XmlReader& xml_in);

  BaryonOperatorInfo(const BaryonOperatorInfo& B) 
     :  icode1(B.icode1), icode2(B.icode2) {}

  BaryonOperatorInfo& operator=(const BaryonOperatorInfo& B)
   {icode1=B.icode1; icode2=B.icode2;
    return *this;}

  BaryonOperatorInfo() :  icode1(0), icode2(0) {}

    // output functions

  std::string getIsospinName() const;

  std::string getFlavor() const;

  Momentum getMomentum() const;

  int getXMomentum() const;

  int getYMomentum() const;

  int getZMomentum() const;

  std::string getIrrep() const;

  int getIrrepRow() const;

  std::string getSpatialType() const;

  int getSpatialIdNumber() const;

  int getDisplacementLength() const;

  std::string output() const;  // XML output of baryon tags

  void output(XmlWriter& xmlout) const;  // XML output of baryon tags

    // returns the flavor std::string, but replaces all 'u' and
    // 'd' characters by 'l'

  std::string getFlavorLS() const
   {std::string tmp=getFlavor();
    for (unsigned int i=0;i<tmp.length();i++)
       if ((tmp[i]=='u')||(tmp[i]=='d')) tmp[i]='l';
    return tmp;}

  bool operator==(const BaryonOperatorInfo& rhs) const;
  bool operator!=(const BaryonOperatorInfo& rhs) const;
  bool operator<(const BaryonOperatorInfo& rhs) const;

  friend class BaryonOperator;
  friend class BaryonHandler;

};



// **************************************************

   //  useful routine for reading multiple BaryonOperators

void readBaryonOperators(XmlReader& xml_in, 
                         set<BaryonOperatorInfo>& bops,
                         std::string& coef_dir);


// **************************************************
  }
}
#endif  
