#ifndef __INLINE_QUARK_LINE_UNDILUTE_H__
#define __INLINE_QUARK_LINE_UNDILUTE_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "quark_handler.h"


// ******************************************************************
// *                                                                *
// * Driver inline measurement that takes the requested quark       *
// * line ends and undilutes them:  changing LI2k -> LIk.  Should   *
// * run in either 3D chroma_laph.  XML input must have the form:   *
// *                                                                *
// *  <chroma>                                                      *
// *   <RNG><Seed> ... </Seed></RNG>                                *
// *   <Param>                                                      *
// *    <nrow>12 12 12 96</nrow>   # lattice size Nx Ny Nz Nt       *
// *    <InlineMeasurements>                                        *
// *    <elem>                                                      *
// *                                                                *
// *     <Name> LAPH_QUARK_LINE_UNDILUTE </Name>                    *
// *     <QuarkLineEndInfo>                                         *
// *       <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>   *
// *       <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>   *
// *       <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>     *
// *       <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>       *
// *       <LaphDilutionScheme> ... </LaphDilutionScheme>           *
// *       <QuarkActionInfo> ... </QuarkActionInfo>                 *
// *     </QuarkLineEndInfo>                                        *
// *     <InputFiles>                                               *
// *       <FileListInfo> ... </FileListInfo>                       *
// *     </InputFiles>                                              *
// *     <OutputFiles>                                              *
// *       <FileListInfo> ... </FileListInfo>                       *
// *     </OutputFiles>                                             *
// *                                                                *
// *    </elem>                                                     *
// *    </InlineMeasurements>                                       *
// *   </Param>                                                     *
// *  </chroma>                                                     *
// *                                                                *
// *                                                                *
// *  If noises are specified in <LaphNoiseList> and time projector *
// *  indices are specified in  <TimeProjIndexList>, then the       *
// *  routine checks that these noises and time projector indices   *
// *  are present, otherwise a "MISSING" string is output.          *
// *  If <TimeProjNumber> is specified instead, then the            *
// *  routine checks that at least this number of time projectors   *
// *  are available for each noise listed.                          *
// *                                                                *
// ******************************************************************


namespace Chroma {
                  
  namespace InlineStochUndiluteQuarkEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochUndiluteQuarkInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below

   set<LaphEnv::LaphNoiseInfo> noises;
   set<int> timeProjIndices;
   int NTimeProjIndices; 

 public:

   StochUndiluteQuarkInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochUndiluteQuarkInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}
   
};
	

// ***********************************************************
  }
}

#endif
