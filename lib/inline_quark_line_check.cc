#include "inline_quark_line_check.h"
#include "chroma.h"


namespace Chroma {
using namespace LaphEnv;

  namespace InlineStochCheckQuarkEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class .

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochCheckQuarkInlineMeas(xml_in, path);
}


const std::string name = "LAPH_QUARK_LINE_CHECK";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}


void StochCheckQuarkInlineMeas::setNoisesTimeProjectors()
{
 noises.clear();
 timeProjIndices.clear();
 NTimeProjIndices=0;
 
 XmlReader xml_rdr(xml_rd);
 if (xml_tag_count(xml_rdr,"LaphNoiseList")==1){
    XmlReader xmlr(xml_rdr,"./descendant-or-self::LaphNoiseList");
    int num_noises=xml_tag_count(xmlr,"LaphNoiseInfo");
    for (int k=1;k<=num_noises;k++){
       ostringstream path;
       path << "./descendant::LaphNoiseInfo["<<k<<"]";
       XmlReader xml_noise(xmlr,path.str());
       LaphNoiseInfo aNoise(xml_noise);
       noises.insert(aNoise);}}

 if (xml_tag_count(xml_rdr,"TimeProjIndexList")==1){
    multi1d<int> time_proj_inds;
    xmlread(xml_rdr,"TimeProjIndexList/Values",time_proj_inds,
                 "LAPH_QUARK_LINE_CHECK");
    for (int i=0;i<time_proj_inds.size();i++){
       if (time_proj_inds[i]>=0)
          timeProjIndices.insert(time_proj_inds[i]);}}
 else{
    xmlreadif(xml_rdr,"TimeProjNumber",NTimeProjIndices,
              "StochCheckQuarkInlineMeas");
    if (NTimeProjIndices<0) NTimeProjIndices=0;}

}


// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochCheckQuarkInlineMeas::operator()(unsigned long update_no,
                                           XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

    // create the handler and set up the info from the
    // XML <QuarkSourceSinkInfo> tag

 if (xml_tag_count(xml_rdr,"QuarkLineEndInfo")!=1){
    QDPIO::cerr << "Must have one <QuarkLineEndInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./QuarkLineEndInfo");
 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 QuarkSmearingInfo qSmear(xmlr);
 DilutionSchemeInfo dil(xmlr);
 QuarkActionInfo quark(xmlr);
 FileListInfo files(xmlr);
 string checklog;
 xmlread(xml_rdr,"LogFile",checklog,"LAPH_QUARK_LINE_CHECK");
 checklog=tidyString(checklog);
 if (checklog.empty()){
    QDPIO::cerr << "empty check log file name"<<endl;
    QDP_abort(1);}

 QDPIO::cout << endl << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<endl<<"Quark Smearing:"<<endl<<qSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<"Dilution Scheme Info:"<<endl<< dil.output()<<endl<<endl;
 QDPIO::cout << endl<<"QuarkAction:"<<endl<< quark.output()<<endl<<endl;

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_QUARK_LINE_CHECK");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 qSmear.output(xml_out);
 dil.output(xml_out);
 quark.output(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

     // create handler

 QuarkChecker Q(gaugeinfo,gSmear,qSmear,dil,quark,files);

    // read the list of noises and time projector indices

 setNoisesTimeProjectors();

 QDPIO::cout << endl <<"Info initialized in QuarkHandler"<<endl;
 Q.outputSuffixMap();
 TextFileWriter fout(checklog);
 fout << "Log Start:\n";
 Q.outputSuffixMap(fout);

 if (noises.size()>0){
    if (timeProjIndices.size()>0){
       for (set<LaphNoiseInfo>::const_iterator nt=noises.begin();
            nt!=noises.end();nt++)
       for (set<int>::const_iterator it=timeProjIndices.begin();
            it!=timeProjIndices.end();it++)
          Q.checkCompletion(*nt,*it,fout);}
    else if (NTimeProjIndices>0)
          Q.query(noises,NTimeProjIndices,fout);}

 fout << "Log Finish.\n";
 fout.close();

} 

// ******************************************************************
  }
} // namespace Chroma
