#ifndef __INLINE_DB_OUTPUT_H__
#define __INLINE_DB_OUTPUT_H__

#include "chroma.h"
#include "meas/inline/abs_inline_measurement.h"

// ******************************************************************
// *                                                                *
// * Driver inline measurement that outputs the contents of a       *
// * Laph db file in ascii text format.  Results are sent to a      * 
// * logfile.  Works in 3-d chroma_laph only.  By default, only     *
// * the header XML and the record keys are output.  If the tag     *
// * "<Verbosity>" is given with value "full", then the data itself *
// * is printed out in the logfile.                                 *
// *                                                                *
// *  <chroma>                                                      *
// *   <RNG><Seed> ... </Seed></RNG>                                *
// *   <Param>                                                      *
// *    <nrow>12 12 12</nrow>   # lattice size Nx Ny Nz             *
// *    <InlineMeasurements>                                        *
// *    <elem>                                                      *
// *                                                                *
// *     <Name> LAPH_DB_OUTPUT </Name>                              *
// *       <DBFileName> ... </DBFileName>                           *
// *       <DataType> ... </DataType>                               *
// *       <LogFile> ... </Logfile>                                 *
// *       <Verbosity>full</Verbosity> (optional)                   *
// *                                                                *
// *    </elem>                                                     *
// *    </InlineMeasurements>                                       *
// *   </Param>                                                     *
// *  </chroma>                                                     *
// *                                                                *
// * The "<DataType>" tag should be one of the following:           *
// *        QuarkSink                                               *
// *        BaryonLineEnd                                           *
// *        MesonLineEnd                                            *
// *                                                                *
// ******************************************************************


namespace Chroma {
                  
#if (QDP_ND == 3)

  namespace InlineStochLaphDBOutputEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochLaphDBOutputInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below


 public:

   StochLaphDBOutputInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochLaphDBOutputInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}
   
};
	

// ***********************************************************
  }
#endif
}

#endif
