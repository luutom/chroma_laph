#ifndef __INLINE_SCALE_LAPH_H__
#define __INLINE_SCALE_LAPH_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "gauge_configuration_info.h"
#include "gauge_configuration_handler.h"
#include "xml_help.h"
#include "field_smearing_info.h"
#include "field_smearing_handler.h"
#include "dilution_scheme_info.h"
#include "laph_noise_info.h"
#include "quark_info.h"
#include "quark_line_end_info.h"
#include "quark_handler.h"
#include "meson_handler.h"
#include "baryon_handler.h"
#include "filelist_info.h"
#include "time_slices.h"
#include "multi_compare.h"


// ***********************************************************************
// *                                                                     *
// *  Driver inline measurement that constructs quantities related to    *
// *  setting scales.  The Omega-baryon mass and the pion mass are       *
// *  quantities that are computed.  Assumes the smeared gauge field     *
// *  time slices and the Laph eigenvectors are available, and that the  *
// *  needed quark line ends are also available.  Must be run in 3D      *
// *  chroma_laph.  XML input must have the form:                        *
// *                                                                     *
// *  <chroma>                                                           *
// *   <RNG><Seed> ... </Seed></RNG>                                     *
// *   <Param>                                                           *
// *    <nrow>12 12 12</nrow>   # lattice size Nx Ny Nz                  *
// *    <InlineMeasurements>                                             *
// *    <elem>                                                           *
// *                                                                     *
// *     <Name> LAPH_SCALE </Name>                                       *
// *                                                                     *
// *     <PionCorrelator>        ...   </PionCorrelator>                 *
// *     <OmegaBaryonCorrelator> ...   </OmegaBaryonCorrelator>          *
// *     <NucleonCorrelator>     ...   </NucleonCorrelator>              *
// *                                                                     *
// *    </elem>                                                          *
// *    </InlineMeasurements>                                            *
// *   </Param>                                                          *
// *  </chroma>                                                          *
// *                                                                     *
// *  The details in the correlator XML tags above are described below.  *
// *                                                                     *
// *  (1) <PionCorrelator> or <OmegaBaryonCorrelator>                    *
// *       or <NucleonCorrelator>                                        *
// *                                                                     *
// *     <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>          *
// *     <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>          *
// *     <QuarkActionInfo> ... </QuarkActionInfo>                        *
// *     <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>            *
// *     <SmearedGaugeFileName> ... </SmearedGaugeFileName>              *
// *     <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>              *
// *     <LaphDilutionScheme> ... </LaphDilutionScheme>                  *
// *     <FileListInfo>  ... </FileListInfo>   (for quark sinks)         *
// *     <MinTimeSeparation> ... </MinTimeSeparation>                    *
// *     <MaxTimeSeparation> ... </MaxTimeSeparation>                    *
// *     <OutputData> ... </OutputData>                                  *
// *                                                                     *
// *  Only simple single-site operators are used:                        *
// *    Pion:    d-bar u   (1 3 + 2 4 - 3 1 - 4 2)   chi - psi format    *
// *    Nucleon:   u u d   ( 1 2 1 - 1 1 2 )     G1g_1 SS 0              *
// *    Omega:     s s s   ( 1 1 1 )             Hg_1  SS 0              *
// *                                                                     *
// *                                                                     *
// ***********************************************************************


namespace Chroma { 

#if (QDP_ND == 3)

  namespace InlineScaleLaphEnv {

 // **************************************************************

extern const std::string name;
bool registerAll();


class LaphScaleInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below

 public:

   LaphScaleInlineMeas(XMLReader& xml_in, const std::string& path) 
                       : xml_rd(xml_in, path) {}

   ~LaphScaleInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}

   void calcPionCorrelator(LaphEnv::XmlReader& xml_in);
   void calcOmegaBaryonCorrelator(LaphEnv::XmlReader& xml_in);
   void calcNucleonCorrelator(LaphEnv::XmlReader& xml_in);


      // utility routines

   void makePions(LaphEnv::QuarkHandler& q1, 
                  const LaphEnv::LaphNoiseInfo& noise1, 
                  LaphEnv::QuarkHandler& q2, 
                  const LaphEnv::LaphNoiseInfo& noise2, 
                  bool source, int source_time, int time_value, 
                  multi2d<Complex>& pion, int ndil);

   void pionCorrelate(const multi2d<Complex>& pion_sink,
                      const multi2d<Complex>& pion_source,
                      multi1d<Complex>& pion_corr_avg,
                      int t, int ndil);

   void evaluateLocalInnerProduct(
                               const LatticeColorVector *v1,
                               const LatticeColorVector *v2,
                               auto_ptr<LatticeComplex>& result);

   Complex evaluateSum(auto_ptr<LatticeComplex>& in);

   void evaluateColorCrossProduct(
                               const LatticeColorVector *v1,
                               const LatticeColorVector *v2,
                               auto_ptr<LatticeColorVector>& result);

   void evaluateColorVectorContract(
                                 const auto_ptr<LatticeColorVector>& v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeComplex>& result);

   void makeOmegaBaryons(LaphEnv::QuarkHandler& s, 
               const LaphEnv::LaphNoiseInfo& noise1,
               const LaphEnv::LaphNoiseInfo& noise2, 
               const LaphEnv::LaphNoiseInfo& noise3, 
               bool source, int source_time, int time_value, 
               int ndil, int bsign, 
               multi3d<Complex>& omega1, multi3d<Complex>& omega2,
               multi3d<Complex>& omega3, multi3d<Complex>& omega4);

   void addOmegaBaryons(LaphEnv::QuarkHandler& s, bool source, 
               int tproj, int t, int ndil, int sign,
               const LaphEnv::LaphNoiseInfo& noise1, 
               const LaphEnv::LaphNoiseInfo& noise2, 
               const LaphEnv::LaphNoiseInfo& noise3, 
               int d1, int d2, int d3,
               multi3d<Complex>& omega1, multi3d<Complex>& omega2,
               multi3d<Complex>& omega3, multi3d<Complex>& omega4);

   void baryonCorrelate(const multi3d<Complex>& baryon_sink,
                        const multi3d<Complex>& baryon_source,
                        multi1d<Complex>& baryon_corr_avg,
                        int t, int ndil);

   void makeNucleons(LaphEnv::QuarkHandler& ud, 
               const LaphEnv::LaphNoiseInfo& noise1,
               const LaphEnv::LaphNoiseInfo& noise2, 
               const LaphEnv::LaphNoiseInfo& noise3, 
               bool source, int source_time, int time_value, 
               int ndil, int bsign, 
               multi3d<Complex>& nucleon1, multi3d<Complex>& nucleon2);

   void addNucleons(LaphEnv::QuarkHandler& ud, bool source, 
               int tproj, int t, int ndil, int sign,
               const LaphEnv::LaphNoiseInfo& noise1, 
               const LaphEnv::LaphNoiseInfo& noise2, 
               const LaphEnv::LaphNoiseInfo& noise3, 
               int d1, int d2, int d3,
               multi3d<Complex>& nucleon1, multi3d<Complex>& nucleon2);


   void pickMesonNoises(const set<LaphEnv::LaphNoiseInfo>& noises,
                    vector<LaphEnv::MesonHandler::MesonNoiseInfo>& choices);

   void pickBaryonNoises(const set<LaphEnv::LaphNoiseInfo>& noises,
                    vector<LaphEnv::BaryonHandler::BaryonNoiseInfo>& choices);

};
	

// ***********************************************************
  }
#endif
}

#endif
