#ifndef GLUEBALL_HANDLER_H
#define GLUEBALL_HANDLER_H

#include "qdp.h"
#include "chromabase.h"
#include "gauge_configuration_info.h"
#include "gauge_configuration_handler.h"
#include "xml_help.h"
#include "field_smearing_info.h"
#include "field_smearing_handler.h"
#include "filelist_info.h"
#include "glueball_operator_info.h"

namespace Chroma {
  namespace LaphEnv {

#if (QDP_ND == 3)

// *****************************************************************
// *                                                               *
// *  "GlueballHandler" handles computation of and subsequent      *
// *  access to the glueball line ends.  When opened NOT in read   *
// *  mode, it uses the "compute" function to calculate the        *
// *  glueball line ends and output them to binary file.  When     *
// *  opened in read mode, one uses the "queryData" and "getData"  *
// *  members to read the previously-calculated glueball line      *
// *  ends from file.                                              *
// *                                                               *
// *  One of these handlers deals with glueball line ends for      *
// *  **one** set of common info parameters, given by              *
// *                                                               *
// *         GaugeConfigurationInfo                                *
// *         GluonSmearingInfo                                     *
// *         FileListInfo                                          *
// *                                                               *
// *  but one handler deals with different                         *
// *                                                               *
// *         glueball operators                                    *
// *                                                               *
// *  File structure and contents:                                 *
// *                                                               *
// *   - Results manipulated by one handler are contained in       *
// *     several files.  Each file has the same stub, but          *
// *     different positive integer suffices.                      *
// *             stub.0                                            *
// *             stub.1                                            *
// *             ...                                               *
// *             stub.N                                            *
// *     The files included are specified in a FileListInfo.       *
// *                                                               *
// *   - The header info in each file has a common part and a      *
// *     part that is specific to that one file:                   *
// *        <GlueballHandlerDataFile>                              *
// *           - common part                                       *
// *           - specific part                                     *
// *        </GlueballHandlerDataFile>                             *
// *                                                               *
// *   - Info common to ALL files in FileListInfo:                 *
// *                                                               *
// *       GaugeConfigurationInfo                                  *
// *       GluonSmearingInfo                                       *
// *                                                               *
// *   - Info specific to EACH file in FileListInfo:               *
// *                                                               *
// *       GlueballOperatorInfo                                    *
// *                                                               *
// *   - DataBase Key for records in EACH file are specified by    *
// *     a GlueballHandler::RecordKey                              *
// *                                                               *
// *       time value                                              *
// *                                                               *
// *     Each record has various dilution indices. Each DB data    *
// *     element has the form                                      *
// *                                                               *
// *       Real data;                                              *
// *                                                               *
// *  All Laph Handlers follow the member naming convention:       *
// *                                                               *
// *    compute....()  to do original computation                  *
// *    getData()      provides access to results                  *
// *    queryData()    to check if data is available               *
// *                                                               *
// *                                                               *
// *  Important REMARKS:   The file structure discussed above      *
// *  is the **final** form used by the get handlers.  However,    *
// *  the compute subroutine produces a small number of files      *
// *  that are much larger, containing multiple glueball operators.*
// *  This is necessary since Lustre does not deal well with       *
// *  many small files.  The idea here is to compute the larger    *
// *  but fewer files on XSEDE machines that use the Lustre file   *
// *  system, tar these fewer files up, then copy to CMU, and use  *
// *  the "reorganize" subroutine to extract the different         *
// *  glueball operators into different files, and to combine the  *
// *  different glueball lines into the same files.  After the     *
// *  reorganization, all glueball line ends for one glueball      *
// *  operator should be gathered together into a single file.     *
// *                                                               *
// *****************************************************************


class GlueballHandler
{

 public:

   struct RecordKey
    {
       int time_value;

       RecordKey(): time_value(0) {}
       RecordKey(int in_time) : time_value(in_time) {}
       RecordKey(const RecordKey& in) : time_value(in.time_value) {}
       RecordKey& operator=(const RecordKey& in) 
         {time_value=in.time_value; return *this;}
       ~RecordKey() {};
       
       bool operator<(const RecordKey& rhs) const {return (time_value<rhs.time_value);}
       bool operator==(const RecordKey& rhs) const {return (time_value==rhs.time_value);}
       bool operator!=(const RecordKey& rhs) const {return (time_value!=rhs.time_value);}
       void output(XmlWriter& xmlw) const;
       
       RecordKey(const unsigned int* buf)
       {time_value=buf[0];}

       int numints() const {return 1;}
       size_t numbytes() const {return sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {buf[0]=time_value;}
       
    }; 

   struct CombinedKey
    {
       RecordKey rkey;
       GlueballOperatorInfo gop;

       CombinedKey();
       CombinedKey(const GlueballOperatorInfo& in_gop,
                   const RecordKey& in_rkey);
       CombinedKey(const CombinedKey& in);
       CombinedKey& operator=(const CombinedKey& in);
       ~CombinedKey();
       
       bool operator<(const CombinedKey& rhs) const;
       bool operator==(const CombinedKey& rhs) const;
       bool operator!=(const CombinedKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       CombinedKey(const unsigned int* buf) : rkey(buf)
       {gop.icode=buf[1];}

       int numints() const {return 2;}
       size_t numbytes() const {return 2*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {rkey.copyTo(buf); buf[1]=gop.icode;}

    }; 


   typedef GlueballOperatorInfo  FileKey;
   typedef Complex               DataType;


 protected:

       // pointers to internal infos (managed by this handler
       // with new and delete)

   const GaugeConfigurationInfo *uPtr;
   const GluonSmearingInfo *gSmearPtr;
   const FileListInfo *fPtr;


       // data I/O handler pointers

   DataPutHandlerMF<GlueballHandler,FileKey,RecordKey,DataType> *DHputPtr;
   DataGetHandlerMF<GlueballHandler,FileKey,RecordKey,DataType> *DHgetPtr;
   DataPutHandlerSF<GlueballHandler,CombinedKey,DataType> *DHSFputPtr;
   bool m_read_mode;


       // Prevent copying ... handler might contain large
       // amounts of data

   GlueballHandler(const GlueballHandler&);
   GlueballHandler& operator=(const GlueballHandler&);



 public:


   GlueballHandler();

   GlueballHandler(const GaugeConfigurationInfo& gaugeinfo,
                   const GluonSmearingInfo& gluonsmear,
                   const FileListInfo& glueballfilelist,
                   bool read_mode=true);

   void setInfo(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const FileListInfo& glueballfilelist,
                bool read_mode=true);

   ~GlueballHandler();

   void clear();



   bool isInfoSet() const;

   const GaugeConfigurationInfo& getGaugeConfigurationInfo() const;

   const GluonSmearingInfo& getGluonSmearingInfo() const;

   const FileListInfo& getFileListInfo() const;

   void getHeader(XmlWriter& xmlout) const;

   void getFileMap(XmlWriter& xmlout) const;

   void outputKeys(XmlWriter& xmlout);


        //  Compute line ends for all glueball operators in "GopInfos".
        //  Handler must NOT be in read mode.
 
   void compute(const set<GlueballOperatorInfo>& GopInfos,
                const string& smeared_gauge_filename,
                const string& smeared_quark_filestub);

        // read from file (loop through file list to find)
        // when in read mode

   const DataType& getData(const GlueballOperatorInfo& gop,
                           int timevalue);

          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

   bool queryData(const GlueballOperatorInfo& gop,
                  int timevalue);

   bool queryData(const GlueballOperatorInfo& mop);


        // remove from internal memory

   void removeData(const GlueballOperatorInfo& mop,
                   int timevalue);

   void removeData(const GlueballOperatorInfo& mop);


   void clearData();   

          // more query subroutines

   set<GlueballOperatorInfo> getGlueballOperators() const;


 private:


   void set_info(const GaugeConfigurationInfo& gaugeinfo,
                 const GluonSmearingInfo& smear,
                 const FileListInfo& flist,
                 bool read_mode);


   bool checkHeader(XmlReader& xmlr, int suffix);
   void writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                    int suffix);

   bool checkHeader(XmlReader& xmlr);
   void writeHeader(XmlWriter& xmlout);

   void filefail(const std::string& message);
   void check_info_set(const std::string& name,
                       int check_mode=0) const;

   friend class DataPutHandlerMF<GlueballHandler,FileKey,RecordKey,DataType>;
   friend class DataGetHandlerMF<GlueballHandler,FileKey,RecordKey,DataType>;
   friend class DataPutHandlerSF<GlueballHandler,CombinedKey,DataType>;

};


// ***************************************************************

#endif
  }
}
#endif  
