#ifndef BARYON_HANDLER_H
#define BARYON_HANDLER_H

#include "qdp.h"
#include "chromabase.h"
#include "gauge_configuration_info.h"
#include "gauge_configuration_handler.h"
#include "xml_help.h"
#include "field_smearing_info.h"
#include "field_smearing_handler.h"
#include "dilution_scheme_info.h"
#include "laph_noise_info.h"
#include "quark_info.h"
#include "quark_line_end_info.h"
#include "quark_handler.h"
#include "baryon_operator.h"
#include "filelist_info.h"
#include "time_slices.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {

#if (QDP_ND == 3)

// *****************************************************************
// *                                                               *
// *  "BaryonHandler" handles computation of and subsequent access *
// *  to the baryon line ends.  When opened NOT in read mode, it   *
// *  uses the "compute" function to calculate the baryon line     *
// *  ends and output them to binary file.  When opened in read    *
// *  mode, one uses the "queryData" and "getData" members to      *
// *  read the previously-calculated baryon line ends from file.   *
// *                                                               *
// *  One of these handlers deals with baryon line ends for        *
// *  **one** set of common info parameters, given by              *
// *                                                               *
// *         GaugeConfigurationInfo                                *
// *         GluonSmearingInfo                                     *
// *         QuarkInfo (for each of the three quarks)              *
// *         FileListInfo                                          *
// *                                                               *
// *  but one handler deals with different                         *
// *                                                               *
// *         baryon operators                                      *
// *         time dilution projector indices                       *
// *         noises                                                *
// *         all spin-eigvec dilution indices                      *
// *                                                               *
// *  File structure and contents:                                 *
// *                                                               *
// *   - Results manipulated by one handler are contained in       *
// *     several files.  Each file has the same stub, but          *
// *     different positive integer suffices.                      *
// *             stub.0                                            *
// *             stub.1                                            *
// *             ...                                               *
// *             stub.N                                            *
// *     The files included are specified in a FileListInfo.       *
// *                                                               *
// *   - The header info in each file has a common part and a      *
// *     part that is specific to that one file:                   *
// *        <BaryonHandlerDataFile>                                *
// *           - common part                                       *
// *           - specific part                                     *
// *        </BaryonHandlerDataFile>                               *
// *                                                               *
// *   - Info common to ALL files in FileListInfo:                 *
// *                                                               *
// *       GaugeConfigurationInfo                                  *
// *       GluonSmearingInfo                                       *
// *       QuarkInfo (for each of the three quarks)                *
// *                                                               *
// *   - Info specific to EACH file in FileListInfo:               *
// *                                                               *
// *       BaryonOperatorInfo (see comments below)                 *
// *                                                               *
// *   - DataBase Key for records in EACH file are specified by    *
// *     a BaryonHandler::RecordKey                                *
// *                                                               *
// *       QuarkLineEndInfo (for each of the three quarks)         *
// *       LaphNoiseInfo (for each of the three quarks)            *
// *       time value                                              *
// *                                                               *
// *     Each record has various dilution indices. Each DB data    *
// *     element has the form                                      *
// *                                                               *
// *       multi3d<Complex> data;                                  *
// *       data(dilind1,dilind2,dilind3);                          *
// *                                                               *
// *                                                               *
// *     Many different baryon operators are constructed           *
// *     simultaneously (since they have common elemental          *
// *     operators), so dumping results to file after              *
// *     each dilution index for quarks 1 and 2 drastically        *
// *     cuts down on the amount of memory needed during the       *
// *     computations.  Results are stored in temporary files      *
// *     by the "compute" routine, and then the data in these      *
// *     files is rearranged and put into the filedb files.        *
// *                                                               *
// *  All Laph Handlers follow the member naming convention:       *
// *                                                               *
// *    compute....()  to do original computation                  *
// *    getData()      provides access to results                  *
// *    queryData()    to check if data is available               *
// *                                                               *
// *  "BaryonLineCalc" is defined in this handler.  It contains    *
// *  the line types of each quark (sink or source, fixed or       *
// *  relative time), the mode of each quark line (normal or       *
// *  with gamma5 hermitian conjugate), and maximum time           *
// *  separation.  This is used only in the "compute" member.      *
// *                                                               *
// *  In each calculation, the following assumptions are made:     *
// *   -- all relative-scheme lines use sink=source times          *
// *   -- all fixed-scheme lines use the same source time          *
// *   -- if all lines use relative scheme, max_sep is set to zero *
// *        and all start times are computed                       *
// *   -- any fixed source (not sink) line sets max_sep to zero    *
// *                                                               *
// *                                                               *
// *  Important REMARKS:   The file structure discussed above      *
// *  is the **final** form used by the get handlers.  However,    *
// *  the compute subroutine produces a small number of files      *
// *  that are much larger, containing multiple hadron operators.  *
// *  This is necessary since Lustre does not deal well with       *
// *  many small files.  The idea here is to compute the larger    *
// *  but fewer files on XSEDE machines that use the Lustre file   *
// *  system, tar these fewer files up, then copy to CMU, and use  *
// *  the "reorganize" subroutine to extract the different hadron  *
// *  operators into different files, and to combine the different *
// *  hadron lines into the same files.  After the reorganization, *
// *  all hadron line ends for one hadron operator should be       *
// *  gathered together into a single file.                        *
// *                                                               *
// *                                                               *
// *****************************************************************


class BaryonHandler
{

 public:

   struct BaryonNoiseInfo 
    {
       LaphNoiseInfo noise1,noise2,noise3; 

       BaryonNoiseInfo(XmlReader& xmlin) 
         : noise1(xmlin,"BaryonNoise/QuarkLine1"),
           noise2(xmlin,"BaryonNoise/QuarkLine2"),
           noise3(xmlin,"BaryonNoise/QuarkLine3") {}

       BaryonNoiseInfo(XmlReader& xmlin, int k) 
         : noise1(xmlin,"BaryonNoise["+int_to_string(k)+"]/QuarkLine1"),
           noise2(xmlin,"BaryonNoise["+int_to_string(k)+"]/QuarkLine2"),
           noise3(xmlin,"BaryonNoise["+int_to_string(k)+"]/QuarkLine3") {}

       BaryonNoiseInfo(const LaphNoiseInfo& in_noise1,
                       const LaphNoiseInfo& in_noise2,
                       const LaphNoiseInfo& in_noise3) 
        : noise1(in_noise1), noise2(in_noise2), noise3(in_noise3) {}

       BaryonNoiseInfo(const BaryonNoiseInfo& in) 
        : noise1(in.noise1), noise2(in.noise2), noise3(in.noise3) {}

       BaryonNoiseInfo& operator=(const BaryonNoiseInfo& in)
        {noise1=in.noise1; noise2=in.noise2; noise3=in.noise3; return *this;}

       ~BaryonNoiseInfo(){}

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool operator<(const BaryonNoiseInfo& rhs) const
        {return multiLessThan(noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3);}

       bool operator==(const BaryonNoiseInfo& rhs) const
        {return multiEqual(noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3);}

       bool operator!=(const BaryonNoiseInfo& rhs) const
        {return multiNotEqual(noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3);}

    };


  struct BaryonLineEndInfo
    {
       QuarkLineEndInfo qline1,qline2,qline3; 

       BaryonLineEndInfo(XmlReader& xmlin) 
         : qline1(xmlin,"BaryonLineEndInfo/QuarkLine1"),
           qline2(xmlin,"BaryonLineEndInfo/QuarkLine2"),
           qline3(xmlin,"BaryonLineEndInfo/QuarkLine3") {}

       BaryonLineEndInfo(XmlReader& xmlin, int k) 
         : qline1(xmlin,"BaryonLineEndInfo["+int_to_string(k)+"]/QuarkLine1"),
           qline2(xmlin,"BaryonLineEndInfo["+int_to_string(k)+"]/QuarkLine2"),
           qline3(xmlin,"BaryonLineEndInfo["+int_to_string(k)+"]/QuarkLine3") {}

       BaryonLineEndInfo(const QuarkLineEndInfo& in_qline1, 
                         const QuarkLineEndInfo& in_qline2,
                         const QuarkLineEndInfo& in_qline3)
         : qline1(in_qline1), qline2(in_qline2), qline3(in_qline3) {}

       BaryonLineEndInfo(const BaryonLineEndInfo& in)
         : qline1(in.qline1), qline2(in.qline2), qline3(in.qline3) {}

       BaryonLineEndInfo& operator=(const BaryonLineEndInfo& in)
         {qline1=in.qline1; qline2=in.qline2; qline3=in.qline3; return *this;}

       ~BaryonLineEndInfo() {}

       bool operator<(const BaryonLineEndInfo& rhs) const
        {return multiLessThan(qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);}

       bool operator==(const BaryonLineEndInfo& rhs) const
        {return multiEqual(qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);}

       bool operator!=(const BaryonLineEndInfo& rhs) const
        {return multiNotEqual(qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);}
    };


       // BaryonLineEndInfo stores three quark lines which hold info
       // about the mode and type, but ALSO about the time projector
       // indices.  BaryonLineCalc only holds info about the mode/type.
       // The noises are also stored.

   class BaryonLineCalc
    {                        // q3, q2, q1 order stored in code
       unsigned int code;    // for each quark, 3 bits (mode, src/snk, fxd/rel)
       int min_time_sep;     // min time separation (forced to be zero if all
                             // lines rel or one line is source+fixed)
       int max_time_sep;     // max time separation (forced to be zero if all
                             // lines rel or one line is source+fixed)
       set<BaryonNoiseInfo> bnoises;

     public:

       BaryonLineCalc(XmlReader& xmlin, int k);

       BaryonLineCalc(const BaryonLineCalc& in)
         : code(in.code), min_time_sep(in.min_time_sep),
           max_time_sep(in.max_time_sep), bnoises(in.bnoises) {}

       BaryonLineCalc& operator=(const BaryonLineCalc& in)
         {code=in.code; bnoises=in.bnoises;
          min_time_sep=in.min_time_sep;
          max_time_sep=in.max_time_sep; 
          return *this;}

       ~BaryonLineCalc() {}

          // If noises were input as "<All/>", this routine takes the
          // available noises in "Quark1Noises", "Quark2Noises", "Quark3Noise",
          // and forms all possible BaryonNoiseInfo objects in "bnoises".
          // Otherwise, it checks the current "bnoises" and removes those
          // noises that are not available as specified by "Quark1Noises", 
          // "Quark2Noises", "Quark3Noises". Also enforces that the quark
          // noises in the baryon are different.

       set<BaryonNoiseInfo> getAvailableNoises(
                      const set<LaphNoiseInfo>& Quark1Noises,
                      const set<LaphNoiseInfo>& Quark2Noises,
                      const set<LaphNoiseInfo>& Quark3Noises) const;

       void output(XmlWriter& xmlout) const;

       std::string output(int indent=0) const;

       bool isSource(int quark) const;

       bool isNormalMode(int quark) const;

       bool isGamma5HermConjMode(int quark) const;

       bool isFixedSourceScheme(int quark) const;

       bool isRelativeSource(int quark) const;

       int getMinTimeSeparation() const {return min_time_sep;}

       int getMaxTimeSeparation() const {return max_time_sep;}

       bool canShareQuarkHandler(int quarkA, int quarkB) const;

       QuarkLineEndInfo getLineInfo(int quark, int t0, int lat_textent) const;

       bool operator<(const BaryonLineCalc& rhs) const
        {return multiLessThan(code,rhs.code,min_time_sep,rhs.min_time_sep,
                              max_time_sep,rhs.max_time_sep);}

       bool operator==(const BaryonLineCalc& rhs) const
        {return multiEqual(code,rhs.code, min_time_sep,rhs.min_time_sep,
                           max_time_sep,rhs.max_time_sep);}

       bool operator!=(const BaryonLineCalc& rhs) const
        {return multiNotEqual(code,rhs.code, min_time_sep,rhs.min_time_sep,
                              max_time_sep,rhs.max_time_sep);}

     private:

       unsigned int read_type(XmlReader& xmlin, const std::string& tag);
       unsigned int read_mode(XmlReader& xmlin, const std::string& tag);
       std::string write_type(unsigned int end_type) const;
       std::string write_mode(unsigned int mode) const;

    };


   class QuarkFileLists
    {
       FileListInfo q1fxdfiles,q2fxdfiles,q3fxdfiles;
       FileListInfo q1relfiles,q2relfiles,q3relfiles;

     public:

       QuarkFileLists(XmlReader& xmlin)
         : q1fxdfiles(xmlin,"Quark1/QuarkInfo/FixedSource"),
           q2fxdfiles(xmlin,"Quark2/QuarkInfo/FixedSource"),
           q3fxdfiles(xmlin,"Quark3/QuarkInfo/FixedSource"),
           q1relfiles(xmlin,"Quark1/QuarkInfo/RelativeSource"),
           q2relfiles(xmlin,"Quark2/QuarkInfo/RelativeSource"),
           q3relfiles(xmlin,"Quark3/QuarkInfo/RelativeSource") {}

       QuarkFileLists(const FileListInfo& q1fixed, const FileListInfo& q1rel,
                      const FileListInfo& q2fixed, const FileListInfo& q2rel,
                      const FileListInfo& q3fixed, const FileListInfo& q3rel)
         :  q1fxdfiles(q1fixed),q2fxdfiles(q2fixed),q3fxdfiles(q3fixed),
            q1relfiles(q1rel),q2relfiles(q2rel),q3relfiles(q3rel) {}

       ~QuarkFileLists() {}

       QuarkFileLists(const QuarkFileLists& in)
         :  q1fxdfiles(in.q1fxdfiles),q2fxdfiles(in.q2fxdfiles),
            q3fxdfiles(in.q3fxdfiles),q1relfiles(in.q1relfiles),
            q2relfiles(in.q2relfiles),q3relfiles(in.q3relfiles) {}

       QuarkFileLists& operator=(const QuarkFileLists& in)
         {q1fxdfiles=in.q1fxdfiles; q2fxdfiles=in.q2fxdfiles;
          q3fxdfiles=in.q3fxdfiles; q1relfiles=in.q1relfiles;
          q2relfiles=in.q2relfiles; q3relfiles=in.q3relfiles; return *this;}

       const FileListInfo& getFixedSchemeFiles(int quark) const;

       const FileListInfo& getRelativeSchemeFiles(int quark) const;

       void output(XmlWriter& xmlout) const;

       std::string output() const;

    };


   struct RecordKey
    {
       LaphNoiseInfo noise1,noise2,noise3;
       QuarkLineEndInfo qline1,qline2,qline3;
       int time_value;

       RecordKey();
       RecordKey(const LaphNoiseInfo& in_noise1, 
                 const LaphNoiseInfo& in_noise2,
                 const LaphNoiseInfo& in_noise3, 
                 const QuarkLineEndInfo& in_qline1,
                 const QuarkLineEndInfo& in_qline2,
                 const QuarkLineEndInfo& in_qline3,
                 int in_time);
       RecordKey(const BaryonNoiseInfo& in_bnoise, 
                 const BaryonLineEndInfo& in_bline,
                 int in_time);
       RecordKey(const RecordKey& in);
       RecordKey& operator=(const RecordKey& in);
       ~RecordKey();

       bool operator<(const RecordKey& rhs) const;
       bool operator==(const RecordKey& rhs) const;
       bool operator!=(const RecordKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       RecordKey(const unsigned int* buf)
       {noise1.store=buf[0]; qline1.store=buf[1];
        noise2.store=buf[2]; qline2.store=buf[3];
        noise3.store=buf[4]; qline3.store=buf[5];
        time_value=buf[6];}

       int numints() const {return 7;}
       size_t numbytes() const {return 7*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {buf[0]=noise1.store; buf[1]=qline1.store;
        buf[2]=noise2.store; buf[3]=qline2.store;
        buf[4]=noise3.store; buf[5]=qline3.store;
        buf[6]=time_value;}
        
    }; 

   struct CombinedKey
    {
       RecordKey rkey;
       BaryonOperatorInfo bop;

       CombinedKey();
       CombinedKey(const BaryonOperatorInfo& in_bop,
                   const RecordKey& in_rkey);
       CombinedKey(const CombinedKey& in);
       CombinedKey& operator=(const CombinedKey& in);
       ~CombinedKey();
       
       bool operator<(const CombinedKey& rhs) const;
       bool operator==(const CombinedKey& rhs) const;
       bool operator!=(const CombinedKey& rhs) const;
       void output(XmlWriter& xmlw) const;
       
       CombinedKey(const unsigned int* buf) : rkey(buf)
       {bop.icode1=buf[7]; bop.icode2=buf[8];}

       int numints() const {return 9;}
       size_t numbytes() const {return 9*sizeof(unsigned int);}
       
       void copyTo(unsigned int* buf) const
       {rkey.copyTo(buf); buf[7]=bop.icode1; buf[8]=bop.icode2;}

    }; 

   typedef BaryonOperatorInfo  FileKey;
   typedef multi3d<Complex>    DataType;


 protected:

       // pointers to internal infos (managed by this handler
       // with new and delete)

   const GaugeConfigurationInfo *uPtr;
   const GluonSmearingInfo *gSmearPtr;
   const QuarkInfo *quark1Ptr, *quark2Ptr, *quark3Ptr;
   const FileListInfo *fPtr;


       // pointers to needed sub-handlers (managed by this handler)
 
   QuarkHandler *qHandler1, *qHandler2, *qHandler3;


       // data I/O handler pointers

   DataPutHandlerMF<BaryonHandler,FileKey,RecordKey,DataType> *DHputPtr;
   DataGetHandlerMF<BaryonHandler,FileKey,RecordKey,DataType> *DHgetPtr;
   DataPutHandlerSF<BaryonHandler,CombinedKey,DataType> *DHSFputPtr;
   DataGetHandlerSF<BaryonHandler,CombinedKey,DataType> *DHSFgetPtr;
   bool m_read_mode;


       // Prevent copying ... handler might contain large
       // amounts of data

   BaryonHandler(const BaryonHandler&);
   BaryonHandler& operator=(const BaryonHandler&);



 public:


   BaryonHandler();

   BaryonHandler(const GaugeConfigurationInfo& gaugeinfo,
                 const GluonSmearingInfo& gluonsmear,
                 const QuarkInfo& quark1, 
                 const QuarkInfo& quark2,
                 const QuarkInfo& quark3,
                 const FileListInfo& baryonfilelist,
                 bool read_mode=true);

   void setInfo(const GaugeConfigurationInfo& gaugeinfo,
                const GluonSmearingInfo& gluonsmear,
                const QuarkInfo& quark1, 
                const QuarkInfo& quark2,
                const QuarkInfo& quark3,
                const FileListInfo& baryonfilelist,
                bool read_mode=true);

   ~BaryonHandler();

   void clear();



   bool isInfoSet() const;

   const GaugeConfigurationInfo& getGaugeConfigurationInfo() const;

   const GluonSmearingInfo& getGluonSmearingInfo() const;

   const QuarkInfo& getQuarkInfo(int quark_number) const;

   const FileListInfo& getFileListInfo() const;

   void getHeader(XmlWriter& xmlout) const;

   void getFileMap(XmlWriter& xmlout) const;

   void outputKeys(XmlWriter& xmlout);


        //  Compute line ends for all baryon operators in "BopInfos" and
        //  all dilution indices.  Repeat for all requested baryon calculations
        //  given in "bcalcs" (which includes all baryon noises to compute).
        //  If the list of baryon noises is empty, all available noises 
        //  in the quark sink files are used.  Handler must NOT be in read mode.
 
   void compute(const set<BaryonOperatorInfo>& BopInfos,
                const string& CoefsTopDirectory,
                const QuarkFileLists& Qfiles,
                const set<BaryonLineCalc>& bcalcs,
                const string& smeared_gauge_filename,
                const string& smeared_quark_filestub);


        //  Merge the baryon line ends in "infiles" into the files in
        //  "baryonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "baryonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
        //  WARNING:  This routine is meant to be used by SERIAL code only.
        //    It will still work in parallel, but a lot of unnecessary broadcasts
        //    are done.
 
   void merge(const FileListInfo& infiles);
   void merge(const vector<FileListInfo>& infiles);
   void reorganize(const vector<FileListInfo>& infiles);


        // read from file (loop through file list to find)
        // when in read mode

   const DataType& getData(const BaryonOperatorInfo& bop,
                           const BaryonLineEndInfo& bline,
                           const BaryonNoiseInfo& bnoise,
                           int timevalue);

   const DataType& getData(const BaryonOperatorInfo& bop,
                           const QuarkLineEndInfo& qline1,
                           const QuarkLineEndInfo& qline2,
                           const QuarkLineEndInfo& qline3,
                           const LaphNoiseInfo& noise1,
                           const LaphNoiseInfo& noise2,
                           const LaphNoiseInfo& noise3,
                           int timevalue);


          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

   bool queryData(const BaryonOperatorInfo& bop,
                  const BaryonLineEndInfo& bline,
                  const BaryonNoiseInfo& bnoise,
                  int timevalue);

   bool queryData(const BaryonOperatorInfo& bop,
                  const QuarkLineEndInfo& qline1,
                  const QuarkLineEndInfo& qline2,
                  const QuarkLineEndInfo& qline3,
                  const LaphNoiseInfo& noise1,
                  const LaphNoiseInfo& noise2,
                  const LaphNoiseInfo& noise3,
                  int timevalue); 

   bool queryData(const BaryonOperatorInfo& bop);


        // remove from internal memory

   void removeData(const BaryonOperatorInfo& bop,
                   const BaryonLineEndInfo& bline,
                   const BaryonNoiseInfo& bnoise,
                   int timevalue);

   void removeData(const BaryonOperatorInfo& bop,
                   const QuarkLineEndInfo& qline1,
                   const QuarkLineEndInfo& qline2,
                   const QuarkLineEndInfo& qline3,
                   const LaphNoiseInfo& noise1,
                   const LaphNoiseInfo& noise2,
                   const LaphNoiseInfo& noise3,
                   int timevalue); 

   void removeData(const BaryonOperatorInfo& bop);


   void clearData();   

          // more query subroutines

   set<BaryonOperatorInfo> getBaryonOperators() const;

   set<BaryonNoiseInfo> getBaryonNoises(const BaryonOperatorInfo& binfo) const; 

   set<BaryonLineEndInfo> getBaryonLineEndInfos(const BaryonOperatorInfo& binfo) const;
   

 private:


   void set_info(const GaugeConfigurationInfo& gaugeinfo,
                 const GluonSmearingInfo& smear,
                 const QuarkInfo& quark1, 
                 const QuarkInfo& quark2,
                 const QuarkInfo& quark3,
                 const FileListInfo& flist,
                 bool read_mode);


   bool checkHeader(XmlReader& xmlr, int suffix);
   void writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                    int suffix);

   bool checkHeader(XmlReader& xmlr);
   void writeHeader(XmlWriter& xmlout);

   void connectQuarkHandlers(const BaryonLineCalc& Bcalc,
                             const QuarkFileLists& Qfiles,
                             const std::string& smeared_gauge_filename,
                             const std::string& smeared_quark_filestub);
   void disconnectQuarkHandlers();
   void filefail(const std::string& message);
   void check_info_set(const std::string& name,
                       int check_mode=0) const;

   void evaluateColorCrossProduct(const LatticeColorVector *v1,
                                  const LatticeColorVector *v2,
                                  auto_ptr<LatticeColorVector>& result);
   void evaluateColorVectorContract(const auto_ptr<LatticeColorVector>& v1,
                                    const LatticeColorVector *v2,
                                    auto_ptr<LatticeComplex>& result);

   friend class DataPutHandlerMF<BaryonHandler,FileKey,RecordKey,DataType>;
   friend class DataGetHandlerMF<BaryonHandler,FileKey,RecordKey,DataType>;
   friend class DataPutHandlerSF<BaryonHandler,CombinedKey,DataType>;
   friend class DataGetHandlerSF<BaryonHandler,CombinedKey,DataType>;

};


// ***************************************************************

#endif
  }
}
#endif  
