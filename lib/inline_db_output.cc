#include "inline_db_output.h"
#include "output_help.h"
#include "quark_handler.h"
#include "baryon_handler.h"
#include "meson_handler.h"

namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 3)

  namespace InlineStochLaphDBOutputEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "SmearGaugeFieldInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochLaphDBOutputInlineMeas(xml_in, path);
}


const std::string name = "LAPH_DB_OUTPUT";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}

	
// *********************************************************************
	
     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochLaphDBOutputInlineMeas::operator()(unsigned long update_no,
                                             XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

 string dbfile;
 xmlread(xml_rdr,"DBFileName",dbfile,"LAPH_DB_OUTPUT");
 string datatype;
 xmlread(xml_rdr,"DataType",datatype,"LAPH_DB_OUTPUT");
 datatype=tidyString(datatype);
 string logfile;
 xmlread(xml_rdr,"LogFile",logfile,"LAPH_DB_OUTPUT");
 string verbosity;
 xmlreadif(xml_rdr,"Verbosity",verbosity,"LAPH_DB_OUTPUT");
 bool printdata=false;
 if (tidyString(verbosity)=="full") printdata=true;

 if (datatype=="QuarkSink"){
    IOMap<QuarkHandler::RecordKey,QuarkHandler::DataType> iom;
    iom.openReadOnly(dbfile,"Laph--QuarkSink");
    iom.outputContents(logfile,printdata);}
 else if (datatype=="BaryonLineEnd"){
    IOMap<BaryonHandler::RecordKey,BaryonHandler::DataType> iom;
    iom.openReadOnly(dbfile,"Laph--BaryonFile");
    iom.outputContents(logfile,printdata);}
 else if (datatype=="MesonLineEnd"){
    IOMap<MesonHandler::RecordKey,MesonHandler::DataType> iom;
    iom.openReadOnly(dbfile,"Laph--MesonFile");
    iom.outputContents(logfile,printdata);}
    
} 

// ******************************************************************
  }
#endif
} // namespace Chroma
