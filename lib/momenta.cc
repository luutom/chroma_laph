#include "momenta.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {


#if (QDP_ND == 3)

    //  sets  exp(-I*p*x) on each lattice site

LatticeComplex makeMomPhaseField(const Momentum& p)
{
 LatticeComplex phases;
 if ((p.x==0)&&(p.y==0)&&(p.z==0)){
    phases=1.0; return phases;}

 LatticeReal p_dot_r = zero;
 if (p.x!=0){
    int mu=0;
    p_dot_r += Layout::latticeCoordinate(mu)*p.x 
               * twopi / Real(Layout::lattSize()[mu]);}
 if (p.y!=0){
    int mu=1;
    p_dot_r += Layout::latticeCoordinate(mu)*p.y 
               * twopi / Real(Layout::lattSize()[mu]);}
 if (p.z!=0){
    int mu=2;
    p_dot_r += Layout::latticeCoordinate(mu)*p.z 
               * twopi / Real(Layout::lattSize()[mu]);}
 phases = cmplx(cos(p_dot_r),-sin(p_dot_r));
 return phases;
}

#endif


   //   Allowed momentum rays:
   //     000  +00  0+0  00+  -00  0-0  00-
   //     ++0  +-0  +0+  +0-  0++  0+-
   //     --0  -+0  -0-  -0+  0--  0-+
   //     +++  ++-  +-+  +--  ---  --+  -+-  -++

bool getMomentumRay(int px, int py, int pz, string& ray)
{
 ray.clear();
 int ppx=(px<0)?-px:px;  // absolute values
 int ppy=(py<0)?-py:py;
 int ppz=(pz<0)?-pz:pz;
 int n=0,nz[3];
 if (ppx>0) nz[n++]=ppx;
 if (ppy>0) nz[n++]=ppy;
 if (ppz>0) nz[n++]=ppz;
 if ((n==2)&&(nz[0]!=nz[1])) return false;
 if ((n==3)&&((nz[0]!=nz[1])||(nz[1]!=nz[2]))) return false;

 char dir[3]={'-','0','+'};
 ppx=(px>0)?1:((px<0)?-1:0);  // get zero or sign
 ppy=(py>0)?1:((py<0)?-1:0);
 ppz=(pz>0)?1:((pz<0)?-1:0);
 ppx+=1; ppy+=1; ppz+=1;
 ray="mom_ray_"; ray+=dir[ppx]; ray+=dir[ppy]; ray+=dir[ppz];
 return true;
}

  // *****************************************************************

#if (QDP_ND == 3)

void evaluateMomentumSums(const vector<LatticeComplex*> phases,
                          const LatticeComplex* in, multi1d<Complex>& results)
{
 int nsums=phases.size();
 results.resize(nsums);
 if (in==0){
    for (int k=0;k<nsums;k++) results[k]=zero; 
    return;}

#if (ARCH_SCALAR == 1)
 const int nsites = Layout::vol();
#elif (ARCH_PARSCALAR == 1)
 const int nsites = Layout::sitesOnNode();
#endif

#if (QDP_USE_OMP_THREADS == 1)

#pragma omp parallel for if(nsums>8) 
 for (int k=0;k<nsums;k++){
    Complex sum=zero;
    for (int i=0;i<nsites;i++){
       sum.elem()+=in->elem(i)*phases[k]->elem(i);}
    results[k]=sum;}

#else 
 for (int k=0;k<nsums;k++){
    Complex sum=zero;
    for (int i=0;i<nsites;i++){
       sum.elem()+=in->elem(i)*phases[k]->elem(i);}
    results[k]=sum;}
#endif


#if (ARCH_PARSCALAR == 1)
 QDPInternal::globalSumArray(results);
#endif
}

#endif

// **************************************************

  }
}
