#include "inverter_info.h"
#include "chromabase.h"
#include <sstream>
using namespace std;


namespace Chroma {
  namespace LaphEnv {



InverterInfo::InverterInfo(const InverterInfo& rhs) 
             : inverter_xml(rhs.inverter_xml), 
               tol(rhs.tol), id(rhs.id), 
               max_iterations(rhs.max_iterations) {}
  
                                        
InverterInfo& InverterInfo::operator=(const InverterInfo& rhs)
{
 inverter_xml = rhs.inverter_xml;
 tol = rhs.tol;
 id = rhs.id;
 max_iterations = rhs.max_iterations;
 return *this;
}


InverterInfo::InverterInfo(XmlReader& xml_in)
{
 xml_tag_assert(xml_in,"InvertParam","InverterInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::InvertParam");
 extract_info_from_reader(xmlr);
}


void InverterInfo::extract_info_from_reader(XmlReader& xmlr)
{
 try{
    xmlread(xmlr,"invType",id, "InverterInfo");
    id=tidyString(id);
    if (  (id!="CG_INVERTER")
        &&(id!="BICGSTAB_INVERTER")
        &&(id!="IBICGSTAB_INVERTER")
        &&(id!="EIG_CG_INVERTER")
        &&(id!="RELIABLE_IBICGSTAB_MP_CLOVER_INVERTER")
        &&(id!="RELIABLE_BICGSTAB_MP_CLOVER_INVERTER")
        &&(id!="RELIABLE_CG_MP_CLOVER_INVERTER")){
       string msg("Bad input XML to InvertInfo\n");
       msg+="Expected <invType> to be one of \n";
       msg+="  CG_INVERTER, RELIABLE_CG_MP_CLOVER_INVERTER,\n";
       msg+="  BICGSTAB_INVERTER, IBICGSTAB_INVERTER,\n";
       msg+="  RELIABLE_IBICGSTAB_MP_CLOVER_INVERTER\n";
       msg+=" or EIG_CG_INVERTER\n";
       xml_cerr(xmlr,msg);
       xmlreadfail(xmlr,"InverterInfo");}
    if (xml_tag_count(xmlr,"RsdCG")==1)
       read(xmlr, "RsdCG", tol);
    else if (xml_tag_count(xmlr,"RsdBiCGStab")==1)
       read(xmlr, "RsdBiCGStab", tol);
    else if (xml_tag_count(xmlr,"RsdTarget")==1)
       read(xmlr, "RsdTarget", tol);
    else{
       xml_cerr(xmlr,"No requested residuum for convergence in InverterInfo");
       xmlreadfail(xmlr,"InverterInfo");}
    if (xml_tag_count(xmlr,"MaxCG")==1)
       read(xmlr, "MaxCG", max_iterations);
    else if (xml_tag_count(xmlr,"MaxBiCGStab")==1)
       read(xmlr, "MaxBiCGStab", max_iterations);
    else if (xml_tag_count(xmlr,"MaxIter")==1)
       read(xmlr, "MaxIter", max_iterations);
    else{
       xml_cerr(xmlr,"No maximum iterations in InvertParamInfo");
       xmlreadfail(xmlr,"InverterInfo");}
    ostringstream strm;
    xmlr.print(strm);
    inverter_xml = strm.str();
//    QDPIO::cout << "Inverter XML:"<<endl << inverter_xml << endl;
//    QDPIO::cout << "Tolerance = " << tol << endl;
//    QDPIO::cout << "Inverter ID = "<<id << endl;
//    QDPIO::cout << "Inverter Info Initialized" << endl;
    }
 catch(std::string& err){
    xml_cerr(xmlr,"ERROR: "+err);
    xml_cerr(xmlr,"could not initialize InverterInfo from XML input");
    xmlreadfail(xmlr,"InverterInfo");}
}


void InverterInfo::checkEqualTolerance(const InverterInfo& rhs) const
{
 if (rhs.tol != tol)
    throw string(__func__)+": Inverter tolerances do not match";
}

void InverterInfo::checkEqual(const InverterInfo& rhs) const
{
 if (!xmlContentIsEqual(inverter_xml,rhs.inverter_xml)){
    std::cerr << "InverterInfo checkEqual failed"<<endl;
    std::cerr << "LHS:"<<endl<<output()<<endl<<"RHS:"<<endl<<rhs.output()<<endl;
    throw string("Inverters do not match");}
}

void InverterInfo::matchXMLverbatim(const InverterInfo& rhs) const
{
 if (inverter_xml!=rhs.inverter_xml)
    throw string("Inverter XML strings do not exactly match");
}

bool InverterInfo::operator==(const InverterInfo& rhs) const
{
 return xmlContentIsEqual(inverter_xml,rhs.inverter_xml);
}


string InverterInfo::output(int indent) const
{
 if (indent==0) return inverter_xml;
 else{
    string pad(3*indent,' ');
    string temp(pad);
    int pos1=0;
    int pos2=inverter_xml.find('\n',pos1);
    while (pos2!=string::npos){
       temp+=inverter_xml.substr(pos1,pos2-pos1+1)+pad;
       pos1=pos2+1;
       pos2=inverter_xml.find('\n',pos1);}
    temp+=inverter_xml.substr(pos1,inverter_xml.length()-pos1+1);
    return temp;}
}


void InverterInfo::output(XmlWriter& xmlout) const
{
 xmlout.writeXML(inverter_xml);
}

// *******************************************************************
  }
}
