#include "glueball_operator_info.h"
#include "xml_help.h"
#include "multi_compare.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {



  // "current" element of xml_in should be <GlueballOp>

  //  encode operator information into two unsigned 32-bit integers
  //   icode:
  //      put all integers into a single unsigned integer 
  //         8 bits irrep, 5 bits irrep row, 4 bits each momx, momy, momz
  //         (first bit is sign, 3 bits remaining), spatial type 6 bits

GlueballOperatorInfo::GlueballOperatorInfo(XmlReader& xml_in)
{
 xml_tag_assert(xml_in,"GlueballOp","GlueballOperatorInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::GlueballOp");

 string irrep,spatialType;
 int momx,momy,momz,irrepRow;

 multi1d<int> p;
 xmlread(xmlr, "Momentum", p, "GlueballOperatorInfo");
 if (p.size()!=3){
    xml_cerr(xml_in,"Bad glueball momentum");
    xmlreadfail(xml_in,"GlueballOperatorInfo");}
 momx=p[0];
 momy=p[1];
 momz=p[2];

 xmlread(xmlr, "Irrep", irrep, "GlueballOperatorInfo");
 irrep=tidyString(irrep);
 xmlread(xmlr, "SpatialType", spatialType, "GlueballOperatorInfo");
 spatialType=tidyString(spatialType);
 xmlread(xmlr, "IrrepRow", irrepRow, "GlueballOperatorInfo");

 if (irrepRow<1){
    xml_cerr(xml_in,"Bad glueball operator input xml data");
    xmlreadfail(xml_in,"GlueballOperatorInfo");}
 if (irrepRow>31){
    xml_cerr(xml_in,"Irrep row > 31 not currently supported");
    QDP_abort(1);}
 if ((abs(momx)>7)||(abs(momy)>7)||(abs(momz)>7)){
    xml_cerr(xml_in,"momentum component magnitude > 7 not currently supported");
    QDP_abort(1);}

 unsigned int ir1,ir2,ir3,ir4,st;
 if      (spatialType=="TrEig" )   st=0;
 else if (spatialType=="TrW1Eig" ) st=1;
 else if (spatialType=="TrW2Eig")  st=2;
 else if (spatialType=="Plaq")     st=3;
 else{
    xml_cerr(xml_in,"spatialType "+spatialType+" not currently supported in GlueballOperatorInfo");
    QDP_abort(1);}

 string irp=irrep+"    ";  // so length at least 4 char
 if      (irp[0]=='A') ir1=0;
 else if (irp[0]=='B') ir1=1;
 else if (irp[0]=='E') ir1=2;
 else if (irp[0]=='T') ir1=3;
 else{
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in GlueballOperatorInfo");
    QDP_abort(1);}

 int k=1;
 if (irp[k]=='1'){ ir2=0; k++;}
 else if (irp[k]=='2'){ ir2=1; k++;}
 else ir2=2;
 
 if (irp[k]=='g'){ ir3=0; k++;}
 else if (irp[k]=='u'){ ir3=1; k++;}
 else ir3=2;
 
 if (irp[k]=='p'){ ir4=0; k++;}
 else if (irp[k]=='m'){ ir4=1; k++;}
 else ir4=2;
 
 if (k!=irrep.length()){
    xml_cerr(xml_in,"irrep "+irrep+" not currently supported in GlueballOperatorInfo");
    QDP_abort(1);}
 
    //  now do the encoding

 icode=ir1; 
 icode<<=2; icode|=ir2;
 icode<<=2; icode|=ir3;
 icode<<=2; icode|=ir4;
 icode<<=5; icode|=irrepRow;
 icode<<=1; icode|=(momx<0)?1:0; 
 icode<<=3; icode|=abs(momx);
 icode<<=1; icode|=(momy<0)?1:0;
 icode<<=3; icode|=abs(momy);
 icode<<=1; icode|=(momz<0)?1:0;
 icode<<=3; icode|=abs(momz);
 icode<<=6; icode|=st;

} 


Momentum GlueballOperatorInfo::getMomentum() const
{
 unsigned int tmp=(icode>>6);
 int pz=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) pz=-pz;
 tmp>>=4;
 int py=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) py=-py;
 tmp>>=4;
 int px=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) px=-px;
 return Momentum(px,py,pz);
}

int GlueballOperatorInfo::getXMomentum() const
{
 unsigned int tmp=(icode>>14);
 int res=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) return -res;
 return res;
}
  
int GlueballOperatorInfo::getYMomentum() const
{
 unsigned int tmp=(icode>>10);
 int res=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) return -res;
 return res;
}

int GlueballOperatorInfo::getZMomentum() const
{
 unsigned int tmp=(icode>>6);
 int res=tmp & 0x7u;
 if (((tmp>>3)&0x1u)==1) return -res;
 return res;
}

std::string GlueballOperatorInfo::getIrrep() const
{
 unsigned int irp=icode>>23;
 unsigned int ir4=irp & 0x3u; irp>>=2;
 unsigned int ir3=irp & 0x3u; irp>>=2;
 unsigned int ir2=irp & 0x3u; irp>>=2;
 unsigned int ir1=irp & 0x3u; irp>>=2;
 string irrep;
 if      (ir1==0) irrep+='A';
 else if (ir1==1) irrep+='B';
 else if (ir1==2) irrep+='E';
 else if (ir1==3) irrep+='T';
 if      (ir2==0) irrep+='1';
 else if (ir2==1) irrep+='2';
 if      (ir3==0) irrep+='g';
 else if (ir3==1) irrep+='u'; 
 if      (ir4==0) irrep+='p';
 else if (ir4==1) irrep+='m';
 return irrep;
}

int GlueballOperatorInfo::getIrrepRow() const
{
 return (icode>>18)&0x1Fu;
}

std::string GlueballOperatorInfo::getSpatialType() const
{
 unsigned int st= icode & 0x3Fu;
 if      (st==0) return string("TrEig");
 else if (st==1) return string("TrW1Eig");
 else if (st==2) return string("TrW2Eig");
 else if (st==3) return string("Plaq");
 return string("");
}
  
bool GlueballOperatorInfo::needsLaphEigenvalues() const
{
 unsigned int st= icode & 0x3Fu;
 return (st<3);
}

bool GlueballOperatorInfo::operator==(const GlueballOperatorInfo& rhs) const
{
 return (icode==rhs.icode);   
}

bool GlueballOperatorInfo::operator!=(const GlueballOperatorInfo& rhs) const
{
 return (icode!=rhs.icode);   
}

bool GlueballOperatorInfo::operator<(const GlueballOperatorInfo& rhs) const
{
 return (icode<rhs.icode);   
}


string GlueballOperatorInfo::output() const
{
 ostringstream oss;
 oss << "<GlueballOp>"<<endl;
 oss << "  <Momentum> " <<getXMomentum()<<" "<<getYMomentum()
      <<" "<<getZMomentum()<<" </Momentum>"<<endl;
 oss << "  <Irrep> "<<getIrrep()<<" </Irrep>"<<endl;
 oss << "  <IrrepRow> "<<getIrrepRow()<<" </IrrepRow>"<<endl;
 oss << "  <SpatialType> "<<getSpatialType()<<" </SpatialType>"<<endl;
 oss << "</GlueballOp>"<<endl;

 return oss.str();
}

void GlueballOperatorInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"GlueballOp");
 multi1d<int> p(3);  p[0]=getXMomentum(); 
 p[1]=getYMomentum(); p[2]=getZMomentum();
 write(xmlout,"Momentum",p);
 write(xmlout,"Irrep",getIrrep());
 write(xmlout,"IrrepRow",getIrrepRow());
 write(xmlout,"SpatialType",getSpatialType());
 pop(xmlout);
}


 // ********************************************************

   //  useful routine for reading multiple GlueballOperatorInfo;
   //  also reads name of coefficient file

void readGlueballOperators(XmlReader& xml_in, 
                           set<GlueballOperatorInfo>& gops)
{
 if (xml_tag_count(xml_in,"GlueballOperators")!=1){
    xml_cerr(xml_in,"XML input to CreateGlueballOperators must have");
    xml_cerr(xml_in," a single <GlueballOperators> ... </GlueballOperators>");
    throw(string("error"));}
 gops.clear();

 int num_mop=xml_tag_count(xml_in,"GlueballOperators/GlueballOp");
 for (int k=1;k<=num_mop;k++){
    ostringstream path;
    path << "./descendant-or-self::GlueballOperators/GlueballOp["<<k<<"]";
    XmlReader xml_op(xml_in,path.str());
    gops.insert(GlueballOperatorInfo(xml_op));}
}

// **************************************************************
  }
}
