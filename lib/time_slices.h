#ifndef TIME_SLICES_H
#define TIME_SLICES_H

#include "qdp.h"
#include <set>
#include "gauge_configuration_info.h"

namespace Chroma {
  namespace LaphEnv {

// *******************************************************************
// *                                                                 *
// *   Objects of class "TimesInfo" store a set of valid times.      *
// *   The XML input must have the format                            *
// *                                                                 *
// *       <TimesInfo>                                               *
// *           <Values> 0 3 7 12 </Values>                           *
// *           <Interval>                                            *
// *              <First>32</First><Length>3</Length>                *
// *           </Interval>                                           *
// *       </TimesInfo>                                              *
// *                                                                 *
// *   Multiple <Values> and <Interval> tags are allowed. <Length>   *
// *   must be at least 1, and no greater than the time extent of    *
// *   the lattice.  A short-cut to specify all time values is to    *
// *   include the tag <All/>.                                       *
// *                                                                 *
// *   One can then iterate through the values (which are sorted     *
// *   with repeats removed) using a set<int>::const_iterator        *
// *   and the begin() and end() members.                            *
// *                                                                 *
// *******************************************************************


class TimesInfo
{

  std::set<int> m_times;

 public:  

  TimesInfo(XmlReader& xml_in, const GaugeConfigurationInfo& gaugeinfo);

  TimesInfo(const TimesInfo& in);

  TimesInfo& operator=(const TimesInfo& in);

  ~TimesInfo(){}


    // output functions

  std::set<int>::const_iterator begin() const { return m_times.begin(); }

  std::set<int>::const_iterator end() const { return m_times.end(); }

  std::string output(int indent = 0) const;

  void output(XmlWriter& xmlout) const;


};


// *************************************************************

  // This function can be used to define Timeslices on the lattice.
  //
  // Usage:
  //     Set timeslices;
  //     timeslices.make(TimeSlice(Tdir)); 
  //     Subset slice = timeslices[t];  // how to get a time slice
  //     int nslices = timeslices.numSubsets();  


class TimeSlice : public SetFunc 
{

 public:

  TimeSlice(int Tdir) : t_dir(Tdir)
  {
   if ((Tdir<0)||(Tdir>=QDP::Nd)){
      QDPIO::cerr << "Invalid time direction in TimeSlice"<<endl;
      QDP_abort(1);}
  }

  int operator()(const multi1d<int>& coord) const
  {
   return coord[t_dir];
  }

  int numSubsets() const 
  {
   return Layout::lattSize()[t_dir];
  }
  
 private: 

  int t_dir;     // Time direction

};

// *********************************************************


  // This function can be used to define on/off Timeslices on 
  // the lattice.  Slices within NumTimes after StartTime are
  // considered "on".  An input value of zero for NumTimes is
  // interpreted as **all** time slices, then StartTime is ignored.
  //
  // Usage:
  //     Set OnOffTimes;
  //     OnOffTimes.make(TimeInterval(StartTime,NumTimes,Tdir)); 
  //     Subset onSlices = OnOffTimes[0];  // how to get a time slice
  //     Subset offSlices = OnOffTimes[1];
  //     int nsubsets = OnOffTimes.numSubsets();  


class TimeInterval : public SetFunc 
{

 public:

  TimeInterval(int StartTime, int NumTimes, int Tdir=QDP::Nd-1) : t_dir(Tdir) 
  {
   if ((Tdir<0)||(Tdir>=QDP::Nd)){
      QDPIO::cerr << "Invalid time direction in TimeInterval"<<endl;
      QDP_abort(1);}
   Textent = Layout::lattSize()[t_dir];
   if ((StartTime<0)||(StartTime>=Textent)||(NumTimes<0)||(NumTimes>Textent)){
      QDPIO::cerr << "Invalid StartTime/NumTimes in TimeInterval"<<endl;
      QDP_abort(1);}
   if (NumTimes>0){
      t_start=StartTime; t_stop=StartTime+NumTimes;}
   else{
      t_start=0; t_stop=Textent;}
  }

  TimeInterval(const TimeInterval& in) 
          : t_dir(in.t_dir), t_start(in.t_start), t_stop(in.t_stop),
            Textent(in.Textent) {}

  TimeInterval& operator=(const TimeInterval& in)
  {
   t_start=in.t_start;
   t_stop=in.t_stop;
   t_dir=in.t_dir;
   Textent=in.Textent;
   return *this;}

  int operator()(const multi1d<int>& coord) const
  {
   int t=coord[t_dir];
   if (t<t_start) t+=Textent;
   if (t<t_stop) return 0;  // in the set to keep (on)
   else return 1;   // in the second set (off)
  }

  int numSubsets() const {return 2;}

  int getStartTime() const {return t_start;}

  int getNumTimes() const {return t_stop-t_start;}

  bool isAll() const {return ((t_stop-t_start)==Textent);}

  bool contains(const TimeInterval& rhs) const
  {
   int tstop=(rhs.t_start>=t_start) ? rhs.t_stop : rhs.t_stop+Textent;
   return (tstop<=t_stop);
  }

  bool contains(int time) const
  {
   int t=(time>=t_start) ? time : time+Textent;
   return (t<t_stop);
  }

  bool operator==(const TimeInterval& rhs) const
  {
   return  ((t_start==rhs.t_start)&&(t_stop==rhs.t_stop)
          &&(t_dir==rhs.t_dir)&&(Textent==rhs.Textent));
  }

  bool operator!=(const TimeInterval& rhs) const
  {
   return  ((t_start!=rhs.t_start)||(t_stop!=rhs.t_stop)
          ||(t_dir!=rhs.t_dir)||(Textent!=rhs.Textent));
  }
  
 private: 

  int t_dir;          // Time direction
  int t_start, t_stop, Textent;

};


// *********************************************************
  }
}
#endif

