#include "inline_glueball_line_ends.h"
#include "chroma.h"

#ifdef TESTING
#include "tests.h"
#endif


namespace Chroma {
using namespace LaphEnv;

#if (QDP_ND == 3)

  namespace InlineStochLaphGlueballEnv {

    //  The crucial create measurement routine. Must be in the *.cc
    //  so that it is local to this file.  Dynamically allocates
    //  and instantiates an object of our class "StochLaphGlueballInlineMeas".

AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
                                        const std::string& path) 
{
 return new StochLaphGlueballInlineMeas(xml_in, path);
}


const std::string name = "LAPH_GLUEBALL_LINE_ENDS";

    // Registration boolean hidden in anonymous namespace.
namespace {
   bool registered = false;
}

    // Register all the factories.  This function may be called many
    // times by other measurements, so we only want to register this
    // inline measurement once.  Hence, the use of the "registered"
    // boolean above (which must be hidden in an anonymous namespace).

bool registerAll() 
{
 bool success = true; 
 if (!registered){
    success &= TheInlineMeasurementFactory::Instance().registerObject(
                      name, createMeasurement);
    registered = true;}
 return success;
}


// *********************************************************************

     // Subroutine which does all of the work!!  Input parameters
     // must be as shown (specified by Chroma).  Actual input to
     // this routine is through the private data member
     //     XMLReader xlm_rdr


void StochLaphGlueballInlineMeas::operator()(unsigned long update_no,
                                             XMLWriter& xmlout) 
{

 XmlReader xml_rdr(xml_rd);

    //  read the common info first

 if (xml_tag_count(xml_rdr,"GlueballCommonInfo")!=1){
    QDPIO::cerr << "Must have one <GlueballCommonInfo> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmlr(xml_rdr,"./GlueballCommonInfo");

 GaugeConfigurationInfo gaugeinfo(xmlr);
 GluonSmearingInfo gSmear(xmlr);
 FileListInfo glueballFiles(xmlr,"GlueballFileList");

 string smeared_gauge_file;
 xmlread(xmlr,"SmearedGaugeFileName",smeared_gauge_file,
         "LAPH_GLUEBALL_LINE_ENDS");
 smeared_gauge_file=tidyString(smeared_gauge_file);

 string smeared_quark_filestub;
 xmlread(xmlr,"SmearedQuarkFileStub",smeared_quark_filestub,
         "LAPH_GLUEBALL_LINE_ENDS");
 smeared_quark_filestub=tidyString(smeared_quark_filestub);

    // echo common input before starting the computations

 QDPIO::cout << endl << endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " *   Laph Task 3D: Compute the glueball line ends          *"<<endl;
 QDPIO::cout << " *                 and write to file as time slices        *"<<endl;
 QDPIO::cout << " *                                                         *"<<endl;
 QDPIO::cout << " ***********************************************************"<<endl;
 QDPIO::cout << endl;
 QDPIO::cout <<endl<<gaugeinfo.output()<<endl;
 QDPIO::cout <<endl<<endl<<"Gluon Smearing:"<<endl<<gSmear.output()<<endl<<endl;
 QDPIO::cout <<endl<<"Glueball File Stub:"<<endl<<glueballFiles.getFileStub()<<endl<<endl;

    //  read the glueball operators

 set<GlueballOperatorInfo> GOps;
 readGlueballOperators(xml_rdr,GOps);

     // read optional job name

 string jobname;
 xmlreadif(xml_rdr,"JobName",jobname,"LAPH_GLUEBALL_LINE_ENDS");

     // echo to the output xml file

 XmlBufferWriter xml_out;
 push(xml_out,"LAPH_GLUEBALL_LINE_ENDS");
 gaugeinfo.output(xml_out);
 gSmear.output(xml_out);
 push(xml_out,"GlueballOperators");
 for (set<GlueballOperatorInfo>::const_iterator git=GOps.begin();git!=GOps.end();++git)
    git->output(xml_out);
 pop(xml_out);
 pop(xml_out);
 xmlout << xml_out.str();

   // construct the glueball handler (NOT in read mode)

 GlueballHandler GH(gaugeinfo,gSmear,glueballFiles,false);

 GH.compute(GOps,smeared_gauge_file,smeared_quark_filestub);

 if (jobname.length()>0)
    QDPIO::cout <<endl<< "SUCCESSFUL GLUEBALL RUN: "<<jobname<<endl;

}

// ******************************************************************
  }
#endif
} // namespace Chroma

