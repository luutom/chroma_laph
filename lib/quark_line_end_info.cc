#include "quark_line_end_info.h"
#include "chromabase.h"
#include <sstream>

namespace Chroma {
  namespace LaphEnv {


// ************************************************************

   // XmlReader constructor

QuarkLineEndInfo::QuarkLineEndInfo(XmlReader& xml_in)
{ 
 xml_tag_assert(xml_in,"QuarkLineEndInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::QuarkLineEndInfo");
 extract_info_from_reader(xmlr);
}

QuarkLineEndInfo::QuarkLineEndInfo(XmlReader& xml_in, const string& inpath)
{
 string path(tidyString(inpath));
 if (xml_tag_count(xml_in,path+"/QuarkLineEndInfo")!=1){
    xml_cerr(xml_in,"Could not find unique path for QuarkLineEndInfo construction");
    xml_cerr(xml_in,"Path was "+path+"/QuarkLineEndInfo");
    xmlreadfail(xml_in,"QuarkLineEndInfo");}
 XmlReader xmlr(xml_in, "./descendant-or-self::"+path+"/QuarkLineEndInfo");
 extract_info_from_reader(xmlr);
}

void QuarkLineEndInfo::extract_info_from_reader(XmlReader& xmlr)
{
 string end_type;
 xmlread(xmlr,"Type",end_type,"QuarkLineEndInfo");
 end_type=tidyString(end_type);
 int type;
 if (end_type=="FixedSource") type=0;
 else if (end_type=="RelativeSource") type=1;
 else if (end_type=="SinkWithFixedSource") type=2;
 else if (end_type=="SinkWithRelativeSource") type=3;
 else{
    xml_cerr(xmlr,"Bad XML input to QuarkLineEndInfo");
    xml_cerr(xmlr,"Invalid <Type> tag");
    xmlreadfail(xmlr,"QuarkLineEndInfo");}

 int source=0;
 if (type>=2){
    xmlread(xmlr,"SourceTimeIndex",source,"QuarkLineEndInfo");}
 if (source<0){
    xml_cerr(xmlr,"Bad XML input to QuarkLineEndInfo");
    xml_cerr(xmlr,"<SourceTimeIndex> must be >= 0");
    xmlreadfail(xmlr,"QuarkLineEndInfo");}

 int mode=0;
 string modestr;
 if (xmlreadif(xmlr,"Mode",modestr,"QuarkLineEndInfo")){
    modestr=tidyString(modestr);
    if (modestr=="Normal") mode=0;
    else if (modestr=="Gamma5HermConj") mode=1;
    else{
       xml_cerr(xmlr,"Bad XML input to QuarkLineEndInfo");
       xml_cerr(xmlr,"Invalid <Mode> tag");
       xmlreadfail(xmlr,"QuarkLineEndInfo");}}

 encode(type,source,mode);
}

  // ************************************************************


void QuarkLineEndInfo::checkEqual(const QuarkLineEndInfo& in) const
{
 if (store!=in.store){
    std::cerr << "QuarkLineEndInfo checkEqual failed"<<endl;
    std::cerr << "LHS:"<<endl<<output()<<endl<<"RHS:"<<endl<<in.output()<<endl;
    throw string("QuarkLineEndInfo does not checkEqual...abort");}
}

  // current line end "matches" that of "in" if both are in the
  // same mode, both are relative or both are fixed schemes,
  // and one is a source while the other is a sink
  //   flip second bit from right of "in", then compare
  //   last three bits from right

bool QuarkLineEndInfo::match(const QuarkLineEndInfo& in) const
{
 unsigned int one= store & 0x7u;  // rightmost 3 bits
 unsigned int two= in.store & 0x7u; // rightmost 3 bits
 two^= 0x2u;  // flip 2nd bit from right
 return one==two;
}


string QuarkLineEndInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<QuarkLineEndInfo>"<<endl;
 oss << pad << "   <Type> ";
 int type= store&typemask;
 if (type==0) oss << "FixedSource";
 else if (type==1) oss << "RelativeSource";
 else if (type==2) oss << "SinkWithFixedSource";
 else oss << "SinkWithRelativeSource";
 oss << " </Type>"<<endl;
 if (type>=2){
    int source=store>>3;
    oss << pad << "   <SourceTimeIndex> "
        << source << " </SourceTimeIndex>"<<endl;}
 oss << pad << "   <Mode> ";
 if ((store&modemask)==0) oss << "Normal";
 else oss << "Gamma5HermConj";
 oss << " </Mode>"<<endl;
 oss << pad << "</QuarkLineEndInfo>"<<endl;
 return oss.str();
}

void QuarkLineEndInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkLineEndInfo");
 int type= store&typemask;
 if (type==0) write(xmlout,"Type","FixedSource");
 else if (type==1) write(xmlout,"Type","RelativeSource");
 else if (type==2) write(xmlout,"Type","SinkWithFixedSource");
 else write(xmlout,"Type","SinkWithRelativeSource");
 if (type>=2){
    int source=store>>3;
    write(xmlout,"SourceTimeIndex",source);}
 if ((store&modemask)==0) write(xmlout,"Mode","Normal");
 else write(xmlout,"Mode","Gamma5HermConj");
 pop(xmlout);
}

int QuarkLineEndInfo::getSourceTime(int time, const GaugeConfigurationInfo& U) const
{
 int type=store&typemask;
 int m_source=store>>3;
 int source_time=time;
 if (type==2) source_time=m_source;
 else if (type==3) source_time=time-m_source;
 int Nt=U.getTimeExtent();
 while (source_time<0) source_time+=Nt;
 while (source_time>=Nt) source_time-=Nt;
 return source_time;
}

// ***********************************************************
  }
}
