#include "laph_noise_info.h"
#include "xml_help.h"
using namespace std;

namespace Chroma {
  namespace LaphEnv {


// *************************************************************

LaphNoiseInfo::LaphNoiseInfo() : store(4) {}

   // XmlReader constructor

LaphNoiseInfo::LaphNoiseInfo(XmlReader& xml_in)
{
 xml_tag_assert(xml_in,"LaphNoiseInfo");
 XmlReader xmlr(xml_in, "./descendant-or-self::LaphNoiseInfo");
 extract_info_from_reader(xmlr);
}

LaphNoiseInfo::LaphNoiseInfo(XmlReader& xml_in, const string& inpath)
{
 string path(tidyString(inpath));
 if (xml_tag_count(xml_in,path+"/LaphNoiseInfo")!=1){
    xml_cerr(xml_in,"Could not find unique path for LaphNoiseInfo construction");
    xml_cerr(xml_in,"Path was "+path+"/LaphNoiseInfo");
    xmlreadfail(xml_in,"LaphNoiseInfo");}
 XmlReader xmlr(xml_in, "./descendant-or-self::"+path+"/LaphNoiseInfo");
 extract_info_from_reader(xmlr);
}


void LaphNoiseInfo::extract_info_from_reader(XmlReader& xml_in)
{
 int znGroup, seed;
 xmlread(xml_in,"ZNGroup", znGroup, "LaphNoiseInfo");
 xmlread(xml_in,"Seed", seed, "LaphNoiseInfo");
 try { 
   check_assignment(xml_in,znGroup,seed);
   encode(znGroup,seed); }
 catch(...){ xmlreadfail(xml_in,"LaphNoiseInfo");}
}

void LaphNoiseInfo::check_assignment(XmlReader& xml_in, int znGroup, int seed)
{
 if ((znGroup!=4)&&(znGroup!=8)&&(znGroup!=32)&&(znGroup!=1)){
    xml_cerr(xml_in,"improper initialization of LaphNoiseInfo");
    xml_cerr(xml_in,"ZNGroup must have integer value 4, 8, or 32");
    throw(string("error"));}
 if (znGroup==1){
    xml_cerr(xml_in,"Warning: znGroup = 1 so debugging ONLY");} 
 if ((seed<0)||(seed>65535)){
    xml_cerr(xml_in,"improper initialization of LaphNoiseInfo");
    xml_cerr(xml_in,"seed must have value 0 to 65535");
    throw(string("error"));}
}

void LaphNoiseInfo::encode(int znGroup, int seed)
{
 store=(unsigned int)seed;
 store<<=6;
 store|=(unsigned int)znGroup;
}

 // *************************************************************

    // copy constructor

LaphNoiseInfo::LaphNoiseInfo(const LaphNoiseInfo& in)
              : store(in.store) {}


LaphNoiseInfo& LaphNoiseInfo::operator=(const LaphNoiseInfo& in)
{
 store=in.store;
 return *this;
}

void LaphNoiseInfo::checkEqual(const LaphNoiseInfo& in) const
{
 if  (store!=in.store)
    throw string("LaphNoiseInfo does not checkEqual");
}

bool LaphNoiseInfo::operator==(const LaphNoiseInfo& in) const
{
 return (store==in.store);
}

bool LaphNoiseInfo::operator!=(const LaphNoiseInfo& in) const
{
 return (store!=in.store);
}

bool LaphNoiseInfo::operator<(const LaphNoiseInfo& in) const
{
 return (store<in.store);
}


// **********************************************************


unsigned long LaphNoiseInfo::getSeed(const GaugeConfigurationInfo& G) const
{
 int traj_num=G.getTrajNum();
 if ((traj_num<0)||(traj_num>65535)){
    std::cerr << " Error in LaphNoiseInfo::getSeed; unsupported HMC"
                << " trajectory number...limited range 0..65535"<<std::endl;
    throw(string("error"));}
 unsigned long m_seed = getSeed();
 unsigned long currTraj = traj_num;
 unsigned long rngseed  = m_seed & 0xFFUL;         // 8 least sig bits of m_seed
 rngseed = (rngseed<<16) | (currTraj & 0xFF00UL);  // 8 most sig bits of currTraj
 rngseed = (rngseed<<8) | (m_seed & 0xFF00UL)      // 8 most sig bits of m_seed
           | (currTraj & 0xFFUL);                  // 8 least sig bits of currTraj
 return rngseed;
}

string LaphNoiseInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<LaphNoiseInfo>"<<endl;
 oss << pad << "  <ZNGroup> " << getZNGroup() << " </ZNGroup>"<<endl;
 oss << pad << "  <Seed> " << getSeed() << " </Seed>"<<endl;
 oss << pad << "</LaphNoiseInfo>"<<endl;
 return oss.str();
}

void LaphNoiseInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"LaphNoiseInfo");
 write(xmlout,"ZNGroup",getZNGroup());
 write(xmlout,"Seed",getSeed());
 pop(xmlout);
}


// *************************************************************
  }
}
