#ifndef LAPH_BARYON_OPERATOR_H
#define LAPH_BARYON_OPERATOR_H

#include "qdp.h"
#include "chromabase.h"
#include "baryon_operator_info.h"
#include "momenta.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   BaryonOperator takes a BaryonOperatorInfo object as input     *
// *   to its constructor.  It reads the expansion coefficients      *
// *   of the baryon operator in terms of its elemental operators.   *
// *                                                                 *
// *   The superposition coefficients have been pre-computed         *
// *   (using a Maple program) and stored in a pre-defined           *
// *   format in a file located in a particular directory            *
// *   structure.  The elementals and coefficients for any           *
// *   given baryon are stored in a single file -- one file          *
// *   for each baryon operator, such as                             *
// *                                                                 *
// *     <top_directory>/nucleon_uud/mom_ray_0+-/Hu_3/DDL_4          *
// *                                                                 *
// *   The class needs access to the files which describe the        *
// *   baryon operators in terms of elementals.  Hence, the class    *
// *   needs to know the top directory above. You must call          *                                      *
// *                                                                 *
// *       BaryonOperator::initialize(coefsTopDirectory);            *
// *                                                                 *
// *   first before using this class.  There is no default           *
// *   constructor for this class.  The constructor takes the        *
// *   above XML (in the form of an XMLReader object) as input.      *
// *   Example usage:                                                *
// *                                                                 *
// *       BaryonOperator::initialize("coefsTopDirectory");          *
// *       XMLReader xml_in;   // assigned somehow                   *
// *       BaryonOperator B(xml_in);                                 *
// *                                                                 *
// *   The constructor does some checks on the input, then attempts  *
// *   to read the coefficients in terms of elementals for that      *
// *   operator.  If found, the object is assigned and the           *
// *   expansion details and coefficients in terms of elementals     *
// *   is stored.  If not found, an error message is output and      *
// *   an abort is issued.                                           *
// *                                                                 *
// *   Allowed momentum rays for baryons:                            *
// *     000  +00  0+0  00+  -00  0-0  00-                           *
// *     ++0  +-0  +0+  +0-  0++  0+-  --0  -+0  -0-  -0+  0--  0-+  *
// *     +++  ++-  +-+  +--  ---  --+  -+-  -++                      *
// *                                                                 *
// *******************************************************************

class BaryonOperator
{

 public:  

  class Quark
   {

    int spin;
    int displace_dir;

   public:

    Quark(int in_spin, int in_disp_dir) : spin(in_spin), displace_dir(in_disp_dir)
     {if ((spin<1)||(spin>4)||(displace_dir<-3)||(displace_dir>3)){
         QDPIO::cerr << "invalid BaryonOperator::Quark"<<endl;
         QDP_abort(1);}}

    Quark(const Quark& rhs) 
     : spin(rhs.spin), displace_dir(rhs.displace_dir) {}

    Quark& operator=(const Quark& rhs)
     {spin=rhs.spin; displace_dir=rhs.displace_dir; return *this;}

    int getSpin() const {return spin;}

    int getDisplacementDirection() const {return displace_dir;}

    bool operator<(const Quark& rhs) const
     {return  multiLessThan(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

    bool operator==(const Quark& rhs) const
     {return  multiEqual(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

    bool operator!=(const Quark& rhs) const
     {return  multiNotEqual(spin,rhs.spin,  displace_dir,rhs.displace_dir);}

   };


  class Diquark
   {

    Quark q1;
    Quark q2;

   public:

    Diquark(int spin1, int disp1, int spin2, int disp2) 
          : q1(spin1,disp1), q2(spin2,disp2) {}

    Diquark(const Quark& qin1, const Quark& qin2)
          : q1(qin1), q2(qin2) {}

    Diquark(const Diquark& rhs) 
     : q1(rhs.q1), q2(rhs.q2) {}

    Diquark& operator=(const Diquark& rhs)
     {q1=rhs.q1; q2=rhs.q2; return *this;}

    Quark getQuark1() const {return q1;}

    Quark getQuark2() const {return q2;}

    int getSpin1() const {return q1.getSpin();}

    int getSpin2() const {return q2.getSpin();}

    int getDisp1() const {return q1.getDisplacementDirection();}

    int getDisp2() const {return q2.getDisplacementDirection();}

    bool operator<(const Diquark& rhs) const
     {return multiLessThan(q1,rhs.q1,  q2,rhs.q2);}

    bool operator==(const Diquark& rhs) const
     {return multiEqual(q1,rhs.q1,  q2,rhs.q2);}

    bool operator!=(const Diquark& rhs) const
     {return multiNotEqual(q1,rhs.q1,  q2,rhs.q2);}

   };



  class Elemental
   {

    Diquark diq;
    Quark q3;

   public:

    Elemental(int spin1, int disp1, int spin2, int disp2, int spin3, int disp3) 
          : diq(Quark(spin1,disp1), Quark(spin2,disp2)), q3(spin3,disp3) {}

    Elemental(const Quark& qin1, const Quark& qin2, const Quark& qin3)
          : diq(qin1,qin2), q3(qin3) {}

    Elemental(const Diquark& dqin, const Quark& qin3)
          : diq(dqin), q3(qin3) {}

    Elemental(const Elemental& rhs) 
     : diq(rhs.diq), q3(rhs.q3) {}

    Elemental& operator=(const Elemental& rhs)
     {diq=rhs.diq; q3=rhs.q3; return *this;}

    Quark getQuark1() const {return diq.getQuark1();}

    Quark getQuark2() const {return diq.getQuark2();}

    Quark getQuark3() const {return q3;}

    Diquark getDiquark12() const {return diq;}

    int getSpin1() const {return diq.getSpin1();}

    int getSpin2() const {return diq.getSpin2();}

    int getSpin3() const {return q3.getSpin();}

    int getDisp1() const {return diq.getDisp1();}

    int getDisp2() const {return diq.getDisp2();}

    int getDisp3() const {return q3.getDisplacementDirection();}

    bool operator<(const Elemental& rhs) const
     {return multiLessThan(diq,rhs.diq,  q3,rhs.q3);}

    bool operator==(const Elemental& rhs) const
     {return multiEqual(diq,rhs.diq,  q3,rhs.q3);}

    bool operator!=(const Elemental& rhs) const
     {return multiNotEqual(diq,rhs.diq,  q3,rhs.q3);}

    std::string output() const
     {std::ostringstream oss;
      oss << " q1=("<<getSpin1()<<","<<getDisp1()<<")";
      oss << " q2=("<<getSpin2()<<","<<getDisp2()<<")";
      oss << " q3=("<<getSpin3()<<","<<getDisp3()<<")";
      return oss.str();}

   };

  struct ElementalTerm
   {
    Elemental el;
    DComplex coef;

    ElementalTerm(const Elemental& inel, const DComplex& incf)
      : el(inel), coef(incf) {}
    ElementalTerm(const ElementalTerm& in)
      : el(in.el), coef(in.coef) {}
    ElementalTerm& operator=(const ElementalTerm& in)
     {el=in.el; coef=in.coef; return *this;}
   };

  struct IndexCoef
   {
    int index;
    DComplex coef;

    IndexCoef(int ind, const DComplex& incf) : index(ind), coef(incf) {}
    IndexCoef(const IndexCoef& in) : index(in.index), coef(in.coef) {}
    IndexCoef& operator=(const IndexCoef& in)
     {index=in.index; coef=in.coef; return *this;}
   };

 private:

  static std::string coefsTopDirectory;
  static bool coefsFlag;

  BaryonOperatorInfo m_info;

  list<ElementalTerm> terms;

         // no copying ... like a handler
  BaryonOperator(const BaryonOperator& B);
  BaryonOperator& operator=(const BaryonOperator& B);


 public:

  static void initialize(const std::string& baryon_coefsTopDirectory);

  BaryonOperator(const BaryonOperatorInfo& bop_info);


    // output functions

  int getDisplacementLength() const { return m_info.getDisplacementLength(); }

  Momentum getMomentumValue() const { return m_info.getMomentum(); }

  const BaryonOperatorInfo& getInfo() const { return m_info; }

  int getNumberOfElementals() const { return terms.size(); }

  typedef list<ElementalTerm>::const_iterator ElementalIterator;

  ElementalIterator begin() const { return terms.begin(); }

  ElementalIterator end() const { return terms.end(); }

  std::string output() const;  // XML output including elementals


};



// **************************************************

  }
}
#endif  
