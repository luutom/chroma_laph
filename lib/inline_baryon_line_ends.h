#ifndef __INLINE_BARYON_LINE_ENDS_H__
#define __INLINE_BARYON_LINE_ENDS_H__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "quark_info.h"
#include "baryon_handler.h"
#include "laph_noise_info.h"

// ***********************************************************************
// *                                                                     *
// *  Driver inline measurement that constructs the baryon line ends     *
// *  (source/sinks).  Assumes that the smeared gauge field              *
// *  time slices and the Laph eigenvectors are available, and that the  *
// *  needed quark line ends are also available.  Must be run in 3D      *
// *  chroma_laph.  XML input must have the form:                        *
// *                                                                     *
// *  <chroma>                                                           *
// *   <RNG><Seed> ... </Seed></RNG>                                     *
// *   <Param>                                                           *
// *    <nrow>12 12 12</nrow>   # lattice size Nx Ny Nz                  *
// *    <InlineMeasurements>                                             *
// *    <elem>                                                           *
// *                                                                     *
// *     <Name> LAPH_BARYON_LINE_ENDS </Name>                            *
// *                                                                     *
// *     <BaryonCommonInfo>        ...   </BaryonCommonInfo>             *
// *     <BaryonOperators>         ...   </BaryonOperators>              *
// *     <BaryonLineCalculations>  ...   </BaryonLineCalculations>       *
// *     <JobName> ... </JobName>  (optional)                            *
// *                                                                     *
// *    </elem>                                                          *
// *    </InlineMeasurements>                                            *
// *   </Param>                                                          *
// *  </chroma>                                                          *
// *                                                                     *
// *  The details in the three major XML tags above are described below. *
// *                                                                     *
// *  (1) Major tag <BaryonCommonInfo>:                                  *
// *                                                                     *
// *     <GaugeConfigurationInfo> ... </GaugeConfigurationInfo>          *
// *     <GluonStoutSmearingInfo> ... </GluonStoutSmearingInfo>          *
// *     <BaryonFileList>   ...  </BaryonFileList>                       *
// *     <SmearedGaugeFileName> ... </SmearedGaugeFileName>              *
// *     <SmearedQuarkFileStub> ... </SmearedQuarkFileStub>              *
// *     <Quark1>  ...  </Quark1>                                        *
// *     <Quark2>  ...  </Quark2>                                        *
// *     <Quark3>  ...  </Quark3>                                        *
// *                                                                     *
// *  There is a <FileListInfo> under the <BaryonFileList> tag.          *
// *  Each <Quark*> tag must contain under it a <QuarkInfo> tag,         *
// *  BUT additionally, file list info must be given under the           *
// *  <FixedSource> and <RelativeSource> tags in the QuarkInfo's.        *
// *  For example:                                                       *
// *                                                                     *
// *     <QuarkInfo>                                                     *
// *        <QuarkActionInfo> ... </QuarkActionInfo>                     *
// *        <QuarkLaphSmearingInfo> ... </QuarkLaphSmearingInfo>         *
// *        <FixedSource>                                                *
// *           <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *           <FileListInfo>  ... </FileListInfo>   (additional tag)    *
// *        </FixedSource>                                               *
// *        <RelativeSource>                                             *
// *           <LaphDilutionScheme> ... </LaphDilutionScheme>            *
// *           <FileListInfo>  ... </FileListInfo>   (additional tag)    *
// *        </RelativeSource>                                            *
// *     </QuarkInfo>                                                    *
// *                                                                     *
// *  (2) Major tag <BaryonOperators>:                                   *
// *                                                                     *
// *        <!-- set of baryon operators must have same light/strange    *
// *             flavor content -->                                      *
// *                                                                     *
// *     <BaryonOperators>                                               *
// *       <ElementalCoefficientDirectory>                               *
// *           coefsTopDirectory                                         *
// *       </ElementalCoefficientDirectory>                              *
// *       <BaryonOp>                                                    *
// *         ...                                                         *
// *       </BaryonOp>                                                   *
// *       <BaryonOp>                                                    *
// *         ...                                                         *
// *       </BaryonOp>                                                   *
// *         ...                                                         *
// *     </BaryonOperators>                                              *
// *                                                                     *
// *  (3) Major tag <BaryonLineCalculations>                             *
// *                                                                     *
// *     <BaryonLineCalculations>                                        *
// *       <BaryonLineCalc>                                              *
// *         <Quark1Type>RelativeSource</Quark1Type>                     *
// *         <Quark2Type>SinkWithFixedSource</Quark2Type>                *
// *         <Quark3Type>SinkWithRelativeSource</Quark3Type>             *
// *         <Quark1Mode>Normal</Quark1Mode>                             *
// *         <Quark2Mode>Gamma5HermConj</Quark2Mode>                     *
// *         <Quark3Mode>Normal</Quark3Mode>                             *
// *         <MinTimeSeparation>0</MinTimeSeparation>  (default 0)       *
// *         <MaxTimeSeparation>24</MaxTimeSeparation>                   *
// *         <BaryonNoises> ... </BaryonNoises>                          *
// *       </BaryonLineCalc>                                             *
// *        ...                                                          *
// *     </BaryonLineCalculations>                                       *
// *                                                                     *
// *   If all quark types are "relative", or if one of the quark lines   *
// *   is a "FixedSource", then "MaxTimeSeparation" is set to zero.      *
// *   The <BaryonNoises> tag is specified by                            *
// *                                                                     *
// *    <BaryonNoises>                                                   *
// *       <BaryonNoise>                                                 *
// *         <QuarkLine1><LaphNoiseInfo>...</LaphNoiseInfo></QuarkLine1> *
// *         <QuarkLine2><LaphNoiseInfo>...</LaphNoiseInfo></QuarkLine2> *
// *         <QuarkLine3><LaphNoiseInfo>...</LaphNoiseInfo></QuarkLine3> *
// *       </BaryonNoise>                                                *
// *         ...                                                         *
// *    </BaryonNoises>                                                  *
// *                                                                     *
// *   Or you can just specify <All/> and the program will look for      *
// *   all noises in the quark sinks and do all permutations.            *
// *                                                                     *
// *                                                                     *
// *                                                                     *
// *   Alternative:  After several separate calculations output to       *
// *   different sets of files, one may wish to merge the output files   *
// *   into a single set.  This inline task also does this.  The input   *
// *   XML to accomplish this is as below:                               *
// *                                                                     *
// *     <Name> LAPH_BARYON_LINE_ENDS </Name>                            *
// *                                                                     *
// *     <BaryonCommonInfo>        ...   </BaryonCommonInfo>             *
// *     <MergeOutput>   filelist to merge   </MergeOutput>              *
// *                                                                     *
// *   This must be run in serial mode (non-parallel).                   *
// *                                                                     *
// *                                                                     *
// ***********************************************************************


namespace Chroma { 

#if (QDP_ND == 3)

  namespace InlineStochLaphBaryonEnv {

 // **************************************************************


extern const std::string name;
bool registerAll();

    /*! \ingroup inlinehadron */

class StochLaphBaryonInlineMeas : public AbsInlineMeasurement 
{

   XMLReader xml_rd;   // holds the XML input for this inline
                        // measurement, for use by the operator()
                        // member below


 public:

   StochLaphBaryonInlineMeas(XMLReader& xml_in, const std::string& path) 
                              : xml_rd(xml_in, path) {}

   ~StochLaphBaryonInlineMeas() {}
      
      //! Do the measurement
   void operator()(const unsigned long update_no, XMLWriter& xmlout); 

   unsigned long getFrequency() const {return 1;}

   
};
	

// ***********************************************************
  }
#endif
}

#endif
