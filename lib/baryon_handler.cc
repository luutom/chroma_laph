#include "baryon_handler.h"
#include "time_slices.h"
#include "multi_compare.h"

namespace Chroma {
  namespace LaphEnv {

 // Define the quantity below to reduce the memory used for the
 // quark sinks.  Causes some quantities to be recomputed as needed.

//#define QUARKMEMORYLITE

#if (QDP_ND == 3)

// **************************************************************


BaryonHandler::RecordKey::RecordKey() : time_value(0) {}

BaryonHandler::RecordKey::~RecordKey() {}

BaryonHandler::RecordKey::RecordKey(const LaphNoiseInfo& in_noise1, 
                                    const LaphNoiseInfo& in_noise2,
                                    const LaphNoiseInfo& in_noise3, 
                                    const QuarkLineEndInfo& in_qline1,
                                    const QuarkLineEndInfo& in_qline2,
                                    const QuarkLineEndInfo& in_qline3,
                                    int in_time)
  : noise1(in_noise1), noise2(in_noise2), noise3(in_noise3),
    qline1(in_qline1), qline2(in_qline2), qline3(in_qline3),
    time_value(in_time) {}

BaryonHandler::RecordKey::RecordKey(const BaryonNoiseInfo& in_bnoise, 
                                    const BaryonLineEndInfo& in_bline,
                                    int in_time)
  : noise1(in_bnoise.noise1), noise2(in_bnoise.noise2), noise3(in_bnoise.noise3),
    qline1(in_bline.qline1), qline2(in_bline.qline2), qline3(in_bline.qline3),
    time_value(in_time) {}

BaryonHandler::RecordKey::RecordKey(const BaryonHandler::RecordKey& in)
  : noise1(in.noise1), noise2(in.noise2), noise3(in.noise3),
    qline1(in.qline1), qline2(in.qline2), qline3(in.qline3),
    time_value(in.time_value) {}

BaryonHandler::RecordKey& BaryonHandler::RecordKey::operator=(
                             const BaryonHandler::RecordKey& in)
{
 noise1=in.noise1; noise2=in.noise2; noise3=in.noise3;
 qline1=in.qline1; qline2=in.qline2; qline3=in.qline3;
 time_value=in.time_value;
 return *this;
}


bool BaryonHandler::RecordKey::operator<(
            const BaryonHandler::RecordKey& rhs) const
{
 return multiLessThan(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3,
               qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);
}

bool BaryonHandler::RecordKey::operator==(
            const BaryonHandler::RecordKey& rhs) const
{
 return multiEqual(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3,
               qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);
}

bool BaryonHandler::RecordKey::operator!=(
            const BaryonHandler::RecordKey& rhs) const
{
 return multiNotEqual(time_value,rhs.time_value,
               noise1,rhs.noise1, noise2,rhs.noise2, noise3,rhs.noise3,
               qline1,rhs.qline1, qline2,rhs.qline2, qline3,rhs.qline3);
}


void BaryonHandler::RecordKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"RecordKey");
 push(xmlw,"QuarkLine1");
 noise1.output(xmlw);
 qline1.output(xmlw);
 pop(xmlw);
 push(xmlw,"QuarkLine2");
 noise2.output(xmlw);
 qline2.output(xmlw);
 pop(xmlw);
 push(xmlw,"QuarkLine3");
 noise3.output(xmlw);
 qline3.output(xmlw);
 pop(xmlw);
 write(xmlw,"TimeValue",time_value);
 pop(xmlw);
}


// *************************************************************************

BaryonHandler::CombinedKey::CombinedKey() {}

BaryonHandler::CombinedKey::~CombinedKey() {}

BaryonHandler::CombinedKey::CombinedKey(const BaryonOperatorInfo& in_bop,
                                        const RecordKey& in_rkey)
  : rkey(in_rkey), bop(in_bop) {}

BaryonHandler::CombinedKey::CombinedKey(const BaryonHandler::CombinedKey& in)
  : rkey(in.rkey), bop(in.bop)  {}

BaryonHandler::CombinedKey& BaryonHandler::CombinedKey::operator=(
                             const BaryonHandler::CombinedKey& in)
{
 bop=in.bop;
 rkey=in.rkey;
 return *this;
}


bool BaryonHandler::CombinedKey::operator<(
            const BaryonHandler::CombinedKey& rhs) const
{
 return multiLessThan(rkey,rhs.rkey, bop,rhs.bop);
}

bool BaryonHandler::CombinedKey::operator==(
            const BaryonHandler::CombinedKey& rhs) const
{
 return multiEqual(rkey,rhs.rkey, bop,rhs.bop);
}

bool BaryonHandler::CombinedKey::operator!=(
            const BaryonHandler::CombinedKey& rhs) const
{
 return multiNotEqual(rkey,rhs.rkey, bop,rhs.bop);
}

void BaryonHandler::CombinedKey::output(XmlWriter& xmlw) const
{
 push(xmlw,"CombinedKey");
 bop.output(xmlw);
 rkey.output(xmlw);
 pop(xmlw);
}


// *************************************************************************

const FileListInfo& BaryonHandler::QuarkFileLists::getFixedSchemeFiles(int quark) const
{
 if (quark==1) return q1fxdfiles;
 else if (quark==2) return q2fxdfiles;
 else if (quark==3) return q3fxdfiles;
 else{
    QDPIO::cerr << "invalid quark number in BaryonHandler::QuarkFileLists"<<endl;
    QDP_abort(1);}
 return q1fxdfiles;  // prevents compiler warnings
}

const FileListInfo& BaryonHandler::QuarkFileLists::getRelativeSchemeFiles(int quark) const
{
 if (quark==1) return q1relfiles;
 else if (quark==2) return q2relfiles;
 else if (quark==3) return q3relfiles;
 else{
    QDPIO::cerr << "invalid quark number in BaryonHandler::QuarkFileLists"<<endl;
    QDP_abort(1);}
 return q1relfiles; // prevents compiler warnings
}


// *************************************************************************

BaryonHandler::BaryonLineCalc::BaryonLineCalc(XmlReader& xmlin, int k)
{
 string tagname="BaryonLineCalc["+int_to_string(k)+"]";
 if (xml_tag_count(xmlin,tagname)!=1){
    QDPIO::cerr << "Bad Xml input to BaryonLineCalc"<<endl;
    xmlreadfail(xmlin,"BaryonLineCalc");}
 XmlReader xmlr(xmlin, "./descendant-or-self::"+tagname);
 max_time_sep=-1;
 unsigned int rcode;
 code=read_mode(xmlr,"Quark3Mode");
 rcode=read_type(xmlr,"Quark3Type"); code<<=2; code|=rcode;
 if (rcode==0) max_time_sep=0;  // fixed source
 rcode=read_mode(xmlr,"Quark2Mode"); code<<=1; code|=rcode;
 rcode=read_type(xmlr,"Quark2Type"); code<<=2; code|=rcode;
 if (rcode==0) max_time_sep=0;  // fixed source
 rcode=read_mode(xmlr,"Quark1Mode"); code<<=1; code|=rcode;
 rcode=read_type(xmlr,"Quark1Type"); code<<=2; code|=rcode;
 min_time_sep=0;  // default
 if (rcode==0) max_time_sep=0;  // fixed source
 if ((0x49u & code)==0x49u) max_time_sep=0; // all relative

   // if all lines are relative or one line is fixed+source,
   // max_time_sep is set to zero; otherwise, must be read
 if (max_time_sep<0){
   xmlreadif(xmlr,"MaxTimeSeparation",max_time_sep,
             "BaryonHandler::BaryonLineCalc");
   xmlreadif(xmlr,"MinTimeSeparation",min_time_sep,
             "BaryonHandler::BaryonLineCalc");}
 if (max_time_sep<0){
    QDPIO::cerr << "Invalid MaxTimeSeparation input to BaryonLineCalc"<<endl;
    xmlreadfail(xmlin,"BaryonLineCalc");}
 if ((min_time_sep<0)||(min_time_sep>max_time_sep)){
    QDPIO::cerr << "Invalid MinTimeSeparation input to BaryonLineCalc"<<endl;
    xmlreadfail(xmlin,"BaryonLineCalc");}

    //  read the baryon noises

 if (xml_tag_count(xmlr,"BaryonNoises")!=1){
    QDPIO::cerr << "Error in reading BaryonLineCalc: must have one <BaryonNoises> tag"<<endl;
    QDP_abort(1);}
 XmlReader xmln(xmlr,"./BaryonNoises");
 if (xml_tag_count(xmln,"All")!=1){
    int ncalc=xml_tag_count(xmln,"BaryonNoise");
    if (ncalc==0){
       QDPIO::cerr << "No baryon noises given and <All/> not specified; quitting"<<endl;
       QDP_abort(1);}
    for (int k=1;k<=ncalc;++k) 
       bnoises.insert(BaryonHandler::BaryonNoiseInfo(xmln,k));}
}


void BaryonHandler::BaryonLineCalc::output(XmlWriter& xmlout) const
{
 push(xmlout,"BaryonLineCalc");
 unsigned int wcode=code;
 write(xmlout,"Quark1Type",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"Quark1Mode",write_mode(0x1u & wcode)); wcode>>=1;
 write(xmlout,"Quark2Type",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"Quark2Mode",write_mode(0x1u & wcode)); wcode>>=1;
 write(xmlout,"Quark3Type",write_type(0x3u & wcode)); wcode>>=2;
 write(xmlout,"Quark3Mode",write_mode(0x1u & wcode));
 write(xmlout,"MinTimeSeparation",min_time_sep);
 write(xmlout,"MaxTimeSeparation",max_time_sep);
 push(xmlout,"BaryonNoises");
 if (bnoises.size()==0){ push(xmlout,"All"); pop(xmlout);}
 else{
    for (set<BaryonHandler::BaryonNoiseInfo>::const_iterator bnit=bnoises.begin();
       bnit!=bnoises.end();++bnit)
       bnit->output(xmlout);}
 pop(xmlout);
 pop(xmlout);
}

string BaryonHandler::BaryonLineCalc::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 unsigned int wcode=code;
 oss << pad << "<BaryonLineCalc>"<<endl;
 oss << pad << "  <Quark1Type>"<< write_type(0x3u & wcode)
     << "</Quark1Type>"<<endl; wcode>>=2;
 oss << pad << "  <Quark1Mode>"<< write_mode(0x1u & wcode)
     << "</Quark1Mode>"<<endl; wcode>>=1;
 oss << pad << "  <Quark2Type>"<< write_type(0x3u & wcode)
     << "</Quark2Type>"<<endl; wcode>>=2;
 oss << pad << "  <Quark2Mode>"<< write_mode(0x1u & wcode)
     << "</Quark2Mode>"<<endl; wcode>>=1;
 oss << pad << "  <Quark3Type>"<< write_type(0x3u & wcode)
     << "</Quark3Type>"<<endl; wcode>>=2;
 oss << pad << "  <Quark3Mode>"<< write_mode(0x1u & wcode)
     << "</Quark3Mode>"<<endl;
 oss << pad << "  <MaxTimeSeparation>"<<max_time_sep
            <<"</MaxTimeSeparation>"<<endl;
 oss << pad << "  <BaryonNoises>"<<endl;
 if (bnoises.size()==0) oss << pad <<"    <All/>"<<endl;
 else{
    for (set<BaryonHandler::BaryonNoiseInfo>::const_iterator bnit=bnoises.begin();
       bnit!=bnoises.end();++bnit)
       oss << bnit->output(indent+1)<<endl;}
 oss << pad << "  </BaryonNoises>"<<endl;
 oss << pad << "</BaryonLineCalc>"<<endl;
 return oss.str();
}

unsigned int BaryonHandler::BaryonLineCalc::read_type(
                   XmlReader& xmlr, const string& tag)
{
 string end_type;
 xmlread(xmlr,tag,end_type,"BaryonHandler::BaryonLineCalc");
 end_type=tidyString(end_type);
 int type=0;
 if (end_type=="FixedSource") type=0;
 else if (end_type=="RelativeSource") type=1;
 else if (end_type=="SinkWithFixedSource") type=2;
 else if (end_type=="SinkWithRelativeSource") type=3;
 else{
    QDPIO::cerr << "Bad Xml input to BaryonHandler::BaryonLineCalc"<<endl;
    QDPIO::cerr << "Invalid <Type> tag"<<endl;
    xmlreadfail(xmlr,"BaryonHandler::BaryonLineCalc");}
 return type;
}

string BaryonHandler::BaryonLineCalc::write_type(unsigned int end_type) const
{
 if (end_type==0) return "FixedSource";
 else if (end_type==1) return "RelativeSource";
 else if (end_type==2) return "SinkWithFixedSource";
 else return "SinkWithRelativeSource";
}

unsigned int BaryonHandler::BaryonLineCalc::read_mode(
                   XmlReader& xmlr, const string& tag)
{
 int mode=0;
 string modestr;
 if (xmlreadif(xmlr,tag,modestr,"BaryonHandler::BaryonLineCalc")){
    modestr=tidyString(modestr);
    if (modestr=="Normal") mode=0;
    else if (modestr=="Gamma5HermConj") mode=1;
    else{
       QDPIO::cerr << "Bad XML input to BaryonHandler::BaryonLineCalc"<<endl;
       QDPIO::cerr << "Invalid <Mode> tag"<<endl;
       xmlreadfail(xmlr,"BaryonHandler::BaryonLineCalc");}}
 return mode;
}

string BaryonHandler::BaryonLineCalc::write_mode(unsigned int mode) const
{
 if (mode==0) return "Normal";
 else return "Gamma5HermConj";
}

bool BaryonHandler::BaryonLineCalc::isSource(int quark) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+1;
 return ((vcode&0x1u)==0);
}

bool BaryonHandler::BaryonLineCalc::isNormalMode(int quark) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+2;
 return ((vcode&0x1u)==0);
}

bool BaryonHandler::BaryonLineCalc::isGamma5HermConjMode(int quark) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1))+2;
 return ((vcode&0x1u)!=0);
}

bool BaryonHandler::BaryonLineCalc::isFixedSourceScheme(int quark) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1));
 return ((vcode&0x1u)==0);
}

bool BaryonHandler::BaryonLineCalc::isRelativeSource(int quark) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int vcode=code;
 vcode>>=(3*(quark-1));
 return ((vcode&0x3u)==1);
}

   //  must be same mode, same dilution scheme to share quark handler

bool BaryonHandler::BaryonLineCalc::canShareQuarkHandler(
                             int quarkA, int quarkB) const
{
 if ((quarkA<1)||(quarkA>3)||(quarkB<1)||(quarkB>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 if (quarkA==quarkB) return true;
 unsigned int modeA=code;
 modeA>>=(3*(quarkA-1))+2;
 modeA&=0x1u;
 unsigned int modeB=code;
 modeB>>=(3*(quarkB-1))+2;
 modeB&=0x1u;
 if (modeA!=modeB) return false;
 unsigned int schemeA=code;
 schemeA>>=(3*(quarkA-1));
 schemeA&=0x1u;
 unsigned int schemeB=code;
 schemeB>>=(3*(quarkB-1));
 schemeB&=0x1u;
 return (schemeA==schemeB);
}

QuarkLineEndInfo BaryonHandler::BaryonLineCalc::getLineInfo(
                      int quark, int t0, int lat_textent) const
{
 if ((quark<1)||(quark>3)){
    QDPIO::cerr << "invalid quark number in baryon"<<endl;
    QDP_abort(1);}
 unsigned int qmode=code;
 qmode>>=(3*(quark-1))+2;
 qmode&=0x1u; 
 unsigned int qtype=code;
 qtype>>=(3*(quark-1));
 qtype&=0x3u;
 int source_index= (qtype==2)? t0 : 0; 
 return QuarkLineEndInfo(qtype,qmode,source_index,lat_textent);   
}

set<BaryonHandler::BaryonNoiseInfo> BaryonHandler::BaryonLineCalc::getAvailableNoises(
                               const set<LaphNoiseInfo>& Quark1Noises,
                               const set<LaphNoiseInfo>& Quark2Noises,
                               const set<LaphNoiseInfo>& Quark3Noises) const
{
 set<BaryonNoiseInfo> result;
 if (bnoises.size()==0){
    for (set<LaphNoiseInfo>::const_iterator it1=Quark1Noises.begin();
         it1!=Quark1Noises.end();it1++)
    for (set<LaphNoiseInfo>::const_iterator it2=Quark2Noises.begin();
         it2!=Quark2Noises.end();it2++)
    for (set<LaphNoiseInfo>::const_iterator it3=Quark3Noises.begin();
         it3!=Quark3Noises.end();it3++)
       if (((*it1)!=(*it2))&&((*it1)!=(*it3))&&((*it2)!=(*it3)))
          result.insert(BaryonHandler::BaryonNoiseInfo(*it1,*it2,*it3));}
 else{
    for (set<BaryonNoiseInfo>::const_iterator it=bnoises.begin();
         it!=bnoises.end();it++)
       if   (((Quark1Noises.find(it->noise1)!=Quark1Noises.end())||(isSource(1)))
          && ((Quark2Noises.find(it->noise2)!=Quark2Noises.end())||(isSource(2)))
          && ((Quark3Noises.find(it->noise3)!=Quark3Noises.end())||(isSource(3)))
          && (it->noise1 != it->noise2) && (it->noise1 != it->noise3)
          && (it->noise2 != it->noise3))
          result.insert(*it);}
 return result;
}

// *************************************************************************


void BaryonHandler::BaryonNoiseInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"BaryonNoiseInfo");
 push(xmlout,"QuarkLine1");
 noise1.output(xmlout);
 pop(xmlout);
 push(xmlout,"QuarkLine2");
 noise2.output(xmlout);
 pop(xmlout);
 push(xmlout,"QuarkLine3");
 noise3.output(xmlout);
 pop(xmlout);
 pop(xmlout);
}

string BaryonHandler::BaryonNoiseInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<BaryonNoiseInfo>"<<endl;
 oss << pad << "  <QuarkLine1>"<<endl;
 oss << noise1.output(indent+1);
 oss << pad << "  </QuarkLine1>"<<endl;
 oss << pad << "  <QuarkLine2>"<<endl;
 oss << noise2.output(indent+1);
 oss << pad << "  </QuarkLine2>"<<endl;
 oss << pad << "  <QuarkLine3>"<<endl;
 oss << noise3.output(indent+1);
 oss << pad << "  </QuarkLine3>"<<endl;
 oss << pad << "</BaryonNoiseInfo>"<<endl;
 return oss.str();
}

// *************************************************************************

void BaryonHandler::QuarkFileLists::output(XmlWriter& xmlout) const
{
 push(xmlout,"QuarkFileLists");
 push(xmlout,"Quark1");
 push(xmlout,"FixedSource");
 q1fxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 q1relfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 push(xmlout,"Quark2");
 push(xmlout,"FixedSource");
 q2fxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 q2relfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 push(xmlout,"Quark3");
 push(xmlout,"FixedSource");
 q3fxdfiles.output(xmlout);
 pop(xmlout);
 push(xmlout,"RelativeSource");
 q3relfiles.output(xmlout);
 pop(xmlout);
 pop(xmlout);
 pop(xmlout);
}

string BaryonHandler::QuarkFileLists::output() const
{
 XmlBufferWriter xmlout;
 output(xmlout);
 return xmlout.str();
}


// *************************************************************************


BaryonHandler::BaryonHandler()
        :  uPtr(0), gSmearPtr(0), quark1Ptr(0), quark2Ptr(0), quark3Ptr(0), 
           fPtr(0), qHandler1(0), qHandler2(0), qHandler3(0), DHputPtr(0), DHgetPtr(0), 
           DHSFputPtr(0), DHSFgetPtr(0), m_read_mode(true) {}

   
BaryonHandler::BaryonHandler(const GaugeConfigurationInfo& gaugeinfo,
                             const GluonSmearingInfo& gluonsmear,
                             const QuarkInfo& quark1, 
                             const QuarkInfo& quark2,
                             const QuarkInfo& quark3,
                             const FileListInfo& flist,
                             bool read_mode)
        :  uPtr(0), gSmearPtr(0), quark1Ptr(0), quark2Ptr(0), quark3Ptr(0), 
           fPtr(0), qHandler1(0), qHandler2(0), qHandler3(0), 
           DHputPtr(0), DHgetPtr(0), DHSFputPtr(0), DHSFgetPtr(0)
{
 set_info(gaugeinfo,gluonsmear,quark1,quark2,quark3,flist,read_mode);
}


void BaryonHandler::setInfo(const GaugeConfigurationInfo& gaugeinfo,
                            const GluonSmearingInfo& gluonsmear,
                            const QuarkInfo& quark1, 
                            const QuarkInfo& quark2,
                            const QuarkInfo& quark3,
                            const FileListInfo& flist,
                            bool read_mode)
{
 clear();
 set_info(gaugeinfo,gluonsmear,quark1,quark2,quark3,flist,read_mode);
}


void BaryonHandler::set_info(const GaugeConfigurationInfo& gaugeinfo,
                             const GluonSmearingInfo& gluonsmear,
                             const QuarkInfo& quark1, 
                             const QuarkInfo& quark2,
                             const QuarkInfo& quark3,
                             const FileListInfo& flist,
                             bool read_mode)
{
 m_read_mode=read_mode;
 try{
    uPtr = new GaugeConfigurationInfo(gaugeinfo);
    gSmearPtr = new GluonSmearingInfo(gluonsmear);
    quark1Ptr = new QuarkInfo(quark1);
    quark2Ptr = new QuarkInfo(quark2);
    quark3Ptr = new QuarkInfo(quark3);
    fPtr = new FileListInfo(flist);   
    if (m_read_mode)
       DHgetPtr = new DataGetHandlerMF<BaryonHandler,FileKey,RecordKey,
                          DataType>(*this,flist,"Laph--BaryonFile",
                          "BaryonHandlerDataFile");}
 catch(...){
    QDPIO::cerr << "allocation problem in BaryonHandler"<<endl;
    clear();
    QDP_abort(1);}
}



void BaryonHandler::connectQuarkHandlers(const BaryonLineCalc& Bcalc,
                                         const QuarkFileLists& Qfiles,
                                         const string& smeared_gauge_filename,
                                         const string& smeared_quark_filestub)
{
 check_info_set("connectQuarkHandlers",2);
 disconnectQuarkHandlers();
 try{

         // connect to quark handler for first quark

    const DilutionSchemeInfo& dil1=(Bcalc.isFixedSourceScheme(1))?
             quark1Ptr->getFixedSourceDilutionScheme() :
             quark1Ptr->getRelativeSourceDilutionScheme();
    const FileListInfo& qfiles1=(Bcalc.isFixedSourceScheme(1))?
             Qfiles.getFixedSchemeFiles(1) :
             Qfiles.getRelativeSchemeFiles(1);
    qHandler1 = new QuarkHandler(*uPtr,*gSmearPtr,quark1Ptr->getSmearing(),
                                 dil1,quark1Ptr->getAction(),qfiles1,
                                 smeared_quark_filestub,
                                 smeared_gauge_filename);
    if (Bcalc.isGamma5HermConjMode(1)) 
       qHandler1->setGamma5HermiticityMode();

         // connect to quark handler for second quark;
         // if line 2 has the same dilution scheme (rel or fxd), same mode,
         // and same quark flavor/action as line 1, then lines 1 and 2
         // can share the same handler (the quark file infos can be used
         // to determine if the same flavor/action)

    const FileListInfo& qfiles2=(Bcalc.isFixedSourceScheme(2))?
             Qfiles.getFixedSchemeFiles(2) :
             Qfiles.getRelativeSchemeFiles(2);
    if ((Bcalc.canShareQuarkHandler(1,2))&&(qfiles2==qfiles1)
        &&((*quark2Ptr)==(*quark1Ptr)))
       qHandler2=qHandler1;
    else{
       const DilutionSchemeInfo& dil2=(Bcalc.isFixedSourceScheme(2))?
                quark2Ptr->getFixedSourceDilutionScheme() :
                quark2Ptr->getRelativeSourceDilutionScheme();
       qHandler2 = new QuarkHandler(*uPtr,*gSmearPtr,quark2Ptr->getSmearing(),
                                    dil2,quark2Ptr->getAction(),qfiles2,
                                    smeared_quark_filestub,
                                    smeared_gauge_filename);
       if (Bcalc.isGamma5HermConjMode(2)) 
          qHandler2->setGamma5HermiticityMode(); }

         // connect to quark handler for third quark

    const FileListInfo& qfiles3=(Bcalc.isFixedSourceScheme(3))?
             Qfiles.getFixedSchemeFiles(3) :
             Qfiles.getRelativeSchemeFiles(3);
    if ((Bcalc.canShareQuarkHandler(1,3))&&(qfiles3==qfiles1)
        &&((*quark3Ptr)==(*quark1Ptr)))
       qHandler3=qHandler1;
    else if ((Bcalc.canShareQuarkHandler(2,3))&&(qfiles3==qfiles2)
          &&((*quark3Ptr)==(*quark2Ptr)))
       qHandler3=qHandler2;
    else{
       const DilutionSchemeInfo& dil3=(Bcalc.isFixedSourceScheme(3))?
                quark3Ptr->getFixedSourceDilutionScheme() :
                quark3Ptr->getRelativeSourceDilutionScheme();
       qHandler3 = new QuarkHandler(*uPtr,*gSmearPtr,quark3Ptr->getSmearing(),
                                    dil3,quark3Ptr->getAction(),qfiles3,
                                    smeared_quark_filestub,
                                    smeared_gauge_filename);
       if (Bcalc.isGamma5HermConjMode(3)) 
          qHandler3->setGamma5HermiticityMode();}

    }
 catch(...){
    QDPIO::cerr << "allocation problem in BaryonHandler::connectQuarkHandlers"<<endl;
    QDP_abort(1);}
}



void BaryonHandler::disconnectQuarkHandlers()
{
 if ((qHandler1==qHandler2)&&(qHandler2==qHandler3)) delete qHandler1;
 else if (qHandler1==qHandler2){ delete qHandler1; delete qHandler3;}
 else if (qHandler1==qHandler3){ delete qHandler1; delete qHandler2;}
 else if (qHandler2==qHandler3){ delete qHandler1; delete qHandler2;}
 else{ delete qHandler1; delete qHandler2; delete qHandler3;}
 qHandler1=0; qHandler2=0; qHandler3=0;
}



BaryonHandler::~BaryonHandler()
{
 clear();
}

void BaryonHandler::clear()
{
 try{
    delete uPtr;
    delete gSmearPtr;
    delete quark1Ptr;
    delete quark2Ptr;
    delete quark3Ptr;
    delete fPtr;
    delete qHandler1;
    delete qHandler2;
    delete qHandler3;
    delete DHputPtr;
    delete DHgetPtr;
    delete DHSFputPtr;
    delete DHSFgetPtr;}
 catch(...){ QDP_abort(1);}
 uPtr=0;
 gSmearPtr=0;
 quark1Ptr=0;
 quark2Ptr=0;
 quark3Ptr=0;
 fPtr=0;
 qHandler1=0;
 qHandler2=0;
 qHandler3=0;
 DHputPtr=0;
 DHgetPtr=0;
 DHSFputPtr=0;
 DHSFgetPtr=0;
}




bool BaryonHandler::isInfoSet() const
{
 return ((uPtr!=0)&&(gSmearPtr!=0)&&(fPtr!=0)
        &&(quark1Ptr!=0)&&(quark2Ptr!=0)
        &&(quark3Ptr!=0));
}


const GaugeConfigurationInfo& BaryonHandler::getGaugeConfigurationInfo() const 
{
 check_info_set("getGaugeConfigurationInfo");
 return *uPtr;
}

const GluonSmearingInfo& BaryonHandler::getGluonSmearingInfo() const
{
 check_info_set("getFieldSmearingInfo");
 return *gSmearPtr;
}

const QuarkInfo& BaryonHandler::getQuarkInfo(int quark_number) const 
{
 check_info_set("getQuarkInfo");
 if ((quark_number<1)||(quark_number>3)){
    QDPIO::cerr << "invalid quark number in BaryonHandler::getQuarkInfo"<<endl
                << "must have value 1,2,3"<<endl;
    QDP_abort(1);}
 if (quark_number==1) return *quark1Ptr;
 else if (quark_number==2) return *quark2Ptr;
 else return *quark3Ptr;
}

const FileListInfo& BaryonHandler::getFileListInfo() const 
{
 check_info_set("getFileListInfo");
 return *fPtr;
}

void BaryonHandler::getFileMap(XmlWriter& xmlout) const
{
 if ((isInfoSet())&&(m_read_mode))
    DHgetPtr->getFileMap(xmlout);
}

void BaryonHandler::outputKeys(XmlWriter& xmlout)
{
 if ((isInfoSet())&&(m_read_mode)) 
    DHgetPtr->outputKeys(xmlout);
}

   // ***************************************************************
   
        //  Compute line ends for all baryon operators in "Bops" and
        //  all dilution indices.  Repeat for all requested baryon calculations
        //  given in "bcalcs" (which includes all baryon noises to compute).
        //  If the list of baryon noises is empty, all available noises 
        //  in the quark sink files are used.


void BaryonHandler::compute(
                      const set<BaryonOperatorInfo>& BopInfos,
                      const string& CoefsTopDirectory,
                      const QuarkFileLists& Qfiles,
                      const set<BaryonLineCalc>& bcalcs,
                      const string& smeared_gauge_filename,
                      const string& smeared_quark_filestub)
{

   // check info is set and handler is NOT in read mode
 check_info_set("compute",2);
   // set so smeared gauge field and Laph eigenvectors kept in memory
// QuarkHandler::setSubHandlersKeepInMemory();

 QDPIO::cout <<endl<<endl;
 QDPIO::cout << "********************************************************"<<endl;
 QDPIO::cout << endl<<"  BaryonHandler::compute commencing..."<<endl<<endl;

 START_CODE();
 double btimer=0.0,qtimer=0.0,otimer=0.0;
 StopWatch rolex,outer;
 outer.start();
 rolex.start();

     // *******************************************************
     // *                                                     *
     // *  set up structures related to the baryon operators  *
     // *                                                     *
     // *******************************************************

 int nbaryon_ops=BopInfos.size();
 if (nbaryon_ops==0){
    QDPIO::cerr << "No baryon operators in BaryonHandler::computeSourcesSinks"<<endl;
    return;}
 SetIndexer<BaryonOperatorInfo> Bops(BopInfos);

   // Check that all baryon operators have the same light-strange flavor

 string flavor=Bops[0].getFlavorLS();
 for (int i=1;i<nbaryon_ops;i++){
    if (Bops[i].getFlavorLS()!=flavor){
       QDPIO::cerr << "All baryon operators must have same flavor content"<<endl;
       return;}}

   // Check that all baryon operators have the same displacement length if nonzero

 int displacement_length=0;
 for (int i=0;i<nbaryon_ops;i++){
    int dl=Bops[i].getDisplacementLength();
    if (displacement_length==0) displacement_length=dl;
    if ((dl!=0)&&(dl!=displacement_length)){
       QDPIO::cerr << "All baryon operators must have the same displacement"
                   << " length if nonzero"<<endl;
       return;}}

    // Create the operators by reading the elemental coefficients

 QDPIO::cout << "Reading the baryon operator elemental terms"<<endl;
 BaryonOperator::initialize(tidyString(CoefsTopDirectory));
 vector<BaryonOperator*> bop(nbaryon_ops);
 for (int k=0;k<nbaryon_ops;k++) bop[k]=0;  // start with null pointers
 try{
    for (int k=0;k<nbaryon_ops;k++)
       bop[k]=new BaryonOperator(Bops[k]);}
 catch(const string& err){
    QDPIO::cerr << "Error in BaryonHandler::compute"<<endl;
    QDPIO::cerr << err <<endl;
    for (int k=0;k<nbaryon_ops;k++) delete bop[k];
    return;}

     //  Output baryon operators and build momenta set

 map<Momentum,int> mominds;
 int momcount=0;
 for (int k=0;k<nbaryon_ops;k++){
    QDPIO::cout << "Baryon operator "<<k<<":"<<endl;
    QDPIO::cout << Bops[k].output()<<endl;
    if (mominds.find(bop[k]->getMomentumValue())==mominds.end())
       mominds.insert(make_pair(bop[k]->getMomentumValue(),momcount++));}

     //  Make and store the momenta phases: our annihilation operators
     //  need exp(-I p.x) 

 multi1d<LatticeComplex> phases(mominds.size());
 for (map<Momentum,int>::const_iterator mit=mominds.begin();
      mit!=mominds.end();mit++){
    phases[mit->second]=makeMomPhaseField(mit->first);}

    // The next step is to construct the "computations" map.
    // A given "computation" is a site color contraction, combined
    // with a summation over all spatial sites, for various momenta.
    // We will then loop over all such "computations", adding the
    // results to the pertinent baryon operators as specified in
    // the computations map.

 typedef vector<BaryonOperator::IndexCoef>  ICList;
 typedef map<int, ICList> MomMap;
 typedef map<BaryonOperator::Quark, MomMap> Q3Map;
 typedef map<BaryonOperator::Diquark, Q3Map> CSMap;

 CSMap diquarks;    // the crucial map structure
 bool comflag=true;   // for synchronizing nodes

   //  Divide "nbaryon_ops" baryon operators onto "nodes" nodes.
   //      x processors get m baryons,  nodes-x get  m-1 baryons.
   //         m = nbaryon_ops/nodes + 1    x = nbaryon_ops % nodes

 int nbaryon_ops_thisnode;
 int opindex_start;

 {int nodes=Layout::numNodes();
 int thisnode=Layout::nodeNumber();
 int m = nbaryon_ops/nodes + 1;
 int x = nbaryon_ops % nodes;
 if (thisnode<x){
    nbaryon_ops_thisnode=m;
    opindex_start=m*thisnode;}
 else{
    nbaryon_ops_thisnode=m-1;
    opindex_start=m*x+(m-1)*(thisnode-x);}}
 int opindex_last=opindex_start+nbaryon_ops_thisnode-1;


 for (int i=0;i<nbaryon_ops;i++){

    int momind=mominds[bop[i]->getMomentumValue()];

    for (BaryonOperator::ElementalIterator it=bop[i]->begin();
                                           it!=bop[i]->end();it++){

       Q3Map& q3map=diquarks[it->el.getDiquark12()];
       MomMap& mommap=q3map[it->el.getQuark3()];
       ICList& indcflist=mommap[momind];
       if ((i>=opindex_start)&&(i<=opindex_last))
          indcflist.push_back(BaryonOperator::IndexCoef(i-opindex_start,it->coef));}}

 for (int k=0;k<nbaryon_ops;k++) delete bop[k];  // don't need these anymore

    //  set up the files for output (local to each node)

 if (nbaryon_ops_thisnode>0){
  try{
    int suffix=fPtr->getMinFileNumber()+Layout::nodeNumber();
    FileListInfo finfo_thisnode(fPtr->getFileStub(),suffix,suffix,
                                fPtr->isModeOverwrite());
    string fname=finfo_thisnode.getFileName(suffix);
    DHSFputPtr = new DataPutHandlerSF<BaryonHandler,CombinedKey,DataType>(
            *this,fname,"Laph--BaryonFile",fPtr->isModeOverwrite(),false);
    }
  catch(...){
    std::cerr << "problem setting up output files in BaryonHandler"<<endl;
    QDP_abort(1);}}

 CSMap::iterator dqit;
 Q3Map::iterator q3it;
 MomMap::iterator mmit;

 {vector<int> num_elementals(mominds.size(),0);
 int diq_q3_count=0;
 for (dqit=diquarks.begin();dqit!=diquarks.end();dqit++){
    diq_q3_count+=(dqit->second).size();
    for (q3it=(dqit->second).begin();q3it!=(dqit->second).end();q3it++)
       for (mmit=(q3it->second).begin();mmit!=(q3it->second).end();mmit++)
          ++num_elementals[mmit->first];}
 QDPIO::cout << endl;
 QDPIO::cout << "              Number of baryon operators = "<<nbaryon_ops<<endl;
 QDPIO::cout << "    Displacement length of all operators = "<<displacement_length<<endl;
 QDPIO::cout << "     Number of q1 x q2 diquark operators = "<<diquarks.size()<<endl;
 QDPIO::cout << "    Number of diquark-quark contractions = "<<diq_q3_count<<endl;
 QDPIO::cout << "             Number of different momenta = "<<mominds.size()<<endl<<endl;
 for (unsigned int k=0;k<mominds.size();k++)
    QDPIO::cout << " Number of elementals for momentum["<<k<<"] = "
                <<num_elementals[k]<<endl;}

 rolex.stop();
 otimer+=rolex.getTimeInSeconds();

     // **********************************************************
     // *                                                        *
     // *    loop over the requested baryon line calculations    *
     // *                                                        *
     // **********************************************************

 for (set<BaryonLineCalc>::const_iterator 
      bcit=bcalcs.begin();bcit!=bcalcs.end();++bcit){

  rolex.start();
  QDPIO::cout << endl<<endl;
  QDPIO::cout <<" ************************************************"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" *    Starting new baryon line calculations:    *"<<endl;
  QDPIO::cout <<" *                                              *"<<endl;
  QDPIO::cout <<" ************************************************"<<endl<<endl;
  QDPIO::cout << bcit->output() << endl<<endl;

   // check for valid baryon operator: all psi fields or all psi-bar fields

  bool sourceflag1=bcit->isSource(1);
  bool sourceflag2=bcit->isSource(2);
  bool sourceflag3=bcit->isSource(3);

  int charge1=(sourceflag1)?1:-1;
  if (bcit->isGamma5HermConjMode(1)) charge1=-charge1;
  int charge2=(sourceflag2)?1:-1;
  if (bcit->isGamma5HermConjMode(2)) charge2=-charge2;
  int charge3=(sourceflag3)?1:-1;
  if (bcit->isGamma5HermConjMode(3)) charge3=-charge3;
  if ((charge1!=charge2)||(charge2!=charge3)){
     QDPIO::cerr << "Invalid baryon field; skipping this computation"<<endl;
     continue;}

   // fire up the needed sub-handlers

  connectQuarkHandlers(*bcit,Qfiles,smeared_gauge_filename,
                       smeared_quark_filestub);
  QDPIO::cout << "connection to QuarkHandlers established"<<endl;

  int ndilproj1=qHandler1->getNumberOfSpinEigvecDilutionProjectors();
  int ndilproj2=qHandler2->getNumberOfSpinEigvecDilutionProjectors();
  int ndilproj3=qHandler3->getNumberOfSpinEigvecDilutionProjectors();
  QDPIO::cout << "Number of spin-eigenvector dilution indices:"<<endl;
  QDPIO::cout << "   Quark 1: "<<ndilproj1<<endl;
  QDPIO::cout << "   Quark 2: "<<ndilproj2<<endl;
  QDPIO::cout << "   Quark 3: "<<ndilproj3<<endl<<endl;

  rolex.stop();
  otimer+=rolex.getTimeInSeconds();

     // get the noises and source times available for each
     // quark line (use all times for RelativeSource); then take
     // intersection of the source times; for noises, take 
     // intersections with requested noises (if any)

  int Textent=uPtr->getTimeExtent();
  set<int> start_times;
  set<BaryonNoiseInfo> bnoise_set;   

  {set<LaphNoiseInfo> noise1set,noise2set,noise3set;
   set<int> tsrc1set,tsrc2set,tsrc3set;
   qHandler1->getNoisesAndSourceTimes(noise1set,tsrc1set);
   qHandler2->getNoisesAndSourceTimes(noise2set,tsrc2set);
   qHandler3->getNoisesAndSourceTimes(noise3set,tsrc3set);

  // hadron noises to compute
   bnoise_set=bcit->getAvailableNoises(noise1set,noise2set,noise3set);

   if (bcit->isRelativeSource(1)){
      for (int t=0;t<Textent;t++) tsrc1set.insert(t);}
   if (bcit->isRelativeSource(2)){
      for (int t=0;t<Textent;t++) tsrc2set.insert(t);}
   if (bcit->isRelativeSource(3)){
      for (int t=0;t<Textent;t++) tsrc3set.insert(t);}
   set<int> tmp;
   set_intersection(tsrc1set.begin(),tsrc1set.end(),
                    tsrc2set.begin(),tsrc2set.end(),
                    inserter(tmp,tmp.begin()));
   set_intersection(tmp.begin(),tmp.end(),
                    tsrc3set.begin(),tsrc3set.end(),
                    inserter(start_times,start_times.begin()));}

   // loop over source times and time separations

  for (set<int>::const_iterator t0=start_times.begin();
       t0!=start_times.end();++t0)
  for (int dt=bcit->getMinTimeSeparation();
           dt<=bcit->getMaxTimeSeparation();++dt){

   int timeval=(*t0)+dt;
   if (timeval>=Textent) timeval-=Textent;

   QuarkLineEndInfo qline1(bcit->getLineInfo(1,*t0,Textent));
   QuarkLineEndInfo qline2(bcit->getLineInfo(2,*t0,Textent));
   QuarkLineEndInfo qline3(bcit->getLineInfo(3,*t0,Textent));

   int tsource1=qline1.getSourceTime(timeval,*uPtr);
   int tsource2=qline2.getSourceTime(timeval,*uPtr);
   int tsource3=qline3.getSourceTime(timeval,*uPtr);
 
   int tproj1=qHandler1->getTimeDilutionProjectorIndex(tsource1);
   int tproj2=qHandler2->getTimeDilutionProjectorIndex(tsource2);
   int tproj3=qHandler3->getTimeDilutionProjectorIndex(tsource3);

   // loop over baryon noises

   for (set<BaryonNoiseInfo>::const_iterator 
        bnit=bnoise_set.begin();bnit!=bnoise_set.end();bnit++){

       // check that all the needed quark sources/sinks are available
       // before beginning the diquarks

     bool flag=true;
     for (int dil1=0;dil1<ndilproj1;dil1++)
        flag = flag && qHandler1->queryData(sourceflag1,bnit->noise1,tproj1,dil1);
     for (int dil2=0;dil2<ndilproj2;dil2++)
        flag = flag && qHandler2->queryData(sourceflag2,bnit->noise2,tproj2,dil2);
     for (int dil3=0;dil3<ndilproj3;dil3++)
        flag = flag && qHandler3->queryData(sourceflag3,bnit->noise3,tproj3,dil3);

     if (!flag){   // not all needed quark sinks available so skip
//       QDPIO::cerr << "Not all needed quark sources/sinks are available"
//                   << " for requested BaryonSourceSink construction"<<endl;
//       QDPIO::cerr << "Skipping this computation"<<endl;
       continue;}

     QDPIO::cout << endl<<endl;
     QDPIO::cout << "Starting computation for time = "<<timeval<<endl;
     QDPIO::cout << "                      Quark line source times: "<<tsource1
                 <<" "<<tsource2<< " " <<tsource3<<endl;
     QDPIO::cout << "   Quark line time dilution projector indices: "<<tproj1
                 <<" "<<tproj2<<" "<<tproj3<<endl;
     QDPIO::cout << " Quark line noises: "<<bnit->noise1.getSeed()
                 <<" "<<bnit->noise2.getSeed()<<" "<<bnit->noise3.getSeed()
                 <<endl<<endl;
     
      // Now carry out the computations.  Each computation puts its results
      // in "results_add", but these are then added into "bresults" which
      // contains the baryon operator source and sink functions by the
      // end of the computations.

     rolex.start();
     auto_ptr<LatticeColorVector> diquark;
     auto_ptr<LatticeComplex> singlet;
     multi1d<Complex> results_add;
     vector< multi3d<Complex> > bresults(nbaryon_ops_thisnode);
     for (int k=0;k<nbaryon_ops_thisnode;k++){
        bresults[k].resize(ndilproj1,ndilproj2,ndilproj3);
        bresults[k]=zero;}

     rolex.stop();
     otimer+=rolex.getTimeInSeconds();

     double intimer=0.0;
     StopWatch seiko,bulova;

     for (int dil1=0;dil1<ndilproj1;dil1++){
     seiko.start();
     double qqtimer=0.0, diqtimer=0.0, cctimer=0.0, 
            sstimer=0.0, cftimer=0.0;
     for (int dil2=0;dil2<ndilproj2;dil2++){

       for (dqit=diquarks.begin();dqit!=diquarks.end();dqit++){

        rolex.start();
        const LatticeColorVector* q1=qHandler1->getData(
                 sourceflag1,bnit->noise1,tproj1,dil1,
                 (dqit->first).getSpin1(),DirPath((dqit->first).getDisp1()),
                 displacement_length,timeval);

        rolex.stop();
        qtimer+=rolex.getTimeInSeconds();
        qqtimer+=rolex.getTimeInSeconds();
        if (q1==0) continue;
        rolex.start();
        const LatticeColorVector* q2=qHandler2->getData(
                 sourceflag2,bnit->noise2,tproj2,dil2,
                 (dqit->first).getSpin2(),DirPath((dqit->first).getDisp2()),
                 displacement_length,timeval);

        rolex.stop();
        qtimer+=rolex.getTimeInSeconds();
        qqtimer+=rolex.getTimeInSeconds();
        if (q2==0) continue;

        rolex.start();
        evaluateColorCrossProduct(q1,q2,diquark);
        rolex.stop();
        btimer+=rolex.getTimeInSeconds();
        diqtimer+=rolex.getTimeInSeconds();

        for (int dil3=0;dil3<ndilproj3;dil3++){

          for (q3it=dqit->second.begin();q3it!=dqit->second.end();q3it++){

            rolex.start();
            const LatticeColorVector* q3=qHandler3->getData(
                         sourceflag3,bnit->noise3,tproj3,dil3,
                         (q3it->first).getSpin(),
                         DirPath((q3it->first).getDisplacementDirection()),
                         displacement_length,timeval);

            rolex.stop();
            qtimer+=rolex.getTimeInSeconds();
            qqtimer+=rolex.getTimeInSeconds();
            if (q3==0) continue;
            rolex.start();
            evaluateColorVectorContract(diquark,q3,singlet);    // color contractions
            rolex.stop();
            btimer+=rolex.getTimeInSeconds();
            cctimer+=rolex.getTimeInSeconds();

            rolex.start();
            bulova.start();
            vector<LatticeComplex*> phptrs(q3it->second.size());
            int phcount=0;
            for (mmit=(q3it->second).begin();mmit!=(q3it->second).end();mmit++){
              int momind=mmit->first;
              phptrs[phcount++]=&(phases[momind]);}
            evaluateMomentumSums(phptrs,singlet.get(),results_add);
            bulova.stop();
            sstimer+=bulova.getTimeInSeconds();

            bulova.start();
            phcount=0;
            for (mmit=(q3it->second).begin();mmit!=(q3it->second).end();phcount++,mmit++){
              const ICList& icit=mmit->second;
//#pragma omp parallel for
              for (unsigned int ic=0;ic<icit.size();ic++){
                int op_index=icit[ic].index;
                DComplex coef=icit[ic].coef;
                bresults[op_index](dil1,dil2,dil3)+=results_add[phcount]*coef;}}

            QDPInternal::broadcast(comflag);  // just to synchronize the nodes
            bulova.stop();
            rolex.stop();
            cftimer+=bulova.getTimeInSeconds();
            btimer+=rolex.getTimeInSeconds();

            } // end of q3it
          } // end of dil3 loop
        } // end of diquark
#ifdef QUARKMEMORYLITE
       if (qHandler2!=qHandler3) qHandler2->clearOnlyDisplacedData();
#endif
       } // end of dil2 loop

      if ((qHandler1!=qHandler2)&&(qHandler1!=qHandler3))
         qHandler1->clearData();

      seiko.stop();
      intimer+=seiko.getTimeInSeconds();
      QDPIO::cout << "   done for outer dilution index = "<<dil1<<" of "
                  <<ndilproj1-1<<" at inside time = "<<intimer<<" seconds"<<endl;
      QDPIO::cout << "             time in quark handler = "
                  <<qqtimer<<"  seconds"<<endl;
      QDPIO::cout << "        time for diquark formation = "
                  <<diqtimer<<"  seconds"<<endl;
      QDPIO::cout << "  time for diquark-q3 contractions = "
                  <<cctimer<<"  seconds"<<endl;
      QDPIO::cout << "             time for spatial sums = "
                  <<sstimer<<"  seconds"<<endl;
      QDPIO::cout << "   time for operator accumulations = "
                  <<cftimer<<"  seconds"<<endl;
      } // end of dil1 loop

     QDPIO::cout << endl;
     QDPIO::cout << "  ... computation done; writing to files"<<endl;

     rolex.start();
     qHandler1->clearData();
     qHandler2->clearData();             // still looking for optimizations here
     qHandler3->clearData();             // within the noise permutations
     rolex.stop();
     qtimer+=rolex.getTimeInSeconds();

         //  output to file

     rolex.start();
     RecordKey rkey(bnit->noise1,bnit->noise2,bnit->noise3, 
                    qline1,qline2,qline3,timeval);
     for (int k=0;k<nbaryon_ops_thisnode;k++){
        DHSFputPtr->putData(CombinedKey(Bops[k+opindex_start],rkey),bresults[k]);}
     DHSFputPtr->flush();
     QDPInternal::broadcast(comflag);  // just to synchronize the nodes
     rolex.stop();
     otimer+=rolex.getTimeInSeconds();
     QDPIO::cout << "  Total time to write to files = "
                 <<rolex.getTimeInSeconds()<<" seconds"<<endl;
     QDPIO::cout << "  This computation completed"<<endl;

    }  // end of loop over noises

   qHandler1->clearGaugeData();
   qHandler2->clearGaugeData();
   qHandler3->clearGaugeData();

  }  // end loop over times 

  disconnectQuarkHandlers();
  }  // end loop over baryon line calculations

 delete DHSFputPtr;
 DHSFputPtr=0;
 END_CODE();
 outer.stop();

 QDPIO::cout << endl<<endl<<" ************************************"<<endl<<endl;
 QDPIO::cout << "BaryonHandler::compute finished: total time = "
             << outer.getTimeInSeconds()<<" seconds"<<endl;
 QDPIO::cout << "    Quark handling time = "<<qtimer<<" seconds"<<endl;
 QDPIO::cout << "  Baryon formation time = "<<btimer<<" seconds"<<endl;
 QDPIO::cout << "  Other (incl. db) time = "<<otimer<<" seconds"<<endl<<endl;

}


// ***************************************************************************************

/*
Complex BaryonHandler::evaluateSum(const LatticeComplex& phases,
                                   auto_ptr<LatticeComplex>& in)
{
 if (in.get()==0) return zero;
 return sum(phases*(*in));
} */

void BaryonHandler::evaluateColorCrossProduct(
                                 const LatticeColorVector *v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeColorVector>& result)
{
 if ((v1==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeColorVector);
 *result=colorCrossProduct(*v1,*v2);
}

void BaryonHandler::evaluateColorVectorContract(
                                 const auto_ptr<LatticeColorVector>& v1,
                                 const LatticeColorVector *v2,
                                 auto_ptr<LatticeComplex>& result)
{
 if ((v1.get()==0)||(v2==0)){
    result.reset();  // delete current object pointed to, set to null
    return;}
 if (result.get()==0) result.reset(new LatticeComplex);
 *result=colorVectorContract(*v1,*v2);
}

// ***************************************************************************************

bool BaryonHandler::checkHeader(XmlReader& xmlr, int suffix)
{
 if (xml_tag_count(xmlr,"BaryonHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::BaryonHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml1(file_xml,"./Quark1");
 QuarkInfo quark1_check(xml1,gauge_check);
 XmlReader xml2(file_xml,"./Quark2");
 QuarkInfo quark2_check(xml2,gauge_check);
 XmlReader xml3(file_xml,"./Quark3");
 QuarkInfo quark3_check(xml3,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quark1Ptr->checkEqual(quark1_check);
    quark2Ptr->checkEqual(quark2_check);
    quark3Ptr->checkEqual(quark3_check); }
 catch(...){ return false;}
 return true;
}


void BaryonHandler::writeHeader(XmlWriter& xmlout, const FileKey& fkey,
                                int suffix)
{
 push(xmlout,"BaryonHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"Quark1");
 quark1Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark2");
 quark2Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark3");
 quark3Ptr->output(xmlout);
 pop(xmlout);
 fkey.output(xmlout);
 pop(xmlout);
}

bool BaryonHandler::checkHeader(XmlReader& xmlr)
{
 if (xml_tag_count(xmlr,"BaryonHandlerDataFile")!=1) return false;
 XmlReader file_xml(xmlr,"./descendant-or-self::BaryonHandlerDataFile");
 GaugeConfigurationInfo gauge_check(file_xml);
 GluonSmearingInfo gsmear_check(file_xml);
 XmlReader xml1(file_xml,"./Quark1");
 QuarkInfo quark1_check(xml1,gauge_check);
 XmlReader xml2(file_xml,"./Quark2");
 QuarkInfo quark2_check(xml2,gauge_check);
 XmlReader xml3(file_xml,"./Quark3");
 QuarkInfo quark3_check(xml3,gauge_check);
 try {
    uPtr->checkEqual(gauge_check);
    gSmearPtr->checkEqual(gsmear_check);
    quark1Ptr->checkEqual(quark1_check);
    quark2Ptr->checkEqual(quark2_check);
    quark3Ptr->checkEqual(quark3_check); }
 catch(...){ return false;}
 return true;
}


void BaryonHandler::writeHeader(XmlWriter& xmlout)
{
 push(xmlout,"BaryonHandlerDataFile");
 uPtr->output(xmlout);
 gSmearPtr->output(xmlout);
 push(xmlout,"Quark1");
 quark1Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark2");
 quark2Ptr->output(xmlout);
 pop(xmlout);
 push(xmlout,"Quark3");
 quark3Ptr->output(xmlout);
 pop(xmlout);
 pop(xmlout);
}

// ************************************************************

void BaryonHandler::getHeader(XmlWriter& xmlout) const
{
 if (isInfoSet()){
    push(xmlout,"BaryonHandlerDataFile");
    uPtr->output(xmlout);
    gSmearPtr->output(xmlout);
    push(xmlout,"Quark1");
    quark1Ptr->output(xmlout);
    pop(xmlout);
    push(xmlout,"Quark2");
    quark2Ptr->output(xmlout);
    pop(xmlout);
    push(xmlout,"Quark3");
    quark3Ptr->output(xmlout);
    pop(xmlout);
    pop(xmlout);}
}

                                           
// ***************************************************************************************



const BaryonHandler::DataType& BaryonHandler::getData(
                                         const BaryonOperatorInfo& bop,
                                         const BaryonLineEndInfo& bline,
                                         const BaryonNoiseInfo& bnoise,
                                         int timevalue)
{
    // check info set and in read mode
 check_info_set("getData",1);
 return DHgetPtr->getData(bop,RecordKey(bnoise,bline,timevalue));
}


const BaryonHandler::DataType& BaryonHandler::getData(
                                         const BaryonOperatorInfo& bop,
                                         const QuarkLineEndInfo& qline1,
                                         const QuarkLineEndInfo& qline2,
                                         const QuarkLineEndInfo& qline3,
                                         const LaphNoiseInfo& noise1,
                                         const LaphNoiseInfo& noise2,
                                         const LaphNoiseInfo& noise3,
                                         int timevalue)
{
 check_info_set("getData",1);
 return DHgetPtr->getData(bop,RecordKey(noise1,noise2,noise3,
                          qline1,qline2,qline3,timevalue));
}

          // "query" function do not read the data and do not
          // allocate memory, they just indicate if the data is
          // available for reading

bool BaryonHandler::queryData(const BaryonOperatorInfo& bop,
                              const BaryonLineEndInfo& bline,
                              const BaryonNoiseInfo& bnoise,
                              int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(bop,RecordKey(bnoise,bline,timevalue));
}


bool BaryonHandler::queryData(const BaryonOperatorInfo& bop,
                              const QuarkLineEndInfo& qline1,
                              const QuarkLineEndInfo& qline2,
                              const QuarkLineEndInfo& qline3,
                              const LaphNoiseInfo& noise1,
                              const LaphNoiseInfo& noise2,
                              const LaphNoiseInfo& noise3,
                              int timevalue)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryData(bop,RecordKey(noise1,noise2,noise3,
                            qline1,qline2,qline3,timevalue));
}



bool BaryonHandler::queryData(const BaryonOperatorInfo& bop)
{
 check_info_set("queryData",1);
 return DHgetPtr->queryFile(bop);
}

        // remove from internal memory

void BaryonHandler::removeData(const BaryonOperatorInfo& bop,
                               const BaryonLineEndInfo& bline,
                               const BaryonNoiseInfo& bnoise,
                               int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(bop,RecordKey(bnoise,bline,timevalue));
}


void BaryonHandler::removeData(const BaryonOperatorInfo& bop,
                               const QuarkLineEndInfo& qline1,
                               const QuarkLineEndInfo& qline2,
                               const QuarkLineEndInfo& qline3,
                               const LaphNoiseInfo& noise1,
                               const LaphNoiseInfo& noise2,
                               const LaphNoiseInfo& noise3,
                               int timevalue)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(bop,RecordKey(noise1,noise2,noise3,
                         qline1,qline2,qline3,timevalue));
}

   // removes all data associated with one baryon operator

void BaryonHandler::removeData(const BaryonOperatorInfo& bop)
{
 if (DHgetPtr!=0)
    DHgetPtr->removeData(bop);
}


void BaryonHandler::clearData()
{
 if (DHgetPtr!=0) DHgetPtr->clearData();
} 
 

set<BaryonOperatorInfo> BaryonHandler::getBaryonOperators() const
{
 check_info_set("getBaryonOperators",1);
 return DHgetPtr->getFileKeys();
}

set<BaryonHandler::BaryonNoiseInfo> BaryonHandler::getBaryonNoises(const BaryonOperatorInfo& binfo) const
{
 check_info_set("getBaryonNoises",1);
 set<BaryonHandler::RecordKey> buffer=DHgetPtr->getKeys(binfo);
 set<BaryonHandler::BaryonNoiseInfo> result;
 for (set<BaryonHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(BaryonNoiseInfo(it->noise1,it->noise2,it->noise3));
 return result;
}

set<BaryonHandler::BaryonLineEndInfo> BaryonHandler::getBaryonLineEndInfos(const BaryonOperatorInfo& binfo) const
{
 check_info_set("getBaryonLineEndInfos",1);
 set<BaryonHandler::RecordKey> buffer=DHgetPtr->getKeys(binfo);
 set<BaryonHandler::BaryonLineEndInfo> result;
 for (set<BaryonHandler::RecordKey>::const_iterator it=buffer.begin();it!=buffer.end();it++)
    result.insert(BaryonLineEndInfo(it->qline1,it->qline2,it->qline3));
 return result;
}



void BaryonHandler::filefail(const string& message)
{
 QDPIO::cerr << message << endl;
 clear();
 QDP_abort(1);
}

void BaryonHandler::check_info_set(const string& name,
                                   int check_mode) const
{
 if (!isInfoSet()){
    QDPIO::cerr << "error in BaryonHandler:"<<endl;
    QDPIO::cerr << "  must setInfo before calling "<<name<<endl;
    QDP_abort(1);}
 if (check_mode==1){
    if (!m_read_mode){
       QDPIO::cerr << "error in BaryonHandler:"<<endl;
       QDPIO::cerr << "  must be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
 else if (check_mode==2){
    if (m_read_mode){
       QDPIO::cerr << "error in BaryonHandler:"<<endl;
       QDPIO::cerr << "  must not be in read mode when calling "<<name<<endl;
       QDP_abort(1);}}
}


 // *********************************************************************

        //  Merge the baryon line ends in "infiles" into the files in
        //  "baryonfilelist" from the constructor.  The files in "infiles" will 
        //  usually contain a set of different file sets from different compute
        //  runs.  Each subset will not have duplicate file keys, but duplicate
        //  file keys will occur overall.  The routine combines all of the
        //  files to produce one set in  "baryonfilelist" that has no
        //  duplicate file keys.  Handler must NOT be in read mode.
 
void BaryonHandler::merge(const FileListInfo& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<BaryonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--BaryonFile",
                "BaryonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"BaryonHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}


void BaryonHandler::merge(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<BaryonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--BaryonFile",
                "BaryonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in MesonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 DHputPtr->merge(infiles,"BaryonHandlerDataFile");
 delete DHputPtr;
 DHputPtr=0;
}


void BaryonHandler::reorganize(const vector<FileListInfo>& infiles)
{
   // check info is set and handler is NOT in read mode
 check_info_set("merge",2);
 if (DHputPtr!=0) delete DHputPtr;
 try{
    DHputPtr = new DataPutHandlerMF<BaryonHandler,FileKey,RecordKey,
               DataType>(*this,*fPtr,"Laph--BaryonFile",
                "BaryonHandlerDataFile");}
  catch(...){
    QDPIO::cerr << "problem setting up output files in BaryonHandler"<<endl;
    QDP_abort(1);}
 DHputPtr->setNoOverWrite();
 
 for (unsigned int k=0;k<infiles.size();k++){
    QDPIO::cout << "merging file list k = "<<k<<endl;
    for (int suffix=infiles[k].getMinFileNumber();suffix<=infiles[k].getMaxFileNumber();suffix++){
       int nrecords=0;
       string fname=infiles[k].getFileName(suffix);
       if (!fileExists(fname)) continue;
       DataGetHandlerSF<BaryonHandler,CombinedKey,DataType> inmerge(
                          *this,fname,"Laph--BaryonFile");
       std::set<CombinedKey> rkeys=inmerge.getKeys();
       for (std::set<CombinedKey>::const_iterator rt=rkeys.begin();rt!=rkeys.end();rt++){
          const DataType& data=inmerge.getData(*rt);
          nrecords++;
          DHputPtr->putData(rt->bop,rt->rkey,data);
          inmerge.removeData(*rt);}
       QDPIO::cout << " file list "<<k<<":  from suffix "<<suffix
                   <<": number of records added = "<<nrecords<<endl;}
    }

 delete DHputPtr;
 DHputPtr=0;
}

 // *********************************************************************

#endif
  }
}
 
