#include "time_slices.h"
#include "xml_help.h"
using namespace std;

namespace Chroma {
  namespace LaphEnv {


// *************************************************************

   // XmlReader constructor

TimesInfo::TimesInfo(XmlReader& xml_in, const GaugeConfigurationInfo& gaugeinfo)
{
 int Textent=gaugeinfo.getTimeExtent();

 if (xml_tag_count(xml_in,"TimesInfo")!=1){
    QDPIO::cerr << "Bad XML input to TimesInfo"<<endl;
    QDPIO::cerr << "Expected one <TimesInfo> tag"<<endl;
    xmlreadfail(xml_in,"TimesInfo");}
 XmlReader xmlr(xml_in, "./descendant-or-self::TimesInfo");

 if (xml_tag_count(xmlr,"All")){
    for (int t=0;t<Textent;t++) m_times.insert(t);
    return;}

 int nvtags = xml_tag_count(xmlr,"Values");
 for (int k=1;k<=nvtags;k++){
    ostringstream oss;
    oss << "./Values["<<k<<"]";
    multi1d<int> vtimes;
    read(xmlr,oss.str(),vtimes);
    for (int j=0;j<vtimes.size();j++){
       int vj=vtimes[j];
       if ((vj>=0)&&(vj<Textent)) m_times.insert(vj);}}

 int nitags = xml_tag_count(xmlr,"Interval");
 for (int k=1;k<=nitags;k++){
    ostringstream oss;
    oss << "./Interval["<<k<<"]";
    XmlReader xmli(xmlr,oss.str());
    int first,length;
    try{
       xmlread(xmli,"First",first,"TimesInfo");
       if ((first<0)||(first>=Textent)) throw(string("error"));
       xmlread(xmli,"Length",length,"TimesInfo");
       if ((length<1)||(length>Textent)) throw(string("error"));}
    catch(...){ xmlreadfail(xmli,"TimesInfo/Interval/First-Length");}
    int vj=first;
    for (int j=0;j<length;j++){
       m_times.insert(vj++);
       if (vj==Textent) vj=0;}}

}


 // *************************************************************

    // copy constructor

TimesInfo::TimesInfo(const TimesInfo& in) : m_times(in.m_times) {}

TimesInfo& TimesInfo::operator=(const TimesInfo& in)
{
 m_times=in.m_times;
 return *this;
}

string TimesInfo::output(int indent) const
{
 string pad(3*indent,' ');
 ostringstream oss;
 oss << pad << "<TimesInfo>"<<endl;
 oss << pad << "   <Values>";
 for (set<int>::const_iterator it=m_times.begin();it!=m_times.end();it++)
    oss << " " << *it;
 oss << " </Values>"<<endl;
 oss << pad << "</TimesInfo>"<<endl;
 return oss.str();
}

void TimesInfo::output(XmlWriter& xmlout) const
{
 push(xmlout,"TimesInfo");
 multi1d<int> vals(m_times.size());
 int k=0;
 for (set<int>::const_iterator it=m_times.begin();it!=m_times.end();it++)
    vals[k++]=*it;
 write(xmlout,"Values",vals);
 pop(xmlout);
}


// *************************************************************
  }
}
