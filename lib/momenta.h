#ifndef MOMENTA_H
#define MOMENTA_H

#include "chromabase.h"
#include "multi_compare.h"
#include <vector>

namespace Chroma {
  namespace LaphEnv {


// *******************************************************************
// *                                                                 *
// *   Support for three dimensional Fourier transforms.             *
// *   Defines a class "Momentum" and a routine "makeMomPhaseField". * 
// *                                                                 *
// *   The class "Momentum" is just a convenient struct for storing  *
// *   a three-momentum defined as three integers (since momentum    *
// *   is quantized on a toroid).                                    *
// *                                                                 *
// *   The routine "makeMomPhaseField" holds the site phases for a   *
// *   given three-momentum.  Returns a LatticeComplex object.       *
// *                                                                 *
// *******************************************************************


struct Momentum
{
 int x,y,z;

 Momentum(){}
 Momentum(int px, int py, int pz) : x(px), y(py), z(pz) {}
 Momentum(const Momentum& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) {}
 Momentum& operator=(const Momentum& rhs)
  {x=rhs.x; y=rhs.y; z=rhs.z; return *this;}
 bool operator<(const Momentum& rhs) const
  {return multiLessThan(x,rhs.x,  y,rhs.y,  z,rhs.z);}
 bool operator==(const Momentum& rhs) const
  {return multiEqual(x,rhs.x,  y,rhs.y,  z,rhs.z);}
 bool operator!=(const Momentum& rhs) const
  {return multiNotEqual(x,rhs.x,  y,rhs.y,  z,rhs.z);}

};


   //   Returns true if (px,py,pz) is an allowed momentum ray:
   //     000  +00  0+0  00+  -00  0-0  00-
   //     ++0  +-0  +0+  +0-  0++  0+-
   //     --0  -+0  -0-  -0+  0--  0-+
   //     +++  ++-  +-+  +--  ---  --+  -+-  -++

bool getMomentumRay(int px, int py, int pz, std::string& ray);

// **********************************************************

#if (QDP_ND == 3)

     //  Makes the momenta phases exp(-I p.x) for our annihilation operators.

LatticeComplex makeMomPhaseField(const Momentum& p);


    // Site-by-site multiplies the lattice complex field "in" by 
    // each of the phase fields in "phases", then returns the
    // lattice sums in "results".

void evaluateMomentumSums(const vector<LatticeComplex*> phases,
                          const LatticeComplex* in, 
                          multi1d<Complex>& results);

#endif

// **************************************************

  }
}
#endif  
