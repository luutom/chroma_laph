#include "baryon_operator.h"
#include "xml_help.h"

using namespace std;

namespace Chroma {
  namespace LaphEnv {



void BaryonOperator::initialize(const string& baryonCoefsTopDirectory)
{
 coefsTopDirectory=tidyString(baryonCoefsTopDirectory);
 if (*coefsTopDirectory.rbegin()!='/') coefsTopDirectory+="/";
 string checkfile=coefsTopDirectory+"nucleon_uud/mom_ray_000/G1g_1/SS_0";

 if (fileExists(checkfile)){
    coefsFlag=true;}
 else{
    QDPIO::cerr << "Baryon coefficient top directory not found"<<endl;
    QDP_abort(1);}
}
  


BaryonOperator::BaryonOperator(const BaryonOperatorInfo& bop_info)
               : m_info(bop_info)
{
 if (!coefsFlag){
    QDPIO::cerr << "Must call BaryonOperator::Initialize(...) before creating baryon objects"<<endl;
    QDP_abort(1);}

    // first, determine the momentum "ray"

 string momRay;
 if (!getMomentumRay(m_info.getXMomentum(),
                     m_info.getYMomentum(),
                     m_info.getZMomentum(), momRay)){
    QDPIO::cerr << "Baryon momentum is not part of allowed ray"<<endl;
    QDP_abort(1);}

    // now look for the baryon operator in the XML coefficient file

 ostringstream oss;
 oss << coefsTopDirectory << m_info.getIsospinName() 
     << "_" << m_info.getFlavor() << "/" << momRay 
     << "/" << m_info.getIrrep() << "_" << m_info.getIrrepRow()
     << "/" << m_info.getSpatialType() << "_" 
     << m_info.getSpatialIdNumber();
 string coefs_file=oss.str();

 if (!fileExists(coefs_file)){
    QDPIO::cerr << "Invalid baryon operator requested"<<endl;
    QDPIO::cerr << "Coefficient file requested was "<<coefs_file<<endl;
    QDP_abort(1);}

 TextFileReader in(coefs_file);

 int nterms;
 Double re,im;
 int s1,s2,s3,d1,d2,d3;
 in >> nterms;
 for (int k=0;k<nterms;k++){
    in >> s1 >> s2 >> s3 >> d1 >> d2 >> d3 >> re >> im;
    terms.push_back(ElementalTerm(Elemental(s1,d1,s2,d2,s3,d3),
                    cmplx(re,im)));}

 if (in.fail()){
    in.close();
    QDPIO::cerr << "error occurred during baryon operator read"<<endl;
    QDP_abort(1);}

 in.close();
}


string BaryonOperator::output() const
{
 ostringstream oss;
 oss << "<BaryonOperatorDetails>"<<endl;
 oss << m_info.output()<<endl;
 oss << "  <Projection>"<<endl;
 for (BaryonOperator::ElementalIterator it=terms.begin();it!=terms.end();it++){
    oss << "     <Term> "<<it->el.output()
           << " coef=cmplx"<<it->coef<<" </Term>"<<endl;
    }
 oss << "  </Projection>"<<endl;
 oss << "</BaryonOperatorDetails>"<<endl;
 return oss.str();
}

    //  static data members initialization

string BaryonOperator::coefsTopDirectory;
bool BaryonOperator::coefsFlag=false;


// **************************************************************
  }
}
